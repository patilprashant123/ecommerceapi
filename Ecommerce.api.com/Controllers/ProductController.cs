﻿using Ecommerce.Cmd;
using Ecommerce.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Http;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;

namespace Web.API
{
    public class ProductController : ApiController
    {
        [HttpOptions]
        public HttpResponseMessage getProduct()
        {
            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Headers.ConnectionClose = true;
            resp.Headers.CacheControl = new CacheControlHeaderValue();
            resp.Headers.CacheControl.Public = true;

            return resp;
        }

        [HttpGet]
        public HttpResponseMessage getProductQty(string StandardCode)
        {
            ProductDetailCmd ObjCmd = new ProductDetailCmd();
            StandardDesignDetail product = new StandardDesignDetail();
            try
            {
                product = ObjCmd.GetProductQty(StandardCode);
            }
            catch (Exception exc)
            {
                throw exc;
            }

            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Content = new ObjectContent<StandardDesignDetail>(product, new JsonMediaTypeFormatter());
            return resp;
        }


        [HttpOptions]
        public HttpResponseMessage getProductQty()
        {
            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Headers.ConnectionClose = true;
            resp.Headers.CacheControl = new CacheControlHeaderValue();
            resp.Headers.CacheControl.Public = true;

            return resp;
        }

        [HttpGet]
        public HttpResponseMessage getProduct(string StandardCode, string CatName, string Height)
        {
            ProductDetailCmd ObjCmd = new ProductDetailCmd();
            MetaProduct product = new MetaProduct();
            try
            {
                product = ObjCmd.GetProduct(StandardCode, 0, 0, CatName, Height);
            }
            catch (Exception exc)
            {
                throw exc;
            }

            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Content = new ObjectContent<MetaProduct>(product, new JsonMediaTypeFormatter());
            return resp;
        }

        [HttpOptions]
        public HttpResponseMessage getAppProductDetail()
        {
            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Headers.ConnectionClose = true;
            resp.Headers.CacheControl = new CacheControlHeaderValue();
            resp.Headers.CacheControl.Public = true;

            return resp;
        }

        [HttpGet]
        public HttpResponseMessage getAppProductDetail(string StandardCode, string CatName, string Height)
        {
            ProductDetailCmd ObjCmd = new ProductDetailCmd();
            MetaProduct product = new MetaProduct();
            try
            {
                product = ObjCmd.GetAppProductDetail(StandardCode, 0, 0, CatName, Height);
            }
            catch (Exception exc)
            {
                throw exc;
            }

            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Content = new ObjectContent<MetaProduct>(product, new JsonMediaTypeFormatter());
            return resp;
        }

        [HttpOptions]
        public HttpResponseMessage NotifyMe()
        {
            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Headers.ConnectionClose = true;
            resp.Headers.CacheControl = new CacheControlHeaderValue();
            resp.Headers.CacheControl.Public = true;

            return resp;
        }

        [HttpPost]
        public HttpResponseMessage NotifyMe(ClsNotify objNotify)
        {
            string sMessage = string.Empty;

            int iResult = 0;

            ShoppingCart oCart = new ShoppingCart();

            try
            {
                if (objNotify.ItemCode == "")
                {
                    sMessage = "Please enter your email address";
                }
                else
                {
                    bool checkIsValidEmail = IsvalidEmail(objNotify.Email);

                    if (!checkIsValidEmail)
                    {
                        sMessage = "Please enter valid email address.";
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(objNotify.Email))
                            iResult = oCart.SaveUpdateNotifyEMail(objNotify.ItemCode, objNotify.Email);

                        if (iResult > 0)
                        {
                            sMessage = "Thanks for showing interest in this product. You will be notified via email once the product is available for purchase.";
                        }
                        else if (oCart.Message.Length > 0)
                        {
                            sMessage = "Thanks for showing interest in this product. You will be notified via email once the product is available for purchase.";
                        }
                    }
                }
            }
            catch (Exception)
            {

            }

            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Content = new ObjectContent<string>(sMessage, new JsonMediaTypeFormatter());
            return resp;
        }

        public static bool IsvalidEmail(string inputEmail)
        {
            string strRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                  @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                  @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
            Regex re = new Regex(strRegex);
            string strEmailn = inputEmail.Replace("\r", "");
            string strEmailr = strEmailn.Replace("\n", "");
            string sfinalEmail = strEmailr.Trim();
            if (re.IsMatch(sfinalEmail))
                return (true);
            else
                return (false);
        }

        [HttpOptions]
        public HttpResponseMessage getCodAndDeliveryTimeLine()
        {
            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Headers.ConnectionClose = true;
            resp.Headers.CacheControl = new CacheControlHeaderValue();
            resp.Headers.CacheControl.Public = true;

            return resp;
        }

        [HttpGet]
        public HttpResponseMessage getCodAndDeliveryTimeLine(int contentid, string ItemCode, string pincode)
        {
            ProductDetailCmd ObjCmd = new ProductDetailCmd();

            CodAndDelivery cod = new CodAndDelivery();

            try
            {
                cod = ObjCmd.getCodAndDeliveryTimeLine(contentid, ItemCode, pincode);
            }
            catch (Exception exc)
            {
                throw exc;
            }

            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Content = new ObjectContent<CodAndDelivery>(cod, new JsonMediaTypeFormatter());
            return resp;
        }

        [HttpGet]
        public HttpResponseMessage getStandardDesignStatus()
        {
            ProductDetailCmd ObjCmd = new ProductDetailCmd();
            List<StandardDesignStatus> productlist = new List<StandardDesignStatus>();
            try
            {
                productlist = ObjCmd.getStandardDesignStatus();

            }
            catch (Exception exc)
            {
                throw exc;
            }

            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Content = new ObjectContent<List<StandardDesignStatus>>(productlist, new JsonMediaTypeFormatter());
            return resp;
        }


        [HttpOptions]
        public HttpResponseMessage getMenu()
        {
            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Headers.ConnectionClose = true;
            resp.Headers.CacheControl = new CacheControlHeaderValue();
            resp.Headers.CacheControl.Public = true;

            return resp;
        }

        [HttpGet]
        public HttpResponseMessage getMenu(string name)
        {
            ProductDetailCmd ObjCmd = new ProductDetailCmd();

            HomePageMenu objMenu = new HomePageMenu();

            try
            {
                if (HttpContext.Current.Cache["TopMenu"] == null)
                {
                    objMenu = ObjCmd.GetMenu();

                    HttpContext.Current.Cache.Insert("TopMenu", objMenu, null, DateTime.Now.AddDays(7), System.Web.Caching.Cache.NoSlidingExpiration);
                }
                else
                {
                    objMenu = (HomePageMenu)HttpContext.Current.Cache["TopMenu"];
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }

            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Content = new ObjectContent<HomePageMenu>(objMenu, new JsonMediaTypeFormatter());
            return resp;
        }
        [HttpOptions]
        public HttpResponseMessage getOTHMenu()
        {
            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Headers.ConnectionClose = true;
            resp.Headers.CacheControl = new CacheControlHeaderValue();
            resp.Headers.CacheControl.Public = true;

            return resp;
        }

        [HttpGet]
        public HttpResponseMessage getOTHMenu(string Name)
        {
            ProductDetailCmd ObjCmd = new ProductDetailCmd();

            HomeMenuList objMenu = new HomeMenuList();

            try
            {
                if (HttpContext.Current.Cache["TopMenuV1"] == null)
                {
                    objMenu = ObjCmd.GetMenuList();

                    HttpContext.Current.Cache.Insert("TopMenuV1", objMenu, null, DateTime.Now.AddDays(7), System.Web.Caching.Cache.NoSlidingExpiration);
                }
                else
                {
                    objMenu = (HomeMenuList)HttpContext.Current.Cache["TopMenuV1"];
                }

            }
            catch (Exception exc)
            {
                throw exc;
            }

            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Content = new ObjectContent<HomeMenuList>(objMenu, new JsonMediaTypeFormatter());
            return resp;
        }

        [HttpGet]
        public HttpResponseMessage ProductSync()
        {
            FeedInfo objInfo = new FeedInfo();

            objInfo.sReset = "1";

            FeedCmd ObjCmd = new FeedCmd();

            if (!string.IsNullOrEmpty(objInfo.sReset))
            {
                ObjCmd.ResetDeltaSync();
            }

            BBMStore objStore = new BBMStore();

            try
            {
                objStore = ObjCmd.GetBBMStoreDataFeed(objInfo);
            }
            catch (Exception exc)
            {
                throw exc;
            }

            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Content = new ObjectContent<BBMStore>(objStore, new JsonMediaTypeFormatter());
            return resp;
        }

        [HttpOptions]
        public HttpResponseMessage GetEMI()
        {
            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Headers.ConnectionClose = true;
            resp.Headers.CacheControl = new CacheControlHeaderValue();
            resp.Headers.CacheControl.Public = true;

            return resp;
        }

        [HttpGet]
        public HttpResponseMessage GetEMI(string FinalAmount)
        {
            CheckOutCmd ObjCmd = new CheckOutCmd();

            EMI ObjEmi = ObjCmd.GetEmiDetails();

            List<Banks> Banks = ObjEmi.ObjBanks;

            List<EmiDetails> EmiDetails = ObjEmi.ObjEmi;

            if (EmiDetails.Count > 0)
            {
                foreach (var emi in EmiDetails)
                {
                    emi.EMIAmount = CalculateEMI(emi.Interest, emi.EMITenure, Convert.ToDecimal(FinalAmount));
                    emi.TotalPayment = emi.EMIAmount * emi.EMITenure;
                    emi.Interest = emi.Interest * 100;
                    emi.EMIInterest = Convert.ToInt32(emi.Interest);
                }
            }

            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Content = new ObjectContent<EMI>(ObjEmi, new JsonMediaTypeFormatter());
            return resp;
        }

        [NonAction]
        public double CalculateEMI(double ROI, int Months, decimal Amount)
        {
            float PrincipleAmount = 0;
            float EMI = 0;
            float InterestRate = 0;
            float Duration = 0;

            try
            {
                ROI = ROI * 100;
                Duration = Convert.ToSingle(Months);
                InterestRate = Convert.ToSingle(ROI / 12 / 100);
                PrincipleAmount = Convert.ToSingle(Amount);

                EMI = Convert.ToSingle(PrincipleAmount * InterestRate * Math.Pow((1 + InterestRate), Duration) / (Math.Pow((1 + InterestRate), Duration) - 1));

            }
            catch
            {
            }

            return EMI;
        }

        [HttpOptions]
        public HttpResponseMessage getProductUIType()
        {
            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Headers.ConnectionClose = true;
            resp.Headers.CacheControl = new CacheControlHeaderValue();
            resp.Headers.CacheControl.Public = true;

            return resp;
        }

        [HttpGet]
        public HttpResponseMessage getProductUIType(string StandardCode)
        {
            int UiTypeId = 0;

            ProductDetailCmd ObjCmd = new ProductDetailCmd();
            try
            {
                UiTypeId = ObjCmd.GetProductUIType(StandardCode);
            }
            catch (Exception exc)
            {
                throw exc;
            }

            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Content = new ObjectContent<int>(UiTypeId, new JsonMediaTypeFormatter());
            return resp;
        }

        [HttpOptions]
        public HttpResponseMessage getProductUITypeV1()
        {
            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Headers.ConnectionClose = true;
            resp.Headers.CacheControl = new CacheControlHeaderValue();
            resp.Headers.CacheControl.Public = true;

            return resp;
        }

        [HttpGet]
        public HttpResponseMessage getProductUITypeV1(string StandardCode)
        {
            ProductUiType objProductUiType = new ProductUiType();

            ProductDetailCmd ObjCmd = new ProductDetailCmd();

            try
            {
                objProductUiType = ObjCmd.GetProductUITypeV1(StandardCode);
            }
            catch (Exception exc)
            {
                throw exc;
            }

            var resp = new HttpResponseMessage(HttpStatusCode.OK);

            resp.Content = new ObjectContent<ProductUiType>(objProductUiType, new JsonMediaTypeFormatter());

            return resp;
        }


        [HttpOptions]
        public HttpResponseMessage getFabrics()
        {
            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Headers.ConnectionClose = true;
            resp.Headers.CacheControl = new CacheControlHeaderValue();
            resp.Headers.CacheControl.Public = true;

            return resp;
        }

        [HttpGet]
        public HttpResponseMessage getFabrics(string str)
        {
            CustomProductCmd cmd = new CustomProductCmd();

            CustomProductList oList = cmd.CustomFabricProductList(null, null, null);

            if (oList != null)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.OK);
                resp.Content = new ObjectContent<CustomProductList>(oList, new JsonMediaTypeFormatter());
                return resp;
            }
            else
            {
                var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                resp.Content = new ObjectContent<CustomProductList>(oList, new JsonMediaTypeFormatter());
                return resp;
            }
        }


        [HttpOptions]
        public HttpResponseMessage getItemDropPrice()
        {
            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Headers.ConnectionClose = true;
            resp.Headers.CacheControl = new CacheControlHeaderValue();
            resp.Headers.CacheControl.Public = true;

            return resp;
        }

        [HttpGet]
        public HttpResponseMessage getItemDropPrice(string ItemCode)
        {
            ProductDetailCmd ObjCmd = new ProductDetailCmd();

            decimal ItemPrice = 0;

            try
            {
                ItemPrice = ObjCmd.getItemDropPrice(ItemCode);
            }
            catch (Exception exc)
            {
                throw exc;
            }

            var resp = new HttpResponseMessage(HttpStatusCode.OK);

            resp.Content = new ObjectContent<decimal>(ItemPrice, new JsonMediaTypeFormatter());

            return resp;
        }



        [HttpOptions]
        public HttpResponseMessage addProductToFlashSaleV1()
        {
            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Headers.ConnectionClose = true;
            resp.Headers.CacheControl = new CacheControlHeaderValue();
            resp.Headers.CacheControl.Public = true;

            return resp;
        }

        [HttpPost]
        public HttpResponseMessage addProductToFlashSaleV1(FlashSaleProduct objFlashSaleProduct)
        {
            string sMessage = string.Empty;

            int iResult = 0;

            ShoppingCart oCart = new ShoppingCart();

            try
            {
                if (objFlashSaleProduct.StartDate != null && objFlashSaleProduct.EndDate != null)
                {
                    //  objFlashSaleProduct.StartDate= DateTime.ParseExact(objFlashSaleProduct.StartDate.ToString(), "yyyy-MM-dd", null);
                    //  objFlashSaleProduct.EndDate = DateTime.ParseExact(objFlashSaleProduct.EndDate.ToString(), "yyyy-MM-dd", null);


                    //DateTime date = Convert.ToDateTime(objFlashSaleProduct.StartDate.ToString().Trim()).ToString("yyyy/MM/dd");
                    if (objFlashSaleProduct.StartDate == DateTime.MinValue)
                    {
                        sMessage = "Start Date is missing";
                    }
                    if (objFlashSaleProduct.EndDate == DateTime.MinValue)
                    {
                        sMessage = "End Date is missing";
                    }


                    if (objFlashSaleProduct.EndDate < objFlashSaleProduct.StartDate)
                    {
                        sMessage = "End Date must be greater than Start Date";

                        var res = new HttpResponseMessage(HttpStatusCode.OK);
                        res.Content = new ObjectContent<string>(sMessage, new JsonMediaTypeFormatter());
                        return res;

                    }
                }

                if (objFlashSaleProduct.ItemCode == "")
                {
                    sMessage = "ItemCode is missing";
                }
                else if (objFlashSaleProduct.StandardCode == "")
                {
                    sMessage = "StandardCode is missing";
                }
                else if (Convert.ToDecimal(objFlashSaleProduct.Mrp) <= 0)
                {
                    sMessage = "Mrp is missing";
                }
                else if (Convert.ToDecimal(objFlashSaleProduct.RetaiPrice) <= 0)
                {
                    sMessage = "RetailPrice is missing";
                }
                else if (objFlashSaleProduct.StartDate == null)
                {
                    sMessage = "Start Date is missing";
                }
                else if (objFlashSaleProduct.EndDate == null)
                {
                    sMessage = "End Date is missing";
                }
                else
                {
                    iResult = oCart.UpdateAppDesign(objFlashSaleProduct);

                    if (iResult > 0)
                    {
                        sMessage = "Success";
                    }
                    else if (oCart.Message.Length > 0)
                    {
                        sMessage = oCart.Message.ToString();
                    }

                }
            }
            catch (Exception ex)
            {
                sMessage = ex.Message;
            }

            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Content = new ObjectContent<string>(sMessage, new JsonMediaTypeFormatter());
            return resp;
        }


        [HttpOptions]
        public HttpResponseMessage RemoveAppItemSale()
        {
            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Headers.ConnectionClose = true;
            resp.Headers.CacheControl = new CacheControlHeaderValue();
            resp.Headers.CacheControl.Public = true;

            return resp;
        }

        [HttpPost]
        public HttpResponseMessage RemoveAppItemSale(FlashSaleProduct objFlashSaleProduct)
        {
            string sMessage = string.Empty;

            int iResult = 0;

            ShoppingCart oCart = new ShoppingCart();

            try
            {
                if (objFlashSaleProduct.ItemCode == "")
                {
                    sMessage = "ItemCode is missing";
                }
                else if (objFlashSaleProduct.StandardCode == "")
                {
                    sMessage = "StandardCode is missing";
                }
                else
                {
                    iResult = oCart.Remove_ItemsFromFlashSale(objFlashSaleProduct);

                    if (iResult > 0)
                    {
                        sMessage = "Success";
                    }
                    else if (oCart.Message.Length > 0)
                    {
                        sMessage = oCart.Message.ToString();
                    }

                }
            }
            catch (Exception)
            {

            }

            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Content = new ObjectContent<string>(sMessage, new JsonMediaTypeFormatter());
            return resp;
        }



        [HttpOptions]
        public HttpResponseMessage getProductImages()
        {
            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Headers.ConnectionClose = true;
            resp.Headers.CacheControl = new CacheControlHeaderValue();
            resp.Headers.CacheControl.Public = true;

            return resp;
        }

        [HttpGet]
        public HttpResponseMessage getProductImages(string StandardCode)
        {
            ProductDetailCmd ObjCmd = new ProductDetailCmd();
            DesignImage product = new DesignImage();
            try
            {
                product = ObjCmd.getProductImages(StandardCode);


                ProductLabelandMaterialList productLabelandMaterialList = new ProductLabelandMaterialList();
                List<ProductCustomLabel> lplabel = new List<ProductCustomLabel>();

                productLabelandMaterialList = ObjCmd.getProductLabelandMaterialList(StandardCode);

                foreach (ProductCustomLabel p in productLabelandMaterialList.productCustomLabel)
                {
                    ProductCustomLabel pll = new ProductCustomLabel();
                    pll.CustomProductLabel = p.CustomProductLabel;
                    pll.ProductCustomLabelId = p.ProductCustomLabelId;


                    List<ProductCustomLabelType> productCustomLabelType = new List<ProductCustomLabelType>();
                    foreach (ProductCustomLabelType label in productLabelandMaterialList.productCustomLabelType)
                    {
                        if (label.ProductCustomLabelTypeId == p.ProductCustomLabelId)
                        {
                            ProductCustomLabelType px = new ProductCustomLabelType();
                            px.LabelTypeName = label.LabelTypeName;
                            px.ProductCustomLabelTypeId = label.ProductCustomLabelTypeId;
                            px.ProductCustomLabelId = label.ProductCustomLabelId;
                            productCustomLabelType.Add(px);
                        }
                    }
                    pll.productCustomLabelType = productCustomLabelType;
                    lplabel.Add(pll);

                }
                product.ProductCustomlabel = lplabel;
            }
            catch (Exception exc)
            {
                throw exc;
            }

            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Content = new ObjectContent<DesignImage>(product, new JsonMediaTypeFormatter());
            return resp;
        }



        [HttpOptions]
        public HttpResponseMessage cropImageUpdateCoordinate()
        {
            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Headers.ConnectionClose = true;
            resp.Headers.CacheControl = new CacheControlHeaderValue();
            resp.Headers.CacheControl.Public = true;

            return resp;
        }

        [HttpPost]
        public HttpResponseMessage cropImageUpdateCoordinate(CropImageDetail objCropImageDetail)
        {
            ProductDetailCmd ObjCmd = new ProductDetailCmd();
            int i = 0;
            try
            {
                i = ObjCmd.CropImage_SaveCoordinate(objCropImageDetail);
            }
            catch (Exception exc)
            {
                throw exc;
            }

            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Content = new ObjectContent<int>(i, new JsonMediaTypeFormatter());
            return resp;
        }
        [HttpOptions]
        public HttpResponseMessage getCoordinateList()
        {
            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Headers.ConnectionClose = true;
            resp.Headers.CacheControl = new CacheControlHeaderValue();
            resp.Headers.CacheControl.Public = true;

            return resp;
        }

        [HttpGet]
        public HttpResponseMessage getCoordinateList(string ItemImageId)
        {
            ProductDetailCmd ObjCmd = new ProductDetailCmd();
            ImageCoordinate product = new ImageCoordinate();
            try
            {
                product = ObjCmd.getCoordinatesList(ItemImageId);
            }
            catch (Exception exc)
            {
                throw exc;
            }

            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Content = new ObjectContent<ImageCoordinate>(product, new JsonMediaTypeFormatter());
            return resp;
        }

        [HttpOptions]
        public HttpResponseMessage removeCoordinate()
        {
            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Headers.ConnectionClose = true;
            resp.Headers.CacheControl = new CacheControlHeaderValue();
            resp.Headers.CacheControl.Public = true;

            return resp;
        }

        [HttpPost]
        public HttpResponseMessage removeCoordinate(int CropId)
        {
            ProductDetailCmd ObjCmd = new ProductDetailCmd();
            string s = string.Empty;
            try
            {
                s = ObjCmd.CropImage_RemoveCoordinate(CropId);
            }
            catch (Exception exc)
            {
                throw exc;
            }

            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Content = new ObjectContent<string>(s, new JsonMediaTypeFormatter());
            return resp;
        }

        [HttpOptions]
        public HttpResponseMessage generateImage()
        {
            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Headers.ConnectionClose = true;
            resp.Headers.CacheControl = new CacheControlHeaderValue();
            resp.Headers.CacheControl.Public = true;

            return resp;
        }

        [HttpGet]
        public HttpResponseMessage generateImage(string standardCode)
        {
            ProductDetailCmd ObjCmd = new ProductDetailCmd();
            ImageCoordinate imagecoordinate = new ImageCoordinate();
            try
            {
                imagecoordinate = ObjCmd.getCorrdinatesDetail(standardCode);
                bool b = ObjCmd.GenerateImagebyCoordinates(imagecoordinate, standardCode);
                imagecoordinate = ObjCmd.getCorrdinatesDetail(standardCode);
            }
            catch (Exception exc)
            {
                throw exc;
            }

            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Content = new ObjectContent<ImageCoordinate>(imagecoordinate, new JsonMediaTypeFormatter());
            return resp;
        }
        /*
        [HttpOptions]
        public HttpResponseMessage CustomProductLabelAndMaterial()
        {
            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Headers.ConnectionClose = true;
            resp.Headers.CacheControl = new CacheControlHeaderValue();
            resp.Headers.CacheControl.Public = true;

            return resp;
        }
        */
        [HttpGet]
        public HttpResponseMessage CustomProductLabelAndMaterial(string StandardCode)
        {
            ProductDetailCmd ObjCmd = new ProductDetailCmd();
            ProductLabelandMaterialList productLabelandMaterialList = new ProductLabelandMaterialList();
            List<ProductCustomLabel> lplabel = new List<ProductCustomLabel>();
            try
            {
                productLabelandMaterialList = ObjCmd.getProductLabelandMaterialList(StandardCode);

                foreach (ProductCustomLabel p in productLabelandMaterialList.productCustomLabel)
                {
                    ProductCustomLabel pll = new ProductCustomLabel();
                    pll.CustomProductLabel = p.CustomProductLabel;
                    pll.ProductCustomLabelId = p.ProductCustomLabelId;


                    List<ProductCustomLabelType> productCustomLabelType = new List<ProductCustomLabelType>();
                    foreach (ProductCustomLabelType label in productLabelandMaterialList.productCustomLabelType)
                    {
                        if (label.ProductCustomLabelTypeId == p.ProductCustomLabelId)
                        {
                            ProductCustomLabelType px = new ProductCustomLabelType();
                            px.LabelTypeName = label.LabelTypeName;
                            px.ProductCustomLabelTypeId = label.ProductCustomLabelTypeId;
                            px.ProductCustomLabelId = label.ProductCustomLabelId;
                            productCustomLabelType.Add(px);
                        }
                    }
                    pll.productCustomLabelType = productCustomLabelType;
                    lplabel.Add(pll);
                }

            }
            catch (Exception exc)
            {
                throw exc;
            }

            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Content = new ObjectContent<List<ProductCustomLabel>>(lplabel, new JsonMediaTypeFormatter());
            return resp;
        }


        [HttpOptions]
        public HttpResponseMessage productlike()
        {
            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Headers.ConnectionClose = true;
            resp.Headers.CacheControl = new CacheControlHeaderValue();
            resp.Headers.CacheControl.Public = true;

            return resp;
        }

        [HttpPost]
        public HttpResponseMessage productlike(int customerId, string standardcode, bool islike)
        {
            ProductDetailCmd ObjCmd = new ProductDetailCmd();
            string s = string.Empty;
            if (customerId > 0)
            {

                try
                {
                    s = ObjCmd.StandardDesign_UpdateLikes(customerId, standardcode, islike);
                    if (s == "-100")
                    {
                        s = "Invalid customer";
                    }
                }
                catch (Exception exc)
                {
                    throw exc;
                }
            }
            else
            {
                s = "Please login to like this product.";
            }
            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Content = new ObjectContent<string>(s, new JsonMediaTypeFormatter());
            return resp;
        }


    }
}
