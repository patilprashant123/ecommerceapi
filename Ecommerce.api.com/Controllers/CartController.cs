﻿
using Ecommerce.Cmd;
using Ecommerce.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Configuration;

namespace Web.API
{
    public class ResponseMessage 
    {
        public string ResponseHtml { get; set; }
        public string CartError { get; set; }
        public int CartItemCount { get; set; }
    }

    public class Cart
    {
        public string ResponseHtml { get; set; }
    }

    public class UpdateCart
    {
        public string itemCode { get; set; }
        public string quantity { get; set; }
        public string cartId { get; set; }
        public bool isGiftWrapChecked { get; set; }
        public string giftWrapPrice { get; set; }
        public string SessionId { get; set; }
        public int CustomerId { get; set; }
    }

    public class DeleteCart
    {
        public string cartId { get; set; }
        public string SessionId { get; set; }
    }

    public class UpdateCoupon
    {
        public string promoCode { get; set; }
        public string SessionId { get; set; }
        public int CustomerId { get; set; }
    }

    public class CartController : ApiController
    {       
        public List<string> messages = new List<string>();

        [HttpOptions]
        public HttpResponseMessage addItemToCart()
        {
            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Headers.ConnectionClose = true;
            resp.Headers.CacheControl = new CacheControlHeaderValue();
            resp.Headers.CacheControl.Public = true;

            return resp;
        }

        [HttpPost]
        public HttpResponseMessage addItemToCart(RootObject ObjRoot)
        {
            CartCmd oCart = new CartCmd();

            bool IsSuccess = false;

            foreach (var objCart in ObjRoot.CartItems)
            {
                if (!objCart.IsNotify)
                {
                    if (!string.IsNullOrEmpty(objCart.ItemCode) && objCart.Qty > 0)
                    {
                        IsSuccess = oCart.addItemToCart(objCart);

                        if (!IsSuccess && !string.IsNullOrEmpty(oCart.CartMsg))
                        {
                            messages.Add(oCart.CartMsg);
                        }
                    }

                    if (oCart.ParentCartItemId > 0)
                    {
                        CustomProductCmd ocmd = new CustomProductCmd();
                        string json = string.Empty;
                        if (oCart.PainTypeId == 1)
                        {
                            ShadecardDetail lst = ocmd.CustomProduct_GetPaintShadecard(oCart.ParentCartItemId.ToString());
                            json = JsonConvert.SerializeObject(lst, Formatting.Indented);
                        }
                        else
                        {
                            PaintsheetDetail lst = ocmd.CustomProduct_GetPaintpaintSheet(oCart.ParentCartItemId.ToString());
                            json = JsonConvert.SerializeObject(lst, Formatting.Indented);
                        }
                        PostJsonToPaintAPI(json, oCart.ParentCartItemId);
                    }
                }
            }

            if (messages == null || messages.Count == 0)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.OK);
                return resp;
            }
            else
            {
                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest);
                resp.Content = new ObjectContent<List<string>>(messages, new JsonMediaTypeFormatter());
                resp.Headers.ConnectionClose = true;
                resp.Headers.CacheControl = new CacheControlHeaderValue();
                resp.Headers.CacheControl.Public = true;
                return resp;
            }
        }


        [HttpOptions]
        public HttpResponseMessage addAppItemToCart()
        {
            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Headers.ConnectionClose = true;
            resp.Headers.CacheControl = new CacheControlHeaderValue();
            resp.Headers.CacheControl.Public = true;

            return resp;
        }

        [HttpPost]
        public HttpResponseMessage addAppItemToCart(RootObject ObjRoot)
        {
            CartCmd oCart = new CartCmd();

            bool IsSuccess = false;

            foreach (var objCart in ObjRoot.CartItems)
            {
                if (!objCart.IsNotify)
                {
                    if (!string.IsNullOrEmpty(objCart.ItemCode) && objCart.Qty > 0)
                    {
                        IsSuccess = oCart.addAppItemToCart(objCart);

                        if (!IsSuccess && !string.IsNullOrEmpty(oCart.CartMsg))
                        {
                            messages.Add(oCart.CartMsg);
                        }
                    }
                    /*
                    if (oCart.ParentCartItemId > 0)
                    {
                        CustomProductCmd ocmd = new CustomProductCmd();
                        string json = string.Empty;
                        if (oCart.PainTypeId == 1)
                        {
                            ShadecardDetail lst = ocmd.CustomProduct_GetPaintShadecard(oCart.ParentCartItemId.ToString());
                            json = JsonConvert.SerializeObject(lst, Formatting.Indented);
                        }
                        else
                        {
                            PaintsheetDetail lst = ocmd.CustomProduct_GetPaintpaintSheet(oCart.ParentCartItemId.ToString());
                            json = JsonConvert.SerializeObject(lst, Formatting.Indented);
                        }
                        PostJsonToPaintAPI(json, oCart.ParentCartItemId);
                    }*/
                }
            }

            if (messages == null || messages.Count == 0)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.OK);
                return resp;
            }
            else
            {
                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest);
                resp.Content = new ObjectContent<List<string>>(messages, new JsonMediaTypeFormatter());
                resp.Headers.ConnectionClose = true;
                resp.Headers.CacheControl = new CacheControlHeaderValue();
                resp.Headers.CacheControl.Public = true;
                return resp;
            }
        }


        [HttpOptions]
        public HttpResponseMessage deleteCart()
        {
            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Headers.ConnectionClose = true;
            resp.Headers.CacheControl = new CacheControlHeaderValue();
            resp.Headers.CacheControl.Public = true;

            return resp;
        }

        [HttpPost]
        public HttpResponseMessage deleteCart(DeleteCart objCart)
        {
            CartCmd objCmd = new CartCmd();
        
            int Count = objCmd.deleteMCartItem(objCart.SessionId, objCart.cartId);

            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Content = new ObjectContent<int>(Count, new JsonMediaTypeFormatter());
            return resp;
        }

        [HttpOptions]
        public HttpResponseMessage updateCart()
        {
            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Headers.ConnectionClose = true;
            resp.Headers.CacheControl = new CacheControlHeaderValue();
            resp.Headers.CacheControl.Public = true;

            return resp;
        }

        [HttpPut]
        public HttpResponseMessage updateCart(UpdateCart objCart)
        {
            CartCmd objCmd = new CartCmd();

            objCmd.updateCart(objCart.CustomerId, objCart.SessionId, objCart.isGiftWrapChecked, objCart.giftWrapPrice, objCart.itemCode, objCart.quantity, objCart.cartId);

            ResponseMessage msg = new ResponseMessage();

            if (!string.IsNullOrEmpty(objCmd.CartMsg))
            {
                msg.CartError = objCmd.CartMsg;

                var resp = new HttpResponseMessage(HttpStatusCode.OK);
                resp.Content = new ObjectContent<ResponseMessage>(msg, new JsonMediaTypeFormatter());
                return resp;
            }
            else
            {
                msg.CartError = string.Empty;

                var resp = new HttpResponseMessage(HttpStatusCode.OK);
                resp.Content = new ObjectContent<ResponseMessage>(msg, new JsonMediaTypeFormatter());
                return resp;
            }
        }


        [HttpOptions]
        public HttpResponseMessage getCart()
        {
            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Headers.ConnectionClose = true;
            resp.Headers.CacheControl = new CacheControlHeaderValue();
            resp.Headers.CacheControl.Public = true;

            return resp;
        }

        [HttpGet]
        public HttpResponseMessage getCart(int CustomerId, string wid)
        {
            CheckOutCmd cmd = new CheckOutCmd();
            CheckOutCartItemDetail code = cmd.getCart(CustomerId, wid);
            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Content = new ObjectContent<CheckOutCartItemDetail>(code, new JsonMediaTypeFormatter());
            resp.Headers.ConnectionClose = true;
            resp.Headers.CacheControl = new CacheControlHeaderValue();
            resp.Headers.CacheControl.Public = true;
            return resp;
        }

        [HttpOptions]
        public HttpResponseMessage UpdateCoupon()
        {
            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Headers.ConnectionClose = true;
            resp.Headers.CacheControl = new CacheControlHeaderValue();
            resp.Headers.CacheControl.Public = true;

            return resp;
        }

        [HttpPut]
        public HttpResponseMessage UpdateCoupon(UpdateCoupon objCoupon)
        {
            CartCmd objCmd = new CartCmd();

            if (string.IsNullOrEmpty(objCoupon.promoCode))
            {
                objCmd.UpdateCoupon(objCoupon.CustomerId, objCoupon.SessionId, string.Empty);
            }
            else
            {
                objCmd.UpdateCoupon(objCoupon.CustomerId, objCoupon.SessionId, objCoupon.promoCode);
            }

            decimal couponvalue = objCmd.GetPromoCouponValue(objCoupon.CustomerId, objCoupon.SessionId);

            ResponseMessage msg = new ResponseMessage();

            msg.CartError = objCmd.CartMsg;

            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Content = new ObjectContent<ResponseMessage>(msg, new JsonMediaTypeFormatter());
            return resp;
        }


        [HttpOptions]
        public HttpResponseMessage clearCart()
        {
            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Headers.ConnectionClose = true;
            resp.Headers.CacheControl = new CacheControlHeaderValue();
            resp.Headers.CacheControl.Public = true;

            return resp;
        }

        [HttpPost]
        public HttpResponseMessage clearCart(string SessionId)
        {
            CheckOutCmd cmd = new CheckOutCmd();
            cmd.ClearShoppingCart(SessionId);
            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Content = new ObjectContent<string>(string.Empty, new JsonMediaTypeFormatter());
            resp.Headers.ConnectionClose = true;
            resp.Headers.CacheControl = new CacheControlHeaderValue();
            resp.Headers.CacheControl.Public = true;
            return resp;
        }

     
        [HttpOptions]
        public HttpResponseMessage getCartItemsCount()
        {
            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Headers.ConnectionClose = true;
            resp.Headers.CacheControl = new CacheControlHeaderValue();
            resp.Headers.CacheControl.Public = true;

            return resp;
        }

        [HttpGet]
        public HttpResponseMessage getCartItemsCount(int CustomerId, string SessionId)
        {
            ResponseMessage msg = new ResponseMessage();
            CartCmd cmd = new CartCmd();
            msg.CartItemCount = cmd.getCartItemsCount(SessionId, CustomerId);
            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Content = new ObjectContent<ResponseMessage>(msg, new JsonMediaTypeFormatter());
            return resp;
        }


        [HttpOptions]
        public HttpResponseMessage SyncCart()
        {
            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Headers.ConnectionClose = true;
            resp.Headers.CacheControl = new CacheControlHeaderValue();
            resp.Headers.CacheControl.Public = true;

            return resp;
        }

        [HttpPost]
        public HttpResponseMessage SyncCart(SyncCart objSyncCart)
        {
            if (objSyncCart != null)
            {
                if (objSyncCart.CustomerId > 0)
                {
                    if (!string.IsNullOrEmpty(objSyncCart.SessionId))
                    {
                        ShoppingCart objCart = new ShoppingCart();
                        objCart.UpdateCartCustomer(objSyncCart.CustomerId, objSyncCart.SessionId);
                    }
                }
            }

            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Content = new ObjectContent<string>(string.Empty, new JsonMediaTypeFormatter());
            resp.Headers.ConnectionClose = true;
            resp.Headers.CacheControl = new CacheControlHeaderValue();
            resp.Headers.CacheControl.Public = true;
            return resp;
        }

        [NonAction]
        public void PostJsonToPaintAPI(string sJsonData, int icartId)
        {
            string response = string.Empty;
            WebClient oClient = new WebClient();
            string Url = System.Configuration.ConfigurationManager.AppSettings["PaintPath"].ToString();  
            if (sJsonData != string.Empty)
            {
                oClient.Headers[HttpRequestHeader.ContentType] = "application/json";
                oClient.Headers[HttpRequestHeader.CacheControl] = "no-cache";
                response = oClient.UploadString(Url, sJsonData);
                var results = JsonConvert.DeserializeObject<PaintShadcardUrl>(response);
                if (results != null)
                {
                    if (results.url.Length > 0)
                    {
                        CustomProductCmd ocmd = new CustomProductCmd();
                        ocmd.CustomProduct_SaveShadcardDetail(results, icartId.ToString());
                    }
                }
            }
        }
    }
}
