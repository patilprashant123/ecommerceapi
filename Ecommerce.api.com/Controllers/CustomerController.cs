﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using Ecommerce.Entities;
using Ecommerce.Cmd;
using System.Net.Http.Formatting;
using System.Data.SqlClient;

namespace Web.API
{
    public class CustomerController : ApiController
    {
        [HttpOptions]
        public HttpResponseMessage loginRegister()
        {
            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Headers.ConnectionClose = true;
            resp.Headers.CacheControl = new CacheControlHeaderValue();
            resp.Headers.CacheControl.Public = true;

            return resp;
        }


        [HttpPost]
        public HttpResponseMessage loginRegister(RegisterCustomer customer)
        {
            CustomerCmd ObjCmd = new CustomerCmd();

            ProBuilderResponse profile = ObjCmd.register(customer);

            if (profile.ProBuilderStatus == false)
            {
                if (customer.email != null)
                {
                    ObjCmd.createProfileBuilder(customer.email);
                }
            }

            var resp = new HttpResponseMessage(HttpStatusCode.OK);

            var responseMsg = new { CustomerId = profile.CustomerId, ProBuilderStatus = profile.ProBuilderStatus };

            resp.Content = new ObjectContent<object>(responseMsg, new JsonMediaTypeFormatter());
            resp.Headers.ConnectionClose = true;
            resp.Headers.CacheControl = new CacheControlHeaderValue();
            resp.Headers.CacheControl.Public = true;

            return resp;
        }

        [HttpOptions]
        public HttpResponseMessage getCustomerAddresses()
        {
            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Headers.ConnectionClose = true;
            resp.Headers.CacheControl = new CacheControlHeaderValue();
            resp.Headers.CacheControl.Public = true;

            return resp;
        }

        [HttpGet]
        public HttpResponseMessage getCustomerAddresses(int CustomerId)
        {
            CheckOutCmd ocmd = new CheckOutCmd();
            List<CheckoutBillingAddress> oBillList = ocmd.GetPreviusBillingAddress(CustomerId);
            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Content = new ObjectContent<List<CheckoutBillingAddress>>(oBillList, new JsonMediaTypeFormatter());

            resp.Headers.ConnectionClose = true;
            resp.Headers.CacheControl = new CacheControlHeaderValue();
            resp.Headers.CacheControl.Public = true;

            return resp;
        }

        [HttpOptions]
        public HttpResponseMessage DeleteCustAddress()
        {
            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Headers.ConnectionClose = true;
            resp.Headers.CacheControl = new CacheControlHeaderValue();
            resp.Headers.CacheControl.Public = true;

            return resp;
        }

        [HttpPost]
        public HttpResponseMessage DeleteCustAddress(int addressId)
        {
            CustomerCmd ObjCmd = new CustomerCmd();
            ObjCmd.DeleteCustomerAddress(addressId);

            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Headers.ConnectionClose = true;
            resp.Headers.CacheControl = new CacheControlHeaderValue();
            resp.Headers.CacheControl.Public = true;

            return resp;

        }


        [HttpOptions]
        public HttpResponseMessage SaveCustAddress()
        {
            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Headers.ConnectionClose = true;
            resp.Headers.CacheControl = new CacheControlHeaderValue();
            resp.Headers.CacheControl.Public = true;

            return resp;
        }

        [HttpPost]
        public HttpResponseMessage SaveCustAddress(CustomerAddress objAddress)
        {
            CheckOutCmd objcmd = new CheckOutCmd();

            int CustomerId = objcmd.saveCustomerAddress(objAddress);

            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Content = new ObjectContent<string>(CustomerId.ToString(), new JsonMediaTypeFormatter());

            return resp;

        }


        [HttpOptions]
        public HttpResponseMessage CreateGuestUser()
        {
            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Headers.ConnectionClose = true;
            resp.Headers.CacheControl = new CacheControlHeaderValue();
            resp.Headers.CacheControl.Public = true;

            return resp;
        }

        [HttpPost]
        public HttpResponseMessage CreateGuestUser(ClsGuestUser objGuestUser)
        {
            CartCmd oUsers = new CartCmd();

            int CustomerId = oUsers.CreateGuestUserMobile(objGuestUser.Email);
          
            var resp = new HttpResponseMessage(HttpStatusCode.OK);

            resp.Content = new ObjectContent<int>(CustomerId, new JsonMediaTypeFormatter());

            return resp;
        }


        [HttpOptions]
        public HttpResponseMessage GuestUserAbandaonCart()
        {
            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Headers.ConnectionClose = true;
            resp.Headers.CacheControl = new CacheControlHeaderValue();
            resp.Headers.CacheControl.Public = true;

            return resp;
        }

        [HttpPost]
        public HttpResponseMessage GuestUserAbandaonCart(GuestAbandonRequest objGuestAbandonRequest)
        {
            CustomerCmd oUsers = new CustomerCmd();

            GuestAbandonResponse ObjResponse = oUsers.GuestUserAbandaonCart(objGuestAbandonRequest);

            var resp = new HttpResponseMessage(HttpStatusCode.OK);

            resp.Content = new ObjectContent<GuestAbandonResponse>(ObjResponse, new JsonMediaTypeFormatter());

            return resp;
        }

        [HttpOptions]
        public HttpResponseMessage getStateCity()
        {
            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Headers.ConnectionClose = true;
            resp.Headers.CacheControl = new CacheControlHeaderValue();
            resp.Headers.CacheControl.Public = true;

            return resp;
        }

        [HttpGet]
        public HttpResponseMessage getStateCity(string PinCode)
        {
            CartCmd objCartCmd = new CartCmd();

            SqlDataReader sdr = objCartCmd.GetStateCity(PinCode);

            ClsAutoStateCity ObjStateCity = new ClsAutoStateCity();

            if (sdr != null)
            {
                while (sdr.Read())
                {
                    ObjStateCity.State = Convert.ToString(sdr["State"]);
                    ObjStateCity.City = Convert.ToString(sdr["City"]);
                    ObjStateCity.PinCode = Convert.ToString(sdr["PinCode"]);
                }
            }

            var resp = new HttpResponseMessage(HttpStatusCode.OK);

            resp.Content = new ObjectContent<ClsAutoStateCity>(ObjStateCity, new JsonMediaTypeFormatter());

            return resp;
        }


    }
}
