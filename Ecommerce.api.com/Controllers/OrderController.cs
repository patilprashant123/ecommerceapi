﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Net.Http.Formatting;
using System.Web.SessionState;
using System.Net.Http.Headers;
using System.Web;
using System.Data;
using System.Configuration;
using System.Web.UI;
using System.Text;
using System.IO;
using Mandrill;
using Ecommerce.Entities;
using Ecommerce.Cmd;
using Uniware;
using System.Web.UI.WebControls;

namespace Web.API
{
    public class OrderResponse
    {
        public int OrderId { get; set; }
        public string OrderNo { get; set; }
        public string OrderSource { get; set; }
        public string PaymentMethod { get; set; }
        public string Error { get; set; }
        public decimal BillAmount { get; set; }

    }

    public class OrderRequest
    {
        public int PaymentMode { get; set; }
        public int Source { get; set; }
        public string PinNo { get; set; }
        public string PaymentType { get; set; } // CC/DB/NB
        public string PaymentMethod { get; set; }
        public int CustomerId { get; set; }
        public string SessionId { get; set; }
    }
    public class ClsProcessPayment
    {
        public string OrderNo { get; set; }
        public string OrderType { get; set; }
        public int CustomerId { get; set; }
        public string SessionId { get; set; }
        public string PaymentStatus { get; set; }
    }

    public class CodOrderRequest
    {
        public int PaymentMode { get; set; }
        public int CustomerId { get; set; }
        public string SessionId { get; set; }
        public string PinNo { get; set; }
    }

    public class OrderPaymentResponse
    {
        public string OrderNo { get; set; }
        public string OrderType { get; set; }
        public int Status { get; set; }
        public string SessionId { get; set; }
        public int CustomerId { get; set; }
    }

    public class OrderController : ApiController, IRequiresSessionState
    {
        [HttpOptions]
        public HttpResponseMessage MakeOrder()
        {
            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Headers.ConnectionClose = true;
            resp.Headers.CacheControl = new CacheControlHeaderValue();
            resp.Headers.CacheControl.Public = true;
            return resp;
        }

        [HttpPost]
        public HttpResponseMessage MakeOrder(OrderRequest objOrderRequest)
        {
            string SessionMsg = ValidateCustomerSession(objOrderRequest.CustomerId, objOrderRequest.SessionId);

            if (!string.IsNullOrEmpty(SessionMsg))
            {
                var resp = new HttpResponseMessage(HttpStatusCode.OK);
                resp.Content = new ObjectContent<OrderResponse>
                    (
                        new OrderResponse
                        {
                            Error = SessionMsg,
                            PaymentMethod = objOrderRequest.PaymentMethod
                        },

                        new JsonMediaTypeFormatter()
                   );
                return resp;
            }

            string Message = MakeOrderValidateCustomerAddress(objOrderRequest.CustomerId);

            if (string.IsNullOrEmpty(Message))
                Message = MakeOrderValidateOrder(objOrderRequest);

            if (!string.IsNullOrEmpty(Message))
            {
                var resp = new HttpResponseMessage(HttpStatusCode.OK);
                resp.Content = new ObjectContent<OrderResponse>
                    (
                        new OrderResponse
                        {
                            Error = Message,
                            PaymentMethod = objOrderRequest.PaymentMethod
                        },

                        new JsonMediaTypeFormatter()
                   );
                return resp;
            }
            else
            {
                OrderCmd objCmd = new OrderCmd();
                objCmd.CreateOrder(objOrderRequest);
                var resp = new HttpResponseMessage(HttpStatusCode.OK);
                resp.Content = new ObjectContent<OrderResponse>
                    (
                        new OrderResponse
                        {
                            OrderNo = objCmd.OrderNo,
                            OrderId = objCmd.OrderId,
                            Error = objCmd.CartMsg,
                            PaymentMethod = objOrderRequest.PaymentMethod,
                            BillAmount = objCmd.BillAmount
                        },

                        new JsonMediaTypeFormatter()
                   );
                return resp;
            }
        }

        [NonAction]
        public string ValidateOrder(OrderRequest objOrderRequest)
        {
            string ErrorMsg = string.Empty;

            CheckOutCmd oCmd = new CheckOutCmd();

            CheckoutCartValidation oValidate = new CheckoutCartValidation();
            oValidate.CustomerId = UserSession.GetCurrentCustId();
            oValidate.SessionId = UserSession.GetWebSessionId();
            oValidate.PaymentMode = Convert.ToInt32(objOrderRequest.PaymentMode);
            oValidate.PinCode = Convert.ToString(objOrderRequest.PinNo.Trim());

            SellerServiciable ObjSellerServiciable = oCmd.CheckOut_Validation(oValidate);

            string Message = string.Empty;

            string StrErrorMsg = string.Empty;

            if (ObjSellerServiciable != null)
            {
                if (ObjSellerServiciable.MsgType != null)
                {
                    Message = ObjSellerServiciable.MsgType;
                }

                if (ObjSellerServiciable.MsgType != null)
                {
                    StrErrorMsg = ObjSellerServiciable.ErrorMsg;
                }
            }

            if (objOrderRequest.PaymentMode == 1) // COD
            {
                if (Message == Convert.ToString(CustomDataTypes.ValidationMessage.CodNotAvailableFurniture))
                {
                    ErrorMsg = "Cash On Delivery Not available on Furniture Products";
                }
                else if (Message == Convert.ToString(CustomDataTypes.ValidationMessage.InvalidPin))
                {
                    ErrorMsg = "Apologies! Product(s) in your cart or Pin Code entered is not serviciable by Cash on Delivery! At the moment you will have to Pay Online or remove products which are not serviciable by CoD.";
                }
                else if (Message == Convert.ToString(CustomDataTypes.ValidationMessage.CodNotAvailable))
                {
                    ErrorMsg = "Apologies! Product(s) in your cart or Pin Code entered is not serviciable by Cash on Delivery! At the moment you will have to Pay Online or remove products which are not serviciable by CoD.";
                }

                else if (Message == Convert.ToString(CustomDataTypes.ValidationMessage.NonDeliveryArea))
                {
                    ErrorMsg = "Apologies! product(" + StrErrorMsg + ") in your cart cannot be delivered to your pin code at the moment! You can remove this product and proceed to the payment. Alternatively you can call our customer support at " + WebSiteConfig.ContactNo + " for any information ";
                }
            }

            if (objOrderRequest.PaymentMode == 2) // Prepaid / EMI
            {
                if (Message == Convert.ToString(CustomDataTypes.ValidationMessage.NonDeliveryArea))
                {
                    ErrorMsg = "Apologies! product(" + StrErrorMsg + ") in your cart cannot be delivered to your pin code at the moment! You can remove this product and proceed to the payment. Alternatively you can call our customer support at " + WebSiteConfig.ContactNo + " for any information ";
                }
            }

            return ErrorMsg;

        }

        [NonAction]
        public string ValidateCustomerAddress()
        {
            CheckOutCmd oCmd = new CheckOutCmd();

            int CustomerId = UserSession.GetCurrentCustId();

            string Message = oCmd.ValidateCustomerAddress(CustomerId);

            return Message;
        }

        [NonAction]
        public string MakeOrderValidateOrder(OrderRequest objOrderRequest)
        {
            string ErrorMsg = string.Empty;

            CheckOutCmd oCmd = new CheckOutCmd();

            CheckoutCartValidation oValidate = new CheckoutCartValidation();
            oValidate.CustomerId = objOrderRequest.CustomerId;
            oValidate.SessionId = objOrderRequest.SessionId;
            oValidate.PaymentMode = Convert.ToInt32(objOrderRequest.PaymentMode);
            oValidate.PinCode = Convert.ToString(objOrderRequest.PinNo.Trim());


            SellerServiciable ObjSellerServiciable = oCmd.CheckOut_Validation(oValidate);

            string Message = string.Empty;

            string StrErrorMsg = string.Empty;

            if (ObjSellerServiciable != null)
            {
                if (ObjSellerServiciable.MsgType != null)
                {
                    Message = ObjSellerServiciable.MsgType;
                }

                if (ObjSellerServiciable.MsgType != null)
                {
                    StrErrorMsg = ObjSellerServiciable.ErrorMsg;
                }
            }


            if (objOrderRequest.PaymentMode == 1) // COD
            {
                if (Message == Convert.ToString(CustomDataTypes.ValidationMessage.CodNotAvailableFurniture))
                {
                    ErrorMsg = "Cash On Delivery Not available on Furniture Products";
                }
                else if (Message == Convert.ToString(CustomDataTypes.ValidationMessage.InvalidPin))
                {
                    ErrorMsg = "Apologies! Product(s) in your cart or Pin Code entered is not serviceable by Cash on Delivery! At the moment you will have to Pay Online or remove products which are not serviceable by COD.";
                }
                else if (Message == Convert.ToString(CustomDataTypes.ValidationMessage.CodNotAvailable))
                {
                    ErrorMsg = "Apologies! Product(s) in your cart or Pin Code entered is not serviceable by Cash on Delivery! At the moment you will have to Pay Online or remove products which are not serviceable by COD.";
                }
                else if (Message == Convert.ToString(CustomDataTypes.ValidationMessage.NonDeliveryArea))
                {
                    ErrorMsg = "Apologies! product( " + StrErrorMsg + ") in your cart cannot be delivered to your pin code at the moment! You can remove this product and proceed to the payment. Alternatively you can call our customer support at " + WebSiteConfig.ContactNo + " for any information ";
                }
            }

            if (objOrderRequest.PaymentMode == 2) // Prepaid / EMI
            {
                if (Message == Convert.ToString(CustomDataTypes.ValidationMessage.NonDeliveryArea))
                {
                    ErrorMsg = "Apologies! product( " + StrErrorMsg + ") in your cart cannot be delivered to your pin code at the moment! You can remove this product and proceed to the payment. Alternatively you can call our customer support at " + WebSiteConfig.ContactNo + " for any information ";
                }
            }

            return ErrorMsg;

        }

        [NonAction]
        public string MakeOrderValidateCustomerAddress(int CustomerId)
        {
            CheckOutCmd oCmd = new CheckOutCmd();

            string Message = oCmd.ValidateCustomerAddress(CustomerId);

            return Message;
        }

        [HttpOptions]
        public HttpResponseMessage ValidateCODOrder()
        {
            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Headers.ConnectionClose = true;
            resp.Headers.CacheControl = new CacheControlHeaderValue();
            resp.Headers.CacheControl.Public = true;
            return resp;
        }

        [HttpPost]
        public HttpResponseMessage ValidateCODOrder(CodOrderRequest objCodRequest)
        {
            string ErrorMsg = string.Empty;

            CheckOutCmd oCmd = new CheckOutCmd();

            CheckoutCartValidation oValidate = new CheckoutCartValidation();
            oValidate.CustomerId = objCodRequest.CustomerId;
            oValidate.SessionId = objCodRequest.SessionId;
            oValidate.PaymentMode = Convert.ToInt32(objCodRequest.PaymentMode);
            oValidate.PinCode = Convert.ToString(objCodRequest.PinNo.Trim());

            SellerServiciable ObjSellerServiciable = oCmd.CheckOut_Validation(oValidate);

            string Message = string.Empty;

            string StrErrorMsg = string.Empty;

            if (ObjSellerServiciable != null)
            {
                if (ObjSellerServiciable.MsgType != null)
                {
                    Message = ObjSellerServiciable.MsgType;
                }

                if (ObjSellerServiciable.MsgType != null)
                {
                    StrErrorMsg = ObjSellerServiciable.ErrorMsg;
                }
            }

            if (objCodRequest.PaymentMode == 1) // COD
            {
                if (Message == Convert.ToString(CustomDataTypes.ValidationMessage.CodNotAvailableFurniture))
                {
                    ErrorMsg = "Cash On Delivery Not available on Furniture Products";
                }
                else if (Message == Convert.ToString(CustomDataTypes.ValidationMessage.InvalidPin))
                {
                    ErrorMsg = "Apologies! Product(s) in your cart or Pin Code entered is not serviciable by Cash on Delivery! At the moment you will have to Pay Online or remove products which are not serviciable by CoD.";
                }
                else if (Message == Convert.ToString(CustomDataTypes.ValidationMessage.CodNotAvailable))
                {
                    ErrorMsg = "Apologies! Product(s) in your cart or Pin Code entered is not serviciable by Cash on Delivery! At the moment you will have to Pay Online or remove products which are not serviciable by CoD.";
                }
                else if (Message == Convert.ToString(CustomDataTypes.ValidationMessage.NonDeliveryArea))
                {
                    ErrorMsg = "Apologies! product( " + StrErrorMsg + ") in your cart cannot be delivered to your pin code at the moment! You can remove this product and proceed to the payment. Alternatively you can call our customer support at " + WebSiteConfig.ContactNo + " for any information ";
                }
            }

            var resp = new HttpResponseMessage(HttpStatusCode.OK);

            resp.Content = new ObjectContent<string>
               (
                   ErrorMsg,
                   new JsonMediaTypeFormatter()
              );

            return resp;

        }


        [HttpOptions]
        public HttpResponseMessage CheckCartDeliveryArea()
        {
            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Headers.ConnectionClose = true;
            resp.Headers.CacheControl = new CacheControlHeaderValue();
            resp.Headers.CacheControl.Public = true;
            return resp;
        }

        [HttpPost]
        public HttpResponseMessage CheckCartDeliveryArea(CheckoutCartValidation objCheckoutCartValidation)
        {
            string ErrorMsg = string.Empty;

            CheckOutCmd oCmd = new CheckOutCmd();

            CheckoutCartValidation oValidate = new CheckoutCartValidation();
            oValidate.CustomerId = objCheckoutCartValidation.CustomerId;
            oValidate.SessionId = objCheckoutCartValidation.SessionId;
            oValidate.PinCode = Convert.ToString(objCheckoutCartValidation.PinCode.Trim());

            List<ServiceableCartItem> ObjSellerServiciable = oCmd.Cart_DeliveryValidation(oValidate);

            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Content = new ObjectContent<List<ServiceableCartItem>>
                (
                    ObjSellerServiciable,
                    new JsonMediaTypeFormatter()
               );
            return resp;
            /*
            string Message = string.Empty;

            string StrErrorMsg = string.Empty;

            if (ObjSellerServiciable != null)
            {
                if (ObjSellerServiciable.MsgType != null)
                {
                    Message = ObjSellerServiciable.MsgType;
                }

                if (ObjSellerServiciable.MsgType != null)
                {
                    StrErrorMsg = ObjSellerServiciable.ErrorMsg;
                }
            }
            
            if (objCodRequest.PaymentMode == 1) // COD
            {
                if (Message == Convert.ToString(CustomDataTypes.ValidationMessage.CodNotAvailableFurniture))
                {
                    ErrorMsg = "Cash On Delivery Not available on Furniture Products";
                }
                else if (Message == Convert.ToString(CustomDataTypes.ValidationMessage.InvalidPin))
                {
                    ErrorMsg = "Apologies! Product(s) in your cart or Pin Code entered is not serviciable by Cash on Delivery! At the moment you will have to Pay Online or remove products which are not serviciable by CoD.";
                }
                else if (Message == Convert.ToString(CustomDataTypes.ValidationMessage.CodNotAvailable))
                {
                    ErrorMsg = "Apologies! Product(s) in your cart or Pin Code entered is not serviciable by Cash on Delivery! At the moment you will have to Pay Online or remove products which are not serviciable by CoD.";
                }
                else if (Message == Convert.ToString(CustomDataTypes.ValidationMessage.NonDeliveryArea))
                {
                    ErrorMsg = "Apologies! product( " + StrErrorMsg + ") in your cart cannot be delivered to your pin code at the moment! You can remove this product and proceed to the payment. Alternatively you can call our customer support at " + WebSiteConfig.ContactNo + " for any information ";
                }
            }

            if (Message == Convert.ToString(CustomDataTypes.ValidationMessage.NonDeliveryArea))
            {
                ErrorMsg = "Apologies! product( " + StrErrorMsg + ") in your cart cannot be delivered to your pin code at the moment! You can remove this product and proceed to the payment. Alternatively you can call our customer support at " + WebSiteConfig.ContactNo + " for any information ";
            }

            var resp = new HttpResponseMessage(HttpStatusCode.OK);

            resp.Content = new ObjectContent<string>
               (
                   ErrorMsg,
                   new JsonMediaTypeFormatter()
              );

            return resp;
            */

        }

        [HttpOptions]
        public HttpResponseMessage getCustomerOrders()
        {
            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Headers.ConnectionClose = true;
            resp.Headers.CacheControl = new CacheControlHeaderValue();
            resp.Headers.CacheControl.Public = true;
            return resp;
        }

        [HttpGet]
        public HttpResponseMessage getCustomerOrders(int CustomerId)
        {
            CustomerCmd ocmd = new CustomerCmd();
            List<Order> orders = ocmd.GetCustomerOrders(CustomerId);

            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Content = new ObjectContent<List<Order>>
                (
                    orders,
                    new JsonMediaTypeFormatter()
               );
            return resp;
        }
        [HttpOptions]
        public HttpResponseMessage getCustomerOrdersByMobileNo()
        {
            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Headers.ConnectionClose = true;
            resp.Headers.CacheControl = new CacheControlHeaderValue();
            resp.Headers.CacheControl.Public = true;
            return resp;
        }

        [HttpGet]
        public HttpResponseMessage getCustomerOrdersByMobileNo(string MobileNo)
        {
            CustomerCmd ocmd = new CustomerCmd();
            List<Order> orders = ocmd.GetCustomerOrdersByMobileNo(MobileNo);

            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Content = new ObjectContent<List<Order>>
                (
                    orders,
                    new JsonMediaTypeFormatter()
               );
            return resp;
        }

        [HttpOptions]
        public HttpResponseMessage processOrder()
        {
            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Headers.ConnectionClose = true;
            resp.Headers.CacheControl = new CacheControlHeaderValue();
            resp.Headers.CacheControl.Public = true;
            return resp;
        }

        [HttpPost]
        public HttpResponseMessage processOrder(OrderPaymentResponse payResponse)
        {
            ShoppingCart objCart = new ShoppingCart();

            if (payResponse.OrderType == "Prepaid")
            {
                PaymentGatewayBase objPayment = new PaymentGatewayBase();

                if (payResponse.Status == 8)
                    objPayment.UpdateOrder(payResponse.OrderNo, 8, 4);
            }

            CheckOutCmd objCmd = new CheckOutCmd();

            ClsPaymentResponse ObjResponse = objCmd.getOrder(payResponse.OrderNo);

            var resp = new HttpResponseMessage(HttpStatusCode.OK);

            resp.Content = new ObjectContent<ClsPaymentResponse>
                (
                    ObjResponse,
                    new JsonMediaTypeFormatter()
               );

            return resp;
        }


        [HttpOptions]
        public HttpResponseMessage DeclineOrder()
        {
            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Headers.ConnectionClose = true;
            resp.Headers.CacheControl = new CacheControlHeaderValue();
            resp.Headers.CacheControl.Public = true;
            return resp;
        }

        [HttpPost]
        public HttpResponseMessage DeclineOrder(OrderPaymentResponse payResponse)
        {
            PaymentGatewayBase objPayment = new PaymentGatewayBase();

            objPayment.UpdateOrder(payResponse.OrderNo, 8, 4);

            var resp = new HttpResponseMessage(HttpStatusCode.OK);

            resp.Content = new ObjectContent<string>
                (
                    string.Empty,
                    new JsonMediaTypeFormatter()
               );

            return resp;
        }



        [NonAction]
        public string ValidateCustomerSession(int CustomerId, string Sessionid)
        {
            CheckOutCmd oCmd = new CheckOutCmd();

            string Message = oCmd.ValidateCustomerSession(CustomerId, Sessionid);

            return Message;
        }

        protected void SendMailToCustomer(string sOrderId, int sOrderType, string sSubject, bool bCodCustType)
        {
            PackingAndDespatch oOders = new PackingAndDespatch();

            Page p = new Page();

            bool isEmailSend = oOders.CheckEmailSend_V1(sOrderId, Convert.ToInt32(CustomDataTypes.OrderEmailType.CodConfirm));

            if (!isEmailSend)
            {

                Control invoice;

                if (sOrderType == Convert.ToInt32(CustomDataTypes.OrderType.OrderType_Cod))
                {
                    if (bCodCustType)
                        invoice = p.LoadControl("~/UserControls/CodOrderConfirm.ascx");
                    else
                        invoice = p.LoadControl("~/UserControls/CodOrderConfirm.ascx");
                }
                else
                {
                    invoice = p.LoadControl("~/UserControls/PrepaidOrderMail.ascx");
                }

                CartCmd oCart = new CartCmd();

                StringBuilder sb = new StringBuilder();
                StringWriter sw = new StringWriter(sb);
                HtmlTextWriter htmlTw = new HtmlTextWriter(sw);
                invoice.RenderControl(htmlTw);
                string controlsHTML = sb.ToString();
                string EmailFrom = "orders@bedbathmore.com";
                string EmailTo = oCart.GetEmail(sOrderId);

                EmailAddress objEmail = new EmailAddress();
                objEmail.email = EmailFrom;
                objEmail.name = "BedBathMore.com";
                string subject = sSubject;
                string sBody = controlsHTML;
                SendWebMail.SendEmailInfo(objEmail, EmailTo, subject, sBody);
                string semailsendorReject = SendMailByMandril.emailsendorReject.ToString();
                if (semailsendorReject == "Sent" || semailsendorReject == "Queued")
                    isEmailSend = true;
                if (isEmailSend)
                    oOders.Update_SendEmailLog(sOrderId, isEmailSend, "1"); // 1 is for Order email*/
            }
        }

    }
}
