﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

//  My Usings
using System.Data.SqlClient;


/// <summary>
/// Summary description for BBMTransectionConnection
/// </summary>
public class BBMLogConnection
{

    #region Variables
    protected SqlConnection oConnect;
    static string sConString;
    #endregion

    #region Constructor

    //  Default constructor
    public BBMLogConnection()
    {
        sConString = ConfigurationManager.ConnectionStrings["BBMTransectionLog"].ToString();
    }
    #endregion

    #region User Functions

    public void OpenBBMLogSQLConnection()
    {
        oConnect = new SqlConnection(sConString);
        oConnect.Open();
    }
    public void CloseBBMLogSQLConnection()
    {
        oConnect.Close();
    }


    #endregion



}