﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

using System.IO;
using System.Data.SqlClient;
using System.Text;
using Ecommerce.ErrorLog;

/// <summary>
/// Summary description for BBmData
/// </summary>
public class BBmDataInfo : BBMLogConnection
{
    public BBmDataInfo()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    StringBuilder sbMsg = new StringBuilder();
    public string Message
    {
        set { sbMsg.Append(value); }
        get { return sbMsg.ToString(); }
    }
    ErrorLogger oErrorLog = new ErrorLogger();
    public DataSet OrderDetail(string sOrderNo)
    {
        SqlCommand sqlCmd = new SqlCommand();
        SqlDataAdapter dtAdptr = new SqlDataAdapter(sqlCmd);

        DataSet dsDataSet = new DataSet();

        try
        {
            OpenBBMLogSQLConnection();
            sqlCmd = new SqlCommand("[OrderDetailForUniCom]", oConnect);
            sqlCmd.CommandType = CommandType.StoredProcedure;

            if (sOrderNo != "")
                sqlCmd.Parameters.Add("@OrderNo", SqlDbType.VarChar).Value = sOrderNo;

            dtAdptr = new SqlDataAdapter(sqlCmd);

            dsDataSet = new DataSet();
            dtAdptr.Fill(dsDataSet);
        }
        catch (Exception ex)
        {
            oErrorLog.Open();
            oErrorLog.Log("OrderDetail Function In BBMDataInfo:" + ex.Message);
            oErrorLog.Flush();
            oErrorLog.Close();
        }
        finally
        {
            dtAdptr.Dispose();
            dtAdptr = null;

            sqlCmd.Dispose();
            sqlCmd = null;

            CloseBBMLogSQLConnection();
        }
        return dsDataSet;
    }
 public DataSet OrderDetailByItemCode(string sOrderNo)
    {
        SqlCommand sqlCmd = new SqlCommand();
        SqlDataAdapter dtAdptr = new SqlDataAdapter(sqlCmd);

        DataSet dsDataSet = new DataSet();

        try
        {
            OpenBBMLogSQLConnection();
            sqlCmd = new SqlCommand("[OrderDetailForUniCom_ByItemCode]", oConnect);
            sqlCmd.CommandType = CommandType.StoredProcedure;

            if (sOrderNo != "")
                sqlCmd.Parameters.Add("@OrderNo", SqlDbType.VarChar).Value = sOrderNo;

            dtAdptr = new SqlDataAdapter(sqlCmd);

            dsDataSet = new DataSet();
            dtAdptr.Fill(dsDataSet);
        }
        catch (Exception ex)
        {
            oErrorLog.Open();
            oErrorLog.Log("OrderDetailByItemCode Function In BBMDataInfo:" + ex.Message);
            oErrorLog.Flush();
            oErrorLog.Close();
        }
        finally
        {
            dtAdptr.Dispose();
            dtAdptr = null;

            sqlCmd.Dispose();
            sqlCmd = null;

            CloseBBMLogSQLConnection();
        }
        return dsDataSet;
    }
    public DataSet CancelOrderDetail(string sOrderNo)
    {
        SqlCommand sqlCmd = new SqlCommand();
        SqlDataAdapter dtAdptr = new SqlDataAdapter(sqlCmd);

        DataSet dsDataSet = new DataSet();

        try
        {
            OpenBBMLogSQLConnection();
            sqlCmd = new SqlCommand("[CancelOrderUniCom]", oConnect);
            sqlCmd.CommandType = CommandType.StoredProcedure;

            if (sOrderNo != "")
                sqlCmd.Parameters.Add("@OrderNo", SqlDbType.VarChar).Value = sOrderNo;

            dtAdptr = new SqlDataAdapter(sqlCmd);

            dsDataSet = new DataSet();
            dtAdptr.Fill(dsDataSet);
        }
        catch (Exception ex)
        {
            oErrorLog.Open();
            oErrorLog.Log("CancelOrderDetail Function In BBMDataInfo:" + ex.Message);
            oErrorLog.Flush();
            oErrorLog.Close();
        }
        finally
        {
            dtAdptr.Dispose();
            dtAdptr = null;

            sqlCmd.Dispose();
            sqlCmd = null;

            CloseBBMLogSQLConnection();
        }
        return dsDataSet;
    }


    public DataSet CancelOrderItem(string sOrderNo, string sOrderItemId)
    {
        SqlCommand sqlCmd = new SqlCommand();
        SqlDataAdapter dtAdptr = new SqlDataAdapter(sqlCmd);

        DataSet dsDataSet = new DataSet();

        try
        {
            OpenBBMLogSQLConnection();
            sqlCmd = new SqlCommand("[CancelOrderItemUniCom]", oConnect);
            sqlCmd.CommandType = CommandType.StoredProcedure;

            if (sOrderNo != "")
                sqlCmd.Parameters.Add("@OrderNo", SqlDbType.VarChar).Value = sOrderNo;

            sqlCmd.Parameters.Add("@OrderItemId", SqlDbType.VarChar).Value = sOrderItemId;

            dtAdptr = new SqlDataAdapter(sqlCmd);

            dsDataSet = new DataSet();
            dtAdptr.Fill(dsDataSet);
        }
        catch (Exception ex)
        {
            oErrorLog.Open();
            oErrorLog.Log("CancelOrderItem Function In BBMDataInfo:" + ex.Message);
            oErrorLog.Flush();
            oErrorLog.Close();
        }
        finally
        {
            dtAdptr.Dispose();
            dtAdptr = null;

            sqlCmd.Dispose();
            sqlCmd = null;

            CloseBBMLogSQLConnection();
        }
        return dsDataSet;
    }
    // If order modified more than once
    public DataSet CancelOrdersInUnicomand(string sOrderNo, string sNo)
    {
        SqlCommand sqlCmd = new SqlCommand();
        SqlDataAdapter dtAdptr = new SqlDataAdapter(sqlCmd);

        DataSet dsDataSet = new DataSet();

        try
        {
            OpenBBMLogSQLConnection();
            sqlCmd = new SqlCommand("[CancelOrderUniCom_ModifiedOld]", oConnect);
            sqlCmd.CommandType = CommandType.StoredProcedure;

            if (sOrderNo != "")
                sqlCmd.Parameters.Add("@OrderNo", SqlDbType.VarChar).Value = sOrderNo;

            sqlCmd.Parameters.Add("@sNo", SqlDbType.Int).Value = sNo;

            dtAdptr = new SqlDataAdapter(sqlCmd);

            dsDataSet = new DataSet();
            dtAdptr.Fill(dsDataSet);
        }
        catch (Exception ex)
        {
            oErrorLog.Open();
            oErrorLog.Log("CancelOrdersInUnicomand Function In BBMDataInfo:" + ex.Message);
            oErrorLog.Flush();
            oErrorLog.Close();
        }
        finally
        {
            dtAdptr.Dispose();
            dtAdptr = null;

            sqlCmd.Dispose();
            sqlCmd = null;

            CloseBBMLogSQLConnection();
        }
        return dsDataSet;
    }
    public DataSet GetDespatchOrderFromUniCom(string sFromdate, string sTodate, string sStausId, string sSearchorder, string sCourier, string sCollection, string sOrderSource)
    {
        SqlCommand sqlCmd = new SqlCommand();
        SqlDataAdapter dtAdptr = new SqlDataAdapter(sqlCmd);

        DataSet dsDataSet = new DataSet();

        try
        {
            OpenBBMLogSQLConnection();
            sqlCmd = new SqlCommand("[Unicom_GetDespatchOrders]", oConnect);
            sqlCmd.CommandType = CommandType.StoredProcedure;

            if (sFromdate != "")
                sqlCmd.Parameters.Add("@Startdate", SqlDbType.DateTime).Value = DateTime.Parse(sFromdate);
            else
                sqlCmd.Parameters.Add("@Startdate", SqlDbType.DateTime).Value = DBNull.Value;

            if (sTodate != "")
                sqlCmd.Parameters.Add("@Enddate", SqlDbType.DateTime).Value = DateTime.Parse(sTodate);
            else
                sqlCmd.Parameters.Add("@Enddate", SqlDbType.DateTime).Value = DBNull.Value;

            if (sStausId != "")
                sqlCmd.Parameters.Add("@StatusId", SqlDbType.VarChar).Value = sStausId;
            else
                sqlCmd.Parameters.Add("@StatusId", SqlDbType.VarChar).Value = DBNull.Value;

            if (sSearchorder != "")
                sqlCmd.Parameters.Add("@SearchOrder", SqlDbType.VarChar).Value = sSearchorder;
            else
                sqlCmd.Parameters.Add("@SearchOrder", SqlDbType.VarChar).Value = DBNull.Value;

            if (sCourier != "")
                sqlCmd.Parameters.Add("@CourierName", SqlDbType.VarChar).Value = sCourier;

            if (sCollection != "")
                sqlCmd.Parameters.Add("@Collection", SqlDbType.VarChar).Value = sCollection;

            if (sOrderSource != "")
                sqlCmd.Parameters.Add("@SourceId", SqlDbType.VarChar).Value = sOrderSource;

            dtAdptr = new SqlDataAdapter(sqlCmd);

            dsDataSet = new DataSet();
            dtAdptr.Fill(dsDataSet);
        }
        catch (Exception ex)
        {
            oErrorLog.Open();
            oErrorLog.Log("GetDespatchOrderFromUniCom Function In BBMDataInfo:" + ex.Message);
            oErrorLog.Flush();
            oErrorLog.Close();
        }
        finally
        {
            dtAdptr.Dispose();
            dtAdptr = null;

            sqlCmd.Dispose();
            sqlCmd = null;

            CloseBBMLogSQLConnection();
        }
        return dsDataSet;
    }

    public void UpdateShippingEmailStatus(string sOrderNo, bool isEmailSend)
    {
        try
        {

            OpenBBMLogSQLConnection();
            SqlCommand cmd = new SqlCommand("[Unicom_UpdateShippingEmail]", oConnect);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@OrderNo", SqlDbType.VarChar).Value = sOrderNo;
            cmd.Parameters.Add("@IsEmailSend ", SqlDbType.VarChar).Value = isEmailSend;

            cmd.ExecuteNonQuery();
        }

        catch (Exception ex)
        {
            oErrorLog.Open();
            oErrorLog.Log("UpdateShippingEmailStatus in BBMdataInfo.cs" + ex.Message);
            oErrorLog.Flush();
            oErrorLog.Close();
        }
        finally
        {
            CloseBBMLogSQLConnection();
        }
    }
}