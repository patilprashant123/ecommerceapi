﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.IO;
using System.Text;

using System.Xml;
using System.Xml.Serialization;
using System.Net;

/// <summary>
/// Summary description for UniComInfo
/// </summary>
namespace Uniware
{
    public class UniComInfo : BBMLogConnection
    {
        public UniComInfo()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        #region Global Variable

        string sOperationType = string.Empty;
        #endregion

        public enum OrderOperationType
        {
            CreateOrder = 1,
            EditAddress = 2,
            CancelOrder = 3,
            CancelOrderItem = 4
        }
        public static string SuccessMsg = "Successfully ! Update in Bedbathmore & Unicommerce .";
        public static string FailedMsg = "Failed ! Update in Unicommerce.";
        public static string OrderItemCancelMsg = "Successfully ! Item Cancelled in Bedbathmore & Unicommerce .";

        public static string CancelMsg = "Order Cancel in Bedbathmore & Unicommerce .";


        #region Category Vaiable

        private string codeField;
        private string nameField;
        private string cutOffField;
        private string taxTypeCodeField;
        private string shipTogetherField;
        private string sMainCategory;
        private bool IsCategorySuccess = false;
        string errorDetail;


        public string Errors
        {
            set { errorDetail = value; }
            get { return errorDetail; }
        }

        private string RemoteADD
        {
            get { return Convert.ToString(HttpContext.Current.Request.ServerVariables["remote_addr"]); }
        }
        private int AdminUserId
        {
            get { return UserSession.GetCurrentUserId(); }
        }
        #endregion

        #region NetworkCredential
        public string UniCommerceNetworkUName
        {
            get { return Convert.ToString(ConfigurationManager.AppSettings["UniComNetWorkUserName"]); }
        }
        public string UniCommerceNetworkPass
        {
            get { return Convert.ToString(ConfigurationManager.AppSettings["UniComNetWorkpass"]); }
        }
        public string UniCommerceUSerName
        {
            get { return Convert.ToString(ConfigurationManager.AppSettings["UniComUserName"]); }
        }
        public string UniCommerceAPiKey
        {
            get { return Convert.ToString(ConfigurationManager.AppSettings["UniComApiKey"]); }
        }
        #endregion


        #region Category property

        public string CategoryCode
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }
        public string MainCategory
        {
            get
            {
                return this.sMainCategory;
            }
            set
            {
                this.sMainCategory = value;
            }
        }
        public string CategoryName
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }
        public string CutOff
        {
            get
            {
                return this.cutOffField;
            }
            set
            {
                this.cutOffField = value;
            }
        }
        public string TaxTypeCode
        {
            get
            {
                return this.taxTypeCodeField;
            }
            set
            {
                this.taxTypeCodeField = value;
            }
        }
        public string ShipTogether
        {
            get
            {
                return this.shipTogetherField;
            }
            set
            {
                this.shipTogetherField = value;
            }
        }
        #endregion

        #region Update Category

        public bool CreateOrEditCategory()
        {
            bool IsSuccess = false;

            try
            {

                NetworkCredential nc = new NetworkCredential(UniCommerceNetworkUName, UniCommerceNetworkPass);
                UnicommerceClient uc = new UnicommerceClient();
                uc.ClientCredentials.UserName.UserName = UniCommerceUSerName;
                uc.ClientCredentials.UserName.Password = UniCommerceAPiKey;


                CreateOrEditCategoryRequest oCategory = new CreateOrEditCategoryRequest();
                oCategory.Code = CategoryCode;
                oCategory.Name = CategoryName;
                oCategory.TaxTypeCode = TaxTypeCode;

                ServiceResponse sResponse = uc.CreateOrEditCategory(oCategory);

                if (sResponse.Successful)
                    IsSuccess = true;


                if (sResponse.Errors != null)
                    errorDetail = Convert.ToString(sResponse.Errors[0].description);

                UpdateLog(CategoryCode, CategoryName, IsSuccess, errorDetail, "UpdateCategoryLog", sOperationType);

            }
            catch (Exception ex)
            {
                Errors = ex.Message.ToString();
                UpdateLog(CategoryCode, CategoryName, IsSuccess, Errors, "UpdateCategoryLog", sOperationType);
            }

            return IsSuccess;
        }


        public void UpdateLog(string sColumnId, string iColumnName, bool IsSuccess, string sErrordetail, string sProcedureName, string sOperationName)
        {
            SqlCommand sqlCmd = new SqlCommand();
            try
            {
                OpenBBMLogSQLConnection();
                sqlCmd = new SqlCommand(sProcedureName, oConnect);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                if (sColumnId != "")
                    sqlCmd.Parameters.Add("@ColumnId", SqlDbType.Int).Value = sColumnId;

                if (iColumnName != "")
                    sqlCmd.Parameters.Add("@ColumnName", SqlDbType.VarChar).Value = iColumnName;

                sqlCmd.Parameters.Add("@IsSuccessfull", SqlDbType.Bit).Value = IsSuccess;
                if (sErrordetail != "")
                    sqlCmd.Parameters.Add("@ErrorDetail", SqlDbType.VarChar).Value = sErrordetail;

                sqlCmd.Parameters.Add("@UserId ", SqlDbType.Int).Value = AdminUserId;
                sqlCmd.Parameters.Add("@UserIpAddress ", SqlDbType.VarChar).Value = RemoteADD;

                if (sOperationName != "")
                    sqlCmd.Parameters.Add("@OperationType", SqlDbType.VarChar).Value = sOperationName;

                //sqlCmd.Parameters.Add("@LogTableName", SqlDbType.VarChar).Value = sTableName;

                sqlCmd.ExecuteNonQuery();

            }
            catch (Exception ex)
            {

            }
            finally
            {
                sqlCmd.Dispose();
                sqlCmd = null;
                CloseBBMLogSQLConnection();
            }

        }

        #endregion


        #region Update Items


        public bool CreateItemType(string ItemCategoryName, string ItemCode, string sItemid, string sSize, string sColor, string sCollection
            , string DisplayCategoryName, string txtOneLineSummary, decimal Mrp, int iWeight, int iLength, int iWidth, int iHeight, string sImageUrl, string sProductUrl
            , string StandardCode, string DesignType, string MainCategory

            )
        {

            bool IsSuccess = false;


            try
            {
                NetworkCredential nc = new NetworkCredential(UniCommerceNetworkUName, UniCommerceNetworkPass);
                UnicommerceClient uc = new UnicommerceClient();

                uc.ClientCredentials.UserName.UserName = UniCommerceUSerName;
                uc.ClientCredentials.UserName.Password = UniCommerceAPiKey;

                ItemTypeRequest oItem = new ItemTypeRequest();
                oItem.Name = ItemCategoryName;
                oItem.ItemSKU = ItemCode;
                oItem.Size = sSize;
                oItem.Color = sColor;
                oItem.Brand = sCollection;
                oItem.CategoryCode = DisplayCategoryName;
                oItem.Description = txtOneLineSummary;
                oItem.MRP = Mrp;
                oItem.MRPSpecified = true;
                oItem.Weight = 1;
                oItem.Width = 1;
                oItem.Height = 1;
                oItem.Length = 1;
                oItem.ImageURL = sImageUrl;
                oItem.ProductPageUrl = sProductUrl;

                int iTotalCustomField = 3;

                CustomFieldsCustomField[] ocustom = new CustomFieldsCustomField[iTotalCustomField];

                for (int i = 0; i <= iTotalCustomField - 1; i++)
                {
                    CustomFieldsCustomField customField = new CustomFieldsCustomField();

                    if (i == 0) // For StandardCode
                    {
                        customField.name = "StandardCode";
                        customField.value = StandardCode;
                        ocustom[i] = customField;
                        customField = null;
                    }
                    else if (i == 1)     // For StandardCode Design Type Eg- Floral,Solid etc
                    {
                        customField.name = "Design";
                        if (DesignType != "Temp")
                            customField.value = DesignType;
                        else
                            customField.value = "";

                        ocustom[i] = customField;
                        customField = null;
                    }
                    else
                    {
                        customField.name = "mCat";
                        customField.value = MainCategory;
                        ocustom[i] = customField;
                        customField = null;
                    }

                }

                /*ocustom[0] = customField;
        
                customField.name = "Design";
                customField.value = DesignType;
                ocustom[1] = customField;*/


                oItem.CustomFields = ocustom;



                string strXML = ToXML(oItem);
                // HttpContext.Current.Response.Write(strXML);

                ServiceResponse sResponse = uc.CreateOrEditItemType(oItem);



                if (sResponse.Successful)
                    IsSuccess = true;


                if (sResponse.Errors != null)
                    Errors = Convert.ToString(sResponse.Errors[0].description);

                UpdateLog(sItemid, ItemCode, IsSuccess, Errors, "[UpdateItemsLog]", sOperationType);

            }
            catch (Exception Ex)
            {
                Errors = Convert.ToString(Ex.Message);
                UpdateLog(sItemid, ItemCode, IsSuccess, Errors, "[UpdateItemsLog]", sOperationType);
            }
            return IsSuccess;

        }
        public string ToXML(Object oObject)
        {
            XmlDocument xmlDoc = new XmlDocument();
            XmlSerializer xmlSerializer = new XmlSerializer(oObject.GetType());
            using (MemoryStream xmlStream = new MemoryStream())
            {
                xmlSerializer.Serialize(xmlStream, oObject);
                xmlStream.Position = 0;
                xmlDoc.Load(xmlStream);
                return xmlDoc.InnerXml;
            }
        }
        #endregion


        #region Orders


        public bool CreateSubOrder_ItemCode(string sOrderNo)
        {
            bool IsSuccess = false;

            try
            {

                NetworkCredential nc = new NetworkCredential(UniCommerceNetworkUName, UniCommerceNetworkPass);
                UnicommerceClient uc = new UnicommerceClient();
                uc.ClientCredentials.UserName.UserName = UniCommerceUSerName;
                uc.ClientCredentials.UserName.Password = UniCommerceAPiKey;

                SaleOrder oSale = new SaleOrder();




                BBmDataInfo bbmData = new BBmDataInfo();
                DataSet dsOrder = bbmData.OrderDetailByItemCode(sOrderNo);

                Address AddBillDetail = new Address();
                AddressRef billref = new AddressRef();
                AddressRef shipRef = new AddressRef();

                int iModifiedOrderSNo = 0;
                if (dsOrder.Tables[0].Rows.Count > 0)
                {

                    iModifiedOrderSNo = Convert.ToInt32(dsOrder.Tables[0].Rows[0]["ModifiedOrderSNo"]);

                    //oSale.Code = Convert.ToString(dsOrder.Tables[0].Rows[0]["OrderNo"]);


                    oSale.DisplayOrderDateTime = Convert.ToDateTime(dsOrder.Tables[0].Rows[0]["OrderDate"]);

                    if (iModifiedOrderSNo > 0)
                        oSale.Code = Convert.ToString(dsOrder.Tables[0].Rows[0]["OrderNo"]) + "-" + Convert.ToString(iModifiedOrderSNo);
                    else
                        oSale.Code = Convert.ToString(dsOrder.Tables[0].Rows[0]["OrderNo"]);


                    oSale.DisplayOrderCode = Convert.ToString(dsOrder.Tables[0].Rows[0]["OrderNo"]);

                    oSale.CustomerCode = Convert.ToString(dsOrder.Tables[0].Rows[0]["CustomerId"]);
                    oSale.NotificationEmail = Convert.ToString(dsOrder.Tables[0].Rows[0]["Email"]);


                    oSale.NotificationMobile = Convert.ToString(dsOrder.Tables[0].Rows[0]["MobileNo"]);
                    oSale.CashOnDelivery = Convert.ToBoolean(dsOrder.Tables[0].Rows[0]["IsCod"]);
                  //  oSale.CashOnDeliverySpecified = Convert.ToBoolean(dsOrder.Tables[0].Rows[0]["IsCod"]);
                    //oSale.Source = Convert.ToString(dsOrder.Tables[0].Rows[0]["SourceName"]);


                    decimal dBillAmount = Convert.ToDecimal(dsOrder.Tables[0].Rows[0]["BillAmount"]);
                    string bIllAmount = string.Empty;

                    if (dBillAmount > 0)
                        bIllAmount = Convert.ToString(dsOrder.Tables[0].Rows[0]["BillAmount"]);
                    else
                        bIllAmount = "1";


                    string DiscountCouponName = Convert.ToString(dsOrder.Tables[0].Rows[0]["DiscountCouponName"]);
                    string DiscountCouponValue = Convert.ToString(dsOrder.Tables[0].Rows[0]["DiscountCouponValue"]);
                    string OfferDiscount = Convert.ToString(dsOrder.Tables[0].Rows[0]["OfferDiscount"]);
                    string ReferenceNo = Convert.ToString(dsOrder.Tables[0].Rows[0]["RefNo"]);
                    string InvoiceNo = Convert.ToString(dsOrder.Tables[0].Rows[0]["InvoiceNo"]);
                    string Source = Convert.ToString(dsOrder.Tables[0].Rows[0]["SourceName"]);


                    int iTotalCustomField = 7;

                    CustomFieldsCustomField[] ocustom = new CustomFieldsCustomField[iTotalCustomField];

                    for (int i = 0; i <= iTotalCustomField - 1; i++)
                    {
                        CustomFieldsCustomField customField = new CustomFieldsCustomField();

                        if (i == 0)
                        {
                            customField.name = "BillAmount";
                            customField.value = bIllAmount;
                            ocustom[i] = customField;
                            customField = null;
                        }
                        else if (i == 1)
                        {
                            customField.name = "DiscountCouponName";
                            customField.value = DiscountCouponName;
                            ocustom[i] = customField;
                            customField = null;
                        }
                        else if (i == 2)
                        {
                            customField.name = "DiscountAmount";
                            customField.value = DiscountCouponValue;
                            ocustom[i] = customField;
                            customField = null;
                        }
                        else if (i == 3)
                        {
                            customField.name = "OfferDiscount";
                            customField.value = OfferDiscount;
                            ocustom[i] = customField;
                            customField = null;
                        }
                        else if (i == 4)
                        {
                            customField.name = "ReferenceNo";
                            customField.value = ReferenceNo;
                            ocustom[i] = customField;
                            customField = null;
                        }
                        else if (i == 5)
                        {
                            customField.name = "InvoiceNo";
                            customField.value = InvoiceNo;
                            ocustom[i] = customField;
                            customField = null;
                        }
                        else if (i == 6)
                        {
                            customField.name = "Source";
                            customField.value = Source;
                            ocustom[i] = customField;
                            customField = null;
                        }

                    }




                    oSale.CustomFields = ocustom;
                    string sBillPhone = string.Empty;
                    if (oSale.CashOnDelivery)
                    {

                        oSale.CashOnDelivery = Convert.ToBoolean(dsOrder.Tables[0].Rows[0]["IsCod"]);
                        oSale.CashOnDeliverySpecified = Convert.ToBoolean(dsOrder.Tables[0].Rows[0]["IsCod"]);
                   
                      
                        AddBillDetail.id = Convert.ToString(dsOrder.Tables[0].Rows[0]["AddressId"]);
                        AddBillDetail.AddressLine1 = Convert.ToString(dsOrder.Tables[0].Rows[0]["BillingAddress"]);
                        AddBillDetail.AddressLine2 = "";
                        AddBillDetail.City = Convert.ToString(dsOrder.Tables[0].Rows[0]["BillingCity"]);
                        AddBillDetail.Name = Convert.ToString(dsOrder.Tables[0].Rows[0]["BillingName"]);

                        if (Convert.ToString(dsOrder.Tables[0].Rows[0]["MobileNo"]) != "")
                            AddBillDetail.Phone = Convert.ToString(dsOrder.Tables[0].Rows[0]["MobileNo"]);
                        else if (Convert.ToString(dsOrder.Tables[0].Rows[0]["OfficePhone"]) != "")
                            AddBillDetail.Phone = Convert.ToString(dsOrder.Tables[0].Rows[0]["OfficePhone"]);
                        else if (Convert.ToString(dsOrder.Tables[0].Rows[0]["HomePhone"]) != "")
                            AddBillDetail.Phone = Convert.ToString(dsOrder.Tables[0].Rows[0]["HomePhone"]);

                        AddBillDetail.State = Convert.ToString(dsOrder.Tables[0].Rows[0]["BillingState"]); //"MH"; // 
                        AddBillDetail.Pincode = Convert.ToString(dsOrder.Tables[0].Rows[0]["BillingPostCode"]);

                        Address[] adrList = new Address[2];
                        billref.@ref = Convert.ToString(dsOrder.Tables[0].Rows[0]["AddressId"]);
                        oSale.BillingAddress = billref;


                        Address AddShippDetail = new Address();
                        adrList = new Address[2];
                        AddShippDetail.id = AddBillDetail.id;
                        AddShippDetail.AddressLine1 = AddBillDetail.AddressLine1;
                        AddShippDetail.AddressLine2 = AddBillDetail.AddressLine2;
                        AddShippDetail.City = AddBillDetail.City;
                        AddShippDetail.Name = AddBillDetail.Name;
                        AddShippDetail.Phone = AddBillDetail.Phone;
                        AddShippDetail.State = AddBillDetail.State;// "MH";//Convert.ToString(dsOrder.Tables[0].Rows[0]["ShippingState"]);
                        AddShippDetail.Pincode = AddBillDetail.Pincode;

                        adrList[0] = AddBillDetail;
                        adrList[1] = AddShippDetail;
                        oSale.Addresses = adrList;



                        shipRef.@ref = Convert.ToString(dsOrder.Tables[0].Rows[0]["ShippingAddressId"]);
                        oSale.BillingAddress = billref;   // Billing and Shipping are same for COD
                        oSale.ShippingAddress = billref;

                    }

                    else if (!oSale.CashOnDelivery)
                    {


                        oSale.CashOnDelivery = true;
                        oSale.CashOnDeliverySpecified = Convert.ToBoolean(dsOrder.Tables[0].Rows[0]["IsCod"]);
                   
                      
                        AddBillDetail.id = Convert.ToString(dsOrder.Tables[0].Rows[0]["AddressId"]);
                        AddBillDetail.AddressLine1 = Convert.ToString(dsOrder.Tables[0].Rows[0]["BillingAddress"]);
                        AddBillDetail.AddressLine2 = "";
                        AddBillDetail.City = Convert.ToString(dsOrder.Tables[0].Rows[0]["BillingCity"]);
                        AddBillDetail.Name = Convert.ToString(dsOrder.Tables[0].Rows[0]["BillingName"]);

                        if (Convert.ToString(dsOrder.Tables[0].Rows[0]["MobileNo"]) != "")
                            AddBillDetail.Phone = Convert.ToString(dsOrder.Tables[0].Rows[0]["MobileNo"]);
                        else if (Convert.ToString(dsOrder.Tables[0].Rows[0]["OfficePhone"]) != "")
                            AddBillDetail.Phone = Convert.ToString(dsOrder.Tables[0].Rows[0]["OfficePhone"]);
                        else if (Convert.ToString(dsOrder.Tables[0].Rows[0]["HomePhone"]) != "")
                            AddBillDetail.Phone = Convert.ToString(dsOrder.Tables[0].Rows[0]["HomePhone"]);


                        AddBillDetail.State = Convert.ToString(dsOrder.Tables[0].Rows[0]["BillingState"]);
                        AddBillDetail.Pincode = Convert.ToString(dsOrder.Tables[0].Rows[0]["BillingPostCode"]);

                        Address[] adrList = new Address[2];
                        billref.@ref = Convert.ToString(dsOrder.Tables[0].Rows[0]["AddressId"]);
                        oSale.BillingAddress = billref;


                        Address AddShippDetail = new Address();
                        adrList = new Address[2];
                        AddShippDetail.id = Convert.ToString(dsOrder.Tables[0].Rows[0]["ShippingAddressId"]);
                        AddShippDetail.AddressLine1 = Convert.ToString(dsOrder.Tables[0].Rows[0]["ShippingAddress"]);
                        AddShippDetail.AddressLine2 = "";
                        AddShippDetail.City = Convert.ToString(dsOrder.Tables[0].Rows[0]["ShippingCity"]);
                        AddShippDetail.Name = Convert.ToString(dsOrder.Tables[0].Rows[0]["ShippingName"]);

                        if (Convert.ToString(dsOrder.Tables[0].Rows[0]["ShipMobile"]) != "")
                            AddShippDetail.Phone = Convert.ToString(dsOrder.Tables[0].Rows[0]["ShipMobile"]);
                        else
                            AddShippDetail.Phone = AddBillDetail.Phone; // if shipping  phone empty then Billing phone

                        AddShippDetail.State = Convert.ToString(dsOrder.Tables[0].Rows[0]["ShippingState"]);
                        AddShippDetail.Pincode = Convert.ToString(dsOrder.Tables[0].Rows[0]["ShippingPostCode"]);

                        adrList[0] = AddBillDetail;
                        adrList[1] = AddShippDetail;
                        oSale.Addresses = adrList;



                        shipRef.@ref = Convert.ToString(dsOrder.Tables[0].Rows[0]["ShippingAddressId"]);
                        oSale.BillingAddress = billref;
                        oSale.ShippingAddress = shipRef;


                    }

                }
                if (dsOrder.Tables[1].Rows.Count > 0)
                {
                    int iTotalItem = Convert.ToInt32(dsOrder.Tables[1].Rows.Count);
                    int iTotalItemQty = Convert.ToInt32(dsOrder.Tables[2].Rows[0]["TotalQtyInOrder"]);

                    SaleOrderItem[] orderItem = new SaleOrderItem[iTotalItemQty];

                    //CustomFieldsCustomField[] customfieldOItems = new CustomFieldsCustomField[iTotalItem];

                    int iTotalQtyInOrder = 0;



                    for (int i = 0; i <= iTotalItem - 1; i++)
                    {
                        int iTotalQtyofItem = Convert.ToInt32(dsOrder.Tables[1].Rows[i]["Quantity"]);


                        for (int j = 0; j <= iTotalQtyofItem - 1; j++)
                        {

                            //CustomFieldsCustomField[] customfieldOItems = new CustomFieldsCustomField[i + 1];
                            SaleOrderItem oSaleOrderItem = new SaleOrderItem();
                            int iQtyVal = j + 1;


                            oSaleOrderItem.ItemSKU = Convert.ToString(dsOrder.Tables[1].Rows[i]["ItemCode"]);
                            oSaleOrderItem.CashOnDeliveryCharges = 0;

                            if (iModifiedOrderSNo > 0)
                                oSaleOrderItem.Code = Convert.ToString(dsOrder.Tables[1].Rows[i]["OrderItemId"]) + "-" + Convert.ToString(iModifiedOrderSNo) + "-" + iQtyVal.ToString();
                            else
                                oSaleOrderItem.Code = Convert.ToString(dsOrder.Tables[1].Rows[i]["OrderItemId"]) + "-" + iQtyVal.ToString();

                            if (Convert.ToBoolean(dsOrder.Tables[1].Rows[i]["IsGiftWrap"]))
                            {

                                oSaleOrderItem.GiftMessage = Convert.ToString(dsOrder.Tables[1].Rows[i]["GiftMessage"]);
                                oSaleOrderItem.GiftWrap = Convert.ToBoolean(dsOrder.Tables[1].Rows[i]["IsGiftWrap"]);
                                oSaleOrderItem.GiftWrapCharges = Convert.ToDecimal(dsOrder.Tables[1].Rows[i]["GiftAmount"]);
                                oSaleOrderItem.GiftWrapChargesSpecified = true;
                                oSaleOrderItem.GiftWrapSpecified = true;
                            }





                            oSaleOrderItem.PrepaidAmount = 1;
                            oSaleOrderItem.SellingPrice = Convert.ToDecimal(dsOrder.Tables[1].Rows[i]["RetailPrice"]);
                            oSaleOrderItem.TotalPrice = Convert.ToDecimal(dsOrder.Tables[1].Rows[i]["TotalSellingPrice"]);
                            //oSaleOrderItem.TotalPrice = Convert.ToDecimal(dsOrder.Tables[1].Rows[i]["Amount"]);
                            oSaleOrderItem.ShippingMethodCode = "STD";

                            decimal dItemDiscount = Convert.ToDecimal(dsOrder.Tables[1].Rows[i]["DiscountValue"]);
                            if (dItemDiscount > 0)
                            {
                                decimal dsellingPrice = Convert.ToDecimal(dsOrder.Tables[1].Rows[i]["RetailPrice"]) - dItemDiscount;
                                decimal dTotalSellingPrice = Convert.ToDecimal(dsOrder.Tables[1].Rows[i]["TotalSellingPrice"]) - dItemDiscount;

                                //oSaleOrderItem.SellingPrice = Convert.ToDecimal(dsOrder.Tables[1].Rows[i]["RetailPrice"]) - dItemDiscount;
                                //oSaleOrderItem.TotalPrice = Convert.ToDecimal(dsOrder.Tables[1].Rows[i]["TotalSellingPrice"]) - dItemDiscount;


                                if (dsellingPrice > 0)
                                {
                                    
                                    oSaleOrderItem.SellingPrice = Convert.ToDecimal(dsOrder.Tables[1].Rows[i]["RetailPrice"]) - dItemDiscount;
                                    oSaleOrderItem.TotalPrice = Convert.ToDecimal(dsOrder.Tables[1].Rows[i]["TotalSellingPrice"]) - dItemDiscount;

                                    oSaleOrderItem.Discount = Convert.ToDecimal(dsOrder.Tables[1].Rows[i]["DiscountValue"]);
                                    oSaleOrderItem.DiscountSpecified = true;
                                }
                                else
                                {
                                    oSaleOrderItem.SellingPrice = 1;
                                    oSaleOrderItem.TotalPrice = 1;
                                }


                               
                            }
                            else
                            {
                                oSaleOrderItem.SellingPrice = Convert.ToDecimal(dsOrder.Tables[1].Rows[i]["RetailPrice"]);
                                oSaleOrderItem.TotalPrice = Convert.ToDecimal(dsOrder.Tables[1].Rows[i]["TotalSellingPrice"]);
                            }


                            //oSaleOrderItem.Discount = Convert.ToDecimal(dsOrder.Tables[1].Rows[i]["DiscountValue"]);
                            orderItem[iTotalQtyInOrder] = oSaleOrderItem;
                            oSaleOrderItem = null;
                            iTotalQtyInOrder++;

                        }
                    }
                    oSale.SaleOrderItems = orderItem;



                    if (oSale.CashOnDelivery)
                    {
                        oSale.BillingAddress = billref;
                    }
                    else
                    {
                        oSale.BillingAddress = billref;
                        oSale.ShippingAddress = shipRef;
                    }



                }


                CreateSaleOrderRequest oCreateOrder = new CreateSaleOrderRequest();
                oCreateOrder.SaleOrder = oSale;
                // string strXML = ToXML(oSale);

                string strXML = ToXML(oSale);
                HttpContext.Current.Response.Write(strXML);

                ServiceResponse oResponse = uc.CreateSaleOrder(oCreateOrder);

                if (oResponse.Successful)
                    IsSuccess = true;


                if (oResponse.Errors != null)
                    Errors = Convert.ToString(oResponse.Errors[0].description);


                UpdateLog("", sOrderNo, IsSuccess, Errors, "[UpdateOrdersLog]", OrderOperationType.CreateOrder.ToString());


            }
            catch (Exception ex)
            {
                Errors = Convert.ToString(ex.Message);
                UpdateLog("", sOrderNo, IsSuccess, Errors, "[UpdateOrdersLog]", OrderOperationType.CreateOrder.ToString());
            }
            return IsSuccess;
        }


        public bool CreateUpdateOrder(string sOrderNo)
        {
            bool IsSuccess = false;

            try
            {

                NetworkCredential nc = new NetworkCredential(UniCommerceNetworkUName, UniCommerceNetworkPass);
                UnicommerceClient uc = new UnicommerceClient();
                uc.ClientCredentials.UserName.UserName = UniCommerceUSerName;
                uc.ClientCredentials.UserName.Password = UniCommerceAPiKey;

                SaleOrder oSale = new SaleOrder();




                BBmDataInfo bbmData = new BBmDataInfo();
                DataSet dsOrder = bbmData.OrderDetail(sOrderNo);

                Address AddBillDetail = new Address();
                AddressRef billref = new AddressRef();
                AddressRef shipRef = new AddressRef();

                int iModifiedOrderSNo = 0;
                if (dsOrder.Tables[0].Rows.Count > 0)
                {

                    iModifiedOrderSNo = Convert.ToInt32(dsOrder.Tables[0].Rows[0]["ModifiedOrderSNo"]);

                    //oSale.Code = Convert.ToString(dsOrder.Tables[0].Rows[0]["OrderNo"]);


                    oSale.DisplayOrderDateTime = Convert.ToDateTime(dsOrder.Tables[0].Rows[0]["OrderDate"]);

                    if (iModifiedOrderSNo > 0)
                        oSale.Code = Convert.ToString(dsOrder.Tables[0].Rows[0]["OrderNo"]) + "-" + Convert.ToString(iModifiedOrderSNo);
                    else
                        oSale.Code = Convert.ToString(dsOrder.Tables[0].Rows[0]["OrderNo"]);


                    oSale.DisplayOrderCode = Convert.ToString(dsOrder.Tables[0].Rows[0]["OrderNo"]);

                    oSale.CustomerCode = Convert.ToString(dsOrder.Tables[0].Rows[0]["CustomerId"]);
                    oSale.NotificationEmail = Convert.ToString(dsOrder.Tables[0].Rows[0]["Email"]);


                    oSale.NotificationMobile = Convert.ToString(dsOrder.Tables[0].Rows[0]["MobileNo"]);
                    oSale.CashOnDelivery = Convert.ToBoolean(dsOrder.Tables[0].Rows[0]["IsCod"]);
                   // oSale.CashOnDeliverySpecified = Convert.ToBoolean(dsOrder.Tables[0].Rows[0]["IsCod"]);
                    //oSale.Source = Convert.ToString(dsOrder.Tables[0].Rows[0]["SourceName"]);


                    decimal dBillAmount = Convert.ToDecimal(dsOrder.Tables[0].Rows[0]["BillAmount"]);
                    string bIllAmount = string.Empty;

                    if (dBillAmount > 0)
                        bIllAmount = Convert.ToString(dsOrder.Tables[0].Rows[0]["BillAmount"]);
                    else
                        bIllAmount = "1";


                    string DiscountCouponName = Convert.ToString(dsOrder.Tables[0].Rows[0]["DiscountCouponName"]);
                    string DiscountCouponValue = Convert.ToString(dsOrder.Tables[0].Rows[0]["DiscountCouponValue"]);
                    string OfferDiscount = Convert.ToString(dsOrder.Tables[0].Rows[0]["OfferDiscount"]);
                    string ReferenceNo = Convert.ToString(dsOrder.Tables[0].Rows[0]["RefNo"]);
                    string InvoiceNo = Convert.ToString(dsOrder.Tables[0].Rows[0]["InvoiceNo"]);
                    string Source = Convert.ToString(dsOrder.Tables[0].Rows[0]["SourceName"]);







                    /*CustomFieldsCustomField[] ocustom = new CustomFieldsCustomField[1];
                    CustomFieldsCustomField customField = new CustomFieldsCustomField();
                    customField.name = "Source";
                    customField.value = Convert.ToString(dsOrder.Tables[0].Rows[0]["SourceName"]);
                    ocustom[0] = customField;
                    oSale.CustomFields = ocustom;*/


                    int iTotalCustomField = 7;

                    CustomFieldsCustomField[] ocustom = new CustomFieldsCustomField[iTotalCustomField];

                    for (int i = 0; i <= iTotalCustomField - 1; i++)
                    {
                        CustomFieldsCustomField customField = new CustomFieldsCustomField();

                        if (i == 0)
                        {
                            customField.name = "BillAmount";
                            customField.value = bIllAmount;
                            ocustom[i] = customField;
                            customField = null;
                        }
                        else if (i == 1)
                        {
                            customField.name = "DiscountCouponName";
                            customField.value = DiscountCouponName;
                            ocustom[i] = customField;
                            customField = null;
                        }
                        else if (i == 2)
                        {
                            customField.name = "DiscountAmount";
                            customField.value = DiscountCouponValue;
                            ocustom[i] = customField;
                            customField = null;
                        }
                        else if (i == 3)
                        {
                            customField.name = "OfferDiscount";
                            customField.value = OfferDiscount;
                            ocustom[i] = customField;
                            customField = null;
                        }
                        else if (i == 4)
                        {
                            customField.name = "ReferenceNo";
                            customField.value = ReferenceNo;
                            ocustom[i] = customField;
                            customField = null;
                        }
                        else if (i == 5)
                        {
                            customField.name = "InvoiceNo";
                            customField.value = InvoiceNo;
                            ocustom[i] = customField;
                            customField = null;
                        }
                        else if (i == 6)
                        {
                            customField.name = "Source";
                            customField.value = Source;
                            ocustom[i] = customField;
                            customField = null;
                        }

                    }




                    oSale.CustomFields = ocustom;
                    string sBillPhone = string.Empty;
                    if (oSale.CashOnDelivery)
                    {


                        oSale.CashOnDelivery = Convert.ToBoolean(dsOrder.Tables[0].Rows[0]["IsCod"]);
                        oSale.CashOnDeliverySpecified = Convert.ToBoolean(dsOrder.Tables[0].Rows[0]["IsCod"]);
                        AddBillDetail.id = Convert.ToString(dsOrder.Tables[0].Rows[0]["AddressId"]);
                        AddBillDetail.AddressLine1 = Convert.ToString(dsOrder.Tables[0].Rows[0]["BillingAddress"]);
                        AddBillDetail.AddressLine2 = "";
                        AddBillDetail.City = Convert.ToString(dsOrder.Tables[0].Rows[0]["BillingCity"]);
                        AddBillDetail.Name = Convert.ToString(dsOrder.Tables[0].Rows[0]["BillingName"]);

                        if (Convert.ToString(dsOrder.Tables[0].Rows[0]["MobileNo"]) != "")
                            AddBillDetail.Phone = Convert.ToString(dsOrder.Tables[0].Rows[0]["MobileNo"]);
                        else if (Convert.ToString(dsOrder.Tables[0].Rows[0]["OfficePhone"]) != "")
                            AddBillDetail.Phone = Convert.ToString(dsOrder.Tables[0].Rows[0]["OfficePhone"]);
                        else if (Convert.ToString(dsOrder.Tables[0].Rows[0]["HomePhone"]) != "")
                            AddBillDetail.Phone = Convert.ToString(dsOrder.Tables[0].Rows[0]["HomePhone"]);






                        AddBillDetail.State = Convert.ToString(dsOrder.Tables[0].Rows[0]["BillingState"]); //"MH"; // 
                        AddBillDetail.Pincode = Convert.ToString(dsOrder.Tables[0].Rows[0]["BillingPostCode"]);

                        Address[] adrList = new Address[2];
                        billref.@ref = Convert.ToString(dsOrder.Tables[0].Rows[0]["AddressId"]);
                        oSale.BillingAddress = billref;


                        Address AddShippDetail = new Address();
                        adrList = new Address[2];
                        AddShippDetail.id = AddBillDetail.id;
                        AddShippDetail.AddressLine1 = AddBillDetail.AddressLine1;
                        AddShippDetail.AddressLine2 = AddBillDetail.AddressLine2;
                        AddShippDetail.City = AddBillDetail.City;
                        AddShippDetail.Name = AddBillDetail.Name;
                        AddShippDetail.Phone = AddBillDetail.Phone;
                        AddShippDetail.State = AddBillDetail.State;// "MH";//Convert.ToString(dsOrder.Tables[0].Rows[0]["ShippingState"]);
                        AddShippDetail.Pincode = AddBillDetail.Pincode;

                        adrList[0] = AddBillDetail;
                        adrList[1] = AddShippDetail;
                        oSale.Addresses = adrList;



                        shipRef.@ref = Convert.ToString(dsOrder.Tables[0].Rows[0]["ShippingAddressId"]);
                        oSale.BillingAddress = billref;   // Billing and Shipping are same for COD
                        oSale.ShippingAddress = billref;

                    }

                    else if (!oSale.CashOnDelivery)
                    {


                        oSale.CashOnDelivery = false;
                        oSale.CashOnDeliverySpecified = true;// Convert.ToBoolean(dsOrder.Tables[0].Rows[0]["IsCod"]);
                   
                        AddBillDetail.id = Convert.ToString(dsOrder.Tables[0].Rows[0]["AddressId"]);
                        AddBillDetail.AddressLine1 = Convert.ToString(dsOrder.Tables[0].Rows[0]["BillingAddress"]);
                        AddBillDetail.AddressLine2 = "";
                        AddBillDetail.City = Convert.ToString(dsOrder.Tables[0].Rows[0]["BillingCity"]);
                        AddBillDetail.Name = Convert.ToString(dsOrder.Tables[0].Rows[0]["BillingName"]);

                        if (Convert.ToString(dsOrder.Tables[0].Rows[0]["MobileNo"]) != "")
                            AddBillDetail.Phone = Convert.ToString(dsOrder.Tables[0].Rows[0]["MobileNo"]);
                        else if (Convert.ToString(dsOrder.Tables[0].Rows[0]["OfficePhone"]) != "")
                            AddBillDetail.Phone = Convert.ToString(dsOrder.Tables[0].Rows[0]["OfficePhone"]);
                        else if (Convert.ToString(dsOrder.Tables[0].Rows[0]["HomePhone"]) != "")
                            AddBillDetail.Phone = Convert.ToString(dsOrder.Tables[0].Rows[0]["HomePhone"]);


                        AddBillDetail.State = Convert.ToString(dsOrder.Tables[0].Rows[0]["BillingState"]);
                        AddBillDetail.Pincode = Convert.ToString(dsOrder.Tables[0].Rows[0]["BillingPostCode"]);

                        Address[] adrList = new Address[2];
                        billref.@ref = Convert.ToString(dsOrder.Tables[0].Rows[0]["AddressId"]);
                        oSale.BillingAddress = billref;


                        Address AddShippDetail = new Address();
                        adrList = new Address[2];
                        AddShippDetail.id = Convert.ToString(dsOrder.Tables[0].Rows[0]["ShippingAddressId"]);
                        AddShippDetail.AddressLine1 = Convert.ToString(dsOrder.Tables[0].Rows[0]["ShippingAddress"]);
                        AddShippDetail.AddressLine2 = "";
                        AddShippDetail.City = Convert.ToString(dsOrder.Tables[0].Rows[0]["ShippingCity"]);
                        AddShippDetail.Name = Convert.ToString(dsOrder.Tables[0].Rows[0]["ShippingName"]);

                        if (Convert.ToString(dsOrder.Tables[0].Rows[0]["ShipMobile"]) != "")
                            AddShippDetail.Phone = Convert.ToString(dsOrder.Tables[0].Rows[0]["ShipMobile"]);
                        else
                            AddShippDetail.Phone = AddBillDetail.Phone; // if shipping  phone empty then Billing phone

                        AddShippDetail.State = Convert.ToString(dsOrder.Tables[0].Rows[0]["ShippingState"]);
                        AddShippDetail.Pincode = Convert.ToString(dsOrder.Tables[0].Rows[0]["ShippingPostCode"]);

                        adrList[0] = AddBillDetail;
                        adrList[1] = AddShippDetail;
                        oSale.Addresses = adrList;



                        shipRef.@ref = Convert.ToString(dsOrder.Tables[0].Rows[0]["ShippingAddressId"]);
                        oSale.BillingAddress = billref;
                        oSale.ShippingAddress = shipRef;


                    }

                }
                if (dsOrder.Tables[1].Rows.Count > 0)
                {
                    int iTotalItem = Convert.ToInt32(dsOrder.Tables[1].Rows.Count);
                    int iTotalItemQty = Convert.ToInt32(dsOrder.Tables[2].Rows[0]["TotalQtyInOrder"]);

                    SaleOrderItem[] orderItem = new SaleOrderItem[iTotalItemQty];

                    //CustomFieldsCustomField[] customfieldOItems = new CustomFieldsCustomField[iTotalItem];

                    int iTotalQtyInOrder = 0;



                    for (int i = 0; i <= iTotalItem - 1; i++)
                    {
                        int iTotalQtyofItem = Convert.ToInt32(dsOrder.Tables[1].Rows[i]["Quantity"]);


                        for (int j = 0; j <= iTotalQtyofItem - 1; j++)
                        {

                            //CustomFieldsCustomField[] customfieldOItems = new CustomFieldsCustomField[i + 1];
                            SaleOrderItem oSaleOrderItem = new SaleOrderItem();
                            int iQtyVal = j + 1;


                            oSaleOrderItem.ItemSKU = Convert.ToString(dsOrder.Tables[1].Rows[i]["ItemCode"]);
                            oSaleOrderItem.CashOnDeliveryCharges = 0;

                            if (iModifiedOrderSNo > 0)
                                oSaleOrderItem.Code = Convert.ToString(dsOrder.Tables[1].Rows[i]["OrderItemId"]) + "-" + Convert.ToString(iModifiedOrderSNo) + "-" + iQtyVal.ToString();
                            else
                                oSaleOrderItem.Code = Convert.ToString(dsOrder.Tables[1].Rows[i]["OrderItemId"]) + "-" + iQtyVal.ToString();

                            if (Convert.ToBoolean(dsOrder.Tables[1].Rows[i]["IsGiftWrap"]))
                            {

                                oSaleOrderItem.GiftMessage = Convert.ToString(dsOrder.Tables[1].Rows[i]["GiftMessage"]);
                                oSaleOrderItem.GiftWrap = Convert.ToBoolean(dsOrder.Tables[1].Rows[i]["IsGiftWrap"]);
                                oSaleOrderItem.GiftWrapCharges = Convert.ToDecimal(dsOrder.Tables[1].Rows[i]["GiftAmount"]);
                                oSaleOrderItem.GiftWrapChargesSpecified = true;
                                oSaleOrderItem.GiftWrapSpecified = true;
                            }





                            oSaleOrderItem.PrepaidAmount = 0;
                            oSaleOrderItem.SellingPrice = Convert.ToDecimal(dsOrder.Tables[1].Rows[i]["RetailPrice"]);
                            if (Convert.ToDecimal(dsOrder.Tables[1].Rows[i]["TotalSellingPrice"]) > 0)
                                oSaleOrderItem.TotalPrice = Convert.ToDecimal(dsOrder.Tables[1].Rows[i]["TotalSellingPrice"]);
                            else
                                oSaleOrderItem.TotalPrice = 1;
                            //oSaleOrderItem.TotalPrice = Convert.ToDecimal(dsOrder.Tables[1].Rows[i]["Amount"]);
                            oSaleOrderItem.ShippingMethodCode = "STD";

                            decimal dItemDiscount = Convert.ToDecimal(dsOrder.Tables[1].Rows[i]["DiscountValue"]);
                            if (dItemDiscount > 0)
                            {
                                //oSaleOrderItem.SellingPrice = Convert.ToDecimal(dsOrder.Tables[1].Rows[i]["RetailPrice"]) - dItemDiscount;

                                if (Convert.ToDecimal(dsOrder.Tables[1].Rows[i]["TotalSellingPrice"]) - dItemDiscount > 0)
                                    oSaleOrderItem.TotalPrice = Convert.ToDecimal(dsOrder.Tables[1].Rows[i]["TotalSellingPrice"]) - dItemDiscount;
                                else
                                    oSaleOrderItem.TotalPrice = 1;


                                oSaleOrderItem.Discount = Convert.ToDecimal(dsOrder.Tables[1].Rows[i]["DiscountValue"]);
                                oSaleOrderItem.DiscountSpecified = true;
                            }
                            else
                            {
                                oSaleOrderItem.SellingPrice = Convert.ToDecimal(dsOrder.Tables[1].Rows[i]["RetailPrice"]);
                                oSaleOrderItem.TotalPrice = Convert.ToDecimal(dsOrder.Tables[1].Rows[i]["TotalSellingPrice"]);
                            }


                            //oSaleOrderItem.Discount = Convert.ToDecimal(dsOrder.Tables[1].Rows[i]["DiscountValue"]);
                            orderItem[iTotalQtyInOrder] = oSaleOrderItem;
                            oSaleOrderItem = null;
                            iTotalQtyInOrder++;

                        }






                        /*SaleOrderItem oSaleOrderItem = new SaleOrderItem();
                        CustomFieldsCustomField[] customfieldOItems = new CustomFieldsCustomField[i + 1];
                        oSaleOrderItem.ItemSKU = Convert.ToString(dsOrder.Tables[1].Rows[i]["ItemCode"]);
                        oSaleOrderItem.CashOnDeliveryCharges = 0;
                        oSaleOrderItem.Code = Convert.ToString(dsOrder.Tables[1].Rows[i]["OrderItemId"]);
                        oSaleOrderItem.GiftMessage = Convert.ToString(dsOrder.Tables[1].Rows[i]["GiftMessage"]);
                        oSaleOrderItem.GiftWrap = Convert.ToBoolean(dsOrder.Tables[1].Rows[i]["IsGiftWrap"]);
                        oSaleOrderItem.GiftWrapCharges = Convert.ToDecimal(dsOrder.Tables[1].Rows[i]["GiftAmount"]);
                        oSaleOrderItem.PrepaidAmount = 0;
                        oSaleOrderItem.SellingPrice = Convert.ToDecimal(dsOrder.Tables[1].Rows[i]["RetailPrice"]);
                        oSaleOrderItem.TotalPrice = Convert.ToDecimal(dsOrder.Tables[1].Rows[i]["Amount"]);
                        oSaleOrderItem.ShippingMethodCode = "STD";
                        oSaleOrderItem.Discount = Convert.ToDecimal(dsOrder.Tables[1].Rows[i]["DiscountValue"]);
                        */
                        // Add Custom Fields


                        /* CustomFieldsCustomField customOItems = new CustomFieldsCustomField();
                         customOItems.name = "Quantity";
                         customOItems.value = Convert.ToString(dsOrder.Tables[1].Rows[i]["Quantity"]);
                         customfieldOItems[i] = customOItems;
                         oSaleOrderItem.CustomFields = customfieldOItems;
                         customOItems = null;*/

                        // Custom Field End


                        /*orderItem[i] = oSaleOrderItem;
                        oSaleOrderItem = null;*/
                    }
                    oSale.SaleOrderItems = orderItem;



                    if (oSale.CashOnDelivery)
                    {
                        oSale.BillingAddress = billref;
                    }
                    else
                    {
                        oSale.BillingAddress = billref;
                        oSale.ShippingAddress = shipRef;
                    }



                }


                CreateSaleOrderRequest oCreateOrder = new CreateSaleOrderRequest();
                oCreateOrder.SaleOrder = oSale;
                // string strXML = ToXML(oSale);

                string strXML = ToXML(oSale);
                //HttpContext.Current.Response.Write(strXML);

                ServiceResponse oResponse = uc.CreateSaleOrder(oCreateOrder);

                if (oResponse.Successful)
                    IsSuccess = true;


                if (oResponse.Errors != null)
                    Errors = Convert.ToString(oResponse.Errors[0].description);


                UpdateLog("", sOrderNo, IsSuccess, Errors, "[UpdateOrdersLog]", OrderOperationType.CreateOrder.ToString());


            }
            catch (Exception ex)
            {
                Errors = Convert.ToString(ex.Message);
                UpdateLog("", sOrderNo, IsSuccess, Errors, "[UpdateOrdersLog]", OrderOperationType.CreateOrder.ToString());
            }
            return IsSuccess;
        }

        public bool EditOrderAddress(string sOrderNo)
        {

            bool isSuccess = false;

            try
            {

                NetworkCredential nc = new NetworkCredential(UniCommerceNetworkUName, UniCommerceNetworkPass);
                UnicommerceClient uc = new UnicommerceClient();
                uc.ClientCredentials.UserName.UserName = UniCommerceUSerName;
                uc.ClientCredentials.UserName.Password = UniCommerceAPiKey;


                BBmDataInfo bbmData = new BBmDataInfo();
                DataSet dsOrder = bbmData.OrderDetail(sOrderNo);
                Address AddBillDetail = new Address();
                AddressRef billref = new AddressRef();
                AddressRef shipRef = new AddressRef();


                EditSaleOrderAddressRequest editAddressrequest = new EditSaleOrderAddressRequest();
                SaleOrderAddress address = new SaleOrderAddress();
                bool IsCashOnDelivery = Convert.ToBoolean(dsOrder.Tables[0].Rows[0]["IsCod"]);
                //decimal dCouponDiscount = Convert.ToDecimal(dsOrder.Tables[0].Rows[0]["DiscountCouponValue"]);
                // string sCouponName = Convert.ToString(dsOrder.Tables[0].Rows[0]["DiscountCouponName"]);



                if (IsCashOnDelivery)
                {
                    AddBillDetail.id = Convert.ToString(dsOrder.Tables[0].Rows[0]["AddressId"]);
                    AddBillDetail.AddressLine1 = Convert.ToString(dsOrder.Tables[0].Rows[0]["BillingAddress"]);
                    AddBillDetail.AddressLine2 = "";
                    AddBillDetail.City = Convert.ToString(dsOrder.Tables[0].Rows[0]["BillingCity"]);
                    AddBillDetail.Name = Convert.ToString(dsOrder.Tables[0].Rows[0]["BillingName"]);
                    AddBillDetail.Phone = Convert.ToString(dsOrder.Tables[0].Rows[0]["MobileNo"]);
                    AddBillDetail.State = Convert.ToString(dsOrder.Tables[0].Rows[0]["BillingState"]); //"MH"; // 
                    AddBillDetail.Pincode = Convert.ToString(dsOrder.Tables[0].Rows[0]["BillingPostCode"]);

                    Address[] adrList = new Address[2];
                    billref.@ref = Convert.ToString(dsOrder.Tables[0].Rows[0]["AddressId"]);
                    // oSale.BillingAddress = billref;


                    Address AddShippDetail = new Address();
                    adrList = new Address[2];
                    AddShippDetail.id = AddBillDetail.id;
                    AddShippDetail.AddressLine1 = AddBillDetail.AddressLine1;
                    AddShippDetail.AddressLine2 = AddBillDetail.AddressLine2;
                    AddShippDetail.City = AddBillDetail.City;
                    AddShippDetail.Name = AddBillDetail.Name;
                    AddShippDetail.Phone = AddBillDetail.Phone;
                    AddShippDetail.State = AddBillDetail.State;// "MH";//Convert.ToString(dsOrder.Tables[0].Rows[0]["ShippingState"]);
                    AddShippDetail.Pincode = AddBillDetail.Pincode;

                    adrList[0] = AddBillDetail;
                    adrList[1] = AddShippDetail;
                    //oSale.Addresses = adrList;



                    shipRef.@ref = Convert.ToString(dsOrder.Tables[0].Rows[0]["ShippingAddressId"]);

                    address.SaleOrderCode = sOrderNo;
                    address.ShippingAddress = billref;
                    address.BillingAddress = billref;
                    address.Addresses = adrList;
                    editAddressrequest.SaleOrderAddress = address;
                    //oSale.BillingAddress = billref;   // Billing and Shipping are same for COD
                    //oSale.ShippingAddress = billref;
                }
                else
                {
                    AddBillDetail.id = Convert.ToString(dsOrder.Tables[0].Rows[0]["AddressId"]);
                    AddBillDetail.AddressLine1 = Convert.ToString(dsOrder.Tables[0].Rows[0]["BillingAddress"]);
                    AddBillDetail.AddressLine2 = "";
                    AddBillDetail.City = Convert.ToString(dsOrder.Tables[0].Rows[0]["BillingCity"]);
                    AddBillDetail.Name = Convert.ToString(dsOrder.Tables[0].Rows[0]["BillingName"]);
                    AddBillDetail.Phone = Convert.ToString(dsOrder.Tables[0].Rows[0]["MobileNo"]);
                    AddBillDetail.State = Convert.ToString(dsOrder.Tables[0].Rows[0]["BillingState"]);
                    AddBillDetail.Pincode = Convert.ToString(dsOrder.Tables[0].Rows[0]["BillingPostCode"]);

                    Address[] adrList = new Address[2];
                    billref.@ref = Convert.ToString(dsOrder.Tables[0].Rows[0]["AddressId"]);
                    // oSale.BillingAddress = billref;


                    Address AddShippDetail = new Address();
                    adrList = new Address[2];
                    AddShippDetail.id = Convert.ToString(dsOrder.Tables[0].Rows[0]["ShippingAddressId"]);
                    AddShippDetail.AddressLine1 = Convert.ToString(dsOrder.Tables[0].Rows[0]["ShippingAddress"]);
                    AddShippDetail.AddressLine2 = "";
                    AddShippDetail.City = Convert.ToString(dsOrder.Tables[0].Rows[0]["ShippingCity"]);
                    AddShippDetail.Name = Convert.ToString(dsOrder.Tables[0].Rows[0]["ShippingName"]);
                    AddShippDetail.Phone = Convert.ToString(dsOrder.Tables[0].Rows[0]["ShipMobile"]);
                    AddShippDetail.State = Convert.ToString(dsOrder.Tables[0].Rows[0]["ShippingState"]);
                    AddShippDetail.Pincode = Convert.ToString(dsOrder.Tables[0].Rows[0]["ShippingPostCode"]);

                    adrList[0] = AddBillDetail;
                    adrList[1] = AddShippDetail;
                    //oSale.Addresses = adrList;



                    shipRef.@ref = Convert.ToString(dsOrder.Tables[0].Rows[0]["ShippingAddressId"]);

                    address.SaleOrderCode = sOrderNo;
                    address.ShippingAddress = shipRef;
                    address.BillingAddress = billref;
                    address.Addresses = adrList;
                    editAddressrequest.SaleOrderAddress = address;
                }
                ServiceResponse oResponse = uc.EditSaleOrderAddress(editAddressrequest);

                if (oResponse.Successful)
                    isSuccess = true;


                if (oResponse.Errors != null)
                    errorDetail = Convert.ToString(oResponse.Errors[0].description);


                UpdateLog("", sOrderNo, isSuccess, errorDetail, "[UpdateOrdersLog]", OrderOperationType.EditAddress.ToString());
            }
            catch (Exception Ex)
            {
                Errors = Convert.ToString(Ex.Message);
                UpdateLog("", sOrderNo, isSuccess, Errors, "[UpdateOrdersLog]", OrderOperationType.EditAddress.ToString());
            }
            return isSuccess;
        }

        public bool CancelOrder(string sOrderNo)
        {

            bool isSuccess = false;
            try
            {
                NetworkCredential nc = new NetworkCredential(UniCommerceNetworkUName, UniCommerceNetworkPass);
                UnicommerceClient uc = new UnicommerceClient();
                uc.ClientCredentials.UserName.UserName = UniCommerceUSerName;
                uc.ClientCredentials.UserName.Password = UniCommerceAPiKey;



                BBmDataInfo bbmData = new BBmDataInfo();
                DataSet dsOrder = bbmData.CancelOrderDetail(sOrderNo);

                SaleOrder saleorder = new SaleOrder();
                saleorder.Code = sOrderNo;


                if (dsOrder.Tables[1].Rows.Count > 0)
                {
                    int iTotalItem = Convert.ToInt32(dsOrder.Tables[1].Rows.Count);
                    CancelSaleOrderRequestSaleOrderSaleOrderItem[] orderItem = new CancelSaleOrderRequestSaleOrderSaleOrderItem[iTotalItem];
                    CancelSaleOrderRequest cancelorder = new CancelSaleOrderRequest();
                    CancelSaleOrderRequestSaleOrder cancelorderRequest = new CancelSaleOrderRequestSaleOrder();
                    /* SaleOrderItem[] oSaleOrderItem = new SaleOrderItem[iTotalItem];
                     for (int i = 0; i <= iTotalItem - 1; i++)
                     {
                         SaleOrderItem SaleOrderItems = new SaleOrderItem();
                         SaleOrderItems.ItemSKU = Convert.ToString(dsOrder.Tables[1].Rows[i]["ItemCode"]);
                         SaleOrderItems.CashOnDeliveryCharges = 0;
                         SaleOrderItems.Code = Convert.ToString(dsOrder.Tables[1].Rows[i]["OrderItemId"]);
                         SaleOrderItems.GiftMessage = Convert.ToString(dsOrder.Tables[1].Rows[i]["GiftMessage"]);
                         SaleOrderItems.GiftWrap = Convert.ToBoolean(dsOrder.Tables[1].Rows[i]["IsGiftWrap"]);
                         SaleOrderItems.GiftWrapCharges = Convert.ToDecimal(dsOrder.Tables[1].Rows[i]["GiftAmount"]);
                         SaleOrderItems.PrepaidAmount = 0;
                         SaleOrderItems.SellingPrice = Convert.ToDecimal(dsOrder.Tables[1].Rows[i]["RetailPrice"]);
                         SaleOrderItems.TotalPrice = Convert.ToDecimal(dsOrder.Tables[1].Rows[i]["Amount"]);
                         SaleOrderItems.ShippingMethodCode = "STD";
                         oSaleOrderItem[i] = SaleOrderItems;
                         SaleOrderItems = null;
                     }
                     */
                    cancelorderRequest.CancellationReason = Convert.ToString(dsOrder.Tables[0].Rows[0]["CancelReason"]);
                    cancelorderRequest.Code = sOrderNo;
                    //cancelorderRequest.SaleOrderItems = orderItem;
                    cancelorder.SaleOrder = cancelorderRequest;
                    ServiceResponse oResponse = uc.CancelSaleOrder(cancelorder);

                    if (oResponse.Successful)
                        isSuccess = true;


                    if (oResponse.Errors != null)
                        Errors = Convert.ToString(oResponse.Errors[0].description);


                    UpdateLog("", sOrderNo, isSuccess, Errors, "[UpdateOrdersLog]", OrderOperationType.CancelOrder.ToString());
                }
            }
            catch (Exception ex)
            {
                Errors = Convert.ToString(ex.Message);
                UpdateLog("", sOrderNo, isSuccess, Errors, "[UpdateOrdersLog]", OrderOperationType.CancelOrder.ToString());
            }

            return isSuccess;
        }



        public bool RemoveItemFromOrder(string sOrderNo, string sOrderItemId)
        {

            bool isSuccess = false;
            try
            {
                NetworkCredential nc = new NetworkCredential(UniCommerceNetworkUName, UniCommerceNetworkPass);
                UnicommerceClient uc = new UnicommerceClient();
                uc.ClientCredentials.UserName.UserName = UniCommerceUSerName;
                uc.ClientCredentials.UserName.Password = UniCommerceAPiKey;



                BBmDataInfo bbmData = new BBmDataInfo();
                DataSet dsOrder = bbmData.CancelOrderItem(sOrderNo, sOrderItemId);

                SaleOrder saleorder = new SaleOrder();
                saleorder.Code = sOrderNo;


                if (dsOrder.Tables[1].Rows.Count > 0)
                {
                    int iTotalItem = Convert.ToInt32(dsOrder.Tables[1].Rows.Count);
                    CancelSaleOrderRequestSaleOrderSaleOrderItem[] oCancelOrderItem = new CancelSaleOrderRequestSaleOrderSaleOrderItem[iTotalItem];
                    CancelSaleOrderRequest cancelorder = new CancelSaleOrderRequest();
                    CancelSaleOrderRequestSaleOrder cancelorderRequest = new CancelSaleOrderRequestSaleOrder();
                    CancelSaleOrderRequestSaleOrderSaleOrderItem cancelOrderItem = new CancelSaleOrderRequestSaleOrderSaleOrderItem();


                    cancelOrderItem.Code = Convert.ToString(dsOrder.Tables[1].Rows[0]["OrderItemId"]);
                    oCancelOrderItem[0] = cancelOrderItem;


                    cancelorderRequest.CancellationReason = Convert.ToString(dsOrder.Tables[0].Rows[0]["CancelReason"]);
                    cancelorderRequest.Code = sOrderNo;
                    cancelorderRequest.SaleOrderItems = oCancelOrderItem;
                    cancelorder.SaleOrder = cancelorderRequest;
                    ServiceResponse oResponse = uc.CancelSaleOrder(cancelorder);

                    if (oResponse.Successful)
                        isSuccess = true;


                    if (oResponse.Errors != null)
                        Errors = Convert.ToString(oResponse.Errors[0].description);


                    UpdateLog("", sOrderNo, isSuccess, Errors, "[UpdateOrdersLog]", OrderOperationType.CancelOrderItem.ToString());
                }
            }
            catch (Exception ex)
            {
                Errors = Convert.ToString(ex.Message);
                UpdateLog("", sOrderNo, isSuccess, Errors, "[UpdateOrdersLog]", OrderOperationType.CancelOrderItem.ToString());
            }


            return isSuccess;
        }





        #endregion



    }
}