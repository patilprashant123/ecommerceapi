using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.IO;
using System.Text;
/// <summary>
/// Summary description for WebSiteConfig
/// </summary>
public static class WebSiteConfig
{
    static string sCurrency;
    static string sDate;

    static WebSiteConfig()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #region Properties
    public static int ProductCatImgHeight
    {
        get { return 299; }
    }
    public static int ProductCatImgWidth
    {
        get { return 221; }
    }


    public static int ProdCatMainImgHeight
    {
        get { return 226; }
    }
    public static int ProdCatMainIImgWidth
    {
        get { return 753; }
    }



    public static int GenCatMainImgHeight
    {
        get { return 415; }
    }
    public static int GenCatMainIImgWidth
    {
        get { return 753; }
    }
    public static int GenCatImgHeight
    {
        get { return 118; }
    }
    public static int GenCatImgWidth
    {
        get { return 250; }
    }


    public static int ProdCatImgHeight
    {
        //get { return 158; }
        get { return 190; }
    }
    public static int ProdCatImgWidth
    {
        // get { return 170; }
        get { return 130; }
    }



    public static int ProdDesignImgHeight
    {
        // get { return 380; }
        get { return 388; }
    }
    public static int ColorDesignImgWidth
    {
        get { return 50; }
    }

    public static int ColorDesignImgHeight
    {
        get { return 50; }
    }
    public static int ProdDesignImgWidth
    {
        //get { return 260; }
        get { return 318; }
    }

    public static int ColorImgHeight
    {
        get { return 25; }
    }
    public static int ColorImgWidth
    {
        get { return 31; }
    }
    public static int ProdThumbHeight
    {
        get { return 30; }
    }
    public static int ProdThumbWidth
    {
        get { return 30; }
    }



    #region Map Category  Path
    public static string DisplayImagePathMapCategory
    {
        get { return ConfigurationManager.AppSettings["ImagePathmapCategory"].ToString(); }
    }
    public static string ImageMapCategoryPath
    {
        get { return HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["ImagePathmapCategory"].ToString()); }
    }
    #endregion

    #region Standard Design  Thumb Path

    public static string DisplayDesignThumbPath
    {
        get { return ConfigurationManager.AppSettings["ImagePath"].ToString(); }
    }
    public static string UploadDesignThumbPath
    {
        get { return HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["ImagePath"].ToString()); }
    }


   
    public static string UploadDesignMediumpath
    {
        get { return HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["ImagePathRoot"].ToString()); }
    }

    public static string UploadMobileDesign
    {
        get { return HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["ImageMobile"].ToString()); }
    }


    public static string UploadOldMobileDesign
    {
        get { return HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["ImageMobileOldPath"].ToString()); }
    }

    public static string UploadBulkMobileDesign
    {
        get { return HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["ImageOldPath"].ToString()); }
    }


    public static string DesignXmlFilePath
    {
        get { return HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["StandardDesignXmlFile"].ToString()); }
    }


    #endregion




    //  Formatted Currency For Transactions
    public static string CurrencyFormat
    {
        set { sCurrency = String.Format("{0:c}", value); }
        get { return sCurrency; }
    }


    #endregion

    public static string ProductXmlFilePath
    {
        get { return HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["ProductXmlFile"].ToString()); }
    }


    public static string HostUrl
    {
        get { return ConfigurationManager.AppSettings["HostServerUrl"].ToString(); }
    }
    public static string SecureHostUrl
    {
        get { return ConfigurationManager.AppSettings["SecureServerUrl"].ToString(); }
    }
    public static string ContactNo
    {
        get { return System.Configuration.ConfigurationManager.AppSettings["ContactNo"]; }
    }
    public static string CropImagePath
    {
        get
        {
            return HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["ImageDirectory"].ToString() + "/CropImages/");
        }
    }


    #region Gati Api Setting

    public static string PackageTrakingUrl
    {
        get { return "http://www.gati.com/webservices/gatiicedkttrack.jsp?dktno="; }
    }

    #endregion

    #region Paint 

    public static string CustomPaintSwatchBookSku
    {
        get { return System.Configuration.ConfigurationManager.AppSettings["CustomPaintSwatchBookSku"]; }
    }

    public static string CustomPaintSwatchSku
    {
        get { return System.Configuration.ConfigurationManager.AppSettings["CustomPaintSwatchSku"]; }
    }

    public static string PaintSheet
    {
        get { return "http://beta.paletly.com:3001/api/paints/paintsheet"; }
    }

    public static string ShadeCard
    {
        get { return "http://beta.paletly.com:3001/api/paints/shadecards"; }
    }

    #endregion
}
