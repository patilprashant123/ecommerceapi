using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for CustomDataTypes
/// </summary>

public static class CustomDataTypes
{   

    public enum EmailType
    {
        Customer_Confirmation = 1,
        Exchange_Credit = 21,
        Exchange_StoredCard = 3,
        Forgot_Password_Temporary = 2,
        Order_Confirmation = 5,
        Return_Recieve = 61,

        Return_Complete_Store = 8,
        Shipping_Confirmation = 9,
        UnSubscribe = 10,
        Welcome_Email = 1,
        EmailToFriend = 3,
        Referals = 13,
        Invite = 14,
        ShareWishList = 15,
        Test = 16,
        AbadonCart_EMail_4Hours = 4,
        AbadonCart_EMail_3days = 5,
        AbadonCart_EMail_13Days = 6,


        Referral_EMail = 7,
        Referral_CouponCreatedMail = 8,
        Referral_FirstOrderPlace = 9,

        AbadonCart_EMail_4Hours_GuestUser = 10,
        AbadonCart_EMail_3days_GuestUser = 11,
        AbadonCart_EMail_13Days_GuestUser = 12,


        Unicom_OrderFailed = 13
    };
    public enum OrderType
    {
        OrderType_Cod = 1,
        OrderType_Web = 2
    };
    public enum ItemCategory { MenBrief = 30, MenVest = 31, KidsBrief = 32, KidsVest = 33 };
    public enum PageType { Update_Order = 1, NewProducts = 2, UpdateOrderV1 = 3 }
    public enum OrderEmailType { Web = 1, Cod = 2, CodConfirm = 3 };
    public enum AdminUserAction { Insert = 701, Update = 702, Delete = 703, Publish = 704, UnPublish = 705, BulkStock = 706, BulkPrice = 707 };
    public enum ValidationMessage { Success = 800, NonDeliveryArea = 801, InvalidPin = 802, CodNotAvailable = 804, CodNotAvailableFurniture = 805, InvalidQuantity = 806 }
    public enum CourierServiceProvider {Delhivery = 53};
    public enum CheckOut  {Register = 1};
    public enum CloudinaryImgStoreType { Store = 1, Store2 = 2 };
    public enum CloudinaryStatusCode { OK= 1 };
    public enum CustomProductType{ Fabric=1, Swatch=2};
    public enum PaintType { Shadecard = 1, Paintsheet = 2 };

}