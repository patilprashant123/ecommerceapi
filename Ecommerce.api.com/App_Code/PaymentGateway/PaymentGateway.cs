﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Web.Mail;
//using IntegrationKit;
using Mandrill;
using Uniware;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using System.Net;
using ExotelSDK;
using System.Collections.Generic;
using Ecommerce.Entities;
using paytm;

public class PaymentGatewayBase : System.Web.UI.Page
{
    ShoppingCart oCart = new ShoppingCart();
    DataSet ds;
    string strBillAmount = string.Empty;
    public string strOrderNo = string.Empty;
    string strOrderDate = string.Empty;
    decimal ItemSubTotal = 0;
    decimal OfferDiscount = 0;
    decimal CouponDiscount = 0;

    string sEmaimSubject = string.Empty;
    string sStandardCode;
    int iDIscountType = 0;
    decimal dDiscount = 0;
    decimal GiftSubtotal = 0;
    int ismsg = 0;
    bool bIsCodOrder = false;
    string sCodThankyouText = "";

    // Marin Pixel Tracking
    string sTotalQty = string.Empty;
    string sProductName = string.Empty;
    string sCategoryName = string.Empty;

    string sCodZipDialThankUMsg = " Your order has been placed successfully via Cash on Delivery. You will <b>receive a CALL</b> shortly to confirm your order details, post which we will process and ship your order.<br />" +
                                          " In case we are unable to reach you or you don&#39;t want to wait for the CALL, -we request you to call our Customer Delight Center on <font style=\"font-size: 14px; font-weight: bold;\">+91-22-2491 0066</font> between 10 AM to 7 PM (Monday to Saturday) to confirm your order.";
    string sCodThankUMsg = "Your order has been confirmed successfully via Cash on Delivery. We will process and ship your order as promised..";


    StringBuilder sbMsg = new StringBuilder();

    SqlConnection con;

    #region SqlConnection
    private void OpenSqlConnection()
    {
        con = new SqlConnection(ConfigurationManager.ConnectionStrings["bbmSqlDb"].ToString());
        con.Open();
    }

    private void CloseSqlConnection()
    {
        con.Close();
    }

    #endregion



    DataSet DsItems = null;
    public StringBuilder StrJsScriptAc1 = new StringBuilder();
    string strBillAmountForTracking = "0";// Use for all order either deciliened or cancel or awaiting for paymnet...



    public string CodThankyouText
    {
        get { return sCodThankyouText; }
        set { sCodThankyouText = value; }
    }

    public string EmailSubject
    {
        set
        {
            sEmaimSubject = value;
        }
        get
        {
            return sEmaimSubject;
        }
    }
    public string HostServerUrl
    {
        get
        {
            return Convert.ToString(ConfigurationManager.AppSettings["HostServerUrl"]);
        }
    }
    public string IllegalAccessMsg
    {
        get
        {
            return "<br/>Illegal Access <a href='" + HostServerUrl + "'><b><i>Click here</i></b> </a> to continue Shopping";
        }
    }

    public string JsScriptAC1
    {
        get { return StrJsScriptAc1.ToString(); }
    }
    public string BillAmountForTracking
    {
        set { strBillAmountForTracking = value; }
        get { return strBillAmountForTracking; }
    }

    public string BillAmount
    {
        set { strBillAmount = value; }
        get { return strBillAmount; }
    }
    public string OrderNO
    {
        set { strOrderNo = value; }
        get { return strOrderNo; }
    }
    public string OrderDate
    {
        set { strOrderDate = value; }
        get { return strOrderDate; }
    }
    // marin Pixcel Code
    public string TotalQty
    {
        set { sTotalQty = value; }
        get { return sTotalQty; }
    }
    public string ProductName
    {
        set { sProductName = value; }
        get { return sProductName; }
    }
    public string CategoryName
    {
        set { sCategoryName = value; }
        get { return sCategoryName; }
    }

    private string CodUserType
    {
        get
        {
            if (!string.IsNullOrEmpty(HttpContext.Current.Request["token"]) && HttpContext.Current.Request["token"] != "")
                return Convert.ToString(HttpContext.Current.Request["token"]);
            else
                return string.Empty;

        }
    }

    #region COD Param

    private string ExotelSID
    {
        get { return Convert.ToString(ConfigurationManager.AppSettings["ExotelSID"]); }
    }
    private string ExotelToken
    {
        get { return Convert.ToString(ConfigurationManager.AppSettings["ExotelToken"]); }
    }
    private string ExotelCodAppUrl
    {
        get { return Convert.ToString(ConfigurationManager.AppSettings["ExotelCodAppUrl"]); }
    }
    private string ExotelVirtualNo
    {
        get { return Convert.ToString(ConfigurationManager.AppSettings["ExotelVirtualNo"]); }
    }
    private string ExotelCallType
    {
        get { return Convert.ToString(ConfigurationManager.AppSettings["ExotelCallType"]); }
    }

    public string SellerBccEmail
    {
        get
        {
            return Convert.ToString(ConfigurationManager.AppSettings["SellerBccEmail"]);
        }
    }

    public bool IsTestPayment
    {
        get
        {
            return Convert.ToBoolean(ConfigurationManager.AppSettings["IsTest"]);
        }
    }

    public bool IsExotel
    {
        get
        {
            return Convert.ToBoolean(ConfigurationManager.AppSettings["IsExotel"]);
        }
    }

    public bool IsSellerNotification
    {
        get
        {
            return Convert.ToBoolean(ConfigurationManager.AppSettings["IsSellerNotification"]);
        }
    }

    public class ZipDialCodeRequest
    {
        public string customerToken { get; set; }
        public string orderRefId { get; set; }
        public string callerPhone { get; set; }
        public string duration { get; set; }
        public string countryCode { get; set; }
        public string amount { get; set; }
        public string pincode { get; set; }

    }

    [Serializable]
    public class ZipDialCodeReponse
    {

        public string message { get; set; }
        public string order_ref_id { get; set; }
        public string transaction_token { get; set; }
        public string Status { get; set; }
    }
    #endregion

    #region totrack
    public string _ecsk = string.Empty;
    public string _ecqu = string.Empty;
    public string _ecpr = string.Empty;
    public string _ecld = string.Empty;
    public string _ecv = string.Empty;
    public string _ecc = string.Empty;
    public string _eccu = string.Empty;
    public string _ecd = string.Empty;
    public string _ecst = string.Empty;
    public string _ectx = string.Empty;
    public string _ecsh = string.Empty;
    public string _ecco = string.Empty;
    public string _ecrg = string.Empty;
    public string _ect = string.Empty;
    public string _p = string.Empty;
    public string ecsk
    {
        set { _ecsk = value; }
        get { return _ecsk; }
    }
    public string ecqu
    {
        set { _ecqu = value; }
        get { return _ecqu; }
    }
    public string ecpr
    {
        set { _ecpr = value; }
        get { return _ecpr; }
    }
    public string ecld
    {
        set { _ecld = value; }
        get { return _ecld; }
    }
    public string ecv
    {
        set { _ecv = value; }
        get { return _ecv; }
    }
    public string ecc
    {
        set { _ecc = value; }
        get { return _ecc; }
    }
    public string eccu
    {
        set { _eccu = value; }
        get { return _eccu; }
    }
    public string ecd
    {
        set { _ecd = value; }
        get { return _ecd; }
    }
    public string ecst
    {
        set { _ecst = value; }
        get { return _ecst; }
    }
    public string ectx
    {
        set { _ectx = value; }
        get { return _ectx; }
    }
    public string ecsh
    {
        set { _ecsh = value; }
        get { return _ecsh; }
    }
    public string ecco
    {
        set { _ecco = value; }
        get { return _ecco; }
    }
    public string ecrg
    {
        set { _ecrg = value; }
        get { return _ecrg; }
    }
    public string ect
    {
        set { _ect = value; }
        get { return _ect; }
    }
    public string p
    {
        set { _p = value; }
        get { return _p; }
    }
    #endregion

    #region PayU Properties


    public string _mihpayid = string.Empty;
    public string _PMerchantId = string.Empty;
    public string _POrderNo = string.Empty;
    public string _POrderAmount = string.Empty;
    public string _ProductInfo = string.Empty;
    public string _PCustomerName = string.Empty;
    public string _PCustomerEmail = string.Empty;
    public string _PStatus = string.Empty;
    public string _PCheckSum = string.Empty;

    public bool IsPayU
    {
        get
        {
            if (Convert.ToString(ConfigurationManager.AppSettings["PaymentGatewayUrl"]).Contains("PayU"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }


    public string PSalt
    {
        get
        {
            if (IsTestPayment)
            {
                return "eCwWELxi";
            }
            else
            {
                return "0WH8JnMZ";
            }
        }
    }

    public string PMerchantId
    {
        get { return _PMerchantId; }
        set { _PMerchantId = value; }
    }

    public string POrderNo
    {
        get { return _POrderNo; }
        set { _POrderNo = value; }
    }

    public string POrderAmount
    {
        get { return _POrderAmount; }
        set { _POrderAmount = value; }
    }

    public string ProductInfo
    {
        get { return _ProductInfo; }
        set { _ProductInfo = value; }
    }

    public string PCustomerName
    {
        get { return _PCustomerName; }
        set { _PCustomerName = value; }
    }

    public string PCustomerEmail
    {
        get { return _PCustomerEmail; }
        set { _PCustomerEmail = value; }
    }
    public string mihpayid
    {
        get { return _mihpayid; }
        set { _mihpayid = value; }
    }
    public string PStatus
    {
        get { return _PStatus; }
        set { _PStatus = value; }
    }

    public string PCheckSum
    {
        get { return _PCheckSum; }
        set { _PCheckSum = value; }
    }


    #endregion


    #region Paytm

    #region PayTMProperties
    public string MerchantKey
    {
        get
        {
            return Convert.ToString(ConfigurationManager.AppSettings["Merchant_KEY"]);
        }
    }
    public string PayTMTxnStatus
    {
        get
        {
            if (!string.IsNullOrEmpty(Request["STATUS"]) && Request["STATUS"] != "")
                return Convert.ToString(Request["STATUS"]);
            else
                return string.Empty;
        }
    }
    #endregion


    public string _TXNID;
    public string TXNID
    {
        set { _TXNID = value; }
        get { return _TXNID; }
    }

    public string _ORDERID;
    public string ORDERID
    {
        set { _ORDERID = value; }
        get { return _ORDERID; }
    }

    public string _BANKTXNID;
    public string BANKTXNID
    {
        set { _BANKTXNID = value; }
        get { return _BANKTXNID; }
    }

    public string _TXNAMOUNT;
    public string TXNAMOUNT
    {
        set { _TXNAMOUNT = value; }
        get { return _TXNAMOUNT; }
    }

    public string _CURRENCY;
    public string CURRENCY
    {
        set { _CURRENCY = value; }
        get { return _CURRENCY; }
    }

    public string _STATUS;
    public string STATUS
    {
        set { _STATUS = value; }
        get { return _STATUS; }
    }

    public string _RESPCODE;
    public string RESPCODE
    {
        set { _RESPCODE = value; }
        get { return _RESPCODE; }
    }

    public string _RESPMSG;
    public string RESPMSG
    {
        set { _RESPMSG = value; }
        get { return _RESPMSG; }
    }

    public string _TXNDATE;
    public string TXNDATE
    {
        set { _TXNDATE = value; }
        get { return _TXNDATE; }
    }


    public string _GATEWAYNAME;
    public string GATEWAYNAME
    {
        set { _GATEWAYNAME = value; }
        get { return _GATEWAYNAME; }
    }


    public string _BANKNAME;
    public string BANKNAME
    {
        set { _BANKNAME = value; }
        get { return _BANKNAME; }
    }

    public string _PAYMENTMODE;
    public string PAYMENTMODE
    {
        set { _PAYMENTMODE = value; }
        get { return _PAYMENTMODE; }
    }
    #endregion


    clsPayment objCls = new clsPayment();

    public void PayUPaymentLog()
    {
        if (!string.IsNullOrEmpty(HttpContext.Current.Request["txnid"]) && HttpContext.Current.Request["txnid"] != "")
        {
            if (!string.IsNullOrEmpty(HttpContext.Current.Request.Form["txnid"].ToString()))
                this.POrderNo = Convert.ToString(HttpContext.Current.Request.Form["txnid"]);

            OrderNO = POrderNo;

            mihpayid = HttpContext.Current.Request["mihpayid"];
            PStatus = HttpContext.Current.Request["status"];
            PCustomerName = HttpContext.Current.Request["firstname"];
            ProductInfo = HttpContext.Current.Request["productinfo"];
            POrderAmount = HttpContext.Current.Request["amount"];
            POrderNo = HttpContext.Current.Request["txnid"];
            PCustomerEmail = HttpContext.Current.Request["email"];
            PMerchantId = HttpContext.Current.Request["key"];
            PStatus = HttpContext.Current.Request["Status"];
            PCheckSum = HttpContext.Current.Request["hash"];
            objCls.mihpayid = Convert.ToString(HttpContext.Current.Request["mihpayid"]);
            objCls.mode = Convert.ToString(HttpContext.Current.Request["mode"]);
            objCls.status = Convert.ToString(HttpContext.Current.Request["status"]);
            objCls.key = Convert.ToString(HttpContext.Current.Request["key"]);
            objCls.txnid = Convert.ToString(HttpContext.Current.Request["txnid"]);
            objCls.Pamount = Convert.ToString(HttpContext.Current.Request["amount"]);
            objCls.Pdiscount = Convert.ToString(HttpContext.Current.Request["discount"]);
            objCls.Offer = Convert.ToString(HttpContext.Current.Request["Offer"]);
            objCls.productinfo = Convert.ToString(HttpContext.Current.Request["productinfo"]);
            objCls.firstname = Convert.ToString(HttpContext.Current.Request["firstname"]);
            objCls.lastname = Convert.ToString(HttpContext.Current.Request["lastname"]);
            objCls.address1 = Convert.ToString(HttpContext.Current.Request["address1"]);
            objCls.address2 = Convert.ToString(HttpContext.Current.Request["address2"]);
            objCls.city = Convert.ToString(HttpContext.Current.Request["city"]);
            objCls.state = Convert.ToString(HttpContext.Current.Request["state"]);
            objCls.country = Convert.ToString(HttpContext.Current.Request["country"]);
            objCls.zipcode = Convert.ToString(HttpContext.Current.Request["zipcode"]);
            objCls.email = Convert.ToString(HttpContext.Current.Request["email"]);
            objCls.phone = Convert.ToString(HttpContext.Current.Request["phone"]);
            objCls.udf1 = Convert.ToString(HttpContext.Current.Request["udf1"]);
            objCls.udf2 = Convert.ToString(HttpContext.Current.Request["udf2"]);
            objCls.udf3 = Convert.ToString(HttpContext.Current.Request["udf3"]);
            objCls.udf4 = Convert.ToString(HttpContext.Current.Request["udf4"]);
            objCls.udf5 = Convert.ToString(HttpContext.Current.Request["udf5"]);
            objCls.hash = Convert.ToString(HttpContext.Current.Request["hash"]);
            objCls.Error = Convert.ToString(HttpContext.Current.Request["Error"]);
            objCls.PG_TYPE = Convert.ToString(HttpContext.Current.Request["PG_TYPE"]);
            objCls.bank_ref_num = Convert.ToString(HttpContext.Current.Request["bank_ref_num"]);
            objCls.shipping_firstname = Convert.ToString(HttpContext.Current.Request["shipping_firstname"]);
            objCls.shipping_lastname = Convert.ToString(HttpContext.Current.Request["shipping_lastname"]);
            objCls.shipping_address1 = Convert.ToString(HttpContext.Current.Request["shipping_address1"]);
            objCls.shipping_address2 = Convert.ToString(HttpContext.Current.Request["shipping_address2"]);
            objCls.shipping_city = Convert.ToString(HttpContext.Current.Request["shipping_city"]);
            objCls.shipping_state = Convert.ToString(HttpContext.Current.Request["shipping_state"]);
            objCls.shipping_country = Convert.ToString(HttpContext.Current.Request["shipping_country"]);
            objCls.shipping_zipcode = Convert.ToString(HttpContext.Current.Request["shipping_zipcode"]);
            objCls.shipping_phone = Convert.ToString(HttpContext.Current.Request["shipping_phone"]);
            objCls.shipping_phoneverified = Convert.ToString(HttpContext.Current.Request["shipping_phoneverified"]);
            objCls.unmappedstatus = Convert.ToString(HttpContext.Current.Request["unmappedstatus"]);
            objCls.UserAgent = Convert.ToString(HttpContext.Current.Request.UserAgent);
            objCls.PayUResponse();



        }
    }

    public void PaytmPaymentLog()
    {

        if (!string.IsNullOrEmpty(Request.Form["RESPCODE"]) && Request.Form["RESPCODE"] != "")
        {

            Dictionary<String, String> parameters = new Dictionary<string, string>();

            TXNID = Request.Form["TXNID"];
            ORDERID = Request.Form["ORDERID"];

            if (Request.Form["BANKTXNID"] == null)
            {
                BANKTXNID = "";
            }
            else
            {
                BANKTXNID = Request.Form["BANKTXNID"];
            }

            TXNAMOUNT = Request.Form["TXNAMOUNT"];


            if (Request.Form["CURRENCY"] == null)
            {
                CURRENCY = "";
            }
            else
            {
                CURRENCY = Request.Form["CURRENCY"];
            }


            //CURRENCY = Request.Form["CURRENCY"];
            STATUS = Request.Form["STATUS"];
            RESPCODE = Request.Form["RESPCODE"];
            RESPMSG = Request.Form["RESPMSG"];
            TXNDATE = Convert.ToString(DateTime.Now);  //Request.Form["TXNDATE"];

            if (Request.Form["GATEWAYNAME"] == null)
            {
                GATEWAYNAME = "";
            }
            else
            {
                GATEWAYNAME = Request.Form["GATEWAYNAME"];
            }

            if (Request.Form["BANKNAME"] == null)
            {
                BANKNAME = "";
            }
            else
            {
                BANKNAME = Request.Form["BANKNAME"];
            }

            if (Request.Form["PAYMENTMODE"] == null)
            {
                PAYMENTMODE = "";
            }
            else
            {
                PAYMENTMODE = Request.Form["PAYMENTMODE"];
            }
            //GATEWAYNAME =Request.Form["GATEWAYNAME"];
            //BANKNAME = Request.Form["BANKNAME"];
            //PAYMENTMODE = Request.Form["PAYMENTMODE"];



            objCls.TXNID = TXNID;
            objCls.ORDERID = ORDERID;
            objCls.BANKTXNID = BANKTXNID;
            objCls.TXNAMOUNT = TXNAMOUNT;
            objCls.CURRENCY = CURRENCY;
            objCls.STATUS = STATUS;
            objCls.RESPCODE = RESPCODE;
            objCls.RESPMSG = RESPMSG;
            objCls.TXNDATE = TXNDATE;

            objCls.GATEWAYNAME = GATEWAYNAME;
            objCls.BANKNAME = BANKNAME;
            objCls.PAYMENTMODE = PAYMENTMODE;

            objCls.PayTMResponse();

        }
    }


    public void ClearShoppingCart()
    {
        OpenSqlConnection();
        string sql = "[ClearShoppingCart]";
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = sql;
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Connection = con;
        cmd.Parameters.Add("@SessionId", SqlDbType.VarChar).Value = UserSession.GetWebSessionId();
        cmd.ExecuteNonQuery();
        CloseSqlConnection();
    }

    public bool ValidatePayCheckSum()
    {
        string sOrderResponse = PSalt + "|" + PStatus + "|||||||||||" + PCustomerEmail + "|" + PCustomerName + "|" + ProductInfo + "|" + POrderAmount + "|" + POrderNo + "|" + PMerchantId;

        string sHash = hashalg.CalculateHash(sOrderResponse, Encoding.ASCII, "SHA512");

        if (PCheckSum == sHash.ToLower())
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool Validate_PayTM_CheckSum()
    {
        //string masterkey = "3wSF4mPzaxWlsiu8";// For test Server;
        string masterkey = "bpy9JlyQ3oAdc&7W"; // For test Server;

        Dictionary<String, String> parameters = new Dictionary<string, string>();
        parameters.Add("MID", Request.Form["MID"]);
        parameters.Add("TXNID", Request.Form["TXNID"]);
        parameters.Add("ORDER_ID", Request.Form["ORDERID"]);
        parameters.Add("BANKTXNID", Request.Form["BANKTXNID"]);
        parameters.Add("TXNAMOUNT", Request.Form["TXNAMOUNT"]);
        parameters.Add("CURRENCY", Request.Form["CURRENCY"]);
        parameters.Add("STATUS", Request.Form["STATUS"]);
        parameters.Add("RESPCODE", Request.Form["RESPCODE"]);
        parameters.Add("RESPMSG", Request.Form["RESPMSG"]);
        parameters.Add("TXNDATE", Request.Form["TXNDATE"]);
        parameters.Add("GATEWAYNAME", Request.Form["GATEWAYNAME"]);
        parameters.Add("BANKNAME", Request.Form["BANKNAME"]);
        parameters.Add("PAYMENTMODE", Request.Form["PAYMENTMODE"]);
        string str = Request.Form["CHECKSUMHASH"];


        if (CheckSum.verifyCheckSum(masterkey, parameters, str))
        {
            return true;
        }
        else
        {
            return false;
        }
    }


    #region Exotel
    //** Send request to Exotel for Cod Confirmation**//
    public void SendReqToExotel(string sOrderNo)
    {

        ExotelResponse oReponse = new ExotelResponse();
        DataSet dsResponse = null;
        string sCallSid = string.Empty;
        DataSet CustomerData = oCart.GetCustomerData(sOrderNo);
        if (CustomerData.Tables.Count > 0)
        {
            if (CustomerData.Tables[0].Rows.Count > 0)
            {
                string sCustomerMobileNo = Convert.ToString(CustomerData.Tables[0].Rows[0]["MobileNo"]);
                if (!string.IsNullOrEmpty(sCustomerMobileNo))
                {

                    // Send Request To WhiteListAPI
                    SendSMS sSendSMS = new SendSMS(ExotelSID, ExotelToken);
                    string response = sSendSMS.execute(ExotelVirtualNo, sCustomerMobileNo, "Message to send");


                    // Connect To Exotel API
                    ConnectCall oConnectToExotel = new ConnectCall(ExotelSID, ExotelToken);
                    string ExotelRequest = oConnectToExotel.connectCustomerToApp(sCustomerMobileNo, ExotelCodAppUrl, ExotelVirtualNo, ExotelCallType, sOrderNo);
                    if (ExotelRequest != null)
                    {
                        dsResponse = oReponse.ConvertXMLToDataSet(ExotelRequest);
                        if (dsResponse.Tables.Count > 0)
                        {
                            if (dsResponse.Tables[0].Rows.Count > 0)
                            {
                                sCallSid = Convert.ToString(dsResponse.Tables[0].Rows[0]["sid"]);
                            }
                        }
                    }
                }

                if (!string.IsNullOrEmpty(sCallSid))
                {
                    ExotelResponse oResponse = new ExotelResponse();
                    oResponse.ExotelRequest_Log(sCallSid, sOrderNo);
                }
            }
        }



    }

    #endregion

    private void SendToZipDail(string sOrderNo)
    {
        ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;

        ZipDialCodeRequest obj = new ZipDialCodeRequest();
        obj.customerToken = "158f115034e9a788958200946f965a4b94a0f779";
        obj.orderRefId = Convert.ToString(sOrderNo);
        DataSet CustomerData = oCart.GetCustomerData(obj.orderRefId);
        obj.duration = "180";
        obj.countryCode = "91";
        obj.amount = CustomerData.Tables[0].Rows[0][1].ToString();
        obj.callerPhone = CustomerData.Tables[0].Rows[0][2].ToString();
        obj.pincode = CustomerData.Tables[0].Rows[0][3].ToString();
        // string IP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
        string UserAgent = HttpContext.Current.Request.ServerVariables["HTTP_USER_AGENT"];
        string requrl = "https://www.zipdial.com/z2cod/startTransaction.action?customerToken=" + obj.customerToken + "&orderRefId=" + obj.orderRefId + "&callerPhone=" + obj.callerPhone + "&duration=" + obj.duration + "&countryCode=" + obj.countryCode + "&amount=" + obj.amount + "&pincode=" + obj.pincode;
        HttpWebRequest http = (HttpWebRequest)HttpWebRequest.Create(requrl);

        HttpWebResponse response = (HttpWebResponse)http.GetResponse();
        string StrResponse = string.Empty;
        using (StreamReader sr = new StreamReader(response.GetResponseStream()))
        {
            StrResponse = sr.ReadToEnd();
        }

        JObject jObject = JObject.Parse(StrResponse);
        ZipDialCodeReponse objnew = new ZipDialCodeReponse();


        objnew.message = (string)jObject["message"];
        objnew.order_ref_id = (string)jObject["order_ref_id"];
        objnew.transaction_token = (string)jObject["transaction_token"];
        objnew.Status = (string)jObject["status"];
        string IP = HttpContext.Current.Request.ServerVariables["remote_addr"].ToString();
        // lblMesage.Text += StrResponse+"<br/>"; 
        int ZipDailData = oCart.ZipDialCodeStatus(obj.orderRefId, obj.customerToken, obj.callerPhone, obj.duration, obj.countryCode, obj.amount, obj.pincode, objnew.transaction_token, objnew.Status, IP, UserAgent);
    }


    public string GetEmail(string Order_Id)
    {
        OpenSqlConnection();

        string sql = "Select C.Email from Orders O inner Join Customer C   on O.CustomerId=C.CustomerId  where O.OrderNo=@OrderNo";

        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = sql;
        cmd.Connection = con;
        cmd.Parameters.Add("@OrderNo", SqlDbType.VarChar, 50).Value = Order_Id;
        object obj = cmd.ExecuteScalar();
        CloseSqlConnection();
        if (obj != null)
        {
            return obj.ToString();
        }
        else
            return "";

    }

    public void UpdateOrderPaymentAndStatus(string OrderNo, int OrderStatus, int PaymentStatus)
    {
        ///UpdateForPendingOrders
        OpenSqlConnection();
        //string sql = "Update Orders Set StatusId=@OrderStatusId , PaymentStatus=@PaymentStatusId where OrderNo=@OrderNo";
        string sql = "[UpdateOrderPaymentAndStatus]";
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = sql;
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Connection = con;
        cmd.Parameters.Add("@OrderNo", SqlDbType.VarChar).Value = OrderNo;
        cmd.Parameters.Add("@OrderStatusId", SqlDbType.BigInt).Value = OrderStatus;
        cmd.Parameters.Add("@PaymentStatusId", SqlDbType.Int).Value = PaymentStatus;
        cmd.ExecuteNonQuery();
        CloseSqlConnection();
    }

    public void UpdateOrder(string OrderNo, int OrderStatus, int PaymentStatus)
    {
        // User when Order Declined
        OpenSqlConnection();
        string sql = "UpdateOrderAndRevertStock";
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = sql;
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Connection = con;
        cmd.Parameters.Add("@OrderNo", SqlDbType.VarChar).Value = OrderNo;
        cmd.Parameters.Add("@OrderStatus", SqlDbType.BigInt).Value = OrderStatus;
        cmd.Parameters.Add("@PaymentStatus", SqlDbType.Int).Value = PaymentStatus;
        cmd.ExecuteNonQuery();
        CloseSqlConnection();
    }



    public void ClearMobileShoppingCart(string SessionId)
    {
        OpenSqlConnection();
        string sql = "[ClearShoppingCart]";
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = sql;
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Connection = con;
        cmd.Parameters.Add("@SessionId", SqlDbType.VarChar).Value = SessionId;
        cmd.ExecuteNonQuery();
        CloseSqlConnection();
    }

    public void RazorPayResponse(RazorPayPayment payment)
    {
        try
        {
            OpenSqlConnection();

            SqlCommand cmd = new SqlCommand("RazorPayPaymentResponse", con);

            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@id", SqlDbType.VarChar).Value = payment.id;
            cmd.Parameters.Add("@entity", SqlDbType.VarChar).Value = payment.entity;
            cmd.Parameters.Add("@amount", SqlDbType.Int).Value = payment.amount / 100;
            cmd.Parameters.Add("@currency", SqlDbType.VarChar).Value = payment.currency;
            cmd.Parameters.Add("@status", SqlDbType.VarChar).Value = payment.status;
            cmd.Parameters.Add("@method", SqlDbType.VarChar).Value = payment.method;
            cmd.Parameters.Add("@description", SqlDbType.VarChar).Value = payment.description;
            cmd.Parameters.Add("@refund_status", SqlDbType.VarChar).Value = payment.refund_status;
            cmd.Parameters.Add("@amount_refunded", SqlDbType.Int).Value = payment.amount_refunded;
            cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = payment.email;
            cmd.Parameters.Add("@contact", SqlDbType.VarChar).Value = payment.contact;
            cmd.Parameters.Add("@address", SqlDbType.VarChar).Value = payment.notes.address;
            cmd.Parameters.Add("@merchant_order_id", SqlDbType.VarChar).Value = payment.notes.merchant_order_id;
            cmd.Parameters.Add("@fee", SqlDbType.VarChar).Value = "";
            cmd.Parameters.Add("@service_tax", SqlDbType.VarChar).Value = "";
            cmd.Parameters.Add("@error_code", SqlDbType.VarChar).Value = payment.error_code;
            cmd.Parameters.Add("@error_description", SqlDbType.VarChar).Value = payment.error_description;
            cmd.Parameters.Add("@created_at", SqlDbType.VarChar).Value = payment.created_at;

            cmd.ExecuteNonQuery();

        }
        catch (Exception ex)
        {
            //oErrorLog.Open();
            //oErrorLog.Log(ex.Message);

            //oErrorLog.Flush();
            //oErrorLog.Close();
        }
        finally
        {
            CloseSqlConnection();
        }
    }

    public void RazorPaymentLog(RazorPayPayment objPayment)
    {
        objCls.RazorPayResponse(objPayment);
    }
}