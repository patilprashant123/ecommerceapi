﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using Ecommerce.conn;
using Ecommerce.ErrorLog;
using Ecommerce.Entities;

/// <summary>
/// Summary description for clsPayment
/// </summary>
public class clsPayment: Connection
{
    ErrorLogger oErrorLog = new ErrorLogger();
    string sRemoteAddress = HttpContext.Current.Request.ServerVariables["remote_addr"].ToString();
    #region Property

    public int _SourceId;
    public DateTime _ReceviedhDate;
    public decimal _Amount;
    public string _RefNo;
    public string _Remarks;
    public int _LoginUserId;
    public int _PayRecvId;
    public int _IsActive;
    public string _CourierName;
    public decimal _ActualAmount;

    public int SourceId
    {
        set { _SourceId = value; }
        get { return _SourceId; }
    }

    public DateTime ReceviedhDate
    {
        set { _ReceviedhDate = value; }
        get { return _ReceviedhDate; }
    }

    public decimal Amount
    {
        set { _Amount = value; }
        get { return _Amount; }
    }

    public string RefNo
    {
        set { _RefNo = value; }
        get { return _RefNo; }
    }

    public string Remarks
    {
        set { _Remarks = value; }
        get { return _Remarks; }
    }

    public int LoginUserId
    {
        set { _LoginUserId = value; }
        get { return _LoginUserId; }
    }

    public int PayRecvId
    {
        set { _PayRecvId = value; }
        get { return _PayRecvId; }
    }

    public int IsActive
    {
        set { _IsActive = value; }
        get { return _IsActive; }
    }

    public string CourierName
    {
        set { _CourierName = value; }
        get { return _CourierName; }
    }
    public decimal ActualAmount
    {
        set { _ActualAmount = value; }
        get { return _ActualAmount; }
    }

    #endregion

    public int CreatePaymentReceviedInfo()
    {
        int PaymentId = 0;
        try
        {
            OpenSQLConnection();
            SqlCommand cmd = new SqlCommand("[CreatePaymentReceviedInfo]", oConnect);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@SourceId", SqlDbType.Int).Value = SourceId;
            cmd.Parameters.Add("@CourierName", SqlDbType.VarChar).Value = CourierName;                   
            cmd.Parameters.Add("@ReceviedhDate", SqlDbType.DateTime).Value = ReceviedhDate;
            cmd.Parameters.Add("@PaymentAmount", SqlDbType.Decimal).Value = Amount;
            cmd.Parameters.Add("@ActualAmount", SqlDbType.Decimal).Value = ActualAmount;            
            cmd.Parameters.Add("@RefNo", SqlDbType.VarChar).Value = RefNo;
            cmd.Parameters.Add("@Remarks", SqlDbType.VarChar).Value = Remarks;
            cmd.Parameters.Add("@LoginUserId", SqlDbType.Int).Value = LoginUserId;
            cmd.Parameters.Add("@IsActive", SqlDbType.Int).Value = IsActive;
      

            SqlParameter paramResult = new SqlParameter("@RValue", SqlDbType.Int);
            paramResult.Direction = ParameterDirection.Output;
            cmd.Parameters.Add(paramResult);
            
            cmd.ExecuteNonQuery();

            PaymentId = Convert.ToInt32(cmd.Parameters["@RValue"].Value);
   
 
        }
        catch (Exception ex)
        {
            oErrorLog.Open();
            oErrorLog.Log(ex.Message);
            oErrorLog.Flush();
            oErrorLog.Close();
        }
        finally
        {
            CloseSQLConnection();
        }
        return PaymentId;
    }

    public int UpdatePaymentReceviedInfo()
    {
        int PaymentId = 0;
        try
        {
            OpenSQLConnection();
            SqlCommand cmd = new SqlCommand("[UpdatePaymentReceviedInfo]", oConnect);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@SourceId", SqlDbType.Int).Value = SourceId;
            cmd.Parameters.Add("@CourierName", SqlDbType.VarChar).Value = CourierName;                            
            cmd.Parameters.Add("@ReceviedhDate", SqlDbType.DateTime).Value = ReceviedhDate;
            cmd.Parameters.Add("@PaymentAmount", SqlDbType.Decimal).Value = Amount;
            cmd.Parameters.Add("@ActualAmount", SqlDbType.Decimal).Value = ActualAmount;        
            cmd.Parameters.Add("@RefNo", SqlDbType.VarChar).Value = RefNo;
            cmd.Parameters.Add("@Remarks", SqlDbType.VarChar).Value = Remarks;
            cmd.Parameters.Add("@LoginUserId", SqlDbType.Int).Value = LoginUserId;
            cmd.Parameters.Add("@PaymentId", SqlDbType.Int).Value = PayRecvId;
            cmd.Parameters.Add("@IsActive", SqlDbType.Int).Value = IsActive;
      
            SqlParameter paramResult = new SqlParameter("@RValue", SqlDbType.Int);
            paramResult.Direction = ParameterDirection.Output;
            cmd.Parameters.Add(paramResult);

            cmd.ExecuteNonQuery();

            PaymentId = Convert.ToInt32(cmd.Parameters["@RValue"].Value);


        }
        catch (Exception ex)
        {
            oErrorLog.Open();
            oErrorLog.Log(ex.Message);
            oErrorLog.Flush();
            oErrorLog.Close();
        }
        finally
        {
            CloseSQLConnection();
        }
        return PaymentId;
    }

    public DataSet GetPaymentSources()
    {
        SqlDataAdapter dtAdptr = new SqlDataAdapter();
        DataSet dsDataSet = new DataSet();
        SqlCommand sqlCmd = new SqlCommand();

        try
        {
            OpenSQLConnection();
            sqlCmd = new SqlCommand("[GetPaymentSources]", oConnect);
            sqlCmd.CommandType = CommandType.StoredProcedure;
            dtAdptr = new SqlDataAdapter(sqlCmd);
            dtAdptr.Fill(dsDataSet);
        }
        catch (Exception ex)
        {
            oErrorLog.Open();
            oErrorLog.Log(ex.Message);
            oErrorLog.Flush();
            oErrorLog.Close();
        }
        finally
        {
            dtAdptr.Dispose();
            dtAdptr = null;

            sqlCmd.Dispose();
            sqlCmd = null;

            CloseSQLConnection();
        }
        return dsDataSet;
    }

    public DataSet GetPaymentReceviedList(DateTime Startdate, DateTime EndDate, string sSourceid)
    {
        SqlDataAdapter dtAdptr = new SqlDataAdapter();
        DataSet dsDataSet = new DataSet();
        SqlCommand sqlCmd = new SqlCommand();

        try
        {
            OpenSQLConnection();
            sqlCmd = new SqlCommand("[GetPaymentReceviedList]", oConnect);
            sqlCmd.CommandType = CommandType.StoredProcedure;

            sqlCmd.Parameters.Add("@StartDate", SqlDbType.DateTime).Value = Startdate;
            sqlCmd.Parameters.Add("@EndDate", SqlDbType.DateTime).Value = EndDate;
            if (!string.IsNullOrEmpty(sSourceid))
                sqlCmd.Parameters.Add("@SourceId", SqlDbType.Int).Value = sSourceid;

            dtAdptr = new SqlDataAdapter(sqlCmd);
            dtAdptr.Fill(dsDataSet);
        }
        catch (Exception ex)
        {
            oErrorLog.Open();
            oErrorLog.Log(ex.Message);
            oErrorLog.Flush();
            oErrorLog.Close();
        }
        finally
        {
            dtAdptr.Dispose();
            dtAdptr = null;

            sqlCmd.Dispose();
            sqlCmd = null;

            CloseSQLConnection();
        }
        return dsDataSet;
    }

    public DataSet GetPaymentReceviedById()
    {
        SqlDataAdapter dtAdptr = new SqlDataAdapter();
        DataSet dsDataSet = new DataSet();
        SqlCommand sqlCmd = new SqlCommand();

        try
        {
            OpenSQLConnection();
            sqlCmd = new SqlCommand("[GetPaymentReceviedById]", oConnect);
            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlCmd.Parameters.Add("@PayRecvId", SqlDbType.Int).Value = PayRecvId;      
            dtAdptr = new SqlDataAdapter(sqlCmd);
            dtAdptr.Fill(dsDataSet);
        }
        catch (Exception ex)
        {
            oErrorLog.Open();
            oErrorLog.Log(ex.Message);
            oErrorLog.Flush();
            oErrorLog.Close();
        }
        finally
        {
            dtAdptr.Dispose();
            dtAdptr = null;

            sqlCmd.Dispose();
            sqlCmd = null;

            CloseSQLConnection();
        }
        return dsDataSet;
    }

    public DataSet GetPaymentDetail()
    {
        SqlDataAdapter dtAdptr = new SqlDataAdapter();
        DataSet dsDataSet = new DataSet();
        SqlCommand sqlCmd = new SqlCommand();

        try
        {
            OpenSQLConnection();
            sqlCmd = new SqlCommand("[GetPaymentDetail]", oConnect);
            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlCmd.Parameters.Add("@PayRecvId", SqlDbType.Int).Value = PayRecvId;
            dtAdptr = new SqlDataAdapter(sqlCmd);
            dtAdptr.Fill(dsDataSet);
        }
        catch (Exception ex)
        {
            oErrorLog.Open();
            oErrorLog.Log(ex.Message);
            oErrorLog.Flush();
            oErrorLog.Close();
        }
        finally
        {
            dtAdptr.Dispose();
            dtAdptr = null;

            sqlCmd.Dispose();
            sqlCmd = null;

            CloseSQLConnection();
        }
        return dsDataSet;
    }

    public DataSet GetPaymentReceivedSources()
    {
        SqlDataAdapter dtAdptr = new SqlDataAdapter();
        DataSet dsDataSet = new DataSet();
        SqlCommand sqlCmd = new SqlCommand();

        try
        {
            OpenSQLConnection();
            sqlCmd = new SqlCommand("[GetPaymentReceviedSources]", oConnect);
            sqlCmd.CommandType = CommandType.StoredProcedure;
            dtAdptr = new SqlDataAdapter(sqlCmd);
            dtAdptr.Fill(dsDataSet);
        }
        catch (Exception ex)
        {
            oErrorLog.Open();
            oErrorLog.Log(ex.Message);
            oErrorLog.Flush();
            oErrorLog.Close();
        }
        finally
        {
            dtAdptr.Dispose();
            dtAdptr = null;

            sqlCmd.Dispose();
            sqlCmd = null;

            CloseSQLConnection();
        }
        return dsDataSet;
    }

    public DataSet GetPaymentSource(string sSource)
    {
        SqlDataAdapter dtAdptr = new SqlDataAdapter();
        DataSet dsDataSet = new DataSet();
        SqlCommand sqlCmd = new SqlCommand();

        try
        {
            OpenSQLConnection();
            sqlCmd = new SqlCommand("GetPaymentSource", oConnect);

            if (!string.IsNullOrEmpty(sSource))
            {
                sqlCmd.Parameters.Add("@Source", SqlDbType.VarChar).Value = sSource;
            }
            else
            {
                sqlCmd.Parameters.Add("@Source", SqlDbType.VarChar).Value = DBNull.Value;
            }

            sqlCmd.CommandType = CommandType.StoredProcedure;
            dtAdptr = new SqlDataAdapter(sqlCmd);
            dtAdptr.Fill(dsDataSet);
        }
        catch (Exception ex)
        {
            oErrorLog.Open();
            oErrorLog.Log(ex.Message);
            oErrorLog.Flush();
            oErrorLog.Close();
        }
        finally
        {
            dtAdptr.Dispose();
            dtAdptr = null;

            sqlCmd.Dispose();
            sqlCmd = null;

            CloseSQLConnection();
        }
        return dsDataSet;
    }


    public DataSet GetPaymentReceivedDetails(string sSource)
    {
        SqlDataAdapter dtAdptr = new SqlDataAdapter();
        DataSet dsDataSet = new DataSet();
        SqlCommand sqlCmd = new SqlCommand();

        try
        {
            OpenSQLConnection();
            sqlCmd = new SqlCommand("[GetPaymentReceivedDetails]", oConnect);
            sqlCmd.Parameters.Add("@Source", SqlDbType.VarChar).Value = sSource;
            sqlCmd.CommandType = CommandType.StoredProcedure;
            dtAdptr = new SqlDataAdapter(sqlCmd);
            dtAdptr.Fill(dsDataSet);
        }
        catch (Exception ex)
        {
            oErrorLog.Open();
            oErrorLog.Log(ex.Message);
            oErrorLog.Flush();
            oErrorLog.Close();
        }
        finally
        {
            dtAdptr.Dispose();
            dtAdptr = null;

            sqlCmd.Dispose();
            sqlCmd = null;

            CloseSQLConnection();
        }
        return dsDataSet;
    }

    public string GetPaymentOutStanding(string sSource)
    {
        SqlDataAdapter dtAdptr = new SqlDataAdapter();
        DataSet dsDataSet = new DataSet();
        SqlCommand sqlCmd = new SqlCommand();

        string OutStanding = string.Empty;
        try
        {
            OpenSQLConnection();
            sqlCmd = new SqlCommand("[GetPaymentOutStanding]", oConnect);
            sqlCmd.Parameters.Add("@Source", SqlDbType.VarChar).Value = sSource;
            sqlCmd.CommandType = CommandType.StoredProcedure;
            OutStanding = Convert.ToString(sqlCmd.ExecuteScalar());
        }
        catch (Exception ex)
        {
            oErrorLog.Open();
            oErrorLog.Log(ex.Message);
            oErrorLog.Flush();
            oErrorLog.Close();
        }
        finally
        {
            dtAdptr.Dispose();
            dtAdptr = null;

            sqlCmd.Dispose();
            sqlCmd = null;

            CloseSQLConnection();
        }

        return OutStanding;
    }

    public void DeletePaymentDetails(string PId)
    {
        SqlDataAdapter dtAdptr = new SqlDataAdapter();
        DataSet dsDataSet = new DataSet();
        SqlCommand sqlCmd = new SqlCommand();

        string OutStanding = string.Empty;
        try
        {
            OpenSQLConnection();
            sqlCmd = new SqlCommand("[DeletePaymentDetails]", oConnect);
            sqlCmd.Parameters.Add("@PId", SqlDbType.VarChar).Value = PId;
            sqlCmd.CommandType = CommandType.StoredProcedure;
            if (PId != string.Empty)
            {
                sqlCmd.ExecuteNonQuery();
            }
        }
        catch (Exception ex)
        {
            oErrorLog.Open();
            oErrorLog.Log(ex.Message);
            oErrorLog.Flush();
            oErrorLog.Close();
        }
        finally
        {
            dtAdptr.Dispose();
            dtAdptr = null;

            sqlCmd.Dispose();
            sqlCmd = null;

            CloseSQLConnection();
        }
    }

    #region PayU
    public string _mihpayid;
    public string mihpayid
    {
        set { _mihpayid = value; }
        get { return _mihpayid; }
    }
    public string _mode;
    public string mode
    {
        set { _mode = value; }
        get { return _mode; }
    }
    public string _status;
    public string status
    {
        set { _status = value; }
        get { return _status; }
    }
    public string _key;
    public string key
    {
        set { _key = value; }
        get { return _key; }
    }
    public string _txnid;
    public string txnid
    {
        set { _txnid = value; }
        get { return _txnid; }
    }
    public string _amount;
    public string amount
    {
        set { _amount = value; }
        get { return _amount; }
    }
    public string _Pamount;
    public string Pamount
    {
        set { _Pamount = value; }
        get { return _Pamount; }
    }
    public string _Pdiscount;
    public string Pdiscount
    {
        set { _Pdiscount = value; }
        get { return _Pdiscount; }
    }
    public string _discount;
    public string discount
    {
        set { _discount = value; }
        get { return _discount; }
    }
    public string _Offer;
    public string Offer
    {
        set { _Offer = value; }
        get { return _Offer; }
    }
    public string _productinfo;
    public string productinfo
    {
        set { _productinfo = value; }
        get { return _productinfo; }
    }
    public string _firstname;
    public string firstname
    {
        set { _firstname = value; }
        get { return _firstname; }
    }
    public string _lastname;
    public string lastname
    {
        set { _lastname = value; }
        get { return _lastname; }
    }
    public string _address1;
    public string address1
    {
        set { _address1 = value; }
        get { return _address1; }
    }
    public string _address2;
    public string address2
    {
        set { _address2 = value; }
        get { return _address2; }
    }
    public string _city;
    public string city
    {
        set { _city = value; }
        get { return _city; }
    }
    public string _state;
    public string state
    {
        set { _state = value; }
        get { return _state; }
    }
    public string _country;
    public string country
    {
        set { _country = value; }
        get { return _country; }
    }
    public string _zipcode;
    public string zipcode
    {
        set { _zipcode = value; }
        get { return _zipcode; }
    }
    public string _email;
    public string email
    {
        set { _email = value; }
        get { return _email; }
    }
    public string _phone;
    public string phone
    {
        set { _phone = value; }
        get { return _phone; }
    }
    public string _udf1;
    public string udf1
    {
        set { _udf1 = value; }
        get { return _udf1; }
    }
    public string _udf2;
    public string udf2
    {
        set { _udf2 = value; }
        get { return _udf2; }
    }
    public string _udf3;
    public string udf3
    {
        set { _udf3 = value; }
        get { return _udf3; }
    }
    public string _udf4;
    public string udf4
    {
        set { _udf4 = value; }
        get { return _udf4; }
    }
    public string _udf5;
    public string udf5
    {
        set { _udf5 = value; }
        get { return _udf5; }
    }
    public string _hash;
    public string hash
    {
        set { _hash = value; }
        get { return _hash; }
    }
    public string _Error;
    public string Error
    {
        set { _Error = value; }
        get { return _Error; }
    }
    public string _PG_TYPE;
    public string PG_TYPE
    {
        set { _PG_TYPE = value; }
        get { return _PG_TYPE; }
    }
    public string _bank_ref_num;
    public string bank_ref_num
    {
        set { _bank_ref_num = value; }
        get { return _bank_ref_num; }
    }
    public string _shipping_firstname;
    public string shipping_firstname
    {
        set { _shipping_firstname = value; }
        get { return _shipping_firstname; }
    }
    public string _shipping_lastname;
    public string shipping_lastname
    {
        set { _shipping_lastname = value; }
        get { return _shipping_lastname; }
    }
    public string _shipping_address1;
    public string shipping_address1
    {
        set { _shipping_address1 = value; }
        get { return _shipping_address1; }
    }
    public string _shipping_address2;
    public string shipping_address2
    {
        set { _shipping_address2 = value; }
        get { return _shipping_address2; }
    }
    public string _shipping_city;
    public string shipping_city
    {
        set { _shipping_city = value; }
        get { return _shipping_city; }
    }
    public string _shipping_state;
    public string shipping_state
    {
        set { _shipping_state = value; }
        get { return _shipping_state; }
    }
    public string _shipping_country;
    public string shipping_country
    {
        set { _shipping_country = value; }
        get { return _shipping_country; }
    }
    public string _shipping_zipcode;
    public string shipping_zipcode
    {
        set { _shipping_zipcode = value; }
        get { return _shipping_zipcode; }
    }
    public string _shipping_phone;
    public string shipping_phone
    {
        set { _shipping_phone = value; }
        get { return _shipping_phone; }
    }
    public string _shipping_phoneverified;
    public string shipping_phoneverified
    {
        set { _shipping_phoneverified = value; }
        get { return _shipping_phoneverified; }
    }
    public string _unmappedstatus;
    public string unmappedstatus
    {
        set { _unmappedstatus = value; }
        get { return _unmappedstatus; }
    }
    public string _UserAgent;
    public string UserAgent
    {
        set { _UserAgent = value; }
        get { return _UserAgent; }
    }
    public string _IPAddress;
    public string IPAddress
    {
        set { _IPAddress = value; }
        get { return _IPAddress; }
    }
    #endregion


    #region Paytm

    public string _TXNID;
    public string TXNID
    {
        set { _TXNID = value; }
        get { return _TXNID; }
    }

    public string _ORDERID;
    public string ORDERID
    {
        set { _ORDERID = value; }
        get { return _ORDERID; }
    }

    public string _BANKTXNID;
    public string BANKTXNID
    {
        set { _BANKTXNID = value; }
        get { return _BANKTXNID; }
    }

    public string _TXNAMOUNT;
    public string TXNAMOUNT
    {
        set { _TXNAMOUNT = value; }
        get { return _TXNAMOUNT; }
    }

    public string _CURRENCY;
    public string CURRENCY
    {
        set { _CURRENCY = value; }
        get { return _CURRENCY; }
    }

    public string _STATUS;
    public string STATUS
    {
        set { _STATUS = value; }
        get { return _STATUS; }
    }


    public string _RESPCODE;
    public string RESPCODE
    {
        set { _RESPCODE = value; }
        get { return _RESPCODE; }
    }

    public string _RESPMSG;
    public string RESPMSG
    {
        set { _RESPMSG = value; }
        get { return _RESPMSG; }
    }

    public string _TXNDATE;
    public string TXNDATE
    {
        set { _TXNDATE = value; }
        get { return _TXNDATE; }
    }

    public string _GATEWAYNAME;
    public string GATEWAYNAME
    {
        set { _GATEWAYNAME = value; }
        get { return _GATEWAYNAME; }
    }

    public string _BANKNAME;
    public string BANKNAME
    {
        set { _BANKNAME = value; }
        get { return _BANKNAME; }
    }

    public string _PAYMENTMODE;
    public string PAYMENTMODE
    {
        set { _PAYMENTMODE = value; }
        get { return _PAYMENTMODE; }
    }
    #endregion

    public void PayUResponse()
    {
        try
        {
            OpenSQLConnection();
            SqlCommand cmd = new SqlCommand("PayUResponse", oConnect);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@mihpayid", SqlDbType.VarChar).Value = mihpayid;
            cmd.Parameters.Add("@mode", SqlDbType.VarChar).Value = mode;
            cmd.Parameters.Add("@status", SqlDbType.VarChar).Value = status;
            cmd.Parameters.Add("@key", SqlDbType.VarChar).Value = key;
            cmd.Parameters.Add("@txnid", SqlDbType.VarChar).Value = txnid;
            cmd.Parameters.Add("@amount", SqlDbType.Decimal).Value = Convert.ToDecimal(Pamount);
            cmd.Parameters.Add("@discount", SqlDbType.Decimal).Value = Convert.ToDecimal(Pdiscount);
            cmd.Parameters.Add("@Offer", SqlDbType.VarChar).Value = Offer;
            cmd.Parameters.Add("@productinfo", SqlDbType.VarChar).Value = productinfo;
            cmd.Parameters.Add("@firstname", SqlDbType.VarChar).Value = firstname;
            cmd.Parameters.Add("@lastname", SqlDbType.VarChar).Value = lastname;
            cmd.Parameters.Add("@address1", SqlDbType.VarChar).Value = address1;
            cmd.Parameters.Add("@address2", SqlDbType.VarChar).Value = address2;
            cmd.Parameters.Add("@city", SqlDbType.VarChar).Value = city;
            cmd.Parameters.Add("@state", SqlDbType.VarChar).Value = state;
            cmd.Parameters.Add("@country", SqlDbType.VarChar).Value = country;
            cmd.Parameters.Add("@zipcode", SqlDbType.VarChar).Value = zipcode;
            cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = email;
            cmd.Parameters.Add("@phone", SqlDbType.VarChar).Value = phone;
            cmd.Parameters.Add("@udf1", SqlDbType.VarChar).Value = udf1;
            cmd.Parameters.Add("@udf2", SqlDbType.VarChar).Value = udf2;
            cmd.Parameters.Add("@udf3", SqlDbType.VarChar).Value = udf3;
            cmd.Parameters.Add("@udf4", SqlDbType.VarChar).Value = udf4;
            cmd.Parameters.Add("@udf5", SqlDbType.VarChar).Value = udf5;
            cmd.Parameters.Add("@hash", SqlDbType.VarChar).Value = hash;
            cmd.Parameters.Add("@Error", SqlDbType.VarChar).Value = Error;
            cmd.Parameters.Add("@PG_TYPE", SqlDbType.VarChar).Value = PG_TYPE;
            cmd.Parameters.Add("@bank_ref_num", SqlDbType.VarChar).Value = bank_ref_num;
            cmd.Parameters.Add("@shipping_firstname", SqlDbType.VarChar).Value = shipping_firstname;
            cmd.Parameters.Add("@shipping_lastname", SqlDbType.VarChar).Value = shipping_lastname;
            cmd.Parameters.Add("@shipping_address1", SqlDbType.VarChar).Value = shipping_address1;
            cmd.Parameters.Add("@shipping_address2", SqlDbType.VarChar).Value = shipping_address2;
            cmd.Parameters.Add("@shipping_city", SqlDbType.VarChar).Value = shipping_city;
            cmd.Parameters.Add("@shipping_state", SqlDbType.VarChar).Value = shipping_state;
            cmd.Parameters.Add("@shipping_country", SqlDbType.VarChar).Value = shipping_country;
            cmd.Parameters.Add("@shipping_zipcode", SqlDbType.VarChar).Value = shipping_zipcode;
            cmd.Parameters.Add("@shipping_phone", SqlDbType.VarChar).Value = shipping_phone;
            cmd.Parameters.Add("@shipping_phoneverified", SqlDbType.VarChar).Value = shipping_phoneverified;
            cmd.Parameters.Add("@unmappedstatus", SqlDbType.VarChar).Value = unmappedstatus;
            cmd.Parameters.Add("@UserAgent", SqlDbType.VarChar).Value = UserAgent;
            cmd.Parameters.Add("@IPAddress", SqlDbType.VarChar).Value = sRemoteAddress;
            cmd.ExecuteNonQuery();
        }
        catch (Exception ex)
        {
            oErrorLog.Open();
            oErrorLog.Log(ex.Message);
            oErrorLog.Flush();
            oErrorLog.Close();
        }
        finally
        {
            CloseSQLConnection();
        }
    }

    public void PayTMResponse()
    {
        try
        {
            OpenSQLConnection();
            SqlCommand cmd = new SqlCommand("PayTMResponse", oConnect);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@TXNID", SqlDbType.VarChar).Value = TXNID;
            cmd.Parameters.Add("@ORDERID", SqlDbType.VarChar).Value = ORDERID;

            cmd.Parameters.Add("@BANKTXNID", SqlDbType.VarChar).Value = BANKTXNID;
            cmd.Parameters.Add("@TXNAMOUNT", SqlDbType.VarChar).Value = TXNAMOUNT;
            cmd.Parameters.Add("@CURRENCY", SqlDbType.VarChar).Value = CURRENCY;
            cmd.Parameters.Add("@STATUS", SqlDbType.VarChar).Value = STATUS;
            cmd.Parameters.Add("@RESPCODE", SqlDbType.VarChar).Value = RESPCODE;
            cmd.Parameters.Add("@RESPMSG", SqlDbType.VarChar).Value = RESPMSG;
            // cmd.Parameters.Add("@TXNDATE", SqlDbType.VarChar).Value =  TXNDATE;
            cmd.Parameters.Add("@GATEWAYNAME", SqlDbType.VarChar).Value = GATEWAYNAME;
            cmd.Parameters.Add("@BANKNAME", SqlDbType.VarChar).Value = BANKNAME;
            cmd.Parameters.Add("@PAYMENTMODE", SqlDbType.VarChar).Value = PAYMENTMODE;

            cmd.ExecuteNonQuery();

        }
        catch (Exception ex)
        {
            oErrorLog.Open();
            oErrorLog.Log(ex.Message);

            oErrorLog.Flush();
            oErrorLog.Close();
        }
        finally
        {
            CloseSQLConnection();
        }
    }

    public void RazorPayResponse(RazorPayPayment payment)
    {
        try
        {
            OpenSQLConnection();
            SqlCommand cmd = new SqlCommand("RazorPayPaymentResponse", oConnect);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@id", SqlDbType.VarChar).Value = payment.id;
            cmd.Parameters.Add("@entity", SqlDbType.VarChar).Value = payment.entity;
            cmd.Parameters.Add("@amount", SqlDbType.Int).Value = payment.amount / 100;
            cmd.Parameters.Add("@currency", SqlDbType.VarChar).Value = payment.currency;
            cmd.Parameters.Add("@status", SqlDbType.VarChar).Value = payment.status;
            cmd.Parameters.Add("@method", SqlDbType.VarChar).Value = payment.method;
            cmd.Parameters.Add("@description", SqlDbType.VarChar).Value = payment.description;
            cmd.Parameters.Add("@refund_status", SqlDbType.VarChar).Value = payment.refund_status;
            cmd.Parameters.Add("@amount_refunded", SqlDbType.Int).Value = payment.amount_refunded;
            cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = payment.email;
            cmd.Parameters.Add("@contact", SqlDbType.VarChar).Value = payment.contact;
            cmd.Parameters.Add("@address", SqlDbType.VarChar).Value = payment.notes.address;
            cmd.Parameters.Add("@merchant_order_id", SqlDbType.VarChar).Value = payment.notes.merchant_order_id;
            cmd.Parameters.Add("@fee", SqlDbType.VarChar).Value = "";
            cmd.Parameters.Add("@service_tax", SqlDbType.VarChar).Value = "";
            cmd.Parameters.Add("@error_code", SqlDbType.VarChar).Value = payment.error_code;
            cmd.Parameters.Add("@error_description", SqlDbType.VarChar).Value = payment.error_description;
            cmd.Parameters.Add("@created_at", SqlDbType.VarChar).Value = payment.created_at;

            cmd.ExecuteNonQuery();

        }
        catch (Exception ex)
        {
            oErrorLog.Open();
            oErrorLog.Log(ex.Message);

            oErrorLog.Flush();
            oErrorLog.Close();
        }
        finally
        {
            CloseSQLConnection();
        }
    }

}