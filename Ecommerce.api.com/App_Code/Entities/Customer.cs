﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ecommerce.Entities
{
    public class RegisterCustomer
    {
        public string name { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string gender { get; set; }
        public string email { get; set; }
    }

    public class ProBuilderResponse
    {
        public int CustomerId { get; set; }
        public bool ProBuilderStatus { get; set; }
    }

    public class ClsAutoStateCity
    {
        public string State { get; set; }
        public string City { get; set; }
        public string PinCode { get; set; }
    }

}