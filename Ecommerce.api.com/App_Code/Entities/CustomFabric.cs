﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ecommerce.Entities
{
    public class CustomProductList
    {
        public List<SwatchList> SwatchList { set; get; }
        public List<Materialdata> Materialdata { get; set; }
        public List<TaxtureData> TaxtureData { get; set; }
        public List<ColorData> ColorData { get; set; }
        public List<CityData> CityData { get; set; }
        public List<ProductImages> SofaImage { set; get; }

    }

    public class CustomProductDetail
    {
        public List<SwatchList> SwatchDetail { set; get; }
    }
    public class CustomProductPackSlip
    {
        public List<PackSlip> PackSlip { set; get; }
    }
    public class PaintsheetDetail
    {
        public string id { get; set; }
        public string name { get; set; }
        public string format { get; set; }        
        public string swatch{ set; get; }
    }

    public class ShadecardDetail
    {
        public string id { get; set; }
        public string name { get; set; }
        public string format { get; set; }
        public List<string> swatches { set; get; }
    }

    public class Materialdata
    {
        public string Material { get; set; }
    }
    public class TaxtureData
    {
        public string Taxture { get; set; }
    }
    public class ColorData
    {
        public string ColorName { get; set; }
    }
    public class SwatchList
    {
        public string ItemCode { get; set; }
        public string FabricItemCode { get; set; }
        public string StandardCode { get; set; }
        public string LargeImage { get; set; }
        public string Width { get; set; }
        public string ThumImage { get; set; }
        public string MediumImage { get; set; }
        public string CollectionName { get; set; }
        public string Weight { get; set; }
        public string Material { get; set; }
        public string OneLineSummary { get; set; }
        public int Mrp { get; set; }
        public int RetailPrice { get; set; }
        public string StockUnit { get; set; }
        public string Taxture { get; set; }
        public string ThreadCount { get; set; }
        public string ColorName { get; set; }
        public string AlternateImages { get; set; }
    }
    public class ProductImages
    {
        public string ImageUrl { get; set; }
    }
    public class CityData
    {
        public string CityName { get; set; }
    }
    public class PackSlip
    {
        public string ItemCode { get; set; }
        public string Material { get; set; }
        public string ItemName { get; set; }
        public string FabricType { get; set; }
    }
    public class PaintShadcardUrl
    {
        public string url { get; set; }

    }


}
