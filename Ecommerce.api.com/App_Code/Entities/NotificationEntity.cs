﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ecommerce.Entities
{
    public class SellerOrderEmail
    {
        public List<SellerInfo> sellerinfo { get; set; }
        public List<SellerNoticationData> sellerOrderInfo { get; set; }
    }
    public class SellerInfo
    {
        public int vendorId { get; set; }
        public string Email { get; set; }
        public string SellerName { get; set; }
        public string VendorName { get; set; }
        public string CustomerEmail { get; set; }
    }
    public class SellerNoticationData
    {
        public string OrderDate { set; get; }
        public string OrderNo { set; get; }
        public string ItemCode { set; get; }
        public string SellerName { set; get; }
        public string BuyerName { set; get; }
        public string Quantity { set; get; }
        public decimal RetailPrice { set; get; }
        public decimal TotalRetailPrice { set; get; }
        public decimal TotalComm { set; get; }
        public decimal ServicetaxAmount { set; get; }
        public decimal PayOut { set; get; }
        public string DeliveryDate { set; get; }
        public string Email { set; get; }
        public int VendorId { set; get; }
        public string Paymentdate { set; get; }
        public string categoryName { set; get; }
    }
}