﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ecommerce.Entities
{
    public class CheckOutCartItemDetail
    {
        public List<CheckOutCartItem> CartItem { get; set; }
        public List<CartZodiacItems> CartZodiacItems { get; set; }
        public CartPaymentDetail CartPaymentDetail { get; set; }
        public IsValidForGuestCheckout IsValidForGuest { get; set; }
    }

    public class CheckOutCartItem
    {
        public int CustomerId { get; set; }
        public int CartId { get; set; }
        public decimal CouponValue { get; set; }
        public string CouponCode { get; set; }
        public int ItemId { get; set; }
        public string SessionId { get; set; }
        public string Quantity { get; set; }
        public string ItemCode { get; set; }
        public string Medium { get; set; }
        public int CategoryId { get; set; }
        public decimal MRP { get; set; }
        public decimal TotalRetailPrice { get; set; }
        public decimal TotalGiftWrapPrice { get; set; }
        public decimal RetailPrice { get; set; }
        public string StandardCode { get; set; }
        public string GiftWrapPrice { get; set; }
        public string GiftWrapQuantity { get; set; }
        public string GiftWrapAmount { get; set; }
        public string GiftMessage { get; set; }
        public string Amount { get; set; }
        public string CATEGORYNAME { get; set; }
        public string CodText { get; set; }
        public string CreatedDate { get; set; }
        public string CollectionName { get; set; }
        public string IsCodProduct { get; set; }
        public string SubClassName { get; set; }
        public string SUbCategory { get; set; }
        public string OnelineSummary { get; set; }
        public string ColorName { get; set; }
        public string SizeName { get; set; }
        public string Description { get; set; }
        public string Discount { get; set; }
        public string IsPersonlize { get; set; }
        public string PersonlizeText { get; set; }

        public string IsbathRobePersonlize { get; set; }
        public string PersonlizebackInitial { get; set; }
        public string PersonlizeCharLimit { get; set; }
        public string PersonlizeFrontCharLimit { get; set; }
        public string DiscountOnBrand { get; set; }
        public string ItemStatus { get; set; }
        public string ShipInDays { get; set; }
        public int BundleItemId { get; set; }
        public int IsBundleItem { get; set; }
        public string Length { get; set; }
        public string Width { get; set; }
        public string Height { get; set; }
        public string StockUnit { get; set; }
        public string CustomProductType { get; set; }
        public string CustomProductImage { get; set; }





    }

    public class CartZodiacItems
    {
        public int TotalQty { get; set; }
        public decimal TotalRetailPrice { get; set; }
        public string SessionId { get; set; }
        public decimal TotalOrderAmt { get; set; }
        public string CollectionName { get; set; }
        public int CartFreeItemId { get; set; }
        public string CategoryName { get; set; }
        public int CartId { get; set; }
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public string SubClassName { get; set; }
        public string ThumbNail { get; set; }
        public string StandardCode { get; set; }
        public string DesignOverview { get; set; }
        public string Large { get; set; }
        public string Cod { get; set; }
        public string IsChoiceItem { get; set; }
        public string CreatedDate { get; set; }
        public string RetailPrice { get; set; }
        public string BundleItemId { get; set; }
        public string ItemMediumImage { get; set; }
        public string Quantity { get; set; }
        public string ItemHexCode { get; set; }
        public string VendorCode { get; set; }
    }
    public class CartPaymentDetail
    {
        public int GiftWrapAmount { get; set; }
        public decimal CouponDiscount { get; set; }
        public decimal SubTotal { get; set; }
        public decimal FinalAmount { get; set; }
    }
    public class IsValidForGuestCheckout
    {
        public int IsNotValidForGuest { get; set; }

    }

    public class CustomerAddress
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public int PinNo { get; set; }
        public string MobileNo { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public int CustomerId { get; set; }
        public int UserId { get; set; }
        public string Email { get; set; }
        public int SourceId { get; set; }
        public int? AddressId { get; set; }
        public bool IsDefalut { get; set; }
    }

    public class CheckoutCartValidation
    {
        public int CustomerId { get; set; }
        public int PaymentMode { get; set; }
        public string PinCode { get; set; }
        public string SessionId { get; set; }        
    }
    

    public class EMI
    {
        public List<Banks> ObjBanks;
        public List<EmiDetails> ObjEmi;
    }

    public class Banks
    {
        public string BankName
        {
            get;
            set;
        }
    }

    public class EmiDetails
    {
        public string BankName
        {
            get;
            set;
        }
        public int EMITenure
        {
            get;
            set;
        }
        public double Interest
        {
            get;
            set;
        }
        public double EMIAmount
        {
            get;
            set;
        }
        public double TotalPayment
        {
            get;
            set;
        }

        public int EMIInterest
        {
            get;
            set;
        }
    }

    public class CheckoutBillingAddress
    {
        public int CustomerId { get; set; }
        public int UserId { get; set; }
        public string Email { get; set; }
        public int SourceId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public int PinNo { get; set; }
        public string City { get; set; }
        public string MobileNo { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public int? AddressId { get; set; }
        public bool IsDefalut { get; set; }
    }

    public class ClsPaymentResponse
    {
        public List<OrderDetails> orderDetails { get; set; }
        public OrderPaymentInfo OrderPaymentInfo { get; set; }
        public List<CustomItems> customItems { get; set; }
    }

    public class OrderDetails
    {
        public string Name { get; set; }
        public string ShippingAddress { get; set; }
        public string ShippingState { get; set; }
        public string ShippingCity { get; set; }
        public string ShippingPostCode { get; set; }
        public int OrderItemId { get; set; }
        public int OrderId { get; set; }
        public string OrderNo { get; set; }
        public string ItemCode { get; set; }
        public string Description { get; set; }
        public decimal Mrp { get; set; }
        public decimal RetailPrice { get; set; }
        public string CategoryName { get; set; }
        public string CollectionName { get; set; }
        public string Medium { get; set; }
        public string OneLineSummary { get; set; }
        public string ShipInDays { get; set; }
        public string MobileNo { get; set; }
        public string OrderStatus { get; set; }
        public string StandardCode { get; set; }
        public int Quantity { get; set; }
        public string MapCategory { get; set; }
        public int CartId { get; set; }
        public int IsBundleItem { get; set; }
        public string ItemStatus { get; set; }

    }

    public class CustomItems
    {
        public string SubClassName { get; set; }
        public string OneLineSummary { get; set; }
        public string StandardCode { get; set; }
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public int BundleItemId { get; set; }
        public int ParentCartItemId { get; set; }
        public string ItemHexCode { get; set; }
        public int Quantity { get; set; }
    }

    public class OrderPaymentInfo
    {
        public decimal BillAmount { get; set; }
        public string DiscountCouponName { get; set; }
        public string DiscountCouponValue { get; set; }
    }   

    public class SellerServiciable
    {
        public string MsgType { get; set; }
        public string ErrorMsg { get; set; }
    }
    public class ServiceableCartItem
    {
        public string MsgType { get; set; }
        public string ItemCode { get; set; }
    }
}