﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ecommerce.Entities
{

    public class BBMStore
    {
        public ICollection<BBMStoredata> BBMStoreDataList { get; set; }
    }

    public class BBMStoredata
    {
        public string StandardCode { get; set; }
        public string Name { get; set; }
        public string ProductUrl { get; set; }
        public string Mrp_Max { get; set; }
        public decimal Mrp_Min { get; set; }
        public decimal Price_Max { get; set; }
        public decimal Price_Min { get; set; }
        public int Discount { get; set; }
        public string Brand { get; set; }
        public string DisplayCategory { get; set; }
        public string IsOnSale { get; set; }
        public string IsTopSeller { get; set; }
        public string IsSoldOut { get; set; }
        public string ColorName { get; set; }
        public string SubClassName { get; set; }
        public string ContentId { get; set; }
        public string ParentCategory { get; set; }
        public string Image { get; set; }
        public int IsCod { get; set; }
        public string DesignType { get; set; }
        public string Desc { get; set; }
        public string Sizes { get; set; }
        public string SKUS { get; set; }
        public string ItemCategory { get; set; }

        public string FName { get; set; }
        public string Series { get; set; }
        public string PrintType { get; set; }
        public string FabricType { get; set; }
        public string LegMaterial { get; set; }
        public string CushionMaterial { get; set; }
        public string ArmStyle { get; set; }
        public string Style { get; set; }
        public string Assembly { get; set; }
        public string ThreadCount { get; set; }
        public string Weight { get; set; }

    }

    public class ClsTokenInfo
    {
        public ICollection<TokenInfo> tokenlist { get; set; }
    }

    public class TokenInfo
    {
        public int Id { get; set; }
        public string Token { get; set; }
        public string TokenCreatedDate { get; set; }

    }

    public class FeedInfo
    {
        public string l1 { get; set; }
        public string l2 { get; set; }
        public string l3 { get; set; }
        public string brand { get; set; }
        public string sReset { get; set; }
    }
}