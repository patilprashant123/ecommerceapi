﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ecommerce.Entities
{
    public class MetaProduct
    {
        public MetaProductDetail metaProductDetail { get; set; }
        public ICollection<MetaProductColor> metaProductColor { get; set; }
        public ICollection<MetaItemDetail> metaItemDetail { get; set; }
        public ICollection<MetaMultipleImages> metaMultipleImages { get; set; }
        public ICollection<MetaBBMProductDescription> metaBBMProductDescription { get; set; }
        public ICollection<MetaFurnitureProductDescription> metaFurnitureProductDescription { get; set; }
        public ICollection<DimensionImages> dimensionImages { get; set; }
        public ICollection<CropImageDetail> cropImageDetail { get; set; }
        public ICollection<RotationalImages> rotationalImages { get; set; }
        public ICollection<BuyerInfo> buyerInfo { get; set; }
    }
    public class StandardDesignDetail
    {
        public StandardDesignInfo standardDesignInfo { get; set; }
    }

    public class StandardDesignInfo
    {
        public string StandardCode { get; set; }
        public string ItemCode { get; set; }
        public int Quantity { get; set; }
        public bool IsPublish { get; set; }
    }
    public class MetaProductDetail
    {
        public ICollection<BundleItemJson> objBundleItemJson;
        public string OneLiner { get; set; }
        public string Brand { get; set; }
        public string ImageUrl { get; set; }
        public int ContentType { get; set; }
        public string Material { get; set; }
        public string Description { get; set; }
        public string ShipDays { get; set; }
        public string AssemblyRequired { get; set; }
        public int IsPersonlize { get; set; }
        public int Isbathrobepersonlize { get; set; }
        public ProductZodiacDetail ProductZodiacDetail { get; set; }
        public ICollection<BundleItemJson> BundleItemJson
        { get { return objBundleItemJson; } set { objBundleItemJson = value; } }
        public int IsCombo { get; set; }
        public int IsWithoutCombo { get; set; }
        public int DeliveryDays { get; set; }
        public int UIType { get; set; }
        public bool IsPublished { get; set; }
        public string SoldBy { get; set; }
        public string ParentCategory { get; set; }
        public int TotalLike { get; set; }
    }

    public class MetaProductColor
    {
        public string ColorName { get; set; }
        public string StandardCode { get; set; }
        public string Height { get; set; }
        public string CatName { get; set; }
    }

    public class MetaItemDetail
    {
        public string StandardCode { get; set; }
        public int ItemID { get; set; }
        public string ItemCode { get; set; }
        public string CategoryName { get; set; }
        public int Quantity { get; set; }
        public int MRP { get; set; }
        public int RetailPrice { get; set; }
        public string ItemDescription { get; set; }
        public string Discount { get; set; }
        public bool CoD { get; set; }
        public string MapCategory { get; set; }
        public string SubClassName { get; set; }
        public int ContentId { get; set; }
        public bool IsPersonlize { get; set; }
        public bool IsBathRobepersonlize { get; set; }
        public int PersonlizeCharLimit { get; set; }
        public int PersonlizeFrontCharLimit { get; set; }
        public string PersonlizeCharCaption { get; set; }
        public string PersonlizeFrontCharCaption { get; set; }
        public string ImageName { get; set; }
        public int itemSet { get; set; }
        public bool IsPublished { get; set; }
        public string Size { get; set; }
    }

    public class MetaMultipleImages
    {
        public string ImageName { get; set; }
    }

    public class MetaBBMProductDescription
    {
        public string ItemCode { get; set; }
        public string Material { get; set; }
        public string CategoryName { get; set; }
        public string Length { get; set; }
        public string Width { get; set; }
        public string Height { get; set; }
        public string Weight { get; set; }
        public string InSet { get; set; }
        public string ShortDesc { get; set; }
        public string LongDesc { get; set; }
        public string Care { get; set; }
        public string ThreadCount { get; set; }
        public string Units { get; set; }
        public string ColorName { get; set; }
        public string Size { get; set; }
        public string Pattern { get; set; }
        public string ReturnPolicy { get; set; }
        public string Warranty { get; set; }
        public string ProductFeatures { get; set; }
    }

    public class MetaFurnitureProductDescription
    {
        public string Material { get; set; }
        public string AssemblyRequired { get; set; }
        public string ColorName { get; set; }
        public string CategoryName { get; set; }
        public string StandardCode { get; set; }
        public string ItemCode { get; set; }
        public string Length { get; set; }
        public string Width { get; set; }
        public string Height { get; set; }
        public string FabricType { get; set; }
        public string Style { get; set; }
        public string LegMaterial { get; set; }
        public string ArmStyle { get; set; }
        public string CushionMaterial { get; set; }
        public string ProductFeatures { get; set; }
        public string LongDescription { get; set; }
        public string FurnitureCare { get; set; }
        public string Warranty { get; set; }
        public string Name { get; set; }
        public string Series { get; set; }
        public string PrintType { get; set; }
        public string Type { get; set; }
        public string Weight { get; set; }
        public string ReturnPolicy { get; set; }
    }

    public class ClsNotify
    {
        public string ItemCode { get; set; }
        public string Email { get; set; }
    }

    public class ClsGuestUser
    {
        public string Email { get; set; }
        public string SessionId { get; set; }
    }

    public class DimensionImages
    {
        public string StandardCode { get; set; }
        public string ItemCode { get; set; }
        public string CategoryName { get; set; }
        public string ImageUrl { get; set; }
    }

    public class GuestAbandonRequest
    {
        public string EncrptId { get; set; }
        public string SessionId { get; set; }
    }

    public class GuestAbandonResponse
    {
        public int CustomerId { get; set; }
        public string Email { get; set; }
    }
    public class FlashSaleProduct
    {
        public string StandardCode { get; set; }
        public string ItemCode { get; set; }
        public decimal Mrp { get; set; }
        public decimal RetaiPrice { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }

    public class DesignImage
    {
        public ICollection<ImageDetail> designImage { get; set; }
        public List<ProductCustomLabel> ProductCustomlabel { get; set; }
    }
    public class ImageCoordinate
    {
        public ICollection<CropImageDetail> cropimageList { get; set; }
    }
    public class ImageDetail
    {
        public string StandardCode { get; set; }
        public string ImageId { get; set; }
        public string ImageUrl { get; set; }
    }
    public class CropImageDetail
    {
        public int CropId { get; set; }
        public int ItemImageId { get; set; }
        public string StandardCode { get; set; }
        public decimal XCoor { get; set; }
        public decimal YCoor { get; set; }
        public decimal Width { get; set; }
        public decimal Height { get; set; }
        public string CropImageUrl { get; set; }
        public string LabelName { get; set; }
        public string DesignImageName { get; set; }
        public string DesignImageUrl { get; set; }
        public string CustomProductLabel { get; set; }        
        public string LabelTypeName { get; set; }
        public string ItemCode{ get; set; }
    }
    public class ProductLabelandMaterialList
    {
        public ICollection<ProductCustomLabel> productCustomLabel { get; set; }
        public ICollection<ProductCustomLabelType> productCustomLabelType { get; set; }
    }

    
    public class ProductCustomLabel
    {
        public int ProductCustomLabelId { get; set; }
        public string CustomProductLabel { get; set; }
        public ICollection<ProductCustomLabelType> productCustomLabelType { get; set; }

    }
    public class ProductCustomLabelType
    {
        public int ProductCustomLabelTypeId { get; set; }
        public int ProductCustomLabelId { get; set; }
        public string LabelTypeName { get; set; }
    }
    public class RotationalImages
    {   
        public string ImageUrl { get; set; }
    }
    public class BuyerInfo
    {
        public string CustomerName { get; set; }
        public string CustomerDescription { get; set; }
        public string ImageUrl { get; set; }
    }


}