﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ecommerce.Entities
{
    public class ProductDetailsByCode
    {
        public ProductStandardDetail ProductStandardDetail { get; set; }
        public ICollection<ProductStandardColor> ProductStandardColor { get; set; }
        public ICollection<ItemDetailsForProduct> ItemDetailsForProduct { get; set; }
        public ICollection<MultipleImages> MultipleImages { get; set; }
        public ICollection<PhotoServices> PhotoServices { get; set; }
        public ICollection<SimilarContentByContentId> SimilarContentByContentId { get; set; }
        public FurnitureProductDetail FurnitureProductDetail { get; set; }
        public ICollection<FurnitureFileForDownload> FurnitureFileForDownload { get; set; }
        public ICollection<FurnitureDimensionImages> FurnitureDimensionImages { get; set; }
        public FurnitureCareReturnPolicy FurnitureCareReturnPolicy { get; set; }
        public string ProductDetails { get; set; }
        public string DimesionItem { get; set; }
        public string Care { get; set; }
        public string ReturnPolicy { get; set; }
        public string Warranty { get; set; }

    }

    public class ProductDetail
    {
        public DesignInfo Design { get; set; }
        public ICollection<ItemDetailsForProduct> Items { get; set; }
        public ICollection<MultipleImages> Images { get; set; }
    }

    public class Products
    {
        public List<ProductDetail> productlist;
    }

    public class FurnitureProductDetail
    {
        public string Material { get; set; }
        public string AssemblyRequired { get; set; }
        public string ColorName { get; set; }
        public string CategoryName { get; set; }
        public string StandardCode { get; set; }
        public string ItemCode { get; set; }
        public decimal Length { get; set; }
        public decimal Width { get; set; }
        public decimal Height { get; set; }
        public string FabricType { get; set; }
        public string Style { get; set; }
        public string LegMaterial { get; set; }
        public string ArmStyle { get; set; }
        public string CushionMaterial { get; set; }
        public string ProductFeatures { get; set; }
        public string LongDescription { get; set; }
        public string FurnitureCare { get; set; }
        public string ReturnPolicy { get; set; }
        public string FAQ { get; set; }
        public string Quality { get; set; }
        public string Warranty { get; set; }
        public string Name { get; set; }
        public string Series { get; set; }
        public string PrintType { get; set; }
        public string Type { get; set; }
        public string Weight { get; set; }
        public string Customization { get; set; }
    }

    public class FurnitureFileForDownload
    {
        public string FileName { get; set; }
        public string FileDescription { get; set; }
    }

    public class FurnitureDimensionImages
    {
        public string CategoryName { get; set; }
        public int CategoryId { get; set; }
        public string Large { get; set; }
        public string ItemCode { get; set; }
    }

    public class FurnitureCareReturnPolicy
    {
        public string Care { get; set; }
        public string ReturnPolicy { get; set; }
        public string FAQ { get; set; }
        public string Warranty { get; set; }
    }


    public class PhotoServices
    {
        public string SecureUrl { get; set; }
        public string Tags { get; set; }
        public bool IsSpace { get; set; }
        public string LargeImage { get; set; }
    }

    public class SimilarContentByContentId
    {
        public string Title { get; set; }
        public string Url { get; set; }
        public string ImageUrl { get; set; }
        public string ShortDescription { get; set; }
        public decimal MaxMrpPrice { get; set; }
        public decimal MinMrpPrice { get; set; }
        public decimal MaxRetailPrice { get; set; }
        public decimal MinRetailPrice { get; set; }
        public int ContentType { get; set; }
        public string ProductType { get; set; }
        public int Priority { get; set; }
        public int ContentId { get; set; }
        public int Cnt { get; set; }
    }

    public class ProductStandardColor
    {
        public string ColorName { get; set; }
        public string StandardCode { get; set; }
        public string Height { get; set; }
        public string CatName { get; set; }
    }

    public class ItemDetailsForProduct
    {
        public string StandardCode { get; set; }
        public int ItemID { get; set; }
        public string ItemCode { get; set; }
        public string CategoryName { get; set; }
        public int Quantity { get; set; }
        public int MRP { get; set; }
        public int RetailPrice { get; set; }
        public string ItemDescription { get; set; }
        public string Discount { get; set; }
        public bool CoD { get; set; }
        public string MapCategory { get; set; }
        public string SubClassName { get; set; }
        public int ContentId { get; set; }
        public bool IsPersonlize { get; set; }
        public bool IsBathRobepersonlize { get; set; }
        public int PersonlizeCharLimit { get; set; }
        public int PersonlizeFrontCharLimit { get; set; }
        public string ImageName { get; set; }
        public int itemSet { get; set; }
        public bool IsPublished { get; set; }

    }


    public class ProductStandardDetail
    {
        public ICollection<BundleItemJson> objBundleItemJson;
        public string OneLiner { get; set; }
        public string Brand { get; set; }
        public string ImageUrl { get; set; }
        public int ContentType { get; set; }
        public string Material { get; set; }
        public string Description { get; set; }
        public string ShipDays { get; set; }
        public string AssemblyRequired { get; set; }
        public string ProductFeatures { get; set; }
        public string LongDescription { get; set; }
        public string ReturnPolicy { get; set; }
        public string Warranty { get; set; }
        public string VideoContent { get; set; }
        public int IsPersonlize { get; set; }
        public int Isbathrobepersonlize { get; set; }
        public ProductZodiacDetail ProductZodiacDetail { get; set; }
        public ICollection<BundleItemJson> BundleItemJson
        { get { return objBundleItemJson; } set { objBundleItemJson = value; } }
        public int IsCombo { get; set; }
        public int IsWithoutCombo { get; set; }
        public bool IsSpace { get; set; }
        public int DeliveryDays { get; set; }
        public int ManufacturingTime { get; set; }
        public int UIType { get; set; }
        public string Care { get; set; }
        public bool IsPublished { get; set; }
        public string SoldBy { get; set; }
        public string ParentCategory { get; set; }

    }

    public class ProductZodiacDetail
    {
        public int BundleQty { get; set; }
        public int IsZodiac { get; set; }
    }

    public class BundleItemJson
    {
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
    }

    public class MultipleImages
    {
        public string ImageName { get; set; }
        public string LargeImage { get; set; }
        public string ThumbImage { get; set; }
    }

    public class ReturnPolicy
    {
        public List<Policy> Policy { get; set; }
    }

    public class Policy
    {
        public string PolicyName { get; set; }
    }

    public class ProductDetails
    {
        public string Care { get; set; }
        public string OneLineSummary { get; set; }
        public string ProductDescription { get; set; }
        public string Material { get; set; }
        public string Mrp { get; set; }
        public string RetailPrice { get; set; }

    }

    public class Features
    {
        public string Feature { get; set; }
    }

    public class ProductFeatures
    {
        public List<Features> Feature { get; set; }
    }

    public class Seatings
    {
        public string Sku { get; set; }
        public string SeatingTypeName { get; set; }
        public string SizeName { get; set; }
        public string Dimension { get; set; }
        public string ThreadCountInside { get; set; }
    }


    public class ProductSeatings
    {
        public List<Seatings> Seating { get; set; }
    }

    public class ProductDimensionUrls
    {
        public string ItemCode { get; set; }
        public string ThumbImageUrl { get; set; }
        public string LargeImageUrl { get; set; }
        public string ImageTitle { get; set; }
        public string Class { get; set; }
    }

    public class CodAndDelivery
    {
        public bool IsDelivery { get; set; }
        public bool IsCod { get; set; }
    }

    public class DesignInfo
    {
        public int ContentId { get; set; }
        public string StandardCode { get; set; }
        public string OneLiner { get; set; }
        public string Brand { get; set; }
        public string ImageUrl { get; set; }
        public int ContentType { get; set; }
        public string Material { get; set; }
        public string Description { get; set; }
        public string ShipDays { get; set; }
        public string AssemblyRequired { get; set; }
        public string ProductFeatures { get; set; }
        public string LongDescription { get; set; }
        public string ReturnPolicy { get; set; }
        public string Warranty { get; set; }
        public int IsPersonlize { get; set; }
        public int Isbathrobepersonlize { get; set; }
        public int IsCombo { get; set; }
        public int IsWithoutCombo { get; set; }
        public bool IsSpace { get; set; }
        public int DeliveryDays { get; set; }
        public int UIType { get; set; }
        public string Care { get; set; }
        public bool IsPublished { get; set; }
    }

    public class ProductCode
    {
        public int StandardDesignId { get; set; }
        public int UITypeId { get; set; }
        public string colorname { get; set; }
        public string ParentCategory { get; set; }
        public string Categoryname { get; set; }
        public string Onelinesummary { get; set; }
        public string CollectionName { get; set; }
        public string subclassname { get; set; }
    }

    public class ProductFeed
    {
        public string standardCode { get; set; }
        public string l1 { get; set; }
        //public string l2 { get; set; }
        public string link { get; set; }
        public int UITypeId { get; set; }
        public string oneliner { get; set; }
        public string brand { get; set; }
        public string description { get; set; }
        public string color { get; set; }
        public string care { get; set; }
        public string material { get; set; }
        public Varients varients { get; set; }
        public string returnPolicy { get; set; }

        public string name { get; set; }
        public string series { get; set; }
        public string printType { get; set; }
        public string type { get; set; }
        public string weight { get; set; }

        public string style { get; set; }
        public string legMaterial { get; set; }
        public string armStyle { get; set; }
        public string cushionMaterial { get; set; }

        public string productFeatures { get; set; }
        public string longDescription { get; set; }

        public List<string> images { get; set; }
        public List<FeedDimension> dimensions { get; set; }

    }

    public class Varients
    {
        public List<string> Sizes { get; set; }
        public List<string> colors { get; set; }
    }

    public class ImageDimension
    {
        public List<string> images { get; set; }
        public List<FeedDimension> dimensions { get; set; }
    }

    public class FeedDimension
    {
        public string SKU { get; set; }
        public string Type { get; set; }
        public string ThreadCount { get; set; }
        public string Description { get; set; }
        public int Length { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
    }


   
    public class StandardDesignStatus
    {
        public string StandardCode { get; set; }
        public bool IsPublish { get; set; }
    }

}