﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ecommerce.Entities
{
    public class RazorPay
    {
        public string key { get; set; }
        public decimal amount { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string image { get; set; }
        public string handler { get; set; }
        public prefill prefill { get; set; }
        public notes notes { get; set; }
        public theme theme { get; set; }
        public string order_id { get; set; }
    }

    public class prefill
    {
        public string name { get; set; }
        public string email { get; set; }
        public string contact { get; set; }
    }

    public class notes
    {
        public string address { get; set; }
        public string merchant_order_id { get; set; }
    }

    public class theme
    {
        public string color { get; set; }
    }

    public class RazorPayPayment
    {
        public string id { get; set; }
        public string entity { get; set; }
        public int amount { get; set; }
        public string currency { get; set; }
        public string status { get; set; }
        public string method { get; set; }
        public string description { get; set; }
        public string refund_status { get; set; }
        public int amount_refunded { get; set; }
        public string email { get; set; }
        public string contact { get; set; }
        public notes notes { get; set; }
        /*public int fee { get; set; }
        public int service_tax { get; set; }*/
        public string error_code { get; set; }
        public string error_description { get; set; }
        public string created_at { get; set; }
    }
}