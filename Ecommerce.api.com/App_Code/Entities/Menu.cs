﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ecommerce.Entities
{    
    public class MenuBed
    {
        public List<MenuParentCategories> menuParentCategories { get; set; }
        public List<MenuSubCategories> menuSubCategories { get; set; }
    }

    public class MenuBath
    {
        public List<MenuParentCategories> menuParentCategories { get; set; }
        public List<MenuSubCategories> menuSubCategories { get; set; }
    }

    public class MenuKids
    {
        public List<MenuParentCategories> menuParentCategories { get; set; }
        public List<MenuSubCategories> menuSubCategories { get; set; }
    }

    public class Menukitchen
    {
        public List<MenuParentCategories> menuParentCategories { get; set; }
        public List<MenuSubCategories> menuSubCategories { get; set; }
    }

    public class MenuDecor
    {
        public List<MenuParentCategories> menuParentCategories { get; set; }
        public List<MenuSubCategories> menuSubCategories { get; set; }
    }

    public class MenuFurniture
    {
        public List<MenuParentCategories> menuParentCategories { get; set; }
        public List<MenuSubCategories> menuSubCategories { get; set; }
    }

    public class MenuParentCategories
    {
        public string SubClassName { get; set; }
        public string CategoryName { get; set; }
    }

    public class MenuSubCategories
    {      
        public string ParentCategoryName { get; set; }
        public string CategoryName { get; set; }
    }

    public class HomePageMenu
    {
        public MenuBed menuBed { get; set; }
        public MenuBath menuBath { get; set; }
        public MenuKids menuKids { get; set; }
        public Menukitchen menukitchen { get; set; }
        public MenuDecor menuDecor { get; set; }
        public MenuFurniture menuFurniture { get; set; }
    }

    public class ProductUiType
    {
        public int UiType { get; set; }
        public bool IsPublished { get; set; }
    }
    public class HomeMenuList
    {
        public List<MainCategory> mainCategory { get; set; }
        public List<ParentCategory> parentCategory { get; set; }
        public List<DisplayCategory> displayCategory { get; set; }

    }
    public class MainCategory
    {
        public string MainCategoryName { get; set; }
        public string SearchMainCategoryName { get; set; }
    }
    public class ParentCategory
    {
        public string MainCategoryName { get; set; }
        public string ParentCategoryName { get; set; }
        public string SearchParentCategoryName { get; set; }
    }
    public class DisplayCategory
    {
        public string MainCategoryName { get; set; }
        public string ParentCategoryName { get; set; }
        public string DisplayCategoryName { get; set; }
        public string SearchDisplayCategoryName { get; set; }
    }
   
}