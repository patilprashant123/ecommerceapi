﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ecommerce.Entities
{
    public class OrderEntity
    {
        public List<OrderInfo> OrderInfo;
        public OrderCharges OrderCharges;
    }

    public class OrderInfo
    {
        public string Name { get; set; }
        public string ShippingAddress { get; set; }
        public string ShippingState { get; set; }
        public string ShippingCity { get; set; }
        public string ShippingPostCode { get; set; }
        public int OrderItemID { get; set; }
        public int OrderId { get; set; }
        public string ItemCode { get; set; }
        public string Description { get; set; }
        public decimal MRP { get; set; }
        public int Quantity { get; set; }
        public int CategoryId { get; set; }
        public string ItemName { get; set; }
        public decimal RetailPrice { get; set; }
        public string StandardCode { get; set; }
        public int GiftWrapPrice { get; set; }
        public string IsOfferItem { get; set; }
        public decimal Amount { get; set; }
        public decimal GiftAmount { get; set; }
        public string CategoryName { get; set; }
        public string IsDiscountItem { get; set; }
        public string Medium { get; set; }
        public string PersonlizeText { get; set; }
        public string PersonlizebackInitial { get; set; }
        public string OneLineSummary { get; set; }
        public bool IsbathRobePersonlize { get; set; }
        public bool IsPersonlize { get; set; }
        public string DesignOverview { get; set; }
        public string CollectionName { get; set; }
        public string SubClassName { get; set; }
        public decimal CValue { get; set; }
        public string BillingCity { get; set; }
        public string BillingCountry { get; set; }
        public string OrderSourceId { get; set; }
        public decimal BillAmount { get; set; }
        public string ShipInDays { get; set; }
        public bool isBundleItem { get; set; }
        public string ColorName { get; set; }
        public string MobileNo { get; set; }
        public string OrderDate { get; set; }
        public string OrderStatus { get; set; }
    }

    public class OrderCharges
    {
        public string BillAmount { get; set; }
        public string DiscountCouponName { get; set; }
        public string DiscountCouponValue { get; set; }
        public string ShippingCharge { get; set; }
        public string total_cart_value { get; set; }
        public string total_purchases { get; set; }
    }

    public class Order
    {
        public int orderId { get; set; }
        public string orderNo { get; set; }
        public DateTime orderDate { get; set; }
        public string billAmount { get; set; }
        public string description { get; set; }
        public List<OrderItem> orderItems { get; set; }
    }

    public class OrderItem
    {
        public int orderId { get; set; }
        public string description { get; set; }
    }
}