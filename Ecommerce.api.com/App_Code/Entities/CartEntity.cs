﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ecommerce.Entities
{    
    public class CartEntity
    {
        public string StanderedCode { get; set; }
        public string ItemCode { get; set; }
        public int Qty { get; set; }
        public bool GiftWrap { get; set; }
        public decimal GiftPrice { get; set; }
        public string Personalize { get; set; }
        public string Front { get; set; }
        public string Back { get; set; }
        public bool IsNotify { get; set; }
        public string BundleItemCode { get; set; }
        public bool IsBundleItem { get; set; }
        public string ProductType { get; set; }
        public string PaintType { get; set; }
        public string SessionId { get; set; }
        public int CustomerId { get; set; }
    }

    public class RootObject
    {
        public List<CartEntity> CartItems { get; set; }
    }

    public class CartItem
    {
        public string CartId { get; set; }
    }

    public class SyncCart
    {
        public int CustomerId { get; set; }
        public string SessionId { get; set; }
        public bool isGuest { get; set; }
    }
}