﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Collections.Generic;
using Ecommerce.conn;
using Ecommerce.ErrorLog;
using Ecommerce.Entities;
using Uniware;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;




/// <summary>
/// Summary description for AmazonS3
/// </summary>

public class AmazonS3: Connection
{   
    public string ErrorMsg { get; set; }
    ErrorLogger oErrorLog = new ErrorLogger();
    public bool UploadCropImageFileOnAmazon(string Filepath, string StandardCode, string uplfilename)
    {
        bool status = false;
        try
        {
            IAmazonS3 client;
            string BucketKey = string.Empty;
            using (client = AWSClientFactory.CreateAmazonS3Client())
            {
                BucketKey = "bbmproductcropimages/" + StandardCode + "/" + uplfilename;
                PutObjectRequest request = new PutObjectRequest();
                request.BucketName = "ot-product-images";
                request.CannedACL = S3CannedACL.PublicRead;

                request.Key = BucketKey;
                request.FilePath = Filepath;
                PutObjectResponse response = client.PutObject(request);
                if (response.HttpStatusCode == System.Net.HttpStatusCode.OK)
                    status = true;
                else
                    status = false;
            }
            return status;
        }
        catch (Exception ex)
        {
            ErrorMsg = Convert.ToString(ex.Message);
        }
        return status;
    }
    public string CropImage(string path, int X, int Y, int Width, int Height, string FileName, string StandardCode)
    {
        string cropImagePath = string.Empty;
        using (System.Drawing.Image img = System.Drawing.Image.FromFile(path))
        {
            string ImgName = System.IO.Path.GetFileName(path);
            using (Bitmap bmpCropped = new Bitmap(Width, Height))
            {
                using (Graphics g = Graphics.FromImage(bmpCropped))
                {

                    Rectangle rectDestination = new Rectangle(0, 0, bmpCropped.Width, bmpCropped.Height);
                    Rectangle rectCropArea = new Rectangle(X, Y, Width, Height);
                    g.DrawImage(img, rectDestination, rectCropArea, GraphicsUnit.Pixel);

                    if (!Directory.Exists(Path.Combine(WebSiteConfig.CropImagePath, StandardCode)))
                    {
                        Directory.CreateDirectory(Path.Combine(WebSiteConfig.CropImagePath, StandardCode));
                    }

                    cropImagePath = Path.Combine(WebSiteConfig.CropImagePath, StandardCode) + "/" + FileName + ".jpg";
                    bmpCropped.Save(cropImagePath);


                }
            }
        }
        return cropImagePath;
    }
    public void Crop_UpdateImageUploadOnAmazon(string Id, string StandardCode)
    {
        SqlCommand sqlCmd = new SqlCommand();
        try
        {
            OpenSQLConnection();
            sqlCmd = new SqlCommand("Crop_UpdateAmazonImageUrl", oConnect);
            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlCmd.Parameters.Add("@Id", SqlDbType.VarChar).Value = Id;
            sqlCmd.Parameters.Add("@StandardCode", SqlDbType.VarChar).Value = StandardCode;
            sqlCmd.ExecuteNonQuery();
        }
        catch (Exception ex)
        {
            oErrorLog.Open();
            oErrorLog.Log("Crop_UpdateImageUploadOnAmazon in AmazonS3" + ex.Message);
            oErrorLog.Flush();
            oErrorLog.Close();
        }
        finally
        {
            sqlCmd.Dispose();
            sqlCmd = null;

            CloseSQLConnection();
        }
    }
}
