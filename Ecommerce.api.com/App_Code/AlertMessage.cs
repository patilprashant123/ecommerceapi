using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using Ecommerce.conn;

/// <summary>
/// Summary description for AlertMessage
/// </summary>

public class AlertMessage : Connection
{
    #region Class Variables
    
    StringBuilder m_Message = new StringBuilder(500);
    StringBuilder m_Subject = new StringBuilder(100);
    public class Status
    {
        public string Msg { get; set; }
        public string MyName { get; set; }
        // public int CmdStatus { get; set; }

    }
    CustomDataTypes.EmailType m_MessageId;

    string m_FromEmail = string.Empty;
    string m_ToEmail = string.Empty;

    string m_TaxAmt = string.Empty;
    string m_ShippingHandlingCharges = string.Empty;
    string m_OrderAmt = string.Empty;
    string m_StoreCredit = string.Empty;
    string m_SubTotal = string.Empty;
    string m_EmailLink = string.Empty;
    #endregion

    #region Class Properties
   

    public static string HostServerUrl
    {
        get { return ConfigurationManager.AppSettings["HostServerUrl"].ToString(); }
    }

    public string MessageString
    {
        set { m_Message.Append(value); }
        get { return m_Message.ToString(); }
    }

    public string Subject
    {
        set { m_Subject.Append(value); }
        get { return m_Subject.ToString(); }
    }

    public string FromEmail
    {
        set { m_FromEmail = value; }
        get { return m_FromEmail; }
    }

    public string ToEmail
    {
        set { m_ToEmail = value; }
        get { return m_ToEmail; }
    }

    public string EmailFriendLink
    {
        set { m_EmailLink = value; }
        get { return m_EmailLink; }
    }

    private CustomDataTypes.EmailType MessageId
    {
        set { m_MessageId = value; }
        get { return m_MessageId; }
    }

    #endregion

    #region Class Constructor
    public AlertMessage()
    {

    }
    public AlertMessage(CustomDataTypes.EmailType MessageType)
    {
        MessageId = MessageType;
        GetMessageFromDb(MessageId);
    }
    #endregion

    #region Tag Parsing
    public void ParseEmailTag(string Email)
    {
        m_Message.Replace("{Email}", Email);
    }
    public void ParseNameTag(string Email)
    {
        m_Message.Replace("{Name}", Email);
    }
    public void ParseWIshListNameTag(string Email)
    {
        m_Message.Replace("{WishListName}", Email);
    }
    public void ParseWishListUrl(string Email)
    {
        m_Message.Replace("{WishListUrl}", Email);
    }
    public void ParseFromEmailTag(string Email)
    {
        m_Message.Replace("{FromEmail}", Email);
    }
    public void ParseFirstNameTag(string Email)
    {
        m_Message.Replace("{FirstName}", Email);
    }
    public void ParseLastNameTag(string Email)
    {
        m_Message.Replace("{FirstName}", Email);
    }
    public void ParseIdTag(string Email)
    {
        m_Message.Replace("{Id}", Email);
    }
    public void ParseUserIdTag(string Email)
    {
        m_Message.Replace("{UserId}", Email);
    }
    public void ParseHostServerUrlTag(string strHostServerUrl)
    {
        m_Message.Replace("{HostServerUrl}", strHostServerUrl);
    }
    public void ParseHostServerUrlTag()
    {
        m_Message.Replace("{HostServerUrl}", HostServerUrl);
    }
    public void ParseEmailLink(string EmailLink)
    {
        m_Message.Replace("{ForwardLink}", EmailLink);
    }
    public void ParseImageUrl(string ImageUrl)
    {
        m_Message.Replace("{Image_Url}", ImageUrl);
    }
    public void ParseCustomTag(string CustomTag, string Value)
    {
        m_Message.Replace(CustomTag, Value);
    }
    public void ParseCustomTagSubject(string CustomTag, string Value)
    {
        m_Subject.Replace(CustomTag, Value);
    }

    public void ParsePasswordTag(string Email)
    {
        m_Message.Replace("{Password}", Email);
    }
    #endregion

    #region User Functions
    public void GetMessageFromDb(CustomDataTypes.EmailType MessageType)
    {

        SqlDataReader dr = null;
        SqlConnection cn = GetConnectionObject();
        try
        {
            SqlCommand cmd = new SqlCommand("GetEmailMessage", cn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@MessageType", SqlDbType.Int).Value = (int)MessageType;
            dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);

            if (dr.Read())
            {
                MessageString = dr.GetString(dr.GetOrdinal("Message"));
                FromEmail = dr.GetString(dr.GetOrdinal("FromEmail"));
                ToEmail = dr.GetString(dr.GetOrdinal("ToEmail"));
                Subject = dr.GetString(dr.GetOrdinal("Subject"));
            }
            dr.Close();
        }
        finally
        {
            CloseConnectionObject(ref cn);
        }
    }

    public DataSet ListAlertMessage()
    {
        SqlConnection cn = GetConnectionObject();
        DataSet ds = new DataSet();
        try
        {
            string query = "SELECT * FROM EmailMessage A Order By MessageId";
            SqlCommand cmd = new SqlCommand(query, cn);
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;
            da.Fill(ds);
            return ds;
        }
        catch (Exception objException)
        {
            throw (objException);
        }
        finally
        {
            CloseConnectionObject(ref cn);
        }

    }
    public SqlDataReader GetEmail(string Email)
    {
        SqlDataReader dr = null;

        try
        {
            OpenSQLConnection();
            string query = "SELECT * FROM WebUsers WHERE  Email=@Email AND ISNULL(IsGuestUser,0)=0";
            SqlCommand cmd = new SqlCommand(query, oConnect);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.Add("@Email", SqlDbType.VarChar).Value = Email;
            dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
        }
        finally
        {

        }
        return dr;
    }
    public void UpdateAlertMesssage(string MessageId, string Description, string Subject, string Message, string FromEmail, string ToEmail)
    {
        SqlConnection cn = GetConnectionObject();
        string query = "UPDATE EmailMessage SET Description=@Description,Subject=@Subject,Message=@Message,FromEmail=@FromEmail,ToEmail=@ToEmail WHERE MessageId=@MessageId";
        SqlCommand cmd = new SqlCommand(query, cn);
        cmd.Parameters.Add("@Description", SqlDbType.VarChar).Value = Description;
        cmd.Parameters.Add("@Subject", SqlDbType.VarChar).Value = Subject;
        cmd.Parameters.Add("@Message", SqlDbType.VarChar).Value = Message;
        cmd.Parameters.Add("@FromEmail", SqlDbType.VarChar).Value = FromEmail;
        cmd.Parameters.Add("@ToEmail", SqlDbType.VarChar).Value = ToEmail;
        cmd.Parameters.Add("@MessageId", SqlDbType.VarChar).Value = MessageId;
        cmd.ExecuteNonQuery();
        CloseConnectionObject(ref cn);

    }

    public int InsertResetPasswordLog(string linkId, string EmailTo, string IP, string UserAgent)
    {
        StringBuilder sbQuery = new StringBuilder();

        int Count = 0;

        try
        {
            OpenSQLConnection();

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandText = "InsertResetPasswordLog";
            sqlCmd.Connection = oConnect;
            sqlCmd.CommandType = CommandType.StoredProcedure;

            sqlCmd.Parameters.Add("@LinkId", SqlDbType.VarChar).Value = linkId;
            sqlCmd.Parameters.Add("@Email", SqlDbType.VarChar).Value = EmailTo;
            sqlCmd.Parameters.Add("@IP", SqlDbType.VarChar).Value = IP;
            sqlCmd.Parameters.Add("@UserAgent", SqlDbType.VarChar).Value = UserAgent;

            Count = Convert.ToInt32(sqlCmd.ExecuteNonQuery());


        }
        catch (Exception objExeption)
        {
            Count = 0;
            throw (objExeption);
        }
        finally
        {
            CloseSQLConnection();
        }

        return Count;
    }

    public string GetUserByLinkId(string LinkId)
    {
        StringBuilder sbQuery = new StringBuilder();
        Status S = new Status();
        string CmdStatus = "0";

        try
        {
            OpenSQLConnection();

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandText = "GetUserByLinkId";
            sqlCmd.Connection = oConnect;
            sqlCmd.CommandType = CommandType.StoredProcedure;

            sqlCmd.Parameters.Add("@LinkId", SqlDbType.VarChar).Value = LinkId;
            sqlCmd.Parameters.Add("@UserName", SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
            sqlCmd.Parameters.Add("@CmdStatus", SqlDbType.Int).Direction = ParameterDirection.Output;
            sqlCmd.Parameters.Add("@Msg", SqlDbType.VarChar, 200).Direction = ParameterDirection.Output;

            sqlCmd.ExecuteNonQuery();
            Object o = sqlCmd.Parameters["@CmdStatus"].Value;

            if (o != null)
            {
                CmdStatus = o.ToString();
            }
            S.Msg = (string)sqlCmd.Parameters["@Msg"].Value;

            o = sqlCmd.Parameters["@UserName"].Value;
            if (o != null)
            {
                S.MyName = o.ToString();
                CmdStatus = o.ToString();
            }


        }
        catch (Exception objExeption)
        {

            throw (objExeption);
        }
        finally
        {
            CloseSQLConnection();
        }

        return CmdStatus;



    }

    #endregion

  

    

    


}
