using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

/// <summary>
/// Summary description for UserSession
/// </summary>
public static class UserSession
{
    public static string PassKey = "kAdNB6tsP2l4sA==";
    public static string PassChars = "01234AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz56789";
    public static string CustomerName = "";
    public static int CustomerId = 0;


    public static string SessionId
    {
        set { SetCookie("sid", value); }
        get
        {

            if (string.IsNullOrEmpty(GetCookie("sid")))
            {
                Guid guidSessionId = Guid.NewGuid();
                SetCookie("sid", guidSessionId.ToString());
                return guidSessionId.ToString().ToUpper();
            }

            return GetCookie("sid");
        }
    }

    public static bool ValidateSession()
    {
        int uid = GetCurrentUserId();
        string sid = GetCurrentSessionId();
        if (uid > 0 && !string.IsNullOrEmpty(sid))
            return true;

        return false;

    }   
    public static int GetCurrentCustId()
    {
        int id = 0;
        string uid = GetCookie("Cid");
        if (CString.IsInteger(uid))
        {
            id = int.Parse(uid);
            if (id > 0)
                return id;
            else
                return id;
        }
        return id;
    }
    public static int GetCurrentOrderId()
    {
        int id = 0;
        string uid = GetCookie("Oid");
        if (CString.IsInteger(uid))
        {
            id = int.Parse(uid);
            if (id > 0)
                return id;
            else
                return id;
        }
        return id;
    }
    public static int GetCurrentUserId()
    {
        int id = 0;
        string uid = GetCookie("uid");

        if (CString.IsInteger(uid))
        {
            id = int.Parse(uid);
            if (id > 0)
                return id;
            else
                return id;
        }
        return id;
    }
    public static int IsGuestUser()
    {
        int id = 0;
        string uid = GetCookie("guestUser");
        if (CString.IsInteger(uid))
        {
            id = int.Parse(uid);
            if (id > 0)
                return id;
            else
                return id;
        }
        return id;
    }
    public static string GetCurrentCustEncryptId()
    {
        string id = GetCookie("EncryptCustId");
        return id;
    }
    public static string GetCurrentWebRoleId()
    {
        string id = GetCookie("WebRoleId");
        return id;
    }
    public static string GetCurrentUsernameWeb()
    {
        string id = GetCookie("unWeb");
        return id;
    }
    public static string GetCurrentUsername()
    {
        string id = GetCookie("un");
        return id;
    }
    public static string GetCurrentCustomerName()
    {
        string id = GetCookie("custName");
        return id;
    }
    public static string GetCurrentRoleName()
    {
        string id = GetCookie("rid");
        return id;
    }
    public static string GetGuestUser()
    {
        string id = Convert.ToString(GetCookie("guestUser"));
        return id;
    }
    public static int GetCurrentWebUserId()
    {        
        int id = 0;
        string uid = GetCookie("Webuid");
        if (CString.IsInteger(uid))
        {            
            id = int.Parse(uid);
            if (id > 0)
                return id;
            else
                return id;
        }
        return id;
    }
    public static bool CheckUserLogin()
    {
        int Webuid = GetCurrentWebUserId();
        string sGuestUser = GetGuestUser();
        string sid = GetWebSessionId();


        if (Webuid > 0 && !string.IsNullOrEmpty(sid) && sGuestUser != "1")
            return true;

        return false;

    }
    public static string GetCurrentProjectName()
    {
        string id = GetCookie("pn");
        return id;
    }
    public static int GetCurrentProjectId()
    {
        int id = 0;
        string uid = GetCookie("pId");
        if (CString.IsInteger(uid))
        {
            id = int.Parse(uid);
            if (id > 0)
                return id;
            else
                return id;
        }
        return id;
    }
    public static int GetCurrentCallId()
    {
        int id = 0;
        string uid = GetCookie("Callid");
        if (CString.IsInteger(uid))
        {
            id = int.Parse(uid);
            if (id > 0)
                return id;
            else
                return id;
        }
        return id;
    }
    public static int GetCurrentSourceId()
    {        
        int id = 0;
        string uid = GetCookie("Source");
        if (CString.IsInteger(uid))
        {
            id = int.Parse(uid);
            if (id > 0)
                return id;
            else
                return id;
        }
        return id;
    }
    public static string GetCurrentSessionId()
    {
        string id = GetCookie("SID");
        return id;
    }
    public static string GetWebSessionId()
    {
        return GetCookie("Wid");
    }
   

    public static bool SetCookie(string cookiename, string cookievalue)
    {
        HttpContext.Current.Response.Cookies[cookiename].Value = cookievalue;
        return true;
    }

    public static bool SetCookie(string cookiename, string cookievalue, int iDaysToExpire)
    {
        try
        {
            HttpCookie objCookie = new HttpCookie(cookiename);
            HttpContext.Current.Response.Cookies.Clear();
            HttpContext.Current.Response.Cookies.Add(objCookie);
            objCookie.Values.Add(cookiename, cookievalue);

            DateTime dtExpiry = DateTime.Now.AddDays(iDaysToExpire);
            HttpContext.Current.Response.Cookies[cookiename].Expires = dtExpiry;
        }
        catch (Exception exc)
        {
            throw exc;
        }
        return true;
    }
    public static string GetCookie(string cookiename)
    {
        string val = "";
        if (HttpContext.Current.Request.Cookies[cookiename] != null)
            val = HttpContext.Current.Request.Cookies[cookiename].Value;
        return val;
    }

  
    public static bool GetUserId()
    {
        int uid = Convert.ToInt32(HttpContext.Current.Session["uid"]);

        string sGuestUser = GuestUser();

        if (uid > 0 && sGuestUser == "0")
            return true;

        return false;

    }

    public static string GuestUser()
    {
        string id = Convert.ToString(HttpContext.Current.Session["guestUser"]);
        return id;
    }

}
