using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Collections.Generic;
using Ecommerce.conn;
using Ecommerce.ErrorLog;
using Ecommerce.Entities;
using Uniware;

public class ShoppingCart : Connection
{
    ErrorLogger oErrorLog = new ErrorLogger();

    StringBuilder sbMsg = new StringBuilder();

    public string Message
    {
        set { sbMsg.Append(value); }
        get { return sbMsg.ToString(); }
    }

    public string CartMsg
    {
        set
        {
            sbMsg.Append(value);
        }
        get
        {
            if (sbMsg != null)
                return sbMsg.ToString();

            return string.Empty;
        }
    }

    public bool CheckGuestUser()
    {
        string s = UserSession.GetCookie("guestUser");

        if (!string.IsNullOrEmpty(s) && s.CompareTo("1") == 0)
        {
            return true;
        }
        return false;
    }

    string sRemoteAddress = HttpContext.Current.Request.ServerVariables["remote_addr"].ToString();

    public int SaveUpdateNotifyEMail(string sItemCode, string sEmail)
    {
        int iNotifyId = 0;
        SqlCommand sqlCmd = new SqlCommand();
        try
        {
            OpenSQLConnection();
            sqlCmd = new SqlCommand("[NotifySaveUpdate]", oConnect);
            sqlCmd.CommandType = CommandType.StoredProcedure;

            sqlCmd.Parameters.Add("@Email", SqlDbType.VarChar).Value = sEmail;
            sqlCmd.Parameters.Add("@ItemCode", SqlDbType.VarChar).Value = sItemCode;
            sqlCmd.Parameters.Add("@IpAddress", SqlDbType.VarChar).Value = sRemoteAddress;


            SqlParameter pNotifyId = new SqlParameter("@NotifyId", SqlDbType.Int);
            pNotifyId.Direction = ParameterDirection.Output;
            sqlCmd.Parameters.Add(pNotifyId);


            SqlParameter pErrorMessage = new SqlParameter("@ErrorMessage", SqlDbType.VarChar, 200);
            pErrorMessage.Direction = ParameterDirection.Output;
            sqlCmd.Parameters.Add(pErrorMessage);

            sqlCmd.ExecuteNonQuery();
            iNotifyId = Convert.ToInt32(pNotifyId.Value);

            if (Convert.ToString(pErrorMessage.Value) != "")
                Message = Convert.ToString(pErrorMessage.Value);

        }
        catch (Exception ex)
        {
            Message = ex.Message.ToString();
        }
        finally
        {

            sqlCmd.Dispose();
            sqlCmd = null;

            CloseSQLConnection();
        }
        return iNotifyId;

    }

    public int UpdateAppDesign(FlashSaleProduct objFlashSaleProduct)
    {
        int iNotifyId = 0;
        SqlCommand sqlCmd = new SqlCommand();
        try
        {
            OpenSQLConnection();
            sqlCmd = new SqlCommand("[App_FlashSale_Update]", oConnect);
            sqlCmd.CommandType = CommandType.StoredProcedure;

            sqlCmd.Parameters.Add("@StandardCode", SqlDbType.VarChar).Value = objFlashSaleProduct.StandardCode;
            sqlCmd.Parameters.Add("@ItemCode", SqlDbType.VarChar).Value = objFlashSaleProduct.ItemCode;
            sqlCmd.Parameters.Add("@Mrp", SqlDbType.Decimal).Value = objFlashSaleProduct.Mrp;
            sqlCmd.Parameters.Add("@RetailPrice", SqlDbType.Decimal).Value = objFlashSaleProduct.RetaiPrice;
            sqlCmd.Parameters.Add("@StartDate", SqlDbType.DateTime).Value = objFlashSaleProduct.StartDate;
            sqlCmd.Parameters.Add("@EndDate", SqlDbType.DateTime).Value = objFlashSaleProduct.EndDate;


            SqlParameter pNotifyId = new SqlParameter("@Id", SqlDbType.Int);
            pNotifyId.Direction = ParameterDirection.Output;
            sqlCmd.Parameters.Add(pNotifyId);


            SqlParameter pErrorMessage = new SqlParameter("@ErrorMessage", SqlDbType.VarChar, 200);
            pErrorMessage.Direction = ParameterDirection.Output;
            sqlCmd.Parameters.Add(pErrorMessage);

            sqlCmd.ExecuteNonQuery();
            iNotifyId = Convert.ToInt32(pNotifyId.Value);

            if (Convert.ToString(pErrorMessage.Value) != "")
                Message = Convert.ToString(pErrorMessage.Value);

        }
        catch (Exception ex)
        {
            Message = ex.Message.ToString();
        }
        finally
        {

            sqlCmd.Dispose();
            sqlCmd = null;

            CloseSQLConnection();
        }
        return iNotifyId;

    }
    public int Remove_ItemsFromFlashSale(FlashSaleProduct objFlashSaleProduct)
    {
        int Id = 0;
        SqlCommand sqlCmd = new SqlCommand();
        try
        {
            OpenSQLConnection();
            sqlCmd = new SqlCommand("[App_RemoveUpdateDesign]", oConnect);
            sqlCmd.CommandType = CommandType.StoredProcedure;

            sqlCmd.Parameters.Add("@StandardCode", SqlDbType.VarChar).Value = objFlashSaleProduct.StandardCode;
            sqlCmd.Parameters.Add("@ItemCode", SqlDbType.VarChar).Value = objFlashSaleProduct.ItemCode;

            SqlParameter pNotifyId = new SqlParameter("@Id", SqlDbType.Int);
            pNotifyId.Direction = ParameterDirection.Output;
            sqlCmd.Parameters.Add(pNotifyId);


            SqlParameter pErrorMessage = new SqlParameter("@ErrorMessage", SqlDbType.VarChar, 200);
            pErrorMessage.Direction = ParameterDirection.Output;
            sqlCmd.Parameters.Add(pErrorMessage);

            sqlCmd.ExecuteNonQuery();
            Id = Convert.ToInt32(pNotifyId.Value);

            if (Convert.ToString(pErrorMessage.Value) != "")
                Message = Convert.ToString(pErrorMessage.Value);

        }
        catch (Exception ex)
        {
            Message = ex.Message.ToString();
        }
        finally
        {

            sqlCmd.Dispose();
            sqlCmd = null;

            CloseSQLConnection();
        }
        return Id;

    }

    public void UpdateCartCustomer(int CustomerId, string SessionId)
    {
        OpenSQLConnection();
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = oConnect;
        cmd.CommandText = "V05_UpdateCartCust_AfterSync";
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.Add("@SessionId", SqlDbType.VarChar, 100).Value = SessionId;

        int iCustomerId = CustomerId;

        cmd.Parameters.Add("@CustomerId", SqlDbType.VarChar, 100).Value = iCustomerId;

        cmd.ExecuteNonQuery();

        CloseSQLConnection();

    }

    public void UpdateOrderPaymentAndStatus(string OrderNo, int OrderStatus, int PaymentStatus)
    {
        try
        {
            OpenSQLConnection();
            string sql = "[UpdateOrderPaymentAndStatus]";
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = sql;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = oConnect;
            cmd.Parameters.Add("@OrderNo", SqlDbType.VarChar).Value = OrderNo;
            cmd.Parameters.Add("@OrderStatusId", SqlDbType.BigInt).Value = OrderStatus;
            cmd.Parameters.Add("@PaymentStatusId", SqlDbType.Int).Value = PaymentStatus;
            cmd.ExecuteNonQuery();
        }
        catch (Exception exc)
        {
            throw exc;
        }
        finally
        {
            CloseSQLConnection();
        }
    }

    public void ClearShoppingCart(string SessionId)
    {
        try
        {
            OpenSQLConnection();
            string sql = "[ClearShoppingCart]";
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = sql;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = oConnect;
            cmd.Parameters.Add("@SessionId", SqlDbType.VarChar).Value = SessionId;
            cmd.ExecuteNonQuery();
            CloseSQLConnection();
        }
        catch (Exception exc)
        {
            throw exc;
        }
        finally
        {
            CloseSQLConnection();
        }
    }

    public DataSet GetCustomerData(string OrderId)
    {
        SqlCommand sqlCmd = new SqlCommand();
        SqlDataAdapter dtAdptr = new SqlDataAdapter(sqlCmd);
        DataSet dsDataSet = new DataSet();
        try
        {
            OpenSQLConnection();
            sqlCmd = new SqlCommand("GetCustomerDetails", oConnect);
            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlCmd.Parameters.Add("@OrderNo", SqlDbType.VarChar).Value = OrderId;
            dtAdptr = new SqlDataAdapter(sqlCmd);
            dsDataSet = new DataSet();
            dtAdptr.Fill(dsDataSet);
        }
        catch (Exception ex)
        {
            oErrorLog.Open();
            oErrorLog.Log("GetCartItems Function In ShoppingCart.cs:" + ex.Message);
            oErrorLog.Flush();
            oErrorLog.Close();
        }
        finally
        {
            dtAdptr.Dispose();
            dtAdptr = null;
            sqlCmd.Dispose();
            sqlCmd = null;
            CloseSQLConnection();
        }
        return dsDataSet;
    }
    public int ZipDialCodeStatus(string OrderID, string Token, string MobilNo, string duration, string countryCode, string amount, string pincode, string transaction_token, string status, string IPAdress, string UserAgent)
    {
        int suceess = 0;
        SqlCommand cmd = new SqlCommand();
        try
        {
            OpenSQLConnection();
            cmd.CommandText = "[ZipDialLog_Staus]";
            cmd.Connection = oConnect;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@OrderNo", SqlDbType.VarChar, 500).Value = OrderID;
            cmd.Parameters.Add("@customerToken", SqlDbType.VarChar, 500).Value = Token;
            cmd.Parameters.Add("@MobileNo", SqlDbType.VarChar, 500).Value = MobilNo;
            cmd.Parameters.Add("@duration", SqlDbType.VarChar, 500).Value = duration;
            cmd.Parameters.Add("@countryCode", SqlDbType.VarChar, 500).Value = countryCode;
            cmd.Parameters.Add("@amount", SqlDbType.VarChar, 500).Value = amount;
            cmd.Parameters.Add("@pincode", SqlDbType.VarChar, 500).Value = pincode;
            cmd.Parameters.Add("@transactionToken", SqlDbType.VarChar, 500).Value = transaction_token;
            cmd.Parameters.Add("@status", SqlDbType.VarChar, 500).Value = status;
            cmd.Parameters.Add("@IPAddress", SqlDbType.VarChar, 500).Value = IPAdress;
            cmd.Parameters.Add("@userAgent", SqlDbType.VarChar, 500).Value = UserAgent;
            cmd.Parameters.Add("@Date", SqlDbType.VarChar, 500).Value = DateTime.Now;
            suceess = cmd.ExecuteNonQuery();
            CloseSQLConnection();
        }
        catch (Exception ex)
        {
            CartMsg = ex.Message.ToString();
            oErrorLog.Open();
            oErrorLog.Log("UpdateCartCoupon Function In ShoppingCart.cs:" + ex.Message);
            oErrorLog.Flush();
            oErrorLog.Close();
        }
        finally
        {
            CloseSQLConnection();
            cmd.Dispose();
        }
        return suceess;
    }

    public string GetEmail(string sOrderId)
    {
        string SessionId = UserSession.GetWebSessionId();
        int CustomerId = UserSession.GetCurrentCustId();
        OpenSQLConnection();
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = oConnect;
        cmd.CommandText = "Select C.Email from Orders O inner Join Customer C on O.CustomerId=C.CustomerId  where O.OrderNo=@OrderId ";
        cmd.CommandType = CommandType.Text;
        cmd.Parameters.Add("@OrderId", SqlDbType.VarChar).Value = sOrderId;
        object obj = cmd.ExecuteScalar();
        CloseSQLConnection();
        if (obj != null)
        {
            return obj.ToString();
        }
        else
            return string.Empty;

        // GetSubTotal();


    }

    public int ConfirmAndPushToUniware(string OrderNo, string StatusId, string sPaymentStatusId)
    {
        int suceess = 0;
        SqlCommand cmd = new SqlCommand();
        try
        {
            /*  OpenSQLConnection();
              cmd.CommandText = "[UpdateOrderPaymentAndStatus]";
              cmd.Connection = oConnect;
              cmd.CommandType = CommandType.StoredProcedure;
              cmd.Parameters.Add("@OrderNo", SqlDbType.VarChar, 500).Value = OrderNo;
              cmd.Parameters.Add("@OrderStatusId", SqlDbType.VarChar, 500).Value = StatusId;
              cmd.Parameters.Add("@PaymentStatusId", SqlDbType.VarChar, 500).Value = sPaymentStatusId;


              suceess = cmd.ExecuteNonQuery();
              CloseSQLConnection();
              */
            UpdateOrderStatus(OrderNo, StatusId, sPaymentStatusId);

            UniComInfo oUniCom = new UniComInfo();
            bool IsSuccess = oUniCom.CreateUpdateOrder(OrderNo);


        }
        catch (Exception ex)
        {
            CartMsg = ex.Message.ToString();
            oErrorLog.Open();
            oErrorLog.Log("ConfirmAndPushToUniware Function In ShoppingCart.cs:" + ex.Message);
            oErrorLog.Flush();
            oErrorLog.Close();
        }
        finally
        {
            CloseSQLConnection();
            cmd.Dispose();
        }
        return suceess;
    }

    public int UpdateOrderStatus(string OrderNo, string StatusId, string sPaymentStatusId)
    {
        int suceess = 0;
        SqlCommand cmd = new SqlCommand();
        try
        {
            OpenSQLConnection();
            cmd.CommandText = "[UpdateOrderPaymentAndStatus]";
            cmd.Connection = oConnect;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@OrderNo", SqlDbType.VarChar, 500).Value = OrderNo;
            cmd.Parameters.Add("@OrderStatusId", SqlDbType.VarChar, 500).Value = StatusId;
            cmd.Parameters.Add("@PaymentStatusId", SqlDbType.VarChar, 500).Value = sPaymentStatusId;
            suceess = cmd.ExecuteNonQuery();
            CloseSQLConnection();
        }
        catch (Exception ex)
        {
            CartMsg = ex.Message.ToString();
            oErrorLog.Open();
            oErrorLog.Log("UpdateOrderStatus Function In ShoppingCart.cs:" + ex.Message);
            oErrorLog.Flush();
            oErrorLog.Close();
        }
        finally
        {
            CloseSQLConnection();
            cmd.Dispose();
        }
        return suceess;
    }

    public int UpdateOrderStatus_Exotel(string OrderNo, string StatusId, string sPaymentStatusId, string sCallSid, string sCallStatus)
    {
        int suceess = 0;
        SqlCommand cmd = new SqlCommand();
        try
        {
            OpenSQLConnection();
            cmd.CommandText = "[UpdateOrderPaymentAndStatus_Exotel]";
            cmd.Connection = oConnect;
            cmd.CommandType = CommandType.StoredProcedure;
            if (OrderNo != "")
                cmd.Parameters.Add("@OrderNo", SqlDbType.VarChar, 500).Value = OrderNo;

            cmd.Parameters.Add("@OrderStatusId", SqlDbType.VarChar, 500).Value = StatusId;
            cmd.Parameters.Add("@PaymentStatusId", SqlDbType.VarChar, 500).Value = sPaymentStatusId;
            cmd.Parameters.Add("@CallsId", SqlDbType.VarChar, 500).Value = sCallSid;
            if (sCallStatus != "")
                cmd.Parameters.Add("@Status", SqlDbType.VarChar, 500).Value = sCallStatus;

            suceess = cmd.ExecuteNonQuery();
            CloseSQLConnection();
        }
        catch (Exception ex)
        {
            CartMsg = ex.Message.ToString();
            oErrorLog.Open();
            oErrorLog.Log("UpdateOrderStatus_Exotel Function In ShoppingCart.cs:" + ex.Message);
            oErrorLog.Flush();
            oErrorLog.Close();
        }
        finally
        {
            CloseSQLConnection();
            cmd.Dispose();
        }
        return suceess;
    }

    public string CancelOrder(string sOrderNo, string CancelReason)
    {
        string iResult = string.Empty;
        try
        {


            OpenSQLConnection();

            SqlCommand cmd = new SqlCommand("[ZipDial_CancelOrder]", oConnect);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@OrderNo", SqlDbType.VarChar).Value = sOrderNo;
            cmd.Parameters.Add("@CancelReason", SqlDbType.VarChar).Value = CancelReason;
            cmd.Parameters.Add("@ErrorMessage", SqlDbType.VarChar, 150).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@RValue", SqlDbType.VarChar, 150).Direction = ParameterDirection.ReturnValue;

            cmd.ExecuteNonQuery();
            iResult = cmd.Parameters["@RValue"].Value.ToString();
            Message = cmd.Parameters["@ErrorMessage"].Value.ToString();
        }

        catch (Exception ex)
        {
            oErrorLog.Open();
            oErrorLog.Log("CancelOrder in PackingAndDespatch.cs" + ex.Message);
            oErrorLog.Flush();
            oErrorLog.Close();
        }
        finally
        {
            CloseSQLConnection();
        }
        return iResult;
    }

    public bool CheckUserCodStatus(string OrderId)
    {
        bool IsValidUser = false;
        SqlCommand sqlCmd = new SqlCommand();
        try
        {
            OpenSQLConnection();
            sqlCmd = new SqlCommand("CheckUserCodStatus", oConnect);
            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlCmd.Parameters.Add("@OrderNo", SqlDbType.VarChar).Value = OrderId;
            sqlCmd.Parameters.Add("@IsValidUser", SqlDbType.Bit).Direction = ParameterDirection.Output;

            sqlCmd.ExecuteNonQuery();
            IsValidUser = Convert.ToBoolean(sqlCmd.Parameters["@IsValidUser"].Value);
        }
        catch (Exception ex)
        {
            oErrorLog.Open();
            oErrorLog.Log("CheckUserCodStatus in ShoppingCart.cs" + ex.Message);
            oErrorLog.Flush();
            oErrorLog.Close();
        }
        finally
        {
            sqlCmd.Dispose();
            sqlCmd = null;

            CloseSQLConnection();
        }
        return IsValidUser;
    }

    public DataSet GetOrderItemsByOrderNo(string sOrderId)
    {
        SqlDataAdapter dtAdptr = new SqlDataAdapter();
        DataSet dsDataSet = new DataSet();
        SqlCommand sqlCmd = new SqlCommand();

        try
        {
            OpenSQLConnection();

            //sqlCmd = new SqlCommand("[GetOrdersByOrderNo_bbm_N2]", oConnect);

            sqlCmd = new SqlCommand("[V05_GetDiscountOrder]", oConnect);

            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlCmd.Parameters.Add("@OrderNo", SqlDbType.VarChar).Value = sOrderId;
            dtAdptr = new SqlDataAdapter(sqlCmd);
            dtAdptr.Fill(dsDataSet);
        }
        catch (Exception ex)
        {
            oErrorLog.Open();
            oErrorLog.Log("GetOrderItemsByOrderNo in ShoppingCart.cs" + ex.Message);
            oErrorLog.Flush();
            oErrorLog.Close();
        }
        finally
        {
            dtAdptr.Dispose();
            dtAdptr = null;

            sqlCmd.Dispose();
            sqlCmd = null;

            CloseSQLConnection();
        }
        return dsDataSet;
    }

    public void UpdateMenu()
    {
        try
        {
            OpenSQLConnection();
            SqlCommand cmd = new SqlCommand("UpdateMenuData", oConnect);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.ExecuteNonQuery();
        }

        catch (Exception ex)
        {
            oErrorLog.Open();
            oErrorLog.Log("UpdateMenuData" + ex.Message);
            oErrorLog.Flush();
            oErrorLog.Close();
        }
        finally
        {
            CloseSQLConnection();
        }
    }
}

