using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Mandrill;
using Newtonsoft.Json;
using System.Configuration;
using System.Text;
using System.Net.Mail;

/// <summary>
/// Summary description for SendMail
/// </summary>
/// 


public class SendWebMail
{
    static bool isMandril = true;


    public SendWebMail()
    {
        //
        // TODO: Add constructor logic here]
        //
    }

    public static void SendEmailInfo(EmailAddress objEmail, string EmailTo, string Subject, string body)
    {
        try
        {
            BBMMailNotification cmd = null;
            if (isMandril)
            {
                cmd = new SendMailByMandril();
            }
            else
            {
                cmd = new SendWebMailBySMTP();
            }
            cmd.SendEmailInfo(objEmail, EmailTo, Subject, body);
        }
        catch (Exception ex)
        {
            //EmailErrorLogger.Open();
            //EmailErrorLogger.Log("SendEmailInfoKashif Function In SendWebMail:" + ex.Message);
            //EmailErrorLogger.Flush();
            //EmailErrorLogger.Close();
        }
    }

    public static void SendEmailTo(EmailAddress objemail, string EmailTo, string Subject, string body)
    {
        BBMMailNotification cmd = null;
        if (isMandril)
        {
            cmd = new SendMailByMandril();
        }
        else
        {
            cmd = new SendWebMailBySMTP();
        }
        cmd.SendEmailTo(objemail, EmailTo, Subject, body);

    }

    public static void SendEmailCc(EmailAddress objemail, string EmailTo, string EmailCc, string Subject, string body)
    {
        BBMMailNotification cmd = null;
        if (isMandril)
        {
            cmd = new SendMailByMandril();
        }
        else
        {
            cmd = new SendWebMailBySMTP();
        }
        cmd.SendEmailCc(objemail, EmailTo, EmailCc, Subject, body);

    }

    public static bool SendReferralEmail(EmailAddress objemail, string EmailTo, string Subject, string body)
    {
        BBMMailNotification cmd = null;
        if (isMandril)
        {
            cmd = new SendMailByMandril();
        }
        else
        {
            cmd = new SendWebMailBySMTP();
        }
        //bool Status = false;
        bool Status = cmd.SendReferralEmail(objemail, EmailTo, Subject, body);

        //cmd.SendReferralEmail(objemail, EmailTo, Subject, body);

        return Status;
    }

    public static void SendEmailTestForUniCom(EmailAddress objemail, string EmailTo, string Subject, string body)
    {
        BBMMailNotification cmd = null;
        if (isMandril)
        {
            cmd = new SendMailByMandril();
        }
        else
        {
            cmd = new SendWebMailBySMTP();
        }
        cmd.SendEmailTestForUniCom(objemail, EmailTo, Subject, body);

    }

    public static void SendEmailToContact(EmailAddress objemail, string EmailTo, string Subject, string body)
    {
        BBMMailNotification cmd = null;
        if (isMandril)
        {
            cmd = new SendMailByMandril();
        }
        else
        {
            cmd = new SendWebMailBySMTP();
        }
        cmd.SendEmailToContact(objemail, EmailTo, Subject, body);

    }

    public static void SendDirectoryEmail(EmailAddress objemail, string EmailTo, string Subject, string body)
    {
        BBMMailNotification cmd = null;
        if (isMandril)
        {
            cmd = new SendMailByMandril();
        }
        else
        {
            cmd = new SendWebMailBySMTP();
        }
        cmd.SendDirectoryEmail(objemail, EmailTo, Subject, body);

    }
}

public abstract class BBMMailNotification
{
    public abstract void SendEmailInfo(EmailAddress objEmail, string EmailTo, string Subject, string body);
    public abstract void SendEmailTo(EmailAddress objEmail, string EmailTo, string Subject, string body);
    public abstract void SendEmailCc(EmailAddress objEmail, string EmailTo, string EmailCc, string Subject, string body);
    public abstract bool SendReferralEmail(EmailAddress objEmail, string EmailTo, string Subject, string body);
    public abstract void SendEmailTestForUniCom(EmailAddress objEmail, string EmailTo, string Subject, string body);
    public abstract void SendEmailToContact(EmailAddress objEmail, string EmailTo, string Subject, string body);
    public abstract void SendDirectoryEmail(EmailAddress objEmail, string EmailTo, string Subject, string body);
}

public class SendWebMailBySMTP : BBMMailNotification
{
    public SendWebMailBySMTP()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public override void SendEmailInfo(EmailAddress objEmail, string EmailTo, string Subject, string body)
    {
        MailMessage mail = new MailMessage();
        try
        {
            SmtpClient smtpClient = new SmtpClient();

            MailAddress fromAddress = new MailAddress(objEmail.email, objEmail.name);

            mail.From = fromAddress;

            mail.To.Add(EmailTo);

            mail.Bcc.Add("bednbathmore@gmail.com");

            mail.Subject = Subject;
            mail.IsBodyHtml = true;
            mail.Body = body;
            smtpClient.Host = System.Configuration.ConfigurationManager.AppSettings["SmtpServer"].ToString();
            smtpClient.DeliveryMethod = SmtpDeliveryMethod.PickupDirectoryFromIis;
            smtpClient.Send(mail);

        }
        catch (Exception ex)
        {
            //EmailErrorLogger.Open();
            //EmailErrorLogger.Log("SendEmailInfo Function In SendWebMail:" + ex.Message);
            //EmailErrorLogger.Flush();
            //EmailErrorLogger.Close();
        }
        finally
        {
            mail.Dispose();
        }
    }

    public override bool SendReferralEmail(EmailAddress objEmail, string EmailTo, string Subject, string body)
    {
        bool Status = false;
        try
        {
            MailMessage mail = new MailMessage();
            SmtpClient smtpClient = new SmtpClient();
            MailAddress fromAddress = new MailAddress(objEmail.email, objEmail.name);

            string[] sRecipients = null;

            if (EmailTo.Contains(",") == true)
            {
                sRecipients = EmailTo.Split(',');
            }
            if (EmailTo.Contains(";") == true)
            {
                sRecipients = EmailTo.Split(';');
            }

            if (sRecipients != null && sRecipients.Length > 0)
            {
                for (int iRunner = 0; iRunner < sRecipients.Length; iRunner++)
                {
                    mail.To.Add(sRecipients[iRunner].ToString());
                    mail.From = fromAddress;
                    mail.Subject = Subject;
                    mail.IsBodyHtml = true;
                    mail.Body = body;
                    smtpClient.Host = System.Configuration.ConfigurationManager.AppSettings["SmtpServer"].ToString();
                    smtpClient.DeliveryMethod = SmtpDeliveryMethod.PickupDirectoryFromIis;
                    smtpClient.Send(mail);
                    mail.To.Clear();
                }
            }
            else
            {
                mail.To.Add(EmailTo);
                mail.From = fromAddress;
                mail.Subject = Subject;
                mail.IsBodyHtml = true;
                mail.Body = body;
                smtpClient.Host = System.Configuration.ConfigurationManager.AppSettings["SmtpServer"].ToString();
                smtpClient.DeliveryMethod = SmtpDeliveryMethod.PickupDirectoryFromIis;
                smtpClient.Send(mail);
            }

            mail.Dispose();
            Status = true;
        }
        catch (Exception ex)
        {
            //EmailErrorLogger.Open();
            //EmailErrorLogger.Log("SendReferralEmail Function In SendWebMail.cs:" + ex.Message);
            //EmailErrorLogger.Flush();
            //EmailErrorLogger.Close();
        }
        return Status;
    }

    public override void SendEmailCc(EmailAddress objEmail, string EmailTo, string EmailCc, string Subject, string body)
    {
        try
        {

            MailMessage mail = new MailMessage();
            SmtpClient smtpClient = new SmtpClient();
            MailAddress fromAddress = new MailAddress(objEmail.email, objEmail.name);


            mail.To.Add(EmailTo);
            mail.From = fromAddress;
            mail.CC.Add(EmailCc);
            mail.Subject = Subject;
            mail.IsBodyHtml = true;
            mail.Body = body;
            smtpClient.Host = System.Configuration.ConfigurationManager.AppSettings["SmtpServer"].ToString();
            smtpClient.Send(mail);
        }

        catch (Exception ex)
        {
            //EmailErrorLogger.Open();
            //EmailErrorLogger.Log("SendEmailCc Function In SendWebMail.cs:" + ex.Message);
            //EmailErrorLogger.Flush();
            //EmailErrorLogger.Close();
        }

    }

    public override void SendEmailTo(EmailAddress objEmail, string EmailTo, string Subject, string body)
    {
        try
        {
            MailMessage mail = new MailMessage();
            SmtpClient smtpClient = new SmtpClient();
            MailAddress fromAddress = new MailAddress(objEmail.email, objEmail.name);

            mail.To.Add(EmailTo);

            mail.From = fromAddress;

            //mail.Bcc.Add("Portico Order<manojm@creativeglobal.in>");

            mail.Subject = Subject;
            mail.IsBodyHtml = true;
            mail.Body = body;
            smtpClient.Host = System.Configuration.ConfigurationManager.AppSettings["SmtpServer"].ToString();
            smtpClient.DeliveryMethod = SmtpDeliveryMethod.PickupDirectoryFromIis;
            smtpClient.Send(mail);

            mail.Dispose();
        }

        catch (Exception ex)
        {
            //EmailErrorLogger.Open();
            //EmailErrorLogger.Log("SendEmailTo Function In SendWebMail.cs:" + ex.Message);
            //EmailErrorLogger.Flush();
            //EmailErrorLogger.Close();
        }

    }
    public override void SendEmailTestForUniCom(EmailAddress objEmail, string EmailTo, string Subject, string body)
    {
        try
        {
            MailMessage mail = new MailMessage();
            SmtpClient smtpClient = new SmtpClient();
            MailAddress fromAddress = new MailAddress(objEmail.email, objEmail.name);

            mail.To.Add(EmailTo);

            mail.From = fromAddress;

            //mail.Bcc.Add("Portico Order<manojm@creativeglobal.in>");

            mail.Subject = Subject;
            mail.IsBodyHtml = true;
            mail.Body = body;
            smtpClient.Host = System.Configuration.ConfigurationManager.AppSettings["SmtpServer"].ToString();
            smtpClient.DeliveryMethod = SmtpDeliveryMethod.PickupDirectoryFromIis;
            smtpClient.Send(mail);

            mail.Dispose();
        }

        catch (Exception ex)
        {
            //EmailErrorLogger.Open();
            //EmailErrorLogger.Log("SendEmailTestForUniCom Function In SendWebMail.cs:" + ex.Message);
            //EmailErrorLogger.Flush();
            //EmailErrorLogger.Close();
        }

    }

    public override void SendEmailToContact(EmailAddress objEmail, string EmailTo, string Subject, string body)
    {
        try
        {
            MailMessage mail = new MailMessage();
            SmtpClient smtpClient = new SmtpClient();
            MailAddress fromAddress = new MailAddress(objEmail.email, objEmail.name);

            mail.To.Add(EmailTo);

            mail.From = fromAddress;

            //mail.Bcc.Add("Portico Order<manojm@creativeglobal.in>");

            mail.Subject = Subject;
            mail.IsBodyHtml = true;
            mail.Body = body;
            smtpClient.Host = System.Configuration.ConfigurationManager.AppSettings["SmtpServer"].ToString();
            smtpClient.DeliveryMethod = SmtpDeliveryMethod.PickupDirectoryFromIis;
            smtpClient.Send(mail);

            mail.Dispose();
        }

        catch (Exception ex)
        {
            //EmailErrorLogger.Open();
            //EmailErrorLogger.Log("SendEmailToContact Function In SendWebMail.cs:" + ex.Message);
            //EmailErrorLogger.Flush();
            //EmailErrorLogger.Close();
        }

    }

    public override void SendDirectoryEmail(EmailAddress objEmail, string EmailTo, string Subject, string body)
    {
        try
        {
            MailMessage mail = new MailMessage();
            SmtpClient smtpClient = new SmtpClient();
            MailAddress fromAddress = new MailAddress(objEmail.email, objEmail.name);

            mail.To.Add(EmailTo);

            mail.From = fromAddress;


            mail.Subject = Subject;
            mail.IsBodyHtml = true;
            mail.Body = body;
            smtpClient.Host = System.Configuration.ConfigurationManager.AppSettings["SmtpServer"].ToString();
            smtpClient.DeliveryMethod = SmtpDeliveryMethod.PickupDirectoryFromIis;
            smtpClient.Send(mail);

            mail.Dispose();
        }

        catch (Exception ex)
        {
            //EmailErrorLogger.Open();
            //EmailErrorLogger.Log("SendEmailToContact Function In SendWebMail.cs:" + ex.Message);
            //EmailErrorLogger.Flush();
            //EmailErrorLogger.Close();
        }

    }

}

public class SendMailByMandril : BBMMailNotification
{
    //public static string ApiKey = "_Ld9pka9xLPa9jul8Tgsmw";

    public static string ApiKey = "JqcxeFHZ1DrZ_BMea0ndZA";
    public static string emailsendorReject = string.Empty;

    public override void SendEmailInfo(EmailAddress EmailFrom, string EmailTo, string Subject, string body)
    {
        try
        {
            var api = new MandrillApi(ApiKey);

            var result = api.SendMessage(new EmailMessage
            {
                to = new List<EmailAddress> { new EmailAddress { email = EmailTo, name = "" } },
                from_email = EmailFrom.email,
                from_name = EmailFrom.name,
                bcc_address = "reachus@olivetheory.com",
                subject = Subject,
                html = body,
                text = "",
                preserve_recipients = false
            });


            foreach (var res in result)
            {
                emailsendorReject = res.Status.ToString();
            }

        }

        catch (Exception ex)
        {
            //EmailErrorLogger.Open();
            //EmailErrorLogger.Log("SendEmailInfo Function In SendWebMail.cs:" + ex.Message);
            //EmailErrorLogger.Flush();
            //EmailErrorLogger.Close();
        }
    }

    public override void SendEmailTo(EmailAddress objEmail, string EmailTo, string Subject, string body)
    {
        try
        {
            var api = new MandrillApi(ApiKey);

            /*EmailAddress objemailto = new EmailAddress();
            objemailto.email = EmailTo;
            objemailto.name = "";*/

            var result = api.SendMessage(new EmailMessage
            {
                to = new List<EmailAddress> { new EmailAddress { email = EmailTo, name = "" } },
                from_email = "orders@olivetheory.com",
                from_name = "www.olivetheory.com",
                subject = Subject,
                html = body,
                text = ""
            });




        }
        catch (Exception ex)
        {

            //EmailErrorLogger.Open();
            //EmailErrorLogger.Log("SendEmailTo1 Function In SendWebMail.cs:" + ex.Message);
            //EmailErrorLogger.Flush();
            //EmailErrorLogger.Close();
        }
    }

    public override void SendEmailCc(EmailAddress objEmail, string EmailTo, string EmailCc, string Subject, string body)
    {
        try
        {
            var api = new MandrillApi(ApiKey);

            EmailAddress objemailto = new EmailAddress();
            objemailto.email = EmailTo;

            EmailAddress objemailcc = new EmailAddress();
            objemailcc.email = EmailCc;

            var result = api.SendMessage(new EmailMessage
            {
                to = new List<EmailAddress> { objemailto, objemailcc },
                from_email = objEmail.email,
                from_name = objEmail.name,
                subject = Subject,
                html = body,
                text = ""
            });

            foreach (var res in result)
            {
                emailsendorReject = res.Status.ToString();
            }
        }
        catch (Exception ex)
        {
            //EmailErrorLogger.Open();
            //EmailErrorLogger.Log("SendEmailCc Function In SendMailByMandril:" + ex.Message);
            //EmailErrorLogger.Flush();
            //EmailErrorLogger.Close();
        }
    }

    public override bool SendReferralEmail(EmailAddress objEmail, string EmailTo, string Subject, string body)
    {
        bool Status = false;
        try
        {
            string[] sRecipients = null;

            if (EmailTo.Contains(",") == true)
            {
                sRecipients = EmailTo.Split(',');
            }
            if (EmailTo.Contains(";") == true)
            {
                sRecipients = EmailTo.Split(';');
            }

            if (sRecipients != null && sRecipients.Length > 0)
            {
                for (int iRunner = 0; iRunner < sRecipients.Length; iRunner++)
                {
                    var api = new MandrillApi(ApiKey);

                    EmailAddress objemailto = new EmailAddress();
                    objemailto.email = sRecipients[iRunner];
                    objemailto.name = "";

                    var result = api.SendMessage(new EmailMessage
                    {
                        to = new List<EmailAddress> { objemailto },
                        from_email = objEmail.email,
                        from_name = objEmail.name,
                        subject = Subject,
                        html = body,
                        text = "",
                        preserve_recipients = true
                    });
                }
            }
            else
            {
                var api = new MandrillApi(ApiKey);

                EmailAddress objemailto = new EmailAddress();
                objemailto.email = EmailTo;
                objemailto.name = "";

                var result = api.SendMessage(new EmailMessage
                {
                    to = new List<EmailAddress> { objemailto },
                    from_email = objEmail.email,
                    from_name = objEmail.name,
                    subject = Subject,
                    html = body,
                    text = "",
                    preserve_recipients = true
                });

            }

            Status = true;
        }
        catch (Exception ex)
        {
            //EmailErrorLogger.Open();
            //EmailErrorLogger.Log("SendReferralEmail Function In SendWebMail.cs:" + ex.Message);
            //EmailErrorLogger.Flush();
            //EmailErrorLogger.Close();
        }

        return Status;
    }


    public override void SendEmailTestForUniCom(EmailAddress EmailFrom, string EmailTo, string Subject, string body)
    {
        try
        {
            var api = new MandrillApi(ApiKey);

            var result = api.SendMessage(new EmailMessage
            {
                to = new List<EmailAddress> { new EmailAddress { email = EmailTo, name = "" } },
                from_email = EmailFrom.email,
                from_name = EmailFrom.name,
                bcc_address = "bednbathmore@gmail.com",
                //bcc_address = "svinays10@gmail.com",
                subject = Subject,
                html = body,
                text = "",
                preserve_recipients = false
            });
        }

        catch (Exception ex)
        {
            //EmailErrorLogger.Open();
            //EmailErrorLogger.Log("SendEmailTestForUniCom Function In SendWebMail.cs:" + ex.Message);
            //EmailErrorLogger.Flush();
            //EmailErrorLogger.Close();
        }
    }

    public override void SendEmailToContact(EmailAddress objEmail, string EmailTo, string Subject, string body)
    {
        try
        {
            var api = new MandrillApi(ApiKey);

            EmailAddress objemailto = new EmailAddress();
            objemailto.email = EmailTo;
            objemailto.name = "";
            var result = api.SendMessage(new EmailMessage
            {
                to = new List<EmailAddress> { new EmailAddress { email = EmailTo, name = "" } },
                from_email = objEmail.email,
                from_name = "www.olivetheory.com",
                bcc_address = "orders@olivetheory.com",
                subject = Subject,
                html = body,
                text = ""
            });
        }
        catch (Exception ex)
        {
            //EmailErrorLogger.Open();
            //EmailErrorLogger.Log("SendEmailToContact Function In SendWebMail.cs:" + ex.Message);
            //EmailErrorLogger.Flush();
            //EmailErrorLogger.Close();
        }
    }

    public override void SendDirectoryEmail(EmailAddress objEmail, string EmailTo, string Subject, string body)
    {
        try
        {
            var api = new MandrillApi(ApiKey);

            EmailAddress objemailto = new EmailAddress();
            objemailto.email = EmailTo;
            objemailto.name = "";
            var result = api.SendMessage(new EmailMessage
            {
                to = new List<EmailAddress> { new EmailAddress { email = EmailTo, name = "" } },
                from_email = objEmail.email,
                from_name = objEmail.name,
                bcc_address = "orders@olivetheory.com",
                subject = Subject,
                html = body,
                text = ""
            });
        }
        catch (Exception ex)
        {
            //EmailErrorLogger.Open();
            //EmailErrorLogger.Log("SendEmailToContact Function In SendWebMail.cs:" + ex.Message);
            //EmailErrorLogger.Flush();
            //EmailErrorLogger.Close();
        }
    }

}


