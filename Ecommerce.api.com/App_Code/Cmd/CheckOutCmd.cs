﻿using Ecommerce.config;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Ecommerce.Entities;
using Dapper;


namespace Ecommerce.Cmd
{
    public class CheckOutCmd : DbConnection
    {
        public CheckOutCartItemDetail CheckOut_GetItems(string Sessionid)
        {
            CheckOutCartItemDetail retVal = null;
            try
            {
                using (IDbConnection con = GetOpenSqlConnection())
                {
                    var dp = new DynamicParameters();
                    dp.Add("@CustomerId", UserSession.GetCurrentCustId(), DbType.Int32);
                    dp.Add("@SessionId", Sessionid, DbType.String);

                    CheckOutCartItemDetail code = new CheckOutCartItemDetail();
                    using (var multi = dbCon.QueryMultiple("[V05_CheckOut_CartItems_V1_L]", dp, commandType: CommandType.StoredProcedure))
                    {
                        code.CartItem = multi.Read<CheckOutCartItem>().ToList();
                        code.CartZodiacItems = multi.Read<CartZodiacItems>().ToList();
                        code.CartPaymentDetail = multi.Read<CartPaymentDetail>().FirstOrDefault();
                        code.IsValidForGuest = multi.Read<IsValidForGuestCheckout>().FirstOrDefault();
                    }
                    retVal = code;
                    con.Close();
                }
            }
            catch (Exception ex)
            {

                throw (ex);
                //Ex.CreateLog(ex);
            }

            return retVal;
        }

        public string Coupon_AutoApply(int iCustomerId, string Sessionid)
        {
            string retVal = string.Empty;
            try
            {
                using (IDbConnection con = GetOpenSqlConnection())
                {
                    var dp = new DynamicParameters();
                    dp.Add("@CustomerId", UserSession.GetCurrentCustId(), DbType.Int32);
                    dp.Add("@SessionId", Sessionid, DbType.String);
                    retVal = dbCon.Query<string>("Coupon_AutoApply", dp, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return retVal;
        }

        public int saveCustomerAddress(CustomerAddress objaddress)
        {
            int retVal = 0;
            try
            {
                using (IDbConnection con = GetOpenSqlConnection())
                {
                    var dp = new DynamicParameters();
                    dp.Add("@CustomerId", objaddress.CustomerId, DbType.Int32);
                    dp.Add("@UserId", objaddress.UserId, DbType.Int32);
                    dp.Add("@FirstName", objaddress.FirstName, DbType.String);
                    dp.Add("@LastName", objaddress.LastName, DbType.String);
                    dp.Add("@MobileNo", objaddress.MobileNo, DbType.String);
                    dp.Add("@SourceId", objaddress.SourceId, DbType.String);
                    dp.Add("@Address", objaddress.Address, DbType.String);
                    dp.Add("@PinNo", objaddress.PinNo, DbType.Int32);
                    dp.Add("@City", objaddress.City, DbType.String);
                    dp.Add("@State", objaddress.State, DbType.String);
                    dp.Add("@Country", objaddress.Country, DbType.String);
                    dp.Add("@CustomerAddressId", objaddress.AddressId, DbType.Int32);
                    dp.Add("@IsDefault", objaddress.IsDefalut, DbType.Boolean);
                    retVal = dbCon.Query<int>("SaveCustomerAddress_L1", dp, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return retVal;
        }

        public CustomerAddress getCustomerAddress()
        {
            CustomerAddress retVal = null;
            try
            {
                using (IDbConnection con = GetOpenSqlConnection())
                {
                    // string strSql = "select * from CustomerAddress where AddressId in (select ShippingAddressId from customer where CustomerId = " + UserSession.GetCurrentCustId() + ")";

                    string strSql = "select * from CustomerAddress where AddressId in (select ShippingAddressId from customer where CustomerId = 73853)";

                    retVal = dbCon.Query<CustomerAddress>(strSql, null, commandType: CommandType.Text).FirstOrDefault();

                    con.Close();
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }

            return retVal;
        }

        public EMI GetEmiDetails()
        {
            EMI ObjEmi = new EMI();
            try
            {
                using (var multi = dbCon.QueryMultiple("GetEmiDetails", null, commandType: CommandType.StoredProcedure))
                {
                    ObjEmi.ObjBanks = multi.Read<Banks>().ToList();
                    ObjEmi.ObjEmi = multi.Read<EmiDetails>().ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ObjEmi;
        }

        public SellerServiciable CheckOut_Validation(CheckoutCartValidation cart)
        {
            SellerServiciable retVal = new Entities.SellerServiciable();
            try
            {
                using (IDbConnection con = GetOpenSqlConnection())
                {
                    var dp = new DynamicParameters();
                    dp.Add("@CustomerId", cart.CustomerId, DbType.Int32);
                    dp.Add("@SessionId ", cart.SessionId, DbType.String);
                    dp.Add("@PinCode", cart.PinCode, DbType.Int32);
                    dp.Add("@PaymentMode", cart.PaymentMode, DbType.String);
                    retVal = dbCon.Query<SellerServiciable>("[CheckOut_CartValidation_Testing_Seller]", dp, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return retVal;
        }

        public CheckoutBillingAddress GetBillingAddress(int iCustomerId)
        {
            CheckoutBillingAddress retVal = null;
            try
            {
                using (IDbConnection con = GetOpenSqlConnection())
                {
                    var dp = new DynamicParameters();
                    dp.Add("@CustId", iCustomerId, DbType.Int32);
                    CheckoutBillingAddress code = new CheckoutBillingAddress();
                    retVal = dbCon.Query<CheckoutBillingAddress>("V05_GetCustomerById", dp, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                throw (ex);
                //Ex.CreateLog(ex);
            }

            return retVal;
        }

        public List<CheckoutBillingAddress> GetPreviusBillingAddress(int iCustomerId)
        {
            List<CheckoutBillingAddress> retVal = null;
            try
            {
                using (IDbConnection con = GetOpenSqlConnection())
                {
                    var dp = new DynamicParameters();
                    dp.Add("@CustId", iCustomerId, DbType.Int32);
                    CheckoutBillingAddress code = new CheckoutBillingAddress();
                    retVal = dbCon.Query<CheckoutBillingAddress>("V05_BillingAdd_GetAddressList", dp, commandType: CommandType.StoredProcedure).ToList();
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                throw (ex);
                //Ex.CreateLog(ex);
            }

            return retVal;
        }

        public CheckOutCartItemDetail getCartItems(string wid)
        {
            CheckOutCmd cmd = new CheckOutCmd();
            CheckOutCartItemDetail code = cmd.CheckOut_GetItems(wid);
            return code;
        }

        public int CheckOut_GetItems_Count()
        {
            int Count = 0;
            try
            {
                using (IDbConnection con = GetOpenSqlConnection())
                {
                    var dp = new DynamicParameters();
                    dp.Add("@CustomerId", UserSession.GetCurrentCustId(), DbType.Int32);
                    dp.Add("@SessionId", UserSession.GetWebSessionId(), DbType.String);

                    Count = con.Query<int>("V05_CheckOut_CartItems_Count", dp, commandType: CommandType.StoredProcedure).FirstOrDefault();

                    con.Close();
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }

            return Count;
        }

        public string ValidateCustomerAddress(int CustomerId)
        {
            string retVal = string.Empty;
            try
            {
                using (IDbConnection con = GetOpenSqlConnection())
                {
                    var dp = new DynamicParameters();
                    dp.Add("@CustomerId", CustomerId, DbType.Int32);
                    retVal = dbCon.Query<string>("[ValidateCustomerAddress]", dp, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                retVal = Convert.ToString(ex.Message);
            }

            return retVal;
        }

        public CustomerAddress GetCustomerPaymentAddress()
        {
            CustomerAddress retVal = new CustomerAddress();
            try
            {
                using (IDbConnection con = GetOpenSqlConnection())
                {
                    var dp = new DynamicParameters();
                    dp.Add("@CustomerId", UserSession.GetCurrentCustId(), DbType.Int32);
                    retVal = dbCon.Query<CustomerAddress>("[GetPaymentCustomerAddress]", dp, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    con.Close();
                }
            }
            catch (Exception ex)
            {
            }

            return retVal;
        }

        public CheckOutCartItemDetail getCart(int CustomerId, string Sessionid)
        {
            CheckOutCartItemDetail retVal = null;
            try
            {
                using (IDbConnection con = GetOpenSqlConnection())
                {
                    var dp = new DynamicParameters();
                    dp.Add("@CustomerId", CustomerId, DbType.Int32);
                    dp.Add("@SessionId", Sessionid, DbType.String);

                    CheckOutCartItemDetail code = new CheckOutCartItemDetail();
                    using (var multi = dbCon.QueryMultiple("[V05_CheckOut_CartItems_V1_L]", dp, commandType: CommandType.StoredProcedure))
                    {
                        code.CartItem = multi.Read<CheckOutCartItem>().ToList();
                        code.CartZodiacItems = multi.Read<CartZodiacItems>().ToList();
                        code.CartPaymentDetail = multi.Read<CartPaymentDetail>().FirstOrDefault();
                        code.IsValidForGuest = multi.Read<IsValidForGuestCheckout>().FirstOrDefault();
                    }
                    retVal = code;
                    con.Close();
                }
            }
            catch (Exception ex)
            {

                throw (ex);
                //Ex.CreateLog(ex);
            }

            return retVal;
        }

        public ClsPaymentResponse getOrder(string OrderNo)
        {
            ClsPaymentResponse retVal = null;
            try
            {
                using (IDbConnection con = GetOpenSqlConnection())
                {
                    var dp = new DynamicParameters();
                    dp.Add("@OrderNo", OrderNo, DbType.String);

                    ClsPaymentResponse code = new ClsPaymentResponse();
                    using (var multi = dbCon.QueryMultiple("[V05_GetDiscountOrder]", dp, commandType: CommandType.StoredProcedure))
                    {
                        code.orderDetails = multi.Read<OrderDetails>().ToList();
                        code.OrderPaymentInfo = multi.Read<OrderPaymentInfo>().FirstOrDefault();
                        code.customItems = multi.Read<CustomItems>().ToList();
                    }
                    retVal = code;
                    con.Close();
                }
            }
            catch (Exception ex)
            {

                throw (ex);
                //Ex.CreateLog(ex);
            }

            return retVal;
        }

        public string getBrandName(string brand)
        {
            string sbrandname = string.Empty;

            try
            {
                using (IDbConnection con = GetOpenSqlConnection())
                {
                    string sql = "select top 1 collectionname from itemcollection WHERE collectionname = '" + brand + "'";

                    sbrandname = dbCon.Query<string>(sql, null, commandType: CommandType.Text).FirstOrDefault();

                    con.Close();
                }
            }
            catch (Exception ex)
            {

                throw (ex);
                //Ex.CreateLog(ex);
            }

            return sbrandname;
        }

        public void ClearShoppingCart(string SessionId)
        {
            try
            {
                using (IDbConnection con = GetOpenSqlConnection())
                {
                    var dp = new DynamicParameters();
                    dp.Add("@SessionId", SessionId, DbType.String);

                    dbCon.Query<int>("ClearShoppingCart", dp, commandType: CommandType.StoredProcedure);

                    con.Close();
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public string ValidateCustomerSession(int CustomerId, string Sessionid)
        {
            string retVal = string.Empty;
            try
            {
                using (IDbConnection con = GetOpenSqlConnection())
                {
                    var dp = new DynamicParameters();
                    dp.Add("@CustomerId", CustomerId, DbType.Int32);
                    dp.Add("@SessionId", Sessionid, DbType.String);
                    retVal = dbCon.Query<string>("[ValidateCustomerSession]", dp, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                retVal = Convert.ToString(ex.Message);
            }

            return retVal;
        }

        public List<ServiceableCartItem> Cart_DeliveryValidation(CheckoutCartValidation cart)
        {
            List<ServiceableCartItem> retVal = new List<ServiceableCartItem>();
            try
            {
                using (IDbConnection con = GetOpenSqlConnection())
                {
                    var dp = new DynamicParameters();
                    if (cart.CustomerId > 0)
                        dp.Add("@CustomerId", cart.CustomerId, DbType.Int32);

                    dp.Add("@SessionId ", cart.SessionId, DbType.String);
                    dp.Add("@PinCode", cart.PinCode, DbType.Int32);
                    //dp.Add("@PaymentMode", cart.PaymentMode, DbType.String);
                    retVal = dbCon.Query<ServiceableCartItem>("[CheckOut_CartDeliveryValidation]", dp, commandType: CommandType.StoredProcedure).ToList();
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return retVal;
        }

    }
}