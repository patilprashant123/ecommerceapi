﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web.SessionState;
using Ecommerce.conn;
using Web.API;

namespace Ecommerce.Cmd
{
    public class OrderCmd : Connection
    {
        decimal _BillAmount;
        string _OrderNo;

        StringBuilder sbMsg = new StringBuilder();

        string sRemoteAddress = HttpContext.Current.Request.ServerVariables["remote_addr"].ToString();

        public string CartMsg
        {
            set
            {
                sbMsg.Append(value);
            }
            get
            {
                if (sbMsg != null)
                    return sbMsg.ToString();

                return string.Empty;
            }
        }

        public decimal BillAmount
        {
            set
            {
                _BillAmount = value;
            }
            get
            {
                return _BillAmount;
            }
        }

        public string OrderNo
        {
            set
            {
                _OrderNo = value;
            }
            get
            {
                return _OrderNo;
            }
        }

        public int OrderId
        {
            get; set;
        }

    

        public SqlDataReader UserOrderList()
        {
            SqlDataReader dr = null;
            SqlCommand sqlCmd = new SqlCommand();
            try
            {
                OpenSQLConnection();
                sqlCmd = new SqlCommand("SELECT O.OrderNo,O.ORDERID,O.ORDERDATE,O.BILLAMOUNT,S.DESCRIPTION,O.TrackingNo,O.DespatchDate FROM ORDERS O INNER JOIN  ORDERSTATUS S ON O.STATUSID=S.STATUSID INNER JOIN CUSTOMER C ON O.CUSTOMERID=C.CUSTOMERID where C.CustomerId=@CustomerId and O.PaymentStatus IN(1,2,3,4,8) ORDER BY O.ORDERDATE DESC", oConnect);
                sqlCmd.CommandType = CommandType.Text;
                sqlCmd.Parameters.Add("@CustomerId", SqlDbType.Int).Value = UserSession.GetCurrentCustId();
                dr = sqlCmd.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                sqlCmd.Dispose();
                sqlCmd = null;
            }
            return dr;
        }

        public void CreateOrder(OrderRequest obj)
        {
            int Source = Convert.ToInt32(obj.Source);

            OpenSQLConnection();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = oConnect;
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.CommandText = "[V05_CreateOrder_V1_Shopping]";

            cmd.Parameters.Add("@SessionId", SqlDbType.VarChar).Value = obj.SessionId;
            cmd.Parameters.Add("@CustomerId", SqlDbType.BigInt).Value = obj.CustomerId;
            if (Source == 2 || Source == 4) //2 for User From WebSite and 3 for Cod Orders
            {
                cmd.Parameters.Add("@StatusId", SqlDbType.VarChar).Value = 3; // Awaiting For Payment
                cmd.Parameters.Add("@SourceId", SqlDbType.VarChar).Value = Source;
                cmd.Parameters.Add("@PaymentStatusId", SqlDbType.VarChar).Value = 1;//PayMent Awaiting
            }
            else //if (Source == 1 || Source == 5)  //User From CRM
            {
                cmd.Parameters.Add("@StatusId", SqlDbType.VarChar).Value = 1; // Send To WareHouse
                cmd.Parameters.Add("@SourceId", SqlDbType.VarChar).Value = Source;
                cmd.Parameters.Add("@PaymentStatusId", SqlDbType.VarChar).Value = 4;//Cash On Delivery
                cmd.Parameters.Add("@AgentId", SqlDbType.Int).Value = UserSession.GetCurrentUserId();
            }

            cmd.Parameters.Add("@PaymentType", SqlDbType.VarChar).Value = obj.PaymentType; //UserSession.GetCookie("PayType");
            cmd.Parameters.Add("@UserAgent", SqlDbType.VarChar).Value = HttpContext.Current.Request.UserAgent;

            SqlParameter paramErrorMsg = new SqlParameter("@ErrorMessage", SqlDbType.VarChar, 400);
            paramErrorMsg.Direction = ParameterDirection.Output;
            cmd.Parameters.Add(paramErrorMsg);

            SqlParameter param = new SqlParameter("@NewOrderId", SqlDbType.BigInt);
            param.Direction = ParameterDirection.Output;
            cmd.Parameters.Add(param);

            SqlParameter paramOrderNo = new SqlParameter("@OrderNo", SqlDbType.VarChar, 50);
            paramOrderNo.Direction = ParameterDirection.Output;
            cmd.Parameters.Add(paramOrderNo);

            SqlParameter paramBillAmount = new SqlParameter("@CreditCardAmount", SqlDbType.Decimal);
            paramBillAmount.Direction = ParameterDirection.Output;
            cmd.Parameters.Add(paramBillAmount);

            SqlDataReader dr = cmd.ExecuteReader();

            if (paramErrorMsg.Value != null)
                CartMsg = paramErrorMsg.Value.ToString();


            if (paramOrderNo.Value != null && !string.IsNullOrEmpty(paramOrderNo.Value.ToString()))
            {
                this.OrderNo = Convert.ToString(paramOrderNo.Value);
            }

            if (paramBillAmount.Value != null && paramBillAmount.Value.ToString() != "")
            {
                this.BillAmount = Convert.ToDecimal(paramBillAmount.Value);
            }
            if (param.Value != null && CString.IsInteger(param.Value.ToString()))
            {
                this.OrderId = int.Parse(param.Value.ToString());
                UserSession.SetCookie("Oid", this.OrderId.ToString());
                // HttpContext.Current.Session["Oid"] = this.OrderId.ToString();
            }
            else
            {
                OrderId = -1;
            }

            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    if (Convert.ToInt32(dr["IsPublished"]) == 0)
                    {
                        CartMsg = "The item " + dr["ItemCode"] + " in your cart is currently not available.";
                    }
                    else
                    {
                        CartMsg = "The item " + dr["ItemCode"] + " in your cart is currently out of stock.";
                    }
                }
                CartMsg = "Please check for availability";
            }

            dr.Close();
            CloseSQLConnection();
        }

    }
}
