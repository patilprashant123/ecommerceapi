﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ecommerce.Entities;
using Ecommerce.config;
using Ecommerce.ErrorLog;
using Dapper;
using System.Data;

namespace Ecommerce.Cmd
{
    public class FeedCmd : DbConnection
    {
        public BBMStore GetBBMStoreDataFeed(FeedInfo objInfo)
        {
            BBMStore retVal = new BBMStore();

            try
            {
                using (IDbConnection con = GetOpenSqlConnection())
                {
                    var dp = new DynamicParameters();

                    if (!string.IsNullOrEmpty(objInfo.l1))
                        dp.Add("@l1", objInfo.l1, DbType.String);
                    else
                        dp.Add("@l1", DBNull.Value, DbType.String);

                    if (!string.IsNullOrEmpty(objInfo.l2))
                        dp.Add("@l2", objInfo.l2, DbType.String);
                    else
                        dp.Add("@l2", DBNull.Value, DbType.String);

                    if (!string.IsNullOrEmpty(objInfo.l3))
                        dp.Add("@l3", objInfo.l3, DbType.String);
                    else
                        dp.Add("@l3", DBNull.Value, DbType.String);

                    if (!string.IsNullOrEmpty(objInfo.brand))
                        dp.Add("@brand", objInfo.brand, DbType.String);
                    else
                        dp.Add("@brand", DBNull.Value, DbType.String);

                    if (objInfo.l1 == "furniture")
                        retVal.BBMStoreDataList = dbCon.Query<BBMStoredata>("[AlphaSync_Furniture]", dp, commandType: CommandType.StoredProcedure).ToList();
                    else if (objInfo.l3 == "mattresses")
                        retVal.BBMStoreDataList = dbCon.Query<BBMStoredata>("[AlphaSync_mattresses]", dp, commandType: CommandType.StoredProcedure).ToList();
                    else
                        retVal.BBMStoreDataList = dbCon.Query<BBMStoredata>("[AlphaSync]", dp, commandType: CommandType.StoredProcedure).ToList();

                    con.Close();
                }
            }
            catch (Exception ex)
            {
            }

            return retVal;
        }

        public void InsertTokenInfo(string token)
        {
            try
            {
                using (IDbConnection con = GetOpenSqlConnection())
                {

                    var dp = new DynamicParameters();

                    dp.Add("@Token", token, DbType.String);

                    dbCon.Query<int>("InsertTokenInfo", dp, commandType: CommandType.StoredProcedure).ToList();


                    con.Close();
                }
            }
            catch (Exception ex)
            {
            }
        }

        public void ResetDeltaSync()
        {
            try
            {
                using (IDbConnection con = GetOpenSqlConnection())
                {

                    dbCon.Query<int>("ResetDeltaSync", null, commandType: CommandType.StoredProcedure);

                    con.Close();
                }
            }
            catch (Exception ex)
            {
            }
        }


        public ClsTokenInfo GetAccessTokens()
        {
            ClsTokenInfo retVal = new ClsTokenInfo();

            try
            {
                using (IDbConnection con = GetOpenSqlConnection())
                {
                    retVal.tokenlist = dbCon.Query<TokenInfo>("select * from Token", null, commandType: CommandType.StoredProcedure).ToList();

                    con.Close();
                }
            }
            catch (Exception ex)
            {
            }

            return retVal;
        }
    }
}