﻿using Ecommerce.config;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Ecommerce.Entities;
using Dapper;
using Ecommerce.ErrorLog;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.IO;
using System.Text;
using System.Configuration;
using Ecommerce;


namespace Ecommerce.Cmd
{
    public class ProductDetailCmd : DbConnection
    {
        ErrorLogger oErrorLog = new ErrorLogger();

        public ProductDetailsByCode GetProductDetailsById(string StandardCode, int ContentType, int CustomerId, string Category, string Height)
        {
            ProductDetailsByCode retVal = null;
            try
            {
                using (IDbConnection con = GetOpenSqlConnection())
                {
                    var dp = new DynamicParameters();
                    dp.Add("@StandardCode", StandardCode, DbType.String);
                    dp.Add("@CustomerId", CustomerId, DbType.Int32);
                    if (!string.IsNullOrEmpty(Category))
                    {
                        dp.Add("@Category", Category, DbType.String);
                    }
                    else
                    {
                        dp.Add("@Category", DBNull.Value, DbType.String);
                    }
                    if (!string.IsNullOrEmpty(Height))
                    {
                        dp.Add("@Height", Height, DbType.String);
                    }
                    else
                    {
                        dp.Add("@Height", DBNull.Value, DbType.String);
                    }

                    ProductDetailsByCode code = new ProductDetailsByCode();
                    using (var multi = dbCon.QueryMultiple("GetProduct", dp, commandType: CommandType.StoredProcedure))
                    {
                        code.ProductStandardDetail = multi.Read<ProductStandardDetail>().FirstOrDefault();
                        code.ProductStandardColor = multi.Read<ProductStandardColor>().ToList();
                        code.ItemDetailsForProduct = multi.Read<ItemDetailsForProduct>().ToList();
                        code.ProductStandardDetail.BundleItemJson = multi.Read<BundleItemJson>().ToList();
                        code.ProductStandardDetail.ProductZodiacDetail = multi.Read<ProductZodiacDetail>().FirstOrDefault();
                        code.MultipleImages = multi.Read<MultipleImages>().ToList();
                    }
                    retVal = code;
                    con.Close();
                }
            }
            catch (Exception exc)
            {
                oErrorLog.Open();
                oErrorLog.Log("ProductDetailsByCode Function In ProductDetailCmd.cs:" + exc.Message);
                oErrorLog.Flush();
                oErrorLog.Close();
            }

            return retVal;
        }

        public ProductCode GetUIType(string StandardCode)
        {
            ProductCode code = new ProductCode();
            try
            {
                using (IDbConnection con = GetOpenSqlConnection())
                {
                    var dp = new DynamicParameters();
                    dp.Add("@StandardCode", StandardCode, DbType.String);

                    code = dbCon.Query<ProductCode>("GetUITypeDetails", dp, commandType: CommandType.StoredProcedure).FirstOrDefault();

                }
            }
            catch (Exception exc)
            {
                oErrorLog.Open();
                oErrorLog.Log("ProductDetailsByCode Function In ProductDetailCmd.cs:" + exc.Message);
                oErrorLog.Flush();
                oErrorLog.Close();
            }

            return code;
        }

        public CodAndDelivery getCodAndDeliveryTimeLine(int ContentId, string ItemCode, string PinNo)
        {
            CodAndDelivery retVal = null;
            try
            {
                using (IDbConnection con = GetOpenSqlConnection())
                {
                    var dp = new DynamicParameters();
                    dp.Add("@StandardDesignId", ContentId, DbType.Int64);
                    dp.Add("@ItemCode", ItemCode, DbType.String);
                    dp.Add("@PinNo", PinNo, DbType.String);

                    retVal = dbCon.Query<CodAndDelivery>("V05_GetCodAndDeliveryTimeLine_Latest", dp, commandType: CommandType.StoredProcedure).FirstOrDefault();

                    con.Close();
                }
            }
            catch (Exception exc)
            {
                oErrorLog.Open();
                oErrorLog.Log("getCodAndDeliveryTimeLine Function In ProductDetailCmd.cs:" + exc.Message);
                oErrorLog.Flush();
                oErrorLog.Close();
            }

            return retVal;
        }

        public List<string> GetDesignList(int RowNum)
        {
            List<string> DesignList = null;
            try
            {
                using (IDbConnection con = GetOpenSqlConnection())
                {
                    string strSql = @"
                                    WITH MyCte AS
                                    (
                                        select StandardDesign.StandardCode,
	                                    RowNum = row_number() OVER ( order by StandardDesign.StandardCode )
                                        from  StandardDesign 
	                                    inner join items on StandardDesign.StandardCode = items.StandardCode
	                                    where StandardDesign.IsPublished = 1 and items.IsPublish = 1
                                        group by StandardDesign.StandardCode
                                    )
                                    SELECT StandardCode
                                    FROM MyCte
                                    WHERE RowNum < 
                                    " + RowNum;

                    DesignList = dbCon.Query<string>(strSql, null, commandType: CommandType.Text).ToList();

                }
            }
            catch (Exception exc)
            {
                oErrorLog.Open();
                oErrorLog.Log("GetDesignList Function In ProductDetailCmd.cs:" + exc.Message);
                oErrorLog.Flush();
                oErrorLog.Close();
            }

            return DesignList;
        }

        public ProductDetail GetProductCatelogInfo(string StandardCode)
        {
            ProductDetail retVal = null;
            try
            {
                using (IDbConnection con = GetOpenSqlConnection())
                {
                    var dp = new DynamicParameters();

                    dp.Add("@StandardCode", StandardCode, DbType.String);

                    ProductDetail code = new ProductDetail();

                    using (var multi = dbCon.QueryMultiple("GetProductCatelogInfo", dp, commandType: CommandType.StoredProcedure))
                    {
                        code.Design = multi.Read<DesignInfo>().FirstOrDefault();
                        code.Items = multi.Read<ItemDetailsForProduct>().ToList();
                        code.Images = multi.Read<MultipleImages>().ToList();
                    }
                    retVal = code;
                    con.Close();
                }
            }
            catch (Exception exc)
            {
                oErrorLog.Open();
                oErrorLog.Log("GetProductInfo Function In ProductDetailCmd.cs:" + exc.Message);
                oErrorLog.Flush();
                oErrorLog.Close();
            }

            return retVal;
        }

        public List<ProductFeed> getProductFeed()
        {
            List<ProductFeed> retVal = null;
            try
            {
                using (IDbConnection con = GetOpenSqlConnection())
                {
                    retVal = dbCon.Query<ProductFeed>("ProductsFeed", null, commandType: CommandType.StoredProcedure).ToList();
                    con.Close();
                }
            }
            catch (Exception exc)
            {
                oErrorLog.Open();
                oErrorLog.Log("getProductFeed Function In ProductDetailCmd.cs:" + exc.Message);
                oErrorLog.Flush();
                oErrorLog.Close();
            }

            return retVal;
        }

        public Varients getProductFeedDetail(string StandardCode)
        {
            Varients varient = new Varients();

            try
            {
                var dp = new DynamicParameters();

                dp.Add("@StandardCode", StandardCode, DbType.String);

                using (var multi = dbCon.QueryMultiple("ProductFeedDetail", dp, commandType: CommandType.StoredProcedure))
                {
                    varient.Sizes = multi.Read<string>().ToList();
                    varient.colors = multi.Read<string>().ToList();
                }
            }
            catch (Exception exc)
            {
                oErrorLog.Open();
                oErrorLog.Log("getProductFeed Function In ProductDetailCmd.cs:" + exc.Message);
                oErrorLog.Flush();
                oErrorLog.Close();
            }

            return varient;
        }

        public ImageDimension getProductImageAndDimension(string StandardCode)
        {
            ImageDimension imageDimension = new ImageDimension();

            try
            {
                var dp = new DynamicParameters();

                dp.Add("@StandardCode", StandardCode, DbType.String);

                using (var multi = dbCon.QueryMultiple("GetProductImageAndDimension", dp, commandType: CommandType.StoredProcedure))
                {
                    imageDimension.images = multi.Read<string>().ToList();
                    imageDimension.dimensions = multi.Read<FeedDimension>().ToList();
                }
            }
            catch (Exception exc)
            {
                oErrorLog.Open();
                oErrorLog.Log("getProductFeed Function In ProductDetailCmd.cs:" + exc.Message);
                oErrorLog.Flush();
                oErrorLog.Close();
            }

            return imageDimension;
        }

        public List<StandardDesignStatus> getStandardDesignStatus()
        {
            List<StandardDesignStatus> retVal = null;
            try
            {
                using (IDbConnection con = GetOpenSqlConnection())
                {
                    retVal = dbCon.Query<StandardDesignStatus>("select lower(ltrim(rtrim(StandardCode))) as StandardCode,cast(dbo.[GetDesignPublished](StandardCode) as bit) as IsPublish from StandardDesign", null, commandType: CommandType.Text).ToList();
                    con.Close();
                }
            }
            catch (Exception exc)
            {
                oErrorLog.Open();
                oErrorLog.Log("getStandardDesignStatus Function In ProductDetailCmd.cs:" + exc.Message);
                oErrorLog.Flush();
                oErrorLog.Close();
            }

            return retVal;
        }

        public MetaProduct GetProduct(string StandardCode, int ContentType, int CustomerId, string Category, string Height)
        {
            MetaProduct retVal = null;
            try
            {
                using (IDbConnection con = GetOpenSqlConnection())
                {
                    var dp = new DynamicParameters();
                    dp.Add("@StandardCode", StandardCode, DbType.String);
                    dp.Add("@CustomerId", CustomerId, DbType.Int32);
                    if (!string.IsNullOrEmpty(Category))
                    {
                        dp.Add("@Category", Category, DbType.String);
                    }
                    else
                    {
                        dp.Add("@Category", DBNull.Value, DbType.String);
                    }
                    if (!string.IsNullOrEmpty(Height))
                    {
                        dp.Add("@Height", Height, DbType.String);
                    }
                    else
                    {
                        dp.Add("@Height", DBNull.Value, DbType.String);
                    }

                    MetaProduct code = new MetaProduct();

                    //using (var multi = dbCon.QueryMultiple("[GetProduct_Mobile_New]", dp, commandType: CommandType.StoredProcedure))

                    //using (var multi = dbCon.QueryMultiple("[GetProduct_Mobile_New_S3]", dp, commandType: CommandType.StoredProcedure))
                    using (var multi = dbCon.QueryMultiple("[GetProduct_WithIMageCoordinate]", dp, commandType: CommandType.StoredProcedure))
                    {
                        code.metaProductDetail = multi.Read<MetaProductDetail>().FirstOrDefault();
                        code.metaProductColor = multi.Read<MetaProductColor>().ToList();
                        code.metaItemDetail = multi.Read<MetaItemDetail>().ToList();
                        code.metaProductDetail.BundleItemJson = multi.Read<BundleItemJson>().ToList();
                        code.metaProductDetail.ProductZodiacDetail = multi.Read<ProductZodiacDetail>().FirstOrDefault();
                        code.metaMultipleImages = multi.Read<MetaMultipleImages>().ToList();

                        if (code.metaProductDetail.UIType == 0 || code.metaProductDetail.UIType == 1 || code.metaProductDetail.UIType == 2 || code.metaProductDetail.UIType == 6 || code.metaProductDetail.UIType == 4)
                        {
                            code.metaBBMProductDescription = multi.Read<MetaBBMProductDescription>().ToList();
                        }
                        else
                        {
                            code.metaBBMProductDescription = multi.Read<MetaBBMProductDescription>().ToList();
                            code.metaBBMProductDescription = null;
                        }

                        if (code.metaProductDetail.UIType == 5 || code.metaProductDetail.UIType == 3)
                        {
                            code.metaFurnitureProductDescription = multi.Read<MetaFurnitureProductDescription>().ToList();
                        }
                        else
                        {
                            code.metaFurnitureProductDescription = multi.Read<MetaFurnitureProductDescription>().ToList();
                            code.metaFurnitureProductDescription = null;
                        }

                        code.dimensionImages = multi.Read<DimensionImages>().ToList();
                        if (code.metaProductDetail.UIType == 5 || code.metaProductDetail.UIType == 3)
                        {
                            code.cropImageDetail = multi.Read<CropImageDetail>().ToList();
                        }
                        code.rotationalImages = multi.Read<RotationalImages>().ToList();
                        code.buyerInfo = multi.Read<BuyerInfo>().ToList();

                    }
                    retVal = code;
                    con.Close();
                }
            }
            catch (Exception exc)
            {
                oErrorLog.Open();
                oErrorLog.Log("GetProduct Function In ProductDetailCmd.cs:" + exc.Message);
                oErrorLog.Flush();
                oErrorLog.Close();
            }

            return retVal;
        }
        public StandardDesignDetail GetProductQty(string StandardCode)
        {
            StandardDesignDetail retVal = null;
            try
            {
                using (IDbConnection con = GetOpenSqlConnection())
                {
                    var dp = new DynamicParameters();
                    dp.Add("@StandardCode", StandardCode, DbType.String);
                    StandardDesignDetail code = new StandardDesignDetail();
                    using (var multi = dbCon.QueryMultiple("[GetProduct_Qty]", dp, commandType: CommandType.StoredProcedure))
                    {
                        code.standardDesignInfo = multi.Read<StandardDesignInfo>().FirstOrDefault();
                    }
                    retVal = code;
                    con.Close();
                }
            }
            catch (Exception exc)
            {
                oErrorLog.Open();
                oErrorLog.Log("GetProductQty Function In ProductDetailCmd.cs:" + exc.Message);
                oErrorLog.Flush();
                oErrorLog.Close();
            }

            return retVal;
        }
        public MetaProduct GetAppProductDetail(string StandardCode, int ContentType, int CustomerId, string Category, string Height)
        {
            MetaProduct retVal = null;
            try
            {
                using (IDbConnection con = GetOpenSqlConnection())
                {
                    var dp = new DynamicParameters();
                    dp.Add("@StandardCode", StandardCode, DbType.String);
                    dp.Add("@CustomerId", CustomerId, DbType.Int32);
                    if (!string.IsNullOrEmpty(Category))
                    {
                        dp.Add("@Category", Category, DbType.String);
                    }
                    else
                    {
                        dp.Add("@Category", DBNull.Value, DbType.String);
                    }
                    if (!string.IsNullOrEmpty(Height))
                    {
                        dp.Add("@Height", Height, DbType.String);
                    }
                    else
                    {
                        dp.Add("@Height", DBNull.Value, DbType.String);
                    }

                    MetaProduct code = new MetaProduct();

                    //using (var multi = dbCon.QueryMultiple("[GetProduct_Mobile_New]", dp, commandType: CommandType.StoredProcedure))

                    using (var multi = dbCon.QueryMultiple("[GetProductDetail_App]", dp, commandType: CommandType.StoredProcedure))
                    {
                        code.metaProductDetail = multi.Read<MetaProductDetail>().FirstOrDefault();
                        code.metaProductColor = multi.Read<MetaProductColor>().ToList();
                        code.metaItemDetail = multi.Read<MetaItemDetail>().ToList();
                        code.metaProductDetail.BundleItemJson = multi.Read<BundleItemJson>().ToList();
                        code.metaProductDetail.ProductZodiacDetail = multi.Read<ProductZodiacDetail>().FirstOrDefault();
                        code.metaMultipleImages = multi.Read<MetaMultipleImages>().ToList();

                        if (code.metaProductDetail.UIType == 0 || code.metaProductDetail.UIType == 1 || code.metaProductDetail.UIType == 2 || code.metaProductDetail.UIType == 6 || code.metaProductDetail.UIType == 4)
                        {
                            code.metaBBMProductDescription = multi.Read<MetaBBMProductDescription>().ToList();
                        }
                        else
                        {
                            code.metaBBMProductDescription = multi.Read<MetaBBMProductDescription>().ToList();
                            code.metaBBMProductDescription = null;
                        }

                        if (code.metaProductDetail.UIType == 5 || code.metaProductDetail.UIType == 3)
                        {
                            code.metaFurnitureProductDescription = multi.Read<MetaFurnitureProductDescription>().ToList();
                        }
                        else
                        {
                            code.metaFurnitureProductDescription = multi.Read<MetaFurnitureProductDescription>().ToList();
                            code.metaFurnitureProductDescription = null;
                        }

                        code.dimensionImages = multi.Read<DimensionImages>().ToList();

                    }
                    retVal = code;
                    con.Close();
                }
            }
            catch (Exception exc)
            {
                oErrorLog.Open();
                oErrorLog.Log("GetProduct Function In ProductDetailCmd.cs:" + exc.Message);
                oErrorLog.Flush();
                oErrorLog.Close();
            }

            return retVal;
        }
        public HomePageMenu GetMenu()
        {
            HomePageMenu menu = new HomePageMenu();

            try
            {
                using (IDbConnection con = GetOpenSqlConnection())
                {

                    using (var multi = dbCon.QueryMultiple("[GetMenuList_V1]", null, commandType: CommandType.StoredProcedure))
                    {
                        List<MenuParentCategories> bedParentCategories = multi.Read<MenuParentCategories>().ToList();
                        List<MenuSubCategories> bedSubCategories = multi.Read<MenuSubCategories>().ToList();

                        MenuBed menuBed = new MenuBed() { menuParentCategories = bedParentCategories, menuSubCategories = bedSubCategories };

                        menu.menuBed = menuBed;

                        List<MenuParentCategories> bathParentCategories = multi.Read<MenuParentCategories>().ToList();
                        List<MenuSubCategories> bathSubCategories = multi.Read<MenuSubCategories>().ToList();

                        MenuBath menuBath = new MenuBath() { menuParentCategories = bathParentCategories, menuSubCategories = bathSubCategories };

                        menu.menuBath = menuBath;

                        List<MenuParentCategories> kidsParentCategories = multi.Read<MenuParentCategories>().ToList();
                        List<MenuSubCategories> kidsSubCategories = multi.Read<MenuSubCategories>().ToList();

                        MenuKids menuKids = new MenuKids() { menuParentCategories = kidsParentCategories, menuSubCategories = kidsSubCategories };

                        menu.menuKids = menuKids;

                        List<MenuParentCategories> decorParentCategories = multi.Read<MenuParentCategories>().ToList();
                        List<MenuSubCategories> decorSubCategories = multi.Read<MenuSubCategories>().ToList();

                        MenuDecor menuDecor = new MenuDecor() { menuParentCategories = decorParentCategories, menuSubCategories = decorSubCategories };

                        menu.menuDecor = menuDecor;

                        List<MenuParentCategories> KitchenParentCategories = multi.Read<MenuParentCategories>().ToList();
                        List<MenuSubCategories> KitchenSubCategories = multi.Read<MenuSubCategories>().ToList();

                        Menukitchen menuKitchen = new Menukitchen() { menuParentCategories = KitchenParentCategories, menuSubCategories = KitchenSubCategories };

                        menu.menukitchen = menuKitchen;

                        List<MenuParentCategories> FurnitureParentCategories = multi.Read<MenuParentCategories>().ToList();
                        List<MenuSubCategories> FurnitureSubCategories = multi.Read<MenuSubCategories>().ToList();

                        MenuFurniture menuFurniture = new MenuFurniture() { menuParentCategories = FurnitureParentCategories, menuSubCategories = FurnitureSubCategories };

                        menu.menuFurniture = menuFurniture;

                    }

                    con.Close();
                }
            }
            catch (Exception exc)
            {
                oErrorLog.Open();
                oErrorLog.Log("GetMenu Function In ProductDetailCmd.cs:" + exc.Message);
                oErrorLog.Flush();
                oErrorLog.Close();
            }

            return menu;
        }

        public HomeMenuList GetMenuList()
        {
            HomeMenuList ObjMenu = new HomeMenuList();
            try
            {
                using (var multi = dbCon.QueryMultiple("[GetMenuList_NewWebsite]", null, commandType: CommandType.StoredProcedure))
                {
                    ObjMenu.mainCategory = multi.Read<MainCategory>().ToList();
                    ObjMenu.parentCategory = multi.Read<ParentCategory>().ToList();
                    ObjMenu.displayCategory = multi.Read<DisplayCategory>().ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ObjMenu;
        }


        public int GetProductUIType(string StandardCode)
        {
            int UiTypeId = 0;

            try
            {
                using (IDbConnection con = GetOpenSqlConnection())
                {
                    UiTypeId = dbCon.Query<int>("select UiTypeId from StandardDesign where StandardCode = '" + StandardCode + "'", null, commandType: CommandType.Text).FirstOrDefault();
                    con.Close();
                }
            }
            catch (Exception exc)
            {
                oErrorLog.Open();
                oErrorLog.Log("GetUiType Function In ProductDetailCmd.cs:" + exc.Message);
                oErrorLog.Flush();
                oErrorLog.Close();
            }

            return UiTypeId;
        }

        public ProductUiType GetProductUITypeV1(string StandardCode)
        {
            ProductUiType objProductUiType = new Entities.ProductUiType();

            try
            {
                using (IDbConnection con = GetOpenSqlConnection())
                {
                    objProductUiType = dbCon.Query<ProductUiType>("select UiTypeId,cast(dbo.GetDesignPublished(StandardCode) as bit) as IsPublished from StandardDesign where StandardCode = '" + StandardCode + "'", null, commandType: CommandType.Text).FirstOrDefault();
                    con.Close();
                }
            }
            catch (Exception exc)
            {
                oErrorLog.Open();
                oErrorLog.Log("GetUiType Function In ProductDetailCmd.cs:" + exc.Message);
                oErrorLog.Flush();
                oErrorLog.Close();
            }

            return objProductUiType;
        }


        public decimal getItemDropPrice(string ItemCode)
        {
            decimal ItemPrice = 0;
            try
            {
                using (IDbConnection con = GetOpenSqlConnection())
                {
                    var dp = new DynamicParameters();
                    dp.Add("@ItemCode", ItemCode, DbType.String);

                    ItemPrice = dbCon.Query<decimal>("getItemDropPrice", dp, commandType: CommandType.StoredProcedure).FirstOrDefault();

                    con.Close();
                }
            }
            catch (Exception exc)
            {
                oErrorLog.Open();
                oErrorLog.Log("getItemDropPrice Function In ProductDetailCmd.cs:" + exc.Message);
                oErrorLog.Flush();
                oErrorLog.Close();
            }

            return ItemPrice;
        }
        public DesignImage getProductImages(string StandardCode)
        {
            DesignImage retVal = null;
            try
            {
                using (IDbConnection con = GetOpenSqlConnection())
                {
                    var dp = new DynamicParameters();
                    dp.Add("@StandardCode", StandardCode, DbType.String);
                    DesignImage code = new DesignImage();
                    using (var multi = dbCon.QueryMultiple("[StandardDesign_GetImages]", dp, commandType: CommandType.StoredProcedure))
                    {
                        code.designImage = multi.Read<ImageDetail>().ToList();
                    }
                    retVal = code;
                    con.Close();
                }
            }
            catch (Exception exc)
            {
                oErrorLog.Open();
                oErrorLog.Log("getProductImages Function In ProductDetailCmd.cs:" + exc.Message);
                oErrorLog.Flush();
                oErrorLog.Close();
            }

            return retVal;
        }
        public ImageCoordinate getCorrdinatesDetail(string StandardCode)
        {
            ImageCoordinate retVal = null;
            try
            {
                using (IDbConnection con = GetOpenSqlConnection())
                {
                    var dp = new DynamicParameters();
                    dp.Add("@StandardCode", StandardCode, DbType.String);
                    ImageCoordinate code = new ImageCoordinate();
                    using (var multi = dbCon.QueryMultiple("[StandardDesign_GetImagesCoordinate]", dp, commandType: CommandType.StoredProcedure))
                    {
                        code.cropimageList = multi.Read<CropImageDetail>().ToList();
                    }
                    retVal = code;
                    con.Close();
                }
            }
            catch (Exception exc)
            {
                oErrorLog.Open();
                oErrorLog.Log("getCorrdinatesDetail Function In ProductDetailCmd.cs:" + exc.Message);
                oErrorLog.Flush();
                oErrorLog.Close();
            }

            return retVal;
        }
        public ImageCoordinate getCoordinatesList(string ItemImageId)
        {
            ImageCoordinate retVal = null;
            try
            {
                using (IDbConnection con = GetOpenSqlConnection())
                {
                    var dp = new DynamicParameters();
                    dp.Add("@ItemImageId", ItemImageId, DbType.String);
                    ImageCoordinate code = new ImageCoordinate();
                    using (var multi = dbCon.QueryMultiple("[Image_GetCoordinatesList]", dp, commandType: CommandType.StoredProcedure))
                    {
                        code.cropimageList = multi.Read<CropImageDetail>().ToList();
                    }
                    retVal = code;
                    con.Close();
                }
            }
            catch (Exception exc)
            {
                oErrorLog.Open();
                oErrorLog.Log("getCoordinatesList Function In ProductDetailCmd.cs:" + exc.Message);
                oErrorLog.Flush();
                oErrorLog.Close();
            }

            return retVal;
        }

        public int CropImage_SaveCoordinate(CropImageDetail cropimageDetail)
        {
            int retVal = 0;
            try
            {
                using (IDbConnection con = GetOpenSqlConnection())
                {
                    var dp = new DynamicParameters();
                    if (cropimageDetail.CropId > 0)
                        dp.Add("@Id", cropimageDetail.CropId, DbType.Int32);

                    dp.Add("@ItemImageId", cropimageDetail.ItemImageId, DbType.Int32);
                    dp.Add("@XCoor", cropimageDetail.XCoor, DbType.Decimal);
                    dp.Add("@YCoor", cropimageDetail.YCoor, DbType.Decimal);

                    if (cropimageDetail.Width > 0)
                        dp.Add("@Width", cropimageDetail.Width, DbType.Decimal);
                    if (cropimageDetail.Height > 0)
                        dp.Add("@Height", cropimageDetail.Height, DbType.Decimal);

                    dp.Add("@CropImageUrl", cropimageDetail.CropImageUrl, DbType.String);
                    dp.Add("@LabelName", cropimageDetail.LabelName, DbType.String);
                    dp.Add("@StandardCode", cropimageDetail.StandardCode, DbType.String);
                    dp.Add("@CustomProductLabel", cropimageDetail.CustomProductLabel, DbType.String);
                    dp.Add("@LabelTypeName", cropimageDetail.LabelTypeName, DbType.String);
                    retVal = dbCon.Query<int>("Crop_SaveImage", dp, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return retVal;
        }
        public ProductLabelandMaterialList getProductLabelandMaterialList(string StandardCode)
        {
            ProductLabelandMaterialList retVal = null;
            try
            {
                using (IDbConnection con = GetOpenSqlConnection())
                {
                    var dp = new DynamicParameters();
                    dp.Add("@StandardCode", StandardCode, DbType.String);
                    ProductLabelandMaterialList code = new ProductLabelandMaterialList();
                    using (var multi = dbCon.QueryMultiple("[CustomProduct_GetLabelandMaterailV1]", dp, commandType: CommandType.StoredProcedure))
                    {
                        code.productCustomLabel = multi.Read<ProductCustomLabel>().ToList();
                        code.productCustomLabelType = multi.Read<ProductCustomLabelType>().ToList();

                    }
                    retVal = code;
                    con.Close();
                }
            }
            catch (Exception exc)
            {
                oErrorLog.Open();
                oErrorLog.Log("getCoordinatesList Function In ProductDetailCmd.cs:" + exc.Message);
                oErrorLog.Flush();
                oErrorLog.Close();
            }

            return retVal;
        }

        public string CropImage_RemoveCoordinate(int iCropImageId)
        {
            string retVal = "failed";
            try
            {
                using (IDbConnection con = GetOpenSqlConnection())
                {
                    var dp = new DynamicParameters();
                    dp.Add("@CropId", iCropImageId, DbType.Int32);

                    retVal = dbCon.Query<string>("Crop_RemoveImageCoordinate", dp, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return retVal;
        }



        public Image DownloadImageFromUrl(string imageUrl)
        {
            Image image = null;

            try
            {
                System.Net.HttpWebRequest webRequest = (System.Net.HttpWebRequest)System.Net.HttpWebRequest.Create(imageUrl);
                webRequest.AllowWriteStreamBuffering = true;

                System.Net.WebResponse webResponse = webRequest.GetResponse();

                Stream stream = webResponse.GetResponseStream();

                image = Image.FromStream(stream);

                webResponse.Close();
            }
            catch (Exception ex)
            {
                throw (ex);
                // return null;
            }

            return image;
        }


        public bool GenerateImagebyCoordinates(ImageCoordinate imageCoordinates, string StandardCode)
        {
            AmazonS3 amazon = new AmazonS3();
            bool result = false;
            if (imageCoordinates != null)
            {

                int i = 0;
                Image image = null;
                string ImageUrl = string.Empty;
                string ImageName = string.Empty;
                string FinalRootPath = string.Empty;
                string ImgExtension = string.Empty;
                foreach (CropImageDetail img in imageCoordinates.cropimageList)
                {


                    if (i == 0)
                    {
                        ImageUrl = img.DesignImageUrl;
                        ImageName = img.DesignImageName;
                        ImgExtension = Path.GetExtension(ImageName);
                        i++;



                        string folderName = StandardCode;

                        if (!Directory.Exists(Path.Combine(WebSiteConfig.CropImagePath, folderName)))
                        {
                            Directory.CreateDirectory(Path.Combine(WebSiteConfig.CropImagePath, folderName));
                        }
                        FinalRootPath = WebSiteConfig.CropImagePath + "\\" + folderName;
                        image = DownloadImageFromUrl(ImageUrl);

                    }
                    if (image != null)
                    {
                        string fileName = System.IO.Path.Combine(FinalRootPath, ImageName);
                        image.Save(fileName);
                        string SaveImagePath = fileName;



                        int x = Convert.ToInt32(img.XCoor);
                        int y = Convert.ToInt32(img.YCoor);

                        int Width = 100;
                        int Height = 100;

                        if (img.Width > 0)
                            Width = Convert.ToInt32(img.Width);
                        if (img.Height > 0)
                            Height = Convert.ToInt32(img.Height);


                        string ImageLabelName = Convert.ToString(img.CropId);

                        string CropImagePath = amazon.CropImage(fileName, x, y, Width, Height, ImageLabelName, StandardCode);

                        string FinalImageName = ImageLabelName + ImgExtension;
                        bool IsImageUploadonAmazon = amazon.UploadCropImageFileOnAmazon(CropImagePath, StandardCode.ToLower(), FinalImageName);


                        if (IsImageUploadonAmazon)
                        {
                            amazon.Crop_UpdateImageUploadOnAmazon(ImageLabelName, StandardCode);
                            result = true;
                        }


                    }



                }
                image.Dispose();

            }



            return result;
        }
        public string StandardDesign_UpdateLikes(int customerid,string standardcode,bool islike)
        {
            string retVal = "failed";
            try
            {
                using (IDbConnection con = GetOpenSqlConnection())
                {
                    var dp = new DynamicParameters();
                    dp.Add("@CustomerId", customerid, DbType.Int32);
                    dp.Add("@StandardCode", standardcode, DbType.String);
                    dp.Add("@IsLike", islike, DbType.Boolean);
                    retVal = dbCon.Query<string>("StandardDesign_UpdateLikes", dp, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return retVal;
        }

    }
}