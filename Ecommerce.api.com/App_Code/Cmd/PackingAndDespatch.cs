﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Text;
using Ecommerce.conn;

/// <summary>
/// Summary description for PackingAndDespatch
/// </summary>
public class PackingAndDespatch : Connection
{
    public PackingAndDespatch()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    StringBuilder sbMsg = new StringBuilder();
    string sRemoteAddress = HttpContext.Current.Request.ServerVariables["remote_addr"].ToString();
    public string Message
    {
        set { sbMsg.Append(value); }
        get { return sbMsg.ToString(); }
    }

    public bool CheckEmailSend_V1(string sOrderNo, int Emailtype)
    {
        SqlCommand sqlCmd = null;
        bool bReturnValue = false;

        try
        {
            OpenSQLConnection();

            sqlCmd = new SqlCommand("[Order_CheckEmailSend]", oConnect);
            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlCmd.Parameters.Add("@OrderNo", SqlDbType.VarChar).Value = sOrderNo;
            sqlCmd.Parameters.Add("@EmailType", SqlDbType.VarChar).Value = Emailtype;
            sqlCmd.Parameters.Add("@RValue", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;

            sqlCmd.ExecuteNonQuery();

            bReturnValue = Convert.ToBoolean(sqlCmd.Parameters["@RValue"].Value);
        }
        catch (Exception ex)
        {           
        }
        finally
        {
            sqlCmd.Dispose();
            sqlCmd = null;

            CloseSQLConnection();
        }
        return bReturnValue;
    }

    public void Update_SendEmailLog(string sOrderNo, bool isEmailSend, string sEmailType)
    {
        try
        {

            OpenSQLConnection();
            SqlCommand cmd = new SqlCommand("[Email_UpdateLog]", oConnect);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@OrderNo", SqlDbType.VarChar).Value = sOrderNo;
            cmd.Parameters.Add("@EmailType ", SqlDbType.VarChar).Value = sEmailType;
            cmd.Parameters.Add("@IsEmailSend ", SqlDbType.Bit).Value = isEmailSend;

            cmd.ExecuteNonQuery();
        }

        catch (Exception ex)
        {
        }
        finally
        {
            CloseSQLConnection();
        }
    }


}