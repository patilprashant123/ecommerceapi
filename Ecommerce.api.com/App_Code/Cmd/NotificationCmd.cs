﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ecommerce.config;
using Ecommerce.Entities;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using Ecommerce.ErrorLog;


namespace Ecommerce.Cmd
{
    public class NotoficationCmd : DbConnection
    {
        ErrorLogger oErrorLog = new ErrorLogger();
        public SellerOrderEmail Notification_SellerOrders(string OrderNo)
        {
            SellerOrderEmail retVal = null;
            try
            {
                using (IDbConnection con = GetOpenSqlConnection())
                {
                    var dp = new DynamicParameters();
                    SellerOrderEmail code = new SellerOrderEmail();
                    dp.Add("@OrderNo", OrderNo, DbType.String);
                    using (var multi = dbCon.QueryMultiple("[Notification_SellerOrders]", dp, commandType: CommandType.StoredProcedure))
                    {
                        code.sellerinfo = multi.Read<SellerInfo>().ToList();
                        code.sellerOrderInfo = multi.Read<SellerNoticationData>().ToList();                        
                    }
                    retVal = code;
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                oErrorLog.Open();
                oErrorLog.Log("Notification_SellerOrders in VendorService.cs" + ex.Message);
                oErrorLog.Flush();
                oErrorLog.Close();
            }

            return retVal;
        }
        public string Notification_EmailStatus(string OrderNo, string SellerEmail, string EmailContent, bool emailStatus)
        {
            string retVal = string.Empty;
            try
            {
                using (IDbConnection con = GetOpenSqlConnection())
                {
                    var dp = new DynamicParameters();
                    dp.Add("@OrderNo", OrderNo, DbType.String);
                    dp.Add("@SellerEmail ", SellerEmail, DbType.String);
                    dp.Add("@EmailContent", EmailContent, DbType.String);
                    dp.Add("@IsEmailSend", emailStatus, DbType.String);
                    retVal = dbCon.Query<string>("[Seller_UpdateOrderNotification]", dp, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                oErrorLog.Open();
                oErrorLog.Log("Notification_SellerOrders in VendorService.cs" + ex.Message);
                oErrorLog.Flush();
                oErrorLog.Close();
            }

            return retVal;
        }
    }
}