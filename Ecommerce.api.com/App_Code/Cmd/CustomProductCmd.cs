﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Dapper;
using Ecommerce.Entities;
using Ecommerce.config;
using Ecommerce.ErrorLog;

namespace Ecommerce.Cmd
{
    public class CustomProductCmd : DbConnection
    {
        ErrorLogger oErrorLog = new ErrorLogger();

        public CustomProductList CustomFabricProductList(string taxture, string Material, string color)
        {
            CustomProductList retVal = null;
            try
            {
                using (IDbConnection con = GetOpenSqlConnection())
                {
                    var dp = new DynamicParameters();
                    dp.Add("@TaxTrue", taxture, DbType.String);
                    dp.Add("@Color", color, DbType.String);
                    dp.Add("@Material", Material, DbType.String);
                    CustomProductList code = new CustomProductList();
                    using (var multi = dbCon.QueryMultiple("CustomFabric_GetList", dp, commandType: CommandType.StoredProcedure))
                    {
                        code.SwatchList = multi.Read<SwatchList>().ToList();
                        code.Materialdata = multi.Read<Materialdata>().ToList();
                        code.TaxtureData = multi.Read<TaxtureData>().ToList();
                        code.ColorData = multi.Read<ColorData>().ToList();
                        code.CityData = multi.Read<CityData>().ToList();
                        code.SofaImage = multi.Read<ProductImages>().ToList();
                    }
                    retVal = code;
                    con.Close();
                }
            }
            catch (Exception exc)
            {
                oErrorLog.Open();
                oErrorLog.Log("CustomProductList Function In CustomProductCmd.cs:" + exc.Message);
                oErrorLog.Flush();
                oErrorLog.Close();
            }

            return retVal;
        }

        public CustomProductDetail CustomFabricDetail(string standardCode)
        {
            CustomProductDetail retVal = null;
            try
            {
                using (IDbConnection con = GetOpenSqlConnection())
                {
                    var dp = new DynamicParameters();
                    dp.Add("@StandardCode", standardCode, DbType.String);
                    CustomProductDetail code = new CustomProductDetail();

                    using (var multi = dbCon.QueryMultiple("CustomFabric_GetDetailByStandardCode", dp, commandType: CommandType.StoredProcedure))
                    {
                        code.SwatchDetail = multi.Read<SwatchList>().ToList();
                    }
                    retVal = code;
                    con.Close();
                }
            }
            catch (Exception exc)
            {
                oErrorLog.Open();
                oErrorLog.Log("CustomProductDetail Function In CustomProductCmd.cs:" + exc.Message);
                oErrorLog.Flush();
                oErrorLog.Close();
            }

            return retVal;
        }

        public CustomProductPackSlip CustomFabric_PackSlip(string sOrderNo, string sPrintLabelid)
        {
            CustomProductPackSlip retVal = null;
            try
            {
                using (IDbConnection con = GetOpenSqlConnection())
                {
                    var dp = new DynamicParameters();
                    dp.Add("@PrintLabelId", sPrintLabelid, DbType.String);
                    dp.Add("@OrderNo", sOrderNo, DbType.String);
                    CustomProductPackSlip code = new CustomProductPackSlip();

                    using (var multi = dbCon.QueryMultiple("CustomFabric_OrderPackSlip", dp, commandType: CommandType.StoredProcedure))
                    {
                        code.PackSlip = multi.Read<PackSlip>().ToList();
                    }
                    retVal = code;
                    con.Close();
                }
            }
            catch (Exception exc)
            {
                oErrorLog.Open();
                oErrorLog.Log("CustomProductPackSlip Function In CustomProductCmd.cs:" + exc.Message);
                oErrorLog.Flush();
                oErrorLog.Close();
            }

            return retVal;
        }


        public ShadecardDetail CustomProduct_GetPaintShadecard(string CartId)
        {
            ShadecardDetail retVal = null;
            try
            {
                using (IDbConnection con = GetOpenSqlConnection())
                {
                    var dp = new DynamicParameters();
                    dp.Add("@CartItemId", CartId, DbType.String);
                    ShadecardDetail code = new ShadecardDetail();
                    using (var multi = dbCon.QueryMultiple("CustomProduct_GetPaintAPi", dp, commandType: CommandType.StoredProcedure))
                    {
                        code = multi.Read<ShadecardDetail>().FirstOrDefault();
                        code.swatches = multi.Read<string>().ToList();
                    }
                    retVal = code;
                    con.Close();
                }
            }
            catch (Exception exc)
            {
                oErrorLog.Open();
                oErrorLog.Log("ShadecardDetail Function In CustomProductCmd.cs:" + exc.Message);
                oErrorLog.Flush();
                oErrorLog.Close();
            }


            return retVal;
        }
        public PaintsheetDetail CustomProduct_GetPaintpaintSheet(string CartId)
        {
            PaintsheetDetail retVal = null;
            try
            {
                using (IDbConnection con = GetOpenSqlConnection())
                {
                    var dp = new DynamicParameters();
                    dp.Add("@CartItemId", CartId, DbType.String);
                    PaintsheetDetail code = new PaintsheetDetail();
                    using (var multi = dbCon.QueryMultiple("CustomProduct_GetPaintAPi", dp, commandType: CommandType.StoredProcedure))
                    {
                        code = multi.Read<PaintsheetDetail>().FirstOrDefault();
                    }
                    retVal = code;
                    con.Close();
                }
            }
            catch (Exception exc)
            {
                oErrorLog.Open();
                oErrorLog.Log("PaintsheetDetail Function In CustomProductCmd.cs:" + exc.Message);
                oErrorLog.Flush();
                oErrorLog.Close();
            }

            return retVal;
        }
        public int CustomProduct_SaveShadcardDetail(PaintShadcardUrl s, string cartId)
        {
            int retVal = 0;
            try
            {
                using (IDbConnection con = GetOpenSqlConnection())
                {
                    var dp = new DynamicParameters();
                    dp.Add("@CartItemId", cartId, DbType.String);
                    dp.Add("@ImageUrl", s.url, DbType.String);
                    retVal = dbCon.Query<int>("CustomProduct_SaveShadeCardImg_V1", dp, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    con.Close();
                }
            }
            catch (Exception exc)
            {
                oErrorLog.Open();
                oErrorLog.Log("CustomProduct_SaveShadcardDetail Function In CustomProductCmd.cs:" + exc.Message);
                oErrorLog.Flush();
                oErrorLog.Close();
            }

            return retVal;
        }

        public int IsMailchmipCustomerExist(int CustomerId)
        {
            int retVal = 0;
            try
            {
                using (IDbConnection con = GetOpenSqlConnection())
                {
                    retVal = dbCon.Query<int>("Select CustomerId FROM MailChimpCustomerLog WHERE CustomerId = " + CustomerId, null, commandType: CommandType.Text).FirstOrDefault();
                    con.Close();
                }
            }
            catch (Exception exc)
            {
                oErrorLog.Open();
                oErrorLog.Log("IsCustomerCartExist Function In CustomProductCmd.cs:" + exc.Message);
                oErrorLog.Flush();
                oErrorLog.Close();
            }

            return retVal;
        }

        public int CreateCustomerCart(int CustomerId)
        {
            int retVal = 0;
            try
            {
                using (IDbConnection con = GetOpenSqlConnection())
                {
                    retVal = dbCon.Query<int>("INSERT INTO MailChimpCustomerCart(CustomerCartId) VALUES('" + CustomerId + "') ", null, commandType: CommandType.Text).FirstOrDefault();
                    con.Close();
                }
            }
            catch (Exception exc)
            {
                oErrorLog.Open();
                oErrorLog.Log("IsCustomerCartExist Function In CustomProductCmd.cs:" + exc.Message);
                oErrorLog.Flush();
                oErrorLog.Close();
            }

            return retVal;
        }
    }
}