﻿using Ecommerce.config;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Ecommerce.Entities;
using Dapper;
using Ecommerce.ErrorLog;

namespace Ecommerce.Cmd
{
    public class CustomerCmd : DbConnection
    {
        ErrorLogger oErrorLog = new ErrorLogger();

        string sRemoteAddress = Convert.ToString(HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"]);

        public ProBuilderResponse register(RegisterCustomer customer)
        {
            ProBuilderResponse response = new ProBuilderResponse();

            try
            {
                using (IDbConnection con = GetOpenSqlConnection())
                {
                    var dp = new DynamicParameters();
                    dp.Add("@UserName", customer.name, DbType.String);
                    dp.Add("@FirstName", customer.firstName, DbType.String);
                    dp.Add("@LastName", customer.lastName, DbType.String);
                    dp.Add("@Gender", "M", DbType.String);
                    dp.Add("@Email", customer.email, DbType.String);
                    dp.Add("@UserIpAddress", sRemoteAddress, DbType.String);
                    dp.Add("@CustomerId", DbType.Int32, direction: ParameterDirection.Output);

                    response = dbCon.Query<ProBuilderResponse>("RegisterCustomer_L1", dp, commandType: CommandType.StoredProcedure).FirstOrDefault();

                    con.Close();
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            return response;
        }

        public void createProfileBuilder(string email)
        {
            try
            {
                using (IDbConnection con = GetOpenSqlConnection())
                {
                    dbCon.Query("INSERT INTO ProfileBuilder(email) VALUES('" + email + "')", null, commandType: CommandType.Text);
                    con.Close();
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }

        public void DeleteCustomerAddress(int addressId)
        {
            try
            {
                using (IDbConnection con = GetOpenSqlConnection())
                {
                    dbCon.Query<int>("DELETE FROM CustomerMultipleAddresses WHERE AddressId = " + addressId, null, commandType: CommandType.Text);
                    con.Close();
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }

        public List<Order> GetCustomerOrders(int CustomerId)
        {
            List<Order> orders = new List<Order>();

            try
            {
                using (IDbConnection con = GetOpenSqlConnection())
                {
                    List<Order> orderList = dbCon.Query<Order>("SELECT O.CustomerId,O.OrderNo,O.ORDERID,O.ORDERDATE,O.BILLAMOUNT,S.DESCRIPTION,O.TrackingNo,O.DespatchDate FROM ORDERS O INNER JOIN  ORDERSTATUS S ON O.STATUSID=S.STATUSID INNER JOIN CUSTOMER C ON O.CUSTOMERID=C.CUSTOMERID where O.CustomerId = " + CustomerId + " AND O.PaymentStatus IN(1,2,3,4,8) ORDER BY O.ORDERDATE DESC", null, commandType: CommandType.Text).ToList();

                    if (orderList.Count > 0)
                    {
                        foreach (Order order in orderList)
                        {
                            List<OrderItem> orderItemList = dbCon.Query<OrderItem>("SELECT OI.Description,OI.OrderId FROM ORDERS O INNER JOIN  OrderItems OI ON OI.OrderId=O.OrderId INNER JOIN  ORDERSTATUS S ON O.STATUSID=S.STATUSID INNER JOIN CUSTOMER C ON O.CUSTOMERID=C.CUSTOMERID where O.PaymentStatus IN(1,2,3,4,8) and O.OrderId =" + order.orderId + " ORDER BY O.ORDERDATE DESC", null, commandType: CommandType.Text).ToList();

                            order.orderItems = orderItemList;

                            orders.Add(order);
                        }
                    }
                    con.Close();
                }
            }
            catch (Exception exc)
            {
                oErrorLog.Open();
                oErrorLog.Log("GetCustomers Function In CustomerCmd.cs:" + exc.Message);
                oErrorLog.Flush();
                oErrorLog.Close();
            }
            return orders;
        }
        public List<Order> GetCustomerOrdersByMobileNo(string MobileNo)
        {
            List<Order> orders = new List<Order>();

            try
            {
                using (IDbConnection con = GetOpenSqlConnection())
                {
                    List<Order> orderList = dbCon.Query<Order>("SELECT O.CustomerId,O.OrderNo,O.ORDERID,O.ORDERDATE,O.BILLAMOUNT,S.DESCRIPTION,O.TrackingNo,O.DespatchDate FROM ORDERS O INNER JOIN  ORDERSTATUS S ON O.STATUSID=S.STATUSID INNER JOIN CUSTOMER C ON O.CUSTOMERID=C.CUSTOMERID where C.MobileNo = '" + MobileNo + "' AND O.PaymentStatus IN(1,2,3,4,8) ORDER BY O.ORDERDATE DESC", null, commandType: CommandType.Text).ToList();

                    if (orderList.Count > 0)
                    {
                        foreach (Order order in orderList)
                        {
                            List<OrderItem> orderItemList = dbCon.Query<OrderItem>("SELECT OI.Description,OI.OrderId FROM ORDERS O INNER JOIN  OrderItems OI ON OI.OrderId=O.OrderId INNER JOIN  ORDERSTATUS S ON O.STATUSID=S.STATUSID INNER JOIN CUSTOMER C ON O.CUSTOMERID=C.CUSTOMERID where O.PaymentStatus IN(1,2,3,4,8) and O.OrderId =" + order.orderId + " ORDER BY O.ORDERDATE DESC", null, commandType: CommandType.Text).ToList();

                            order.orderItems = orderItemList;

                            orders.Add(order);
                        }
                    }
                    con.Close();
                }
            }
            catch (Exception exc)
            {
                oErrorLog.Open();
                oErrorLog.Log("GetCustomers Function In CustomerCmd.cs:" + exc.Message);
                oErrorLog.Flush();
                oErrorLog.Close();
            }
            return orders;
        }
        public int getCustomerDetails(string email)
        {
            int CustomerId = 0;

            try
            {
                using (IDbConnection con = GetOpenSqlConnection())
                {
                    CustomerId = dbCon.Query<int>("SELECT CustomerId from WebUsers C WHERE IsGuestUser = 0 AND email = '" + email + "'", null, commandType: CommandType.Text).FirstOrDefault();

                    con.Close();
                }
            }
            catch (Exception exc)
            {
                oErrorLog.Open();
                oErrorLog.Log("getCustomerDetails Function In CustomerCmd.cs:" + exc.Message);
                oErrorLog.Flush();
                oErrorLog.Close();
            }
            return CustomerId;
        }

        public int getCustomerGuest(string email)
        {
            int CustomerId = 0;

            try
            {
                using (IDbConnection con = GetOpenSqlConnection())
                {
                    CustomerId = dbCon.Query<int>("SELECT CustomerId from WebUsers C WHERE IsGuestUser = 1 AND email = '" + email + "'", null, commandType: CommandType.Text).FirstOrDefault();

                    con.Close();
                }
            }
            catch (Exception exc)
            {
                oErrorLog.Open();
                oErrorLog.Log("getCustomerGuest Function In CustomerCmd.cs:" + exc.Message);
                oErrorLog.Flush();
                oErrorLog.Close();
            }
            return CustomerId;
        }

        public int IsCustomerExists(string email)
        {
            int CustomerId = 0;

            try
            {
                using (IDbConnection con = GetOpenSqlConnection())
                {
                    CustomerId = dbCon.Query<int>("SELECT CustomerId from WebUsers WHERE IsGuestUser = 0 AND email = '" + email + "'", null, commandType: CommandType.Text).FirstOrDefault();

                    con.Close();
                }
            }
            catch (Exception exc)
            {
                oErrorLog.Open();
                oErrorLog.Log("IsCustomerExists Function In CustomerCmd.cs:" + exc.Message);
                oErrorLog.Flush();
                oErrorLog.Close();
            }
            return CustomerId;
        }

        public GuestAbandonResponse GuestUserAbandaonCart(GuestAbandonRequest objGuestAbandonRequest)
        {
            GuestAbandonResponse response = new GuestAbandonResponse();

            string CustomerId = CString.Decryptdata(objGuestAbandonRequest.EncrptId);

            try
            {
                using (IDbConnection con = GetOpenSqlConnection())
                {
                    var dp = new DynamicParameters();
                    dp.Add("@CustomerId", Convert.ToInt32(CustomerId), DbType.Int32);
                    dp.Add("@SessionId", objGuestAbandonRequest.SessionId, DbType.String);
                    response = dbCon.Query<GuestAbandonResponse>("GuestAbandanCart", dp, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    con.Close();
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }

            return response;
        }

    }
}