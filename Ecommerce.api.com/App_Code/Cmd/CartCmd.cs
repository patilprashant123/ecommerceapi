﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Text;
using Ecommerce.conn;
using Ecommerce.Entities;
using System.Configuration;

namespace Ecommerce.Cmd
{
    public class CartCmd : Connection
    {
        string sRemoteAddress = Convert.ToString(HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"]);

        StringBuilder sbMsg = new StringBuilder();

        public string Message
        {
            set { sbMsg.Append(value); }
            get { return sbMsg.ToString(); }
        }

        public string CartMsg
        {
            get; set;
        }

        public int ParentCartItemId
        {
            get; set;
        }

        public int PainTypeId
        {
            get; set;
        }

        public bool CreateCartItems(CartEntity ObjCart)
        {
            bool retVal = false;

            try
            {
                OpenSQLConnection();

                SqlCommand cmd = new SqlCommand();

                if (ObjCart.ProductType == "customfabric")
                    cmd.CommandText = "[V05_CreateCartItem_CustomFabric]";
                else if (ObjCart.IsBundleItem)
                    cmd.CommandText = "[CreateCartItem_MultipleProduct_Paint_v1]";
                else
                    cmd.CommandText = "V05_CreateCartItem_V1";


                cmd.Connection = oConnect;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@ItemCode", SqlDbType.VarChar).Value = ObjCart.ItemCode;

                string sWebSessionId = string.Empty;

                if (UserSession.GetWebSessionId() != "")
                    sWebSessionId = UserSession.GetWebSessionId();

                cmd.Parameters.Add("@SessionId", SqlDbType.VarChar).Value = sWebSessionId;
                cmd.Parameters.Add("@Quantity", SqlDbType.BigInt).Value = ObjCart.Qty;

                if (!string.IsNullOrEmpty(ObjCart.Personalize))
                {
                    cmd.Parameters.Add("@PersonlizeText", SqlDbType.VarChar).Value = ObjCart.Personalize;
                    cmd.Parameters.Add("@PersonlizebackInitial", SqlDbType.VarChar).Value = ObjCart.Back;
                }
                else if (!string.IsNullOrEmpty(ObjCart.Front) && !string.IsNullOrEmpty(ObjCart.Back))
                {
                    cmd.Parameters.Add("@PersonlizeText", SqlDbType.VarChar).Value = ObjCart.Front;
                    cmd.Parameters.Add("@PersonlizebackInitial", SqlDbType.VarChar).Value = ObjCart.Back;
                }
                else
                {
                    cmd.Parameters.Add("@PersonlizeText", SqlDbType.VarChar).Value = ObjCart.Personalize;
                    cmd.Parameters.Add("@PersonlizebackInitial", SqlDbType.VarChar).Value = ObjCart.Back;
                }


                if (ObjCart.IsBundleItem)
                {
                    if (ObjCart.BundleItemCode != "")
                        cmd.Parameters.Add("@BundleItemCode", SqlDbType.VarChar).Value = ObjCart.BundleItemCode;
                }

                if (UserSession.GetCurrentCustId() > 0)
                {
                    cmd.Parameters.Add("@CustomerId", SqlDbType.Int).Value = UserSession.GetCurrentCustId();
                }

                if (ObjCart.GiftWrap)
                {
                    cmd.Parameters.Add("@GiftWrapQty", SqlDbType.Int).Value = ObjCart.Qty;
                    cmd.Parameters.Add("@GiftWrapPrice", SqlDbType.Decimal).Value = Convert.ToDecimal(ObjCart.GiftPrice);
                }

                SqlParameter paramErrorMsg = new SqlParameter("@ErrorMessage", SqlDbType.VarChar, 400);
                paramErrorMsg.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(paramErrorMsg);

                SqlParameter paramParentCartId = null;
                SqlParameter paramPaintType = null;

                if (ObjCart.IsBundleItem)
                {
                    paramParentCartId = new SqlParameter("@ParentCartId", SqlDbType.VarChar, 400);
                    paramParentCartId.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(paramParentCartId);

                    paramPaintType = new SqlParameter("@PaintTypeId", SqlDbType.VarChar, 400);
                    paramPaintType.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(paramPaintType);
                }

                cmd.ExecuteNonQuery();

                if (ObjCart.IsBundleItem)
                {
                    if (paramParentCartId.Value != null)
                        ParentCartItemId = Convert.ToInt32(paramParentCartId.Value);

                    if (paramPaintType.Value != null)
                        PainTypeId = Convert.ToInt32(paramPaintType.Value);
                }

                retVal = true;

            }
            catch (Exception ex)
            {
                CartMsg = ex.Message.ToString().Trim();
            }
            finally
            {
                CloseSQLConnection();
            }
            return retVal;
        }

        public void UpdateCartCoupon(string sCouponCode)
        {
            SqlCommand cmd = new SqlCommand();
            try
            {
                OpenSQLConnection();
                cmd.CommandText = "UpdateCartCoupon_v1";
                cmd.Connection = oConnect;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@CustomerId", SqlDbType.Int).Value = UserSession.GetCurrentCustId();
                cmd.Parameters.Add("@SessionId", SqlDbType.VarChar, 100).Value = UserSession.GetWebSessionId();
                cmd.Parameters.Add("@CouponCode", SqlDbType.VarChar, 20).Value = sCouponCode.Trim().ToString();

                SqlParameter paramErrorMsg = new SqlParameter("@ErrorMessage", SqlDbType.VarChar, 400);
                paramErrorMsg.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(paramErrorMsg);

                cmd.ExecuteNonQuery();


                if (paramErrorMsg.Value != null)
                    CartMsg = paramErrorMsg.Value.ToString();


                CloseSQLConnection();
            }
            catch (Exception ex)
            {
                CartMsg = ex.Message.ToString();
            }
            finally
            {
                CloseSQLConnection();
                cmd.Dispose();
            }
        }

        public decimal GetCouponValue()
        {
            string SessionId = UserSession.GetWebSessionId();
            decimal scouponvalue = 0;

            OpenSQLConnection();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = oConnect;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "GetCouponValue_bbm_V1";
            cmd.Parameters.Add("@SessionId", SqlDbType.VarChar).Value = SessionId;
            cmd.Parameters.Add("@CustomerId", SqlDbType.Int).Value = UserSession.GetCurrentCustId();
            cmd.Parameters.Add("@CouponValue", SqlDbType.Decimal).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@CouponName", SqlDbType.VarChar, 50).Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();
            double fcoupon = Convert.ToDouble(cmd.Parameters["@CouponValue"].Value);
            scouponvalue = Convert.ToDecimal(cmd.Parameters["@CouponValue"].Value);
            CloseSQLConnection();
            return scouponvalue;
        }

        public int deleteCartItem(string CartId)
        {
            int Count = 0;
            string SessionId = UserSession.GetWebSessionId();
            if (!string.IsNullOrEmpty(SessionId))
            {
                OpenSQLConnection();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = oConnect;
                cmd.CommandText = "[DeletecartItems_Popup_MK_V2]";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@CartId", SqlDbType.Int).Value = int.Parse(CartId);
                Count = Convert.ToInt32(cmd.ExecuteNonQuery());
                CloseSQLConnection();
            }
            return Count;
        }

        public void updateCartItems(bool IsGiftWrapChecked, string GiftWrapPrice, string ItemCode, string Quantity, string sCartId)
        {
            SqlCommand cmd = new SqlCommand();
            try
            {
                OpenSQLConnection();
                cmd.CommandText = "[V05_UpdateCartItemQty_VS]";
                cmd.Connection = oConnect;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@ItemCode", SqlDbType.VarChar, 40).Value = ItemCode;
                cmd.Parameters.Add("@SessionId", SqlDbType.VarChar, 100).Value = UserSession.GetWebSessionId();
                cmd.Parameters.Add("@Quantity", SqlDbType.Int).Value = long.Parse(Quantity);
                if (UserSession.GetCurrentCustId() > 0)
                {
                    cmd.Parameters.Add("@CustomerId", SqlDbType.Int).Value = UserSession.GetCurrentCustId();
                }
                if (IsGiftWrapChecked)
                {
                    cmd.Parameters.Add("@GiftWrapQty", SqlDbType.Int).Value = long.Parse(Quantity);
                    cmd.Parameters.Add("@GiftWrapPrice", SqlDbType.Decimal).Value = decimal.Parse(GiftWrapPrice);
                }

                cmd.Parameters.Add("@CartItemId", SqlDbType.VarChar).Value = sCartId;

                SqlParameter paramErrorMsg = new SqlParameter("@ErrorMessage", SqlDbType.VarChar, 400);
                paramErrorMsg.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(paramErrorMsg);


                if (paramErrorMsg.Value != null)
                    CartMsg = paramErrorMsg.Value.ToString();

                cmd.ExecuteNonQuery();
                CloseSQLConnection();
            }
            catch (Exception ex)
            {
                CartMsg = ex.Message.ToString();
            }
            finally
            {
                CloseSQLConnection();
                cmd.Dispose();
            }
        }

        public int getCartItemCount()
        {
            CheckOutCmd cmd = new CheckOutCmd();

            CheckOutCartItemDetail code = cmd.CheckOut_GetItems(UserSession.GetWebSessionId());

            return code.CartItem.Sum(s => Convert.ToInt32(s.Quantity));

        }

        public void ClearShoppingCart()
        {
            OpenSQLConnection();
            string sql = "[ClearShoppingCart]";
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = sql;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = oConnect;
            cmd.Parameters.Add("@SessionId", SqlDbType.VarChar).Value = UserSession.GetWebSessionId();
            cmd.ExecuteNonQuery();
            CloseSQLConnection();
        }

        public bool CheckUserCodStatus(string OrderId)
        {
            bool IsValidUser = false;
            SqlCommand sqlCmd = new SqlCommand();
            try
            {
                OpenSQLConnection();
                sqlCmd = new SqlCommand("CheckUserCodStatus", oConnect);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.Add("@OrderNo", SqlDbType.VarChar).Value = OrderId;
                sqlCmd.Parameters.Add("@IsValidUser", SqlDbType.Bit).Direction = ParameterDirection.Output;

                sqlCmd.ExecuteNonQuery();
                IsValidUser = Convert.ToBoolean(sqlCmd.Parameters["@IsValidUser"].Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                sqlCmd.Dispose();
                sqlCmd = null;

                CloseSQLConnection();
            }
            return IsValidUser;
        }

        public bool CheckEmailSend_V1(string sOrderNo, int Emailtype)
        {
            SqlCommand sqlCmd = null;
            bool bReturnValue = false;

            try
            {
                OpenSQLConnection();

                sqlCmd = new SqlCommand("[Order_CheckEmailSend]", oConnect);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.Add("@OrderNo", SqlDbType.VarChar).Value = sOrderNo;
                sqlCmd.Parameters.Add("@EmailType", SqlDbType.VarChar).Value = Emailtype;
                sqlCmd.Parameters.Add("@RValue", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;

                sqlCmd.ExecuteNonQuery();

                bReturnValue = Convert.ToBoolean(sqlCmd.Parameters["@RValue"].Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                sqlCmd.Dispose();
                sqlCmd = null;

                CloseSQLConnection();
            }
            return bReturnValue;
        }

        public void Update_SendEmailLog(string sOrderNo, bool isEmailSend, string sEmailType)
        {
            try
            {

                OpenSQLConnection();
                SqlCommand cmd = new SqlCommand("[Email_UpdateLog]", oConnect);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@OrderNo", SqlDbType.VarChar).Value = sOrderNo;
                cmd.Parameters.Add("@EmailType ", SqlDbType.VarChar).Value = sEmailType;
                cmd.Parameters.Add("@IsEmailSend ", SqlDbType.Bit).Value = isEmailSend;

                cmd.ExecuteNonQuery();
            }

            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseSQLConnection();
            }
        }

        public string GetEmail(string sOrderId)
        {
            string SessionId = UserSession.GetWebSessionId();
            int CustomerId = UserSession.GetCurrentCustId();
            OpenSQLConnection();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = oConnect;
            cmd.CommandText = "Select C.Email from Orders O inner Join Customer C on O.CustomerId=C.CustomerId  where O.OrderNo=@OrderId ";
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.Add("@OrderId", SqlDbType.VarChar).Value = sOrderId;
            object obj = cmd.ExecuteScalar();
            CloseSQLConnection();
            if (obj != null)
            {
                return obj.ToString();
            }
            else
                return string.Empty;

            // GetSubTotal();


        }

        public DataSet GetCustomerData(string OrderId)
        {
            SqlCommand sqlCmd = new SqlCommand();
            SqlDataAdapter dtAdptr = new SqlDataAdapter(sqlCmd);
            DataSet dsDataSet = new DataSet();
            try
            {
                OpenSQLConnection();
                sqlCmd = new SqlCommand("GetCustomerDetails", oConnect);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.Add("@OrderNo", SqlDbType.VarChar).Value = OrderId;
                dtAdptr = new SqlDataAdapter(sqlCmd);
                dsDataSet = new DataSet();
                dtAdptr.Fill(dsDataSet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                dtAdptr.Dispose();
                dtAdptr = null;
                sqlCmd.Dispose();
                sqlCmd = null;
                CloseSQLConnection();
            }
            return dsDataSet;
        }

        public int CreateGuestUser(string sEmailAddress)
        {
            int iUserId = 0;
            int iCustomerId = 0;

            SqlCommand sqlCmd = new SqlCommand();
            try
            {
                OpenSQLConnection();

                //int CustId = GetCustId();
                sqlCmd = new SqlCommand("NewCreateWebUserLogin", oConnect);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                //this.CustomerId = CustId;
                //sqlCmd.Parameters.Add("@CustomerId", SqlDbType.BigInt).Value = CustId;

                sqlCmd.Parameters.Add("@Email", SqlDbType.VarChar).Value = sEmailAddress;
                sqlCmd.Parameters.Add("@UserIpAddress", SqlDbType.VarChar).Value = sRemoteAddress;
                sqlCmd.Parameters.Add("@IsGuestUser", SqlDbType.VarChar).Value = 1;

                SqlParameter pUserId = new SqlParameter("@UserId", SqlDbType.Int);
                pUserId.Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add(pUserId);


                SqlParameter pCustomerId = new SqlParameter("@CustomerId", SqlDbType.Int);
                pCustomerId.Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add(pCustomerId);


                sqlCmd.ExecuteNonQuery();
                iUserId = Convert.ToInt32(pUserId.Value);

                if (iUserId > 0)
                {
                    string sessionId = UserSession.GetWebSessionId();

                    if (string.IsNullOrEmpty(sessionId))
                    {
                        Guid guidSessionId = Guid.NewGuid();
                        sessionId = guidSessionId.ToString();
                    }

                    UserSession.SetCookie("Sid ", sessionId.ToString());
                    UserSession.SetCookie("Cid", sqlCmd.Parameters["@CustomerId"].Value.ToString());
                    UserSession.SetCookie("guestUser", "1");

                    iCustomerId = Convert.ToInt32(pCustomerId.Value);
                }

            }
            catch (Exception ex)
            {

            }
            finally
            {

                sqlCmd.Dispose();
                sqlCmd = null;

                CloseSQLConnection();
            }
            return iCustomerId;

        }

        public DataSet GetProductDetails(string StandardCode, int CustomerId)
        {
            OpenSQLConnection();

            DataSet dsDataSet;
            SqlCommand cmd = new SqlCommand();

            cmd.CommandText = "[V06_GetProductDetails]";

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = oConnect;

            cmd.Parameters.Add("@StandardCode", SqlDbType.VarChar).Value = StandardCode;

            cmd.Parameters.Add("@CustomerId", SqlDbType.VarChar).Value = CustomerId;

            dsDataSet = new DataSet();

            SqlDataAdapter dtAdptr = new SqlDataAdapter(cmd);

            dtAdptr.Fill(dsDataSet);

            CloseSQLConnection();

            return dsDataSet;

        }

        public SqlDataReader GetStateCity(string PinCode)
        {
            SqlDataReader dr = null;
            SqlCommand sqlCmd = new SqlCommand();
            try
            {

                OpenSQLConnection();
                sqlCmd = new SqlCommand("GetStateCity", oConnect);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.Add("@PinCode", SqlDbType.Int).Value = PinCode;
                dr = sqlCmd.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception ex)
            {
                //oErrorLog.Open();
                //oErrorLog.Log("GetCodCustomerAdress: Function IN CashOnDelivery.cs:" + ex.Message);
                //oErrorLog.Flush();
                //oErrorLog.Close();
            }
            finally
            {
                sqlCmd.Dispose();
                sqlCmd = null;
            }
            return dr;
        }

        public DataSet GetCustomerWelcomeCoupon(string sWebUserId)
        {
            SqlCommand sqlCmd = new SqlCommand();
            SqlDataAdapter dtAdptr = new SqlDataAdapter(sqlCmd);

            DataSet dsDataSet = new DataSet();

            try
            {
                OpenSQLConnection();
                sqlCmd = new SqlCommand("V05_GetCustomerWelcomeCoupon", oConnect);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.Add("@WebUserId", SqlDbType.VarChar).Value = sWebUserId;

                dtAdptr = new SqlDataAdapter(sqlCmd);
                dsDataSet = new DataSet();
                dtAdptr.Fill(dsDataSet);
            }
            catch (Exception ex)
            {
                //oErrorLog.Open();
                //oErrorLog.Log("GetCustomerWelcomeCoupon Function In WishList.cs:" + ex.Message);
                //oErrorLog.Flush();
                //oErrorLog.Close();
            }
            finally
            {
                dtAdptr.Dispose();
                dtAdptr = null;

                sqlCmd.Dispose();
                sqlCmd = null;

                CloseSQLConnection();
            }
            return dsDataSet;
        }

        public int UpdateOrderStatus_Exotel(string OrderNo, string StatusId, string sPaymentStatusId, string sCallSid, string sCallStatus)
        {
            int suceess = 0;
            SqlCommand cmd = new SqlCommand();
            try
            {
                OpenSQLConnection();
                cmd.CommandText = "[UpdateOrderPaymentAndStatus_Exotel]";
                cmd.Connection = oConnect;
                cmd.CommandType = CommandType.StoredProcedure;
                if (OrderNo != "")
                    cmd.Parameters.Add("@OrderNo", SqlDbType.VarChar, 500).Value = OrderNo;

                cmd.Parameters.Add("@OrderStatusId", SqlDbType.VarChar, 500).Value = StatusId;
                cmd.Parameters.Add("@PaymentStatusId", SqlDbType.VarChar, 500).Value = sPaymentStatusId;
                cmd.Parameters.Add("@CallsId", SqlDbType.VarChar, 500).Value = sCallSid;
                if (sCallStatus != "")
                    cmd.Parameters.Add("@Status", SqlDbType.VarChar, 500).Value = sCallStatus;

                suceess = cmd.ExecuteNonQuery();
                CloseSQLConnection();
            }
            catch (Exception ex)
            {
                CartMsg = ex.Message.ToString();
                //oErrorLog.Open();
                //oErrorLog.Log("UpdateOrderStatus_Exotel Function In ShoppingCart.cs:" + ex.Message);
                //oErrorLog.Flush();
                //oErrorLog.Close();
            }
            finally
            {
                CloseSQLConnection();
                cmd.Dispose();
            }
            return suceess;
        }

        public int ConfirmAndPushToUniware(string OrderNo, string StatusId, string sPaymentStatusId)
        {
            int suceess = 0;
            SqlCommand cmd = new SqlCommand();
            try
            {
                /*  OpenSQLConnection();
                  cmd.CommandText = "[UpdateOrderPaymentAndStatus]";
                  cmd.Connection = oConnect;
                  cmd.CommandType = CommandType.StoredProcedure;
                  cmd.Parameters.Add("@OrderNo", SqlDbType.VarChar, 500).Value = OrderNo;
                  cmd.Parameters.Add("@OrderStatusId", SqlDbType.VarChar, 500).Value = StatusId;
                  cmd.Parameters.Add("@PaymentStatusId", SqlDbType.VarChar, 500).Value = sPaymentStatusId;


                  suceess = cmd.ExecuteNonQuery();
                  CloseSQLConnection();
                  */
                UpdateOrderStatus(OrderNo, StatusId, sPaymentStatusId);

                //UniComInfo oUniCom = new UniComInfo();
                //bool IsSuccess = oUniCom.CreateUpdateOrder(OrderNo);


            }
            catch (Exception ex)
            {
                CartMsg = ex.Message.ToString();
                //oErrorLog.Open();
                //oErrorLog.Log("ConfirmAndPushToUniware Function In ShoppingCart.cs:" + ex.Message);
                //oErrorLog.Flush();
                //oErrorLog.Close();
            }
            finally
            {
                CloseSQLConnection();
                cmd.Dispose();
            }
            return suceess;
        }

        public int UpdateOrderStatus(string OrderNo, string StatusId, string sPaymentStatusId)
        {
            int suceess = 0;
            SqlCommand cmd = new SqlCommand();
            try
            {
                OpenSQLConnection();
                cmd.CommandText = "[UpdateOrderPaymentAndStatus]";
                cmd.Connection = oConnect;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@OrderNo", SqlDbType.VarChar, 500).Value = OrderNo;
                cmd.Parameters.Add("@OrderStatusId", SqlDbType.VarChar, 500).Value = StatusId;
                cmd.Parameters.Add("@PaymentStatusId", SqlDbType.VarChar, 500).Value = sPaymentStatusId;
                suceess = cmd.ExecuteNonQuery();
                CloseSQLConnection();
            }
            catch (Exception ex)
            {
                CartMsg = ex.Message.ToString();
                //oErrorLog.Open();
                //oErrorLog.Log("UpdateOrderStatus Function In ShoppingCart.cs:" + ex.Message);
                //oErrorLog.Flush();
                //oErrorLog.Close();
            }
            finally
            {
                CloseSQLConnection();
                cmd.Dispose();
            }
            return suceess;
        }

        public string CancelOrder(string sOrderNo, string CancelReason)
        {
            string iResult = string.Empty;
            try
            {


                OpenSQLConnection();

                SqlCommand cmd = new SqlCommand("[ZipDial_CancelOrder]", oConnect);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@OrderNo", SqlDbType.VarChar).Value = sOrderNo;
                cmd.Parameters.Add("@CancelReason", SqlDbType.VarChar).Value = CancelReason;
                cmd.Parameters.Add("@ErrorMessage", SqlDbType.VarChar, 150).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@RValue", SqlDbType.VarChar, 150).Direction = ParameterDirection.ReturnValue;

                cmd.ExecuteNonQuery();
                iResult = cmd.Parameters["@RValue"].Value.ToString();
                Message = cmd.Parameters["@ErrorMessage"].Value.ToString();
            }

            catch (Exception ex)
            {
                //oErrorLog.Open();
                //oErrorLog.Log("CancelOrder in PackingAndDespatch.cs" + ex.Message);
                //oErrorLog.Flush();
                //oErrorLog.Close();
            }
            finally
            {
                CloseSQLConnection();
            }
            return iResult;
        }

        public int getCartCount()
        {
            string SessionId = UserSession.GetWebSessionId();
            int CustomerId = UserSession.GetCurrentCustId();

            OpenSQLConnection();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = oConnect;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "[V05_CheckOut_CartItems_Count]";

            cmd.Parameters.Add("@SessionId", SqlDbType.VarChar, 500).Value = SessionId;
            cmd.Parameters.Add("@CustomerId", SqlDbType.VarChar, 500).Value = CustomerId;


            int Count = Convert.ToInt32(cmd.ExecuteScalar());
            CloseSQLConnection();

            return Count;
        }

        public bool addItemToCart(CartEntity ObjCart)
        {
            bool retVal = false;

            try
            {
                OpenSQLConnection();

                SqlCommand cmd = new SqlCommand();

                if (ObjCart.ProductType == "customfabric")
                    cmd.CommandText = "[V05_CreateCartItem_CustomFabric]";
                else if (ObjCart.IsBundleItem)
                    cmd.CommandText = "[CreateCartItem_MultipleProduct_Paint_v1]";
                else
                    cmd.CommandText = "V05_CreateCartItem_V1";


                cmd.Connection = oConnect;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@ItemCode", SqlDbType.VarChar).Value = ObjCart.ItemCode;

                string sWebSessionId = string.Empty;

                if (ObjCart.SessionId != "")
                    sWebSessionId = ObjCart.SessionId;

                cmd.Parameters.Add("@SessionId", SqlDbType.VarChar).Value = sWebSessionId;
                cmd.Parameters.Add("@Quantity", SqlDbType.BigInt).Value = ObjCart.Qty;

                if (!string.IsNullOrEmpty(ObjCart.Personalize))
                {
                    cmd.Parameters.Add("@PersonlizeText", SqlDbType.VarChar).Value = ObjCart.Personalize;
                    cmd.Parameters.Add("@PersonlizebackInitial", SqlDbType.VarChar).Value = ObjCart.Back;
                }
                else if (!string.IsNullOrEmpty(ObjCart.Front) && !string.IsNullOrEmpty(ObjCart.Back))
                {
                    cmd.Parameters.Add("@PersonlizeText", SqlDbType.VarChar).Value = ObjCart.Front;
                    cmd.Parameters.Add("@PersonlizebackInitial", SqlDbType.VarChar).Value = ObjCart.Back;
                }
                else
                {
                    cmd.Parameters.Add("@PersonlizeText", SqlDbType.VarChar).Value = ObjCart.Personalize;
                    cmd.Parameters.Add("@PersonlizebackInitial", SqlDbType.VarChar).Value = ObjCart.Back;
                }

                if (ObjCart.IsBundleItem)
                {
                    if (ObjCart.BundleItemCode != "")
                        cmd.Parameters.Add("@BundleItemCode", SqlDbType.VarChar).Value = ObjCart.BundleItemCode;
                }

                if (ObjCart.CustomerId > 0)
                {
                    cmd.Parameters.Add("@CustomerId", SqlDbType.Int).Value = ObjCart.CustomerId;
                }

                if (ObjCart.GiftWrap)
                {
                    cmd.Parameters.Add("@GiftWrapQty", SqlDbType.Int).Value = ObjCart.Qty;
                    cmd.Parameters.Add("@GiftWrapPrice", SqlDbType.Decimal).Value = Convert.ToDecimal(ObjCart.GiftPrice);
                }

                SqlParameter paramErrorMsg = new SqlParameter("@ErrorMessage", SqlDbType.VarChar, 400);
                paramErrorMsg.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(paramErrorMsg);

                SqlParameter paramParentCartId = null;
                SqlParameter paramPaintType = null;

                if (ObjCart.IsBundleItem)
                {
                    paramParentCartId = new SqlParameter("@ParentCartId", SqlDbType.VarChar, 400);
                    paramParentCartId.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(paramParentCartId);

                    paramPaintType = new SqlParameter("@PaintTypeId", SqlDbType.VarChar, 400);
                    paramPaintType.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(paramPaintType);
                }

                cmd.ExecuteNonQuery();

                if (ObjCart.IsBundleItem)
                {
                    if (paramParentCartId.Value != null)
                        ParentCartItemId = Convert.ToInt32(paramParentCartId.Value);

                    if (paramPaintType.Value != null)
                        PainTypeId = Convert.ToInt32(paramPaintType.Value);
                }

                retVal = true;

            }
            catch (Exception ex)
            {
                CartMsg = ex.Message.ToString().Trim();
            }
            finally
            {
                CloseSQLConnection();
            }
            return retVal;
        }
        public bool addAppItemToCart(CartEntity ObjCart)
        {
            bool retVal = false;

            try
            {
                OpenSQLConnection();

                SqlCommand cmd = new SqlCommand();


                cmd.CommandText = "V05_CreateCartItem_App";
                cmd.Connection = oConnect;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@ItemCode", SqlDbType.VarChar).Value = ObjCart.ItemCode;

                string sWebSessionId = string.Empty;

                if (ObjCart.SessionId != "")
                    sWebSessionId = ObjCart.SessionId;

                cmd.Parameters.Add("@SessionId", SqlDbType.VarChar).Value = sWebSessionId;
                cmd.Parameters.Add("@Quantity", SqlDbType.BigInt).Value = ObjCart.Qty;

                if (!string.IsNullOrEmpty(ObjCart.Personalize))
                {
                    cmd.Parameters.Add("@PersonlizeText", SqlDbType.VarChar).Value = ObjCart.Personalize;
                    cmd.Parameters.Add("@PersonlizebackInitial", SqlDbType.VarChar).Value = ObjCart.Back;
                }
                else if (!string.IsNullOrEmpty(ObjCart.Front) && !string.IsNullOrEmpty(ObjCart.Back))
                {
                    cmd.Parameters.Add("@PersonlizeText", SqlDbType.VarChar).Value = ObjCart.Front;
                    cmd.Parameters.Add("@PersonlizebackInitial", SqlDbType.VarChar).Value = ObjCart.Back;
                }
                else
                {
                    cmd.Parameters.Add("@PersonlizeText", SqlDbType.VarChar).Value = ObjCart.Personalize;
                    cmd.Parameters.Add("@PersonlizebackInitial", SqlDbType.VarChar).Value = ObjCart.Back;
                }

                if (ObjCart.IsBundleItem)
                {
                    if (ObjCart.BundleItemCode != "")
                        cmd.Parameters.Add("@BundleItemCode", SqlDbType.VarChar).Value = ObjCart.BundleItemCode;
                }

                if (ObjCart.CustomerId > 0)
                {
                    cmd.Parameters.Add("@CustomerId", SqlDbType.Int).Value = ObjCart.CustomerId;
                }

                if (ObjCart.GiftWrap)
                {
                    cmd.Parameters.Add("@GiftWrapQty", SqlDbType.Int).Value = ObjCart.Qty;
                    cmd.Parameters.Add("@GiftWrapPrice", SqlDbType.Decimal).Value = Convert.ToDecimal(ObjCart.GiftPrice);
                }

                SqlParameter paramErrorMsg = new SqlParameter("@ErrorMessage", SqlDbType.VarChar, 400);
                paramErrorMsg.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(paramErrorMsg);

                SqlParameter paramParentCartId = null;
                SqlParameter paramPaintType = null;

                if (ObjCart.IsBundleItem)
                {
                    paramParentCartId = new SqlParameter("@ParentCartId", SqlDbType.VarChar, 400);
                    paramParentCartId.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(paramParentCartId);

                    paramPaintType = new SqlParameter("@PaintTypeId", SqlDbType.VarChar, 400);
                    paramPaintType.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(paramPaintType);
                }

                cmd.ExecuteNonQuery();

                if (ObjCart.IsBundleItem)
                {
                    if (paramParentCartId.Value != null)
                        ParentCartItemId = Convert.ToInt32(paramParentCartId.Value);

                    if (paramPaintType.Value != null)
                        PainTypeId = Convert.ToInt32(paramPaintType.Value);
                }

                retVal = true;

            }
            catch (Exception ex)
            {
                CartMsg = ex.Message.ToString().Trim();
            }
            finally
            {
                CloseSQLConnection();
            }
            return retVal;
        }
        public void updateCart(int CustomerId, string SessionId, bool IsGiftWrapChecked, string GiftWrapPrice, string ItemCode, string Quantity, string sCartId)
        {
            SqlCommand cmd = new SqlCommand();
            try
            {
                OpenSQLConnection();
                cmd.CommandText = "[V05_UpdateCartItemQty_VS]";
                cmd.Connection = oConnect;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@ItemCode", SqlDbType.VarChar, 40).Value = ItemCode;
                cmd.Parameters.Add("@SessionId", SqlDbType.VarChar, 100).Value = SessionId;
                cmd.Parameters.Add("@Quantity", SqlDbType.Int).Value = long.Parse(Quantity);
                if (CustomerId > 0)
                {
                    cmd.Parameters.Add("@CustomerId", SqlDbType.Int).Value = CustomerId;
                }
                if (IsGiftWrapChecked)
                {
                    cmd.Parameters.Add("@GiftWrapQty", SqlDbType.Int).Value = long.Parse(Quantity);
                    cmd.Parameters.Add("@GiftWrapPrice", SqlDbType.Decimal).Value = decimal.Parse(GiftWrapPrice);
                }

                cmd.Parameters.Add("@CartItemId", SqlDbType.VarChar).Value = sCartId;

                SqlParameter paramErrorMsg = new SqlParameter("@ErrorMessage", SqlDbType.VarChar, 400);
                paramErrorMsg.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(paramErrorMsg);


                if (paramErrorMsg.Value != null)
                    CartMsg = paramErrorMsg.Value.ToString();

                cmd.ExecuteNonQuery();
                CloseSQLConnection();
            }
            catch (Exception ex)
            {
                CartMsg = ex.Message.ToString();
            }
            finally
            {
                CloseSQLConnection();
                cmd.Dispose();
            }
        }

        public void UpdateCoupon(int CustomerId, string SessionId, string sCouponCode)
        {
            SqlCommand cmd = new SqlCommand();
            try
            {
                OpenSQLConnection();
                cmd.CommandText = "UpdateCartCoupon_v1";
                cmd.Connection = oConnect;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@CustomerId", SqlDbType.Int).Value = CustomerId;
                cmd.Parameters.Add("@SessionId", SqlDbType.VarChar, 100).Value = SessionId;
                cmd.Parameters.Add("@CouponCode", SqlDbType.VarChar, 20).Value = sCouponCode.Trim().ToString();

                SqlParameter paramErrorMsg = new SqlParameter("@ErrorMessage", SqlDbType.VarChar, 400);
                paramErrorMsg.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(paramErrorMsg);

                cmd.ExecuteNonQuery();


                if (paramErrorMsg.Value != null)
                    CartMsg = paramErrorMsg.Value.ToString();


                CloseSQLConnection();
            }
            catch (Exception ex)
            {
                CartMsg = ex.Message.ToString();
            }
            finally
            {
                CloseSQLConnection();
                cmd.Dispose();
            }
        }

        public decimal GetPromoCouponValue(int CustomerId, string SessionId)
        {
            decimal scouponvalue = 0;

            OpenSQLConnection();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = oConnect;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "GetCouponValue_bbm_V1";
            cmd.Parameters.Add("@SessionId", SqlDbType.VarChar).Value = SessionId;
            cmd.Parameters.Add("@CustomerId", SqlDbType.Int).Value = CustomerId;
            cmd.Parameters.Add("@CouponValue", SqlDbType.Decimal).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@CouponName", SqlDbType.VarChar, 50).Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();
            double fcoupon = Convert.ToDouble(cmd.Parameters["@CouponValue"].Value);
            scouponvalue = Convert.ToDecimal(cmd.Parameters["@CouponValue"].Value);
            CloseSQLConnection();
            return scouponvalue;
        }

        public int deleteMCartItem(string SessionId, string CartId)
        {
            int Count = 0;
            if (!string.IsNullOrEmpty(SessionId))
            {
                OpenSQLConnection();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = oConnect;
                cmd.CommandText = "[DeletecartItems_Popup_MK_V2]";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@CartId", SqlDbType.Int).Value = int.Parse(CartId);
                Count = Convert.ToInt32(cmd.ExecuteNonQuery());
                CloseSQLConnection();
            }
            return Count;
        }

        public int getCartItemsCount(string SessionId, int CustomerId)
        {

            OpenSQLConnection();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = oConnect;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "[V05_CheckOut_CartItems_Count]";

            cmd.Parameters.Add("@SessionId", SqlDbType.VarChar, 500).Value = SessionId;
            cmd.Parameters.Add("@CustomerId", SqlDbType.VarChar, 500).Value = CustomerId;


            int Count = Convert.ToInt32(cmd.ExecuteScalar());
            CloseSQLConnection();

            return Count;
        }

        public int CreateErrorLog(string Error, string stk, string InException)
        {

            OpenSQLConnection();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = oConnect;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "[BBM_ErrorLog]";

            cmd.Parameters.Add("@Error", SqlDbType.VarChar).Value = Error;
            cmd.Parameters.Add("@StackTrace", SqlDbType.VarChar).Value = stk;
            cmd.Parameters.Add("@InException", SqlDbType.VarChar).Value = InException;

            int Count = Convert.ToInt32(cmd.ExecuteNonQuery());

            CloseSQLConnection();

            return Count;
        }

        public int CreateGuestUserMobile(string sEmailAddress)
        {
            int iUserId = 0;
            int iCustomerId = 0;

            SqlCommand sqlCmd = new SqlCommand();
            try
            {
                OpenSQLConnection();

                sqlCmd = new SqlCommand("NewCreateWebUserLogin", oConnect);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                sqlCmd.Parameters.Add("@Email", SqlDbType.VarChar).Value = sEmailAddress;
                sqlCmd.Parameters.Add("@UserIpAddress", SqlDbType.VarChar).Value = sRemoteAddress;
                sqlCmd.Parameters.Add("@IsGuestUser", SqlDbType.VarChar).Value = 1;

                SqlParameter pUserId = new SqlParameter("@UserId", SqlDbType.Int);
                pUserId.Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add(pUserId);


                SqlParameter pCustomerId = new SqlParameter("@CustomerId", SqlDbType.Int);
                pCustomerId.Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add(pCustomerId);


                sqlCmd.ExecuteNonQuery();
                iUserId = Convert.ToInt32(pUserId.Value);

                if (iUserId > 0)
                {
                    iCustomerId = Convert.ToInt32(pCustomerId.Value);
                }

            }
            catch (Exception ex)
            {

            }
            finally
            {

                sqlCmd.Dispose();
                sqlCmd = null;

                CloseSQLConnection();
            }
            return iCustomerId;

        }

        public DataSet GetOrderItemsForEncompass(string OrderNo)
        {
            SqlCommand sqlCmd = new SqlCommand();
            SqlDataAdapter dtAdptr = new SqlDataAdapter(sqlCmd);
            DataSet dsDataSet = new DataSet();
            try
            {
                OpenSQLConnection();
                sqlCmd = new SqlCommand("GetOrderItemsForEncompass", oConnect);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.Add("@OrderNo", SqlDbType.VarChar).Value = OrderNo;
                dtAdptr = new SqlDataAdapter(sqlCmd);
                dsDataSet = new DataSet();
                dtAdptr.Fill(dsDataSet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                dtAdptr.Dispose();
                dtAdptr = null;
                sqlCmd.Dispose();
                sqlCmd = null;
                CloseSQLConnection();
            }
            return dsDataSet;
        }


        public int UpdateItemStockEncompass(string ItemCode, int Quantity)
        {
            SqlConnection objConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["encompassSqlDb"].ToString());

            objConnection.Open();

            int Count = 0;

            SqlCommand sqlCmd = new SqlCommand();

            try
            {
                sqlCmd.Connection = objConnection;
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.CommandText = "UpdateItemsStockEncompass";

                sqlCmd.Parameters.Add("@ItemCode", SqlDbType.VarChar).Value = ItemCode;
                sqlCmd.Parameters.Add("@Quantity", SqlDbType.Int).Value = Quantity;

                Count = Convert.ToInt32(sqlCmd.ExecuteNonQuery());
            }
            catch (Exception ex)
            {

            }
            finally
            {
                sqlCmd.Dispose();
                sqlCmd = null;
                objConnection.Close();
            }
            return Count;
        }
    }
}