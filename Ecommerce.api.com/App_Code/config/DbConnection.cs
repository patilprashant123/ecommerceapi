﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Data;

namespace Ecommerce.config
{
    public class DbConnection
    {
        IDbConnection m_con = null;
        public string DefaultConnectionSetting { get; set; }
        public IDbConnection dbCon
        {
            get
            {
                if (m_con == null)
                {
                    m_con = GetOpenSqlConnection();
                }
                if (m_con.State == ConnectionState.Closed)
                {
                    m_con.Open();
                }
                if (m_con.State == ConnectionState.Open)
                {
                    m_con.Close();
                }

                return m_con;
            }
        }


        public string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings[DefaultConnectionSetting].ConnectionString;
            }
        }

        public DbConnection()
        {
            this.DefaultConnectionSetting = "bbmSqlDb";
        }

        public void CloseSqlConnection(IDbConnection con)
        {
            con.Close();
        }


        public SqlConnection OpenConnection()
        {
            SqlConnection con = new SqlConnection();
            con.ConnectionString = this.ConnectionString;
            con.Open();

            return con;
        }

        public SqlConnection GetOpenSqlConnection()
        {
            SqlConnection con = new SqlConnection();
            con.ConnectionString = this.ConnectionString;
            con.Open();

            return con;
        }
        public SqlConnection GetOpenSqlConnection(string ConnectionString)
        {
            SqlConnection con = new SqlConnection();
            con.ConnectionString = ConnectionString;
            con.Open();

            return con;
        }
    }
}