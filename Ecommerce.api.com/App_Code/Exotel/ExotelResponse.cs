﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Collections.Generic;

using System.Xml;
using System.Xml.Serialization;
using System.Net;
using System.IO;
using System.Text.RegularExpressions;
using Ecommerce.conn;
using Ecommerce.ErrorLog;
/// <summary>
/// Summary description for ExotelResponse
/// </summary>
public class ExotelResponse : Connection
{
    public ExotelResponse()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    #region local Variables
    ErrorLogger oErrorLog = new ErrorLogger();
    #endregion
    public int ExotelResponse_Log(string CallSid, string Status, string RecordingUrl, string dateUpdated, string callStatus, string sCustomField, string OrderStatus,
                                 string sDirection, string ForwardedFrom, string DialCallDuration, string StartTime, string EndTime, string CallType, string DialWhomNumber, string flow_id, string tenant_id)
    {
        int suceess = 0;
        SqlCommand cmd = new SqlCommand();
        try
        {
            OpenSQLConnection();
            cmd.CommandText = "[Exotel_UpdateCodVerificationLog]";
            cmd.Connection = oConnect;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@CallSid", SqlDbType.VarChar, 500).Value = CallSid;
            cmd.Parameters.Add("@Status", SqlDbType.VarChar, 500).Value = Status;
            cmd.Parameters.Add("@RecordingUrl", SqlDbType.VarChar, 500).Value = RecordingUrl;
            cmd.Parameters.Add("@DateUpdated", SqlDbType.VarChar, 500).Value = dateUpdated;
            cmd.Parameters.Add("@CallStatus", SqlDbType.VarChar, 500).Value = callStatus;
            cmd.Parameters.Add("@CustomField", SqlDbType.VarChar, 500).Value = sCustomField;
            cmd.Parameters.Add("@Direction", SqlDbType.VarChar, 500).Value = sDirection;
            cmd.Parameters.Add("@ForwardedFrom", SqlDbType.VarChar, 500).Value = ForwardedFrom;
            cmd.Parameters.Add("@DialCallDuration", SqlDbType.VarChar, 500).Value = DialWhomNumber;
            cmd.Parameters.Add("@StartTime", SqlDbType.VarChar, 500).Value = StartTime;
            cmd.Parameters.Add("@EndTime", SqlDbType.VarChar, 500).Value = EndTime;
            cmd.Parameters.Add("@CallType", SqlDbType.VarChar, 500).Value = CallType;
            cmd.Parameters.Add("@DialWhomNumber", SqlDbType.VarChar, 500).Value = DialWhomNumber;
            cmd.Parameters.Add("@flow_id", SqlDbType.VarChar, 500).Value = flow_id;
            cmd.Parameters.Add("@tenant_id", SqlDbType.VarChar, 500).Value = tenant_id;
            cmd.Parameters.Add("@Digits", SqlDbType.VarChar, 500).Value = HttpUtility.HtmlDecode(OrderStatus);
            suceess = cmd.ExecuteNonQuery();
            CloseSQLConnection();
        }
        catch (Exception ex)
        {
            oErrorLog.Open();
            oErrorLog.Log("ExotelResponse Function In ExotelResponse.cs:" + ex.Message);
            oErrorLog.Flush();
            oErrorLog.Close();
        }
        finally
        {
            CloseSQLConnection();
            cmd.Dispose();
        }
        return suceess;
    }

    public void ExotelRequest_Log(string CallSid, string sOrderNo)
    {
        
        SqlCommand cmd = new SqlCommand();
        try
        {
            OpenSQLConnection();
            cmd.CommandText = "[ExotelRequest_Log]";
            cmd.Connection = oConnect;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@CallSid", SqlDbType.VarChar, 500).Value = CallSid;
            cmd.Parameters.Add("@OrderNo", SqlDbType.VarChar, 500).Value = sOrderNo;
            cmd.ExecuteNonQuery();
            CloseSQLConnection();
        }
        catch (Exception ex)
        {
            oErrorLog.Open();
            oErrorLog.Log("ExotelRequest_Log Function In ExotelResponse.cs:" + ex.Message);
            oErrorLog.Flush();
            oErrorLog.Close();
        }
        finally
        {
            CloseSQLConnection();
            cmd.Dispose();
        }
    }
    public DataSet ConvertXMLToDataSet(string xmlData)
    {
        StringReader stream = null;
        XmlTextReader reader = null;
        try
        {
            DataSet xmlDS = new DataSet();
            stream = new StringReader(xmlData);            
            reader = new XmlTextReader(stream);
            xmlDS.ReadXml(reader);
            return xmlDS;
        }
        catch
        {
            return null;
        }
        finally
        {
            if (reader != null) reader.Close();
        }
    }

}