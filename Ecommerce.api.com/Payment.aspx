﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Payment.aspx.cs" Inherits="Ecommerce.api.com.Payment" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body onload="document.form1.submit();">
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <center>
        <strong>Redirecting to Secure Payment Gateway.......</strong><br />
        Please wait and do not press <b>BACK</b> or <b>REFRESH</b> button of your browser.<br />
        <br />
        <br />
        <asp:Label ID="lbAmount" runat="server"></asp:Label><br />
        <br />
    </center>
    <form id="form1" name="form1" action="<%=PaymentUrl%>" method="post">
        <%=UserControlsHTML%>
    </form>
</body>
</html>
