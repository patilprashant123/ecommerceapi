﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Ecommerce.api.com
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ShoppingCart oShoppingCart = new ShoppingCart();

            oShoppingCart.UpdateMenu();

            HttpContext.Current.Cache.Remove("TopMenu");
            HttpContext.Current.Cache.Remove("TopMenuV1");

        }
    }
}