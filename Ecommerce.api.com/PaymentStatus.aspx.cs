﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Uniware;
using System.Text;
using System.IO;
using Mandrill;
using System.Web.UI.HtmlControls;
using Ecommerce.Entities;
using Ecommerce.Cmd;
using IntegrationKit;
using Razorpay.Api;
using Newtonsoft.Json;
using paytm;


namespace Ecommerce.api.com
{
    public partial class PaymentStatus : PaymentGatewayBase
    {
        #region Global Variables
        NotoficationCmd notification = new NotoficationCmd();
        PackingAndDespatch oOders = new PackingAndDespatch();

        public string OrderNo { get; set; }
        public string OrderId { get; set; }
        public string PaymentType { get; set; }

        string sCodZipDialThankUMsg = " Your order has been placed successfully via Cash on Delivery. You will <b>receive a CALL</b> shortly to confirm your order details, post which we will process and ship your order.<br />" +
                                   " In case we are unable to reach you or you don&#39;t want to wait for the CALL, -we request you to call our Customer Delight Center on <font style=\"font-size: 14px; font-weight: bold;\">+91-22-2491 0066</font> between 10 AM to 7 PM (Monday to Saturday) to confirm your order.";
        string sCodThankUMsg = "Your order has been confirmed successfully via Cash on Delivery. We will process and ship your order as promised..";

        public string PaymentErrorCode = string.Empty;

        #endregion

        #region SqlConnection

        SqlConnection con;

        private void OpenSqlConnection()
        {
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["bbmSqlDb"].ToString());
            con.Open();
        }

        private void CloseSqlConnection()
        {
            con.Close();
        }


        #endregion

        #region Page Propertites

        private string ExotelSID
        {
            get { return Convert.ToString(ConfigurationManager.AppSettings["ExotelSID"]); }
        }
        private string ExotelToken
        {
            get { return Convert.ToString(ConfigurationManager.AppSettings["ExotelToken"]); }
        }
        private string ExotelCodAppUrl
        {
            get { return Convert.ToString(ConfigurationManager.AppSettings["ExotelCodAppUrl"]); }
        }
        private string ExotelVirtualNo
        {
            get { return Convert.ToString(ConfigurationManager.AppSettings["ExotelVirtualNo"]); }
        }
        private string ExotelCallType
        {
            get { return Convert.ToString(ConfigurationManager.AppSettings["ExotelCallType"]); }
        }


        private string OrderEmail
        {
            get { return Convert.ToString(ConfigurationManager.AppSettings["OrderEmail"]); }
        }

        public string Name
        {
            get; set;
        }

        public string _Address = string.Empty;

        public string Address
        {
            get; set;
        }

        public string PinCode
        {
            get; set;
        }

        public string State
        {
            get; set;
        }


        public string City
        {
            get; set;
        }

        public string _MobileNo = string.Empty;

        public string MobileNo
        {
            get; set;
        }

        public decimal merchTotal
        {
            get; set;
        }

        public string razorpay_payment_id
        {
            get
            {
                if (Request.Cookies["razorpay_payment_id"] != null)
                {
                    return Request.Cookies["razorpay_payment_id"].Value;
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        public string ProcessOrderUrl
        {
            get
            {
                return Convert.ToString(ConfigurationManager.AppSettings["ProcessOrder"]);
            }
        }
        #endregion

        #region PayTM
        public int PayTMResponseCode
        {
            get
            {
                if (!string.IsNullOrEmpty(Request["RESPCODE"]) && Request["RESPCODE"] != "")
                    return Convert.ToInt32(Request["RESPCODE"]);
                else
                    return 0;


            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            Response.ExpiresAbsolute = DateTime.Now.AddDays(-1d);
            Response.Expires = -1500;
            Response.CacheControl = "no-cache";

            if (!Page.IsPostBack)
            {
                if (Request.Form["Order_Id"] != null) //Handle CC Avenue Payment Gatweay Response
                {
                    if (!string.IsNullOrEmpty(Request.Form["Order_Id"].ToString()))
                    {
                        this.OrderId = Convert.ToString(Request.Form["Order_Id"]);
                        OrderNo = this.OrderId;
                        bool bstatus = GetCCAvenuepaymentStatus();
                    }
                }
                else if (!string.IsNullOrEmpty(razorpay_payment_id))
                {
                    RazorPayResponse();
                }
                else if (PayTMResponseCode > 0)
                {
                    PayTMResponse();
                }
                else // Handles COD & Prepaid PayU Respose
                {
                    PayUResponse();
                }
            }
        }

        public override void VerifyRenderingInServerForm(Control control)
        {

        }

        #region Pay Response and COD Order Response

        public void PayTMResponse()
        {

            if (!string.IsNullOrEmpty(Request["ORDERID"]) && Request["ORDERID"] != "")
            {


                Dictionary<string, string> parameters = new Dictionary<string, string>();
                string paytmChecksum = "";

                foreach (string key in Request.Form.Keys)
                {
                    parameters.Add(key.Trim(), Request.Form[key].Trim());

                    // Response.Write(key.Trim() + " - " + Request.Form[key].Trim() + "<br />");
                }

                if (parameters.ContainsKey("CHECKSUMHASH"))
                {
                    paytmChecksum = parameters["CHECKSUMHASH"];
                    parameters.Remove("CHECKSUMHASH");
                }


                if (!string.IsNullOrEmpty(Request.Form["RESPCODE"]) && Request.Form["RESPCODE"] != "")
                {
                    //Response.Write("Checksum Matched");
                    PaytmPaymentLog();
                    if (!string.IsNullOrEmpty(Request["ORDERID"]) && Request["ORDERID"] != "")
                    {

                        this.POrderNo = Convert.ToString(Request.Form["ORDERID"]);
                        OrderNo = POrderNo;
                        Get_PayTM_PaymentPayUStatus();
                        Response.Redirect(ProcessOrderUrl + this.POrderNo);

                    }


                }
                /*
                else
                {
                    Response.Write("Checksum MisMatch");
                }*/
            }

        }

        public void PayUResponse()
        {
            if (!string.IsNullOrEmpty(Request["txnid"]) && Request["txnid"] != "")
            {
                PayUPaymentLog();
            }

            if (!string.IsNullOrEmpty(HttpContext.Current.Request["txnid"]) && HttpContext.Current.Request["txnid"] != "")
            {
                this.POrderNo = Convert.ToString(HttpContext.Current.Request.Form["txnid"]);

                OrderNO = POrderNo;
            }

            if (!string.IsNullOrEmpty(Request["txnid"]) && Request["txnid"] != "")
            {
                if (!string.IsNullOrEmpty(Request["txnid"]) && Request["txnid"] != "")
                {
                    if (!string.IsNullOrEmpty(Request.Form["txnid"].ToString()))
                        this.OrderId = Convert.ToString(Request.Form["txnid"]);
                }

                GetPaymentPayUStatus();

                Response.Redirect(ProcessOrderUrl + this.OrderId);

            }


            if (!string.IsNullOrEmpty(UserSession.GetCookie("CodOrderId")))
            {
                PaymentType = "Cash On Delivery";

                ShoppingCart oCart = new ShoppingCart();
                bool iSGuestUser = oCart.CheckGuestUser();
                string sOrderId = Convert.ToString(UserSession.GetCookie("CodOrderId"));
                OrderNO = sOrderId;

                Session["CodOrderId"] = OrderNO;

                if (iSGuestUser.ToString() != "1")
                {
                    OrderNO = Convert.ToString(UserSession.GetCookie("CodOrderId"));

                    bool IsExistingUser = oCart.CheckUserCodStatus(OrderNO);

                    if (IsExistingUser)
                    {
                        CodThankyouText = sCodThankUMsg;

                        EmailSubject = "Your Cash On Delivery Order No - " + OrderNO + " has been confirmed";

                        UpdateOrderPaymentAndStatus(sOrderId, 1, 3);

                        SendMailToCustomer(sOrderId, Convert.ToInt32(CustomDataTypes.OrderType.OrderType_Cod), EmailSubject, true);

                        if (!IsTestPayment)
                        {
                            UniComInfo oUniCom = new UniComInfo();
                            bool IsSuccess = oUniCom.CreateUpdateOrder(OrderNO);

                            if (IsSellerNotification)
                            {
                                SendOrderNotificationToSeller(OrderNO);
                            }
                        }
                    }
                    else
                    {

                        UpdateOrderPaymentAndStatus(sOrderId, 3, 3);
                        CodThankyouText = sCodZipDialThankUMsg;
                        EmailSubject = "Your Cash on delivery Order No:- " + OrderNO;

                        SendMailToCustomer(sOrderId, Convert.ToInt32(CustomDataTypes.OrderType.OrderType_Cod), EmailSubject, false);

                        if (!IsExotel)
                        {
                            SendReqToExotel(OrderNO);
                        }
                    }

                    HttpContext.Current.Response.Cookies.Add(new HttpCookie("CodOrderId", ""));

                    Response.Redirect(ProcessOrderUrl + this.OrderNO);

                }

                return;
            }
        }
        private void Get_PayTM_PaymentPayUStatus()
        {
            int StatusId = 1;
            bool checkSum = false;
            try
            {
                checkSum = Validate_PayTM_CheckSum();
            }
            catch { }


            if (checkSum && STATUS.ToLower() == "TXN_SUCCESS".ToLower())
            {


                UpdateOrderPaymentAndStatus(POrderNo, StatusId, 2);

                if (!IsTestPayment)
                {
                    EmailSubject = "Olive Theory Order Confirmed " + POrderNo;

                    SendMailToCustomer(POrderNo, Convert.ToInt32(CustomDataTypes.OrderType.OrderType_Web), EmailSubject, false);

                    UniComInfo oUniCom = new UniComInfo();

                    bool IsSuccess = oUniCom.CreateUpdateOrder(POrderNo);

                    if (IsSellerNotification)
                    {
                        SendOrderNotificationToSeller(POrderNo);
                    }

                    UpdateStockEncompass(POrderNo);

                }
                Session["Oid"] = null;
            }
            if (STATUS.ToLower() == "TXN_FAILURE".ToLower())
            {
                UpdateOrder(POrderNo, 8, 4);

            }

        }



        private void GetPaymentPayUStatus()
        {
            int StatusId = 1;

            if (ValidatePayCheckSum() && PStatus == "success")
            {
                UpdateOrderPaymentAndStatus(POrderNo, StatusId, 2);

                if (!IsTestPayment)
                {
                    EmailSubject = "Olive Theory Order Confirmed " + OrderId;

                    SendMailToCustomer(OrderId, Convert.ToInt32(CustomDataTypes.OrderType.OrderType_Web), EmailSubject, false);

                    UniComInfo oUniCom = new UniComInfo();

                    bool IsSuccess = oUniCom.CreateUpdateOrder(POrderNo);

                    if (IsSellerNotification)
                    {
                        SendOrderNotificationToSeller(POrderNo);
                    }

                    UpdateStockEncompass(POrderNo);

                }

                Session["Oid"] = null;
            }
            else if (ValidatePayCheckSum() && PStatus == "failure")
            {
                UpdateOrder(POrderNo, 8, 4);
            }
            else if (ValidatePayCheckSum() && PStatus == "pending")
            {
                UpdateOrderPaymentAndStatus(POrderNo, 7, 5);
            }
        }

        #endregion

        #region Send Email To Customer

        protected void SendMailToCustomer(string sOrderId, int sOrderType, string sSubject, bool bCodCustType)
        {
            bool isEmailSend = oOders.CheckEmailSend_V1(sOrderId, Convert.ToInt32(CustomDataTypes.OrderEmailType.CodConfirm));

            if (!isEmailSend)
            {
                Context.Items["OrderId"] = sOrderId;

                Control invoice;

                if (sOrderType == Convert.ToInt32(CustomDataTypes.OrderType.OrderType_Cod))
                {
                    if (bCodCustType)
                        invoice = LoadControl("~/UserControls/CodOrderConfirm.ascx");
                    else
                        invoice = LoadControl("~/UserControls/CodOrderReceived.ascx");
                }
                else
                {
                    invoice = LoadControl("~/UserControls/prepaidordermail.ascx");
                }

                ShoppingCart oCart = new ShoppingCart();


                StringBuilder sb = new StringBuilder();
                StringWriter sw = new StringWriter(sb);
                HtmlTextWriter htmlTw = new HtmlTextWriter(sw);
                invoice.RenderControl(htmlTw);
                string controlsHTML = sb.ToString();
                string EmailFrom = "orders@olivetheory.com";
                string EmailTo = oCart.GetEmail(sOrderId);

                EmailAddress objEmail = new EmailAddress();
                objEmail.email = EmailFrom;
                objEmail.name = "OliveTheory.com";
                string subject = sSubject;
                string sBody = controlsHTML;


                SendWebMail.SendEmailInfo(objEmail, EmailTo, subject, sBody);
                string semailsendorReject = SendMailByMandril.emailsendorReject.ToString();
                if (semailsendorReject == "Sent" || semailsendorReject == "Queued")
                    isEmailSend = true;

                if (isEmailSend)
                    oOders.Update_SendEmailLog(sOrderId, isEmailSend, "1"); // 1 is for Order email





            }
        }

        #endregion

        #region Seller Email Notification

        private void SendOrderNotificationToSeller(string OrderNo)
        {
            try
            {
                SellerOrderEmail sellerOrderEmail = notification.Notification_SellerOrders(OrderNo);
                if (sellerOrderEmail != null)
                {
                    foreach (var data in sellerOrderEmail.sellerinfo)
                    {
                        int vendorid = data.vendorId;
                        if (!string.IsNullOrEmpty(data.CustomerEmail))
                        {
                            List<SellerNoticationData> orderItems = sellerOrderEmail.sellerOrderInfo.Where(x => x.VendorId == vendorid).ToList();
                            SendOrderNotification(orderItems, data.Email, OrderNo, data.SellerName);
                        }
                    }
                }
            }
            catch { }
        }

        private void SendOrderNotification(List<SellerNoticationData> sellerOrderInfo, string Email, string OrderNo, string sellerName)
        {
            bool isSend = false;
            string controlsHTML = string.Empty;
            EmailSellerOrder.Visible = true;
            EmailSellerOrder.SellerName = sellerName;
            EmailSellerOrder.LoadOrderData(sellerOrderInfo);
            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            HtmlTextWriter htmlTw = new HtmlTextWriter(sw);
            EmailSellerOrder.RenderControl(htmlTw);
            controlsHTML = sb.ToString();
            EmailSellerOrder.Visible = false;
            string subject = "New Order From OliveTheory.com " + OrderNo;
            string sFromEmail = "orders@olivetheory.com";
            EmailAddress objEmail = new EmailAddress();
            objEmail.email = sFromEmail;
            objEmail.name = "OliveTheory.com";
            SendWebMail.SendEmailCc(objEmail, Email, SellerBccEmail, subject, controlsHTML);
            string emailsendorReject = SendMailByMandril.emailsendorReject;

            if (emailsendorReject == "Sent" || emailsendorReject == "Queued")
                isSend = true;

            notification.Notification_EmailStatus(OrderNo, Email, controlsHTML, isSend);

        }

        #endregion

        #region CC Avenue Response

        private bool GetCCAvenuepaymentStatus()
        {
            string Order_Id, Amount, AuthDesc, Checksum, newChecksum, status;

            string Merchant_Id = "M_man21227_21227";
            string WorkingKey = "2q3s7xkt7keyer1tjk";
            //string Merchant_Id = "308";
            //string workingkey = "E45E1CA5F0BA96F81072ECE6DE3AA6EA";
            string redirectURL = "http://localhost:54558/PaymentStatus.aspx";

            libfuncs myUtility = new libfuncs();
            Order_Id = OrderId.ToString();
            Amount = Request.Form["Amount"];
            BillAmountForTracking = Amount;
            AuthDesc = Request.Form["AuthDesc"];
            status = Request.Form["Status"];
            Checksum = Request.Form["Checksum"];
            Checksum = myUtility.verifychecksum(Merchant_Id, Order_Id, Amount, AuthDesc, WorkingKey, Checksum);

            if ((Checksum == "true") && (AuthDesc == "Y"))
            {

                int StatusId = 1;

                UpdateOrderPaymentAndStatus(Order_Id, StatusId, 2);

                string Email = GetEmail(Order_Id);

                EmailAddress objemail = new EmailAddress();
                objemail.email = "orders@bedbathmore.com";
                objemail.name = "BedBathMore.com";

                EmailSubject = "BedBathMore Order Confirmed- " + OrderId;

                //SendMailToCustomer(OrderId, Convert.ToInt32(CustomDataTypes.OrderType.OrderType_Web), EmailSubject, false);

                //Push to Uniware

                //UniComInfo oUniCom = new UniComInfo();

                //bool IsSuccess = oUniCom.CreateUpdateOrder(OrderNO);

                return true;

            }
            else if ((Checksum == "true") && (AuthDesc == "N"))
            {
                UpdateOrder(Order_Id, 8, 4);

                return false;
            }
            else if ((Checksum == "true") && (AuthDesc == "B"))
            {
                UpdateOrderPaymentAndStatus(Order_Id, 7, 5);

                return false;
            }
            else
            {
                return false;
            }
        }

        #endregion

        public void RazorPayResponse()
        {
            string sOrderNo = string.Empty;

            string paymentmethod = string.Empty;

            string Razorkey = ConfigurationManager.AppSettings["RazorKey"];

            string RazorSecret = ConfigurationManager.AppSettings["RazorSecret"];

            if (!string.IsNullOrEmpty(razorpay_payment_id))
            {
                RazorpayClient client = new RazorpayClient(Razorkey, RazorSecret);

                Razorpay.Api.Payment pay = client.Payment.Fetch(razorpay_payment_id);

                Object objPaymentAuthorize = pay.Attributes;

                RazorPayPayment payAuthorized = JsonConvert.DeserializeObject<RazorPayPayment>(objPaymentAuthorize.ToString());

                Dictionary<string, object> options = new Dictionary<string, object>();

                options.Add("amount", payAuthorized.amount);

                Razorpay.Api.Payment paymentCaptured = null;

                RazorPayPayment objPaymentCaptured = null;

                try
                {
                    paymentCaptured = pay.Capture(options);

                    Object objCaptured = paymentCaptured.Attributes;

                    objPaymentCaptured = JsonConvert.DeserializeObject<RazorPayPayment>(objCaptured.ToString());
                }
                catch (Exception e)
                {

                }

                if (objPaymentCaptured != null)
                {
                    if (string.IsNullOrEmpty(objPaymentCaptured.error_code))
                    {
                        if (objPaymentCaptured.id != string.Empty)
                            RazorPaymentLog(objPaymentCaptured);

                        sOrderNo = objPaymentCaptured.notes.merchant_order_id;

                        paymentmethod = objPaymentCaptured.method;

                        PStatus = objPaymentCaptured.status;

                        PaymentErrorCode = Convert.ToString(objPaymentCaptured.error_code);
                    }
                    else
                    {
                        if (payAuthorized.id != string.Empty)
                            RazorPaymentLog(payAuthorized);

                        sOrderNo = payAuthorized.notes.merchant_order_id;

                        paymentmethod = payAuthorized.method;

                        PStatus = payAuthorized.status;

                        PaymentErrorCode = Convert.ToString(payAuthorized.error_code);
                    }
                }
                else
                {
                    if (payAuthorized.id != string.Empty)
                        RazorPaymentLog(payAuthorized);

                    sOrderNo = payAuthorized.notes.merchant_order_id;

                    paymentmethod = payAuthorized.method;

                    PStatus = payAuthorized.status;

                    PaymentErrorCode = Convert.ToString(payAuthorized.error_code);
                }
            }

            if (!string.IsNullOrEmpty(razorpay_payment_id))
            {
                this.POrderNo = sOrderNo;

                OrderNO = POrderNo;
            }

            if (!string.IsNullOrEmpty(razorpay_payment_id))
            {
                if (!string.IsNullOrEmpty(razorpay_payment_id))
                {
                    this.OrderId = OrderNO;
                }

                HttpContext.Current.Response.Cookies.Add(new HttpCookie("razorpay_payment_id", ""));

                if (GetPaymentRazorStatus())
                {
                    PaymentType = Convert.ToString(paymentmethod);
                }

                return;
            }
        }

        private bool GetPaymentRazorStatus()
        {
            int StatusId = 1;

            if (PStatus == "captured" && string.IsNullOrEmpty(PaymentErrorCode))
            {
                UpdateOrderPaymentAndStatus(POrderNo, StatusId, 2);

                Context.Items["OrderId"] = POrderNo;

                if (!IsTestPayment)
                {
                    EmailSubject = "Olive Theory Order Confirmed- " + OrderId;

                    SendMailToCustomer(OrderId, Convert.ToInt32(CustomDataTypes.OrderType.OrderType_Web), EmailSubject, false);

                    UniComInfo oUniCom = new UniComInfo();
                    bool IsSuccess = oUniCom.CreateUpdateOrder(OrderNO);

                    if (IsSellerNotification)
                    {
                        SendOrderNotificationToSeller(OrderNO);
                    }

                    UpdateStockEncompass(OrderNO);

                }

                Session["WebOrderId"] = null;

                Session["Oid"] = null;

                Response.Redirect(ProcessOrderUrl + OrderNO);

                return true;
            }
            else if (PStatus == "failed" || PStatus == "created" || PStatus == "authorized")
            {
                UpdateOrder(POrderNo, 8, 4);

                Response.Redirect(ProcessOrderUrl + OrderNO);

                return false;
            }
            else
            {
                return false;
            }
        }

        public void UpdateStockEncompass(string OrderNo)
        {
            CartCmd objCmd = new CartCmd();

            DataSet dsItemsData = objCmd.GetOrderItemsForEncompass(OrderNo);

            if (dsItemsData.Tables.Count > 0)
            {
                if (dsItemsData.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow drItemRow in dsItemsData.Tables[0].Rows)
                    {
                        string ItemCode = Convert.ToString(drItemRow["ItemCode"]);

                        int Quantity = Convert.ToInt32(drItemRow["Quantity"]);

                        objCmd.UpdateItemStockEncompass(ItemCode, Quantity);
                    }
                }
            }
        }
    }
}