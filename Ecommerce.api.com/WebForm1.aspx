﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="Ecommerce.api.com.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>cod-1st-email</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>

            <%--<table align="center" width="800px" cellspacing="0px" cellpadding="0px" style="border: solid 1px #ababab;">

                <tr>
                    <td colspan="2" align="left" style="padding: 30px 30px 30px 30px;">

                        <a href="#">
                            <img src="images/headerlogo.jpg"></a>
                    </td>
                </tr>

                <tr>
                    <td style="padding: 0px 0px 0px 50px;">
                        <table width="700px">
                            <tr>
                                <td colspan="2" align="left" style="padding: 0px 0px 0px 50px;">
                                    <p style="color: #3e0009; line-height: 25px;">
                                        Dear<span style="text-decoration:underline">&nbsp; <%=CustomerName %></span>,
                                        <br>
                                        <br />
                                        We're so happy that you've found something to add to your home.<br>
                                        We're going to call you soon to verify and confirm your order.<br>
                                        And as soon as we do, satisfaction isn't far away
                                    </p>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="2" align="left" style="padding: 40px 10px 10px 50px;">
                                    <p style="color: #3e0009;"><strong style="color: #3e0009; text-decoration: underline;">About Your Order :</strong></p>
                                    <p style="color: #3e0009;"><strong style="color: #3e0009;">Order number : <%=OrderNo %> </strong></p>
                                    <p style="color: #3e0009;"><strong style="color: #3e0009;">Date of order : <%=OrderDate %> </strong></p>
                                    <p id="pDiscount" runat="server" style="color: #3e0009;"><strong style="color: #3e0009;">Discount Coupon (<%=DiscountCoupon %>) : <%=DiscountCouponVal %> </strong></p>
                                    <p style="color: #3e0009;"><strong style="color: #3e0009;">Order Amount : <%=BillAmount %> </strong></p>

                                      <p style="color: #3e0009;"><strong style="color: #3e0009;">Shipping information : <br /><br /> <%=ShippingAddress %> ,<%=ShippingState %>, <%=ShippingCity %> </strong></p>
                                         <p style="color: #3e0009;"><strong style="color: #3e0009;">Product details : </strong>-</p>
                             
                                </td>
                            </tr>




                            
                        </table>
                    </td>
                </tr>


                <tr>
                    <td style="padding: 0px 0px 0px 50px;">
                        <table width="700px">



                            <tr>
                                <td colspan="2" style="padding: 10px 10px 10px 50px;">
                                 
                                    <table cellspacing="0px" cellpadding="0px" style="border: solid 1px #553e46; font-size: 12" width="100%">

                                        <asp:Repeater ID="grvCartItems" runat="server">
                                            <HeaderTemplate>
                                                <tr>
                                                    <th style="border-bottom: solid 1px #553e46; border-right: solid 1px #553e46; background-color: #e4e5e7; color: #3e0009;" height="40px">Product Code</th>
                                                    <th style="border-bottom: solid 1px #553e46; border-right: solid 1px #553e46; background-color: #e4e5e7; color: #3e0009;" height="40px">Item Name</th>
                                                    <th style="border-bottom: solid 1px #553e46; border-right: solid 1px #553e46; background-color: #e4e5e7; color: #3e0009;" height="40px">Brand</th>
                                                    <th style="border-bottom: solid 1px #553e46; border-right: solid 1px #553e46; background-color: #e4e5e7; color: #3e0009;" height="40px">Price</th>
                                                    <th style="border-bottom: solid 1px #553e46; border-right: solid 1px #553e46; background-color: #e4e5e7; color: #3e0009;" height="40px">Qty</th>
                                                    <th style="border-bottom: solid 1px #553e46; border-right: solid 1px #553e46; background-color: #e4e5e7; color: #3e0009;" height="40px">Price (Rs)</th>
                                                    <th style="border-bottom: solid 1px #553e46; border-right: solid 1px #553e46; background-color: #e4e5e7; color: #3e0009;" height="40px">Total Price (Rs)</th>
                                                </tr>

                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td style="border-right: solid 1px #553e46; color: #c9174b; font-weight: bold" height="40px" align="center"><%#Eval("ItemCode")%></td>
                                                    <td style="border-right: solid 1px #553e46; color: #c9174b; font-weight: bold" height="40px" align="center"><%# Eval("CategoryName")%></td>
                                                    <td style="border-right: solid 1px #553e46; color: #c9174b; font-weight: bold" height="40px" align="center"><%#Eval("CollectionName") %></td>
                                                    <td style="border-right: solid 1px #553e46; color: #c9174b; font-weight: bold" height="40px" align="center"><%# Math.Round(Convert.ToDecimal(Eval("RetailPrice"))).ToString("0.00") %></td>
                                                    <td style="border-right: solid 1px #553e46; color: #c9174b; font-weight: bold" height="40px" align="center"><%# Eval("Quantity") %></td>
                                                    <td style="border-right: solid 1px #553e46; color: #c9174b; font-weight: bold" height="40px" align="center"><%# Math.Round(Convert.ToDecimal(Eval("RetailPrice"))).ToString("0.00") %></td>
                                                    <td style="border-right: solid 1px #553e46; color: #c9174b; font-weight: bold" height="40px" align="center"><%# Math.Round(Convert.ToDecimal(Eval("Amount"))).ToString("0.00") %></td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </table>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="2" style="padding: 10px 10px 10px 50px;">
                                    <p style="color: #3e0009;">Please ship the order by logging into your panel.</p>
                                </td>
                            </tr>

                            <tr>

                                <td width="300" style="padding: 10px 50px 40px 50px;">
                                    <p style="color: #3e0009; line-height: 25px;">
                                        Need to get in touch?<br>
                                        <br />
                                        <strong style="color: #3e0009;">Email:</strong> reachus@olivetheory.com<br />
                                        <strong style="color: #3e0009;">Phone:</strong> +91-22-3044 0066<br />
                                        10am - 7pm, Monday to Saturday
                                    </p>
                                </td>

                                <td width="300" style="padding: 50px 0px 40px 10px;" align="right">
                                    <p style="color: #3e0009; line-height: 25px;">
                                        Happy shipping<br>
                                        The Olive Theorists
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>


                <tr>
                    <td colspan="2">
                        <img src="images/footer.jpg" width="100%" height="185"></td>
                </tr>

            </table>--%>


        </div>
    </form>
</body>
</html>
