﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Ecommerce.api.com
{
    public partial class Payment : System.Web.UI.Page
    {
        public bool IsRazorPay = true;

        public string _UserControlsHTML = string.Empty;

        string OrderID;

        bool IsTest = false;
        string _url = string.Empty;

        public string PaymentMethod
        {
            get
            {
                if (Session["PaymentType"] != null)
                    return Convert.ToString(Session["PaymentType"]);
                else
                    return string.Empty;
            }
        }

        public string razorpay_payment_id
        {
            get
            {
                if (Session["razorpay_payment_id"] != null)
                    return Convert.ToString(Session["razorpay_payment_id"]);
                else
                    return string.Empty;
            }
        }
        public string PrepaidServiceProvider
        {
            get
            {
                if (Request.QueryString["PType"] != null)
                    return Convert.ToString(Request.QueryString["PType"]);
                else
                    return string.Empty;
            }
        }

        public string UserControlsHTML
        {
            get
            {
                return _UserControlsHTML;
            }
            set
            {
                _UserControlsHTML = value;
            }
        }

        public bool IsTestPayment
        {
            get
            {
                return Convert.ToBoolean(ConfigurationManager.AppSettings["IsTest"]);
            }
        }

        public string GetPaymentUrl()
        {
            string Param = Convert.ToString(ConfigurationManager.AppSettings["PaymentGatewayUrl"]);

            if (!string.IsNullOrEmpty(PaymentMethod) && PaymentMethod == "EMI")
            {
                //PaymentUrl = "https://www.ccavenue.com/shopzone/cc_details.jsp";

                if (IsTestPayment)
                {
                    PaymentUrl = "https://test.payu.in/_payment";
                }
                else
                {
                    PaymentUrl = "https://secure.payu.in/_payment";
                }

            }

            else
            {
                if (PrepaidServiceProvider != "")
                {
                    if (PrepaidServiceProvider.ToLower() == "paytm")
                    {
                        if (IsTestPayment)
                        {
                            PaymentUrl = "https://pguat.paytm.com/oltp-web/processTransaction?orderid=" + OrderID;
                        }
                        else
                        {
                            PaymentUrl = "https://secure.paytm.in/oltp-web/processTransaction?orderid=" + OrderID;
                        }
                        Url = "Paytm";
                    }
                }

                if (Param == "Payu")
                {
                    if (IsTestPayment)
                    {
                        PaymentUrl = "https://test.payu.in/_payment";
                    }
                    else
                    {
                        PaymentUrl = "https://secure.payu.in/_payment";
                    }
                }
            }
            return Url;
        }

        public string Url
        {
            get { return Convert.ToString(ConfigurationManager.AppSettings["PaymentGatewayUrl"]); }
            set { _url = value; }
        }

        public string _PaymentUrl = string.Empty;


        public string PaymentUrl
        {
            get { return _PaymentUrl; }
            set { _PaymentUrl = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadControl();
            }
        }

        public void LoadControl()
        {
            Session["Oid"] = Convert.ToString(Request.QueryString["Oid"]);
            Session["OrderId"] = Convert.ToString(Request.QueryString["Oid"]);
            Session["PaymentType"] = Convert.ToString(Request.QueryString["PType"]);
            Session["razorpay_payment_id"] = Convert.ToString(Request.QueryString["razorpay_payment_id"]);
            OrderID = Convert.ToString(Session["Oid"]);
            string PaymentGatewayUrl = Convert.ToString(ConfigurationManager.AppSettings["PaymentGatewayUrl"]);

            if (PaymentMethod == "COD")
            {
                HttpContext.Current.Response.Cookies.Add(new HttpCookie("CodOrderId", Convert.ToString(Request.QueryString["Oid"])));
                HttpContext.Current.Response.Cookies.Add(new HttpCookie("Cid", Convert.ToString(Request.QueryString["Cid"])));
                HttpContext.Current.Response.Cookies.Add(new HttpCookie("guestUser", string.Empty));
                Response.Redirect("/PaymentStatus.aspx");
            }

            if (!string.IsNullOrEmpty(razorpay_payment_id) && PaymentGatewayUrl == "Razor")
            {
                HttpContext.Current.Response.Cookies.Add(new HttpCookie("razorpay_payment_id", razorpay_payment_id));
                Response.Redirect("/PaymentStatus.aspx");
            }

            GetPaymentUrl();

            Control Products = null;

            if (PrepaidServiceProvider.ToLower() == "paytm")
            {
                Products = (UserControl)LoadControl("/UserControls/uc-payment-paytm.ascx");
            }
            else if (!string.IsNullOrEmpty(PaymentMethod) && PaymentMethod == "EMI")
            {
                //Products = (UserControl)LoadControl("/UserControls/uc-cc-avenue.ascx");
                Products = (UserControl)LoadControl("/UserControls/uc-payment-payu.ascx");
            }
            else if (Url == "Payu")
            {
                Products = (UserControl)LoadControl("/UserControls/uc-payment-payu.ascx");
            }
            else if (Url == "CCAvenue")
            {
                Products = (UserControl)LoadControl("/UserControls/PaymentCCAvenue.ascx");
            }
            else
            {
                Response.Redirect("http://www.olivetheory.com");
            }

            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            HtmlTextWriter htmlTw = new HtmlTextWriter(sw);
            Products.RenderControl(htmlTw);
            UserControlsHTML = sb.ToString();

        }

        public override void VerifyRenderingInServerForm(Control control)
        { /* Do nothing */ }

        public override bool EnableEventValidation
        {
            get { return false; }
            set { /* Do nothing */}
        }
    }
}