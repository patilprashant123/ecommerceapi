﻿
using Mandrill;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ecommerce.Entities;
using Ecommerce.Cmd;
using System.Linq;
using System.Data;

namespace Ecommerce.api.com.codstatus.Exotel
{
    public partial class Default : System.Web.UI.Page
    {
        #region property

        string StatusId = string.Empty;
        string Confirm = "\"1\"";
        string Reject = "\"2\"";
        string sPaymentStatusId = "3";
        int OrderStatus = 0;
        ShoppingCart oShoppingCart = new ShoppingCart();
        NotoficationCmd notification = new NotoficationCmd();

        public string SellerBccEmail
        {
            get
            {
                return Convert.ToString(ConfigurationManager.AppSettings["SellerBccEmail"]);
            }
        }
        public string CallSid
        {
            get
            {
                if (!string.IsNullOrEmpty(Request["CallSid"]) && Request["CallSid"] != "")
                    return Convert.ToString(Request["CallSid"]);
                else
                    return string.Empty;
            }
        }
        public string Status
        {
            get
            {
                if (!string.IsNullOrEmpty(Request["Status"]) && Request["Status"] != "")
                    return Convert.ToString(Request["Status"]);
                else
                    return string.Empty;
            }
        }
        public string RecordingUrl
        {
            get
            {
                if (!string.IsNullOrEmpty(Request["RecordingUrl"]) && Request["RecordingUrl"] != "")
                    return Convert.ToString(Request["RecordingUrl"]);
                else
                    return string.Empty;
            }
        }
        public string DateUpdated
        {
            get
            {
                if (!string.IsNullOrEmpty(Request["DateUpdated"]) && Request["DateUpdated"] != "")
                    return Convert.ToString(Request["DateUpdated"]);
                else
                    return string.Empty;
            }
        }
        public string sCustomField
        {
            get
            {
                if (!string.IsNullOrEmpty(Request["CustomField"]) && Request["CustomField"] != "")
                    return Convert.ToString(Request["CustomField"]);
                else
                    return string.Empty;
            }
        }
        public string CallStatus
        {
            get
            {
                if (!string.IsNullOrEmpty(Request["CallStatus"]) && Request["CallStatus"] != "")
                    return Convert.ToString(Request["CallStatus"]);
                else
                    return string.Empty;
            }
        }
        public string SDirection
        {
            get
            {
                if (!string.IsNullOrEmpty(Request["Direction"]) && Request["Direction"] != "")
                    return Convert.ToString(Request["Direction"]);
                else
                    return string.Empty;
            }
        }


        public string ForwardedFrom
        {
            get
            {
                if (!string.IsNullOrEmpty(Request["ForwardedFrom"]) && Request["ForwardedFrom"] != "")
                    return Convert.ToString(Request["ForwardedFrom"]);
                else
                    return string.Empty;
            }
        }
        public string DialCallDuration
        {
            get
            {
                if (!string.IsNullOrEmpty(Request["DialCallDuration"]) && Request["DialCallDuration"] != "")
                    return Convert.ToString(Request["DialCallDuration"]);
                else
                    return string.Empty;
            }
        }
        public string StartTime
        {
            get
            {
                if (!string.IsNullOrEmpty(Request["StartTime"]) && Request["StartTime"] != "")
                    return Convert.ToString(Request["StartTime"]);
                else
                    return string.Empty;
            }
        }
        public string Digits
        {
            get
            {
                if (!string.IsNullOrEmpty(Request["digits"]) && Request["digits"] != "")
                    return Convert.ToString(Request["digits"]);
                else
                    return string.Empty;
            }
        }
        public string EndTime
        {
            get
            {
                if (!string.IsNullOrEmpty(Request["EndTime"]) && Request["EndTime"] != "")
                    return Convert.ToString(Request["EndTime"]);
                else
                    return string.Empty;
            }
        }
        public string CallType
        {
            get
            {
                if (!string.IsNullOrEmpty(Request["CallType"]) && Request["CallType"] != "")
                    return Convert.ToString(Request["CallType"]);
                else
                    return string.Empty;
            }
        }

        public string DialWhomNumber
        {
            get
            {
                if (!string.IsNullOrEmpty(Request["DialWhomNumber"]) && Request["DialWhomNumber"] != "")
                    return Convert.ToString(Request["DialWhomNumber"]);
                else
                    return string.Empty;
            }
        }
        public string flow_id
        {
            get
            {
                if (!string.IsNullOrEmpty(Request["flow_id"]) && Request["flow_id"] != "")
                    return Convert.ToString(Request["flow_id"]);
                else
                    return string.Empty;
            }
        }
        public string tenant_id
        {
            get
            {
                if (!string.IsNullOrEmpty(Request["tenant_id"]) && Request["tenant_id"] != "")
                    return Convert.ToString(Request["tenant_id"]);
                else
                    return string.Empty;
            }
        }
        public bool IsSellerNotification
        {
            get
            {
                return Convert.ToBoolean(ConfigurationManager.AppSettings["IsSellerNotification"]);
            }
        }

        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            ExotelResponse oResponse = new ExotelResponse();
            oResponse.ExotelResponse_Log(CallSid, Status, RecordingUrl, DateUpdated, CallStatus, sCustomField, Digits, SDirection
                                             , ForwardedFrom, DialCallDuration, StartTime, EndTime, CallType, DialWhomNumber, flow_id, tenant_id);

            if (Digits == Confirm)
                OrderStatus = 1;
            else if (Digits == Reject)
                OrderStatus = 2;

            // sCustomField   - OrderNo we are passing to Exotel ***/

            // no-answer      - Customer not respond
            // busy           - Customer reject call

            // OrderStatus=1  - Customer Confirm Order
            // OrderStatus=0  - Customer Reject Order




            string sEmailSubject = "Your Cash On Delivery Order No - " + sCustomField + " has been confirmed";
            string sCancelReason = "Order is cancelled by user- Exotel";

            if (OrderStatus == 0)
            {
                StatusId = "12";
                int sucess = 0;

                if (Status == "no-answer" || Status == "busy")
                    sucess = oShoppingCart.UpdateOrderStatus_Exotel(sCustomField, StatusId, sPaymentStatusId, CallSid, Status);
            }
            else if (OrderStatus == 1)
            {

                StatusId = "1";
                int sucess = oShoppingCart.ConfirmAndPushToUniware(sCustomField, StatusId, sPaymentStatusId);
                SendMailToCustomer(sCustomField, sEmailSubject);
                
                if (IsSellerNotification)
                {
                    SendOrderNotificationToSeller(sCustomField);
                }

                UpdateStockEncompass(sCustomField);

            }
            else if (OrderStatus == 2)
            {
                StatusId = "4";
                string sResult = oShoppingCart.CancelOrder(sCustomField, sCancelReason);
            }
        }
        public override void VerifyRenderingInServerForm(Control control)
        {
            // Confirms that an HtmlForm control is rendered for the
            // specified ASP.NET server control at run time.
            // No code required here.
        }
        private void SendCouponEmailToCustomer(string sReferrerEmail, string sCustomerEMailId, string sCouponCode, string sCoupnoExpiry)
        {
            string HostUrl = ConfigurationManager.AppSettings["HostServerUrl"].ToString();
            AlertMessage emailMsg = new AlertMessage(CustomDataTypes.EmailType.Referral_FirstOrderPlace);
            emailMsg.ParseCustomTag("{HostServerUrl}", HostUrl);
            emailMsg.ParseCustomTag("{FriendsEmail}", sCustomerEMailId);
            emailMsg.ParseCustomTag("{CouponCode}", sCouponCode);
            emailMsg.ParseCustomTag("{insertdate}", sCoupnoExpiry);
            string FromEmail = emailMsg.FromEmail;
            string body = emailMsg.MessageString;
            string subject = emailMsg.Subject;

            try
            {

                EmailAddress objEmail = new EmailAddress();
                objEmail.email = FromEmail;
                objEmail.name = "BedBathMore.com";
                SendWebMail.SendEmailTo(objEmail, sReferrerEmail, subject, body);
            }
            catch (Exception ex)
            {
                string sMsg = ex.Message.ToString();
            }
        }
        public void SendMailToCustomer(string sOrderId, string sSubject)
        {

            PackingAndDespatch oPacking = new PackingAndDespatch();
            bool isEmailSend = oPacking.CheckEmailSend_V1(sOrderId, Convert.ToInt32(CustomDataTypes.OrderEmailType.Cod));
            if (!isEmailSend)
            {
                Context.Items["OrderId"] = sOrderId;
                Control invoice;

                //invoice = LoadControl("~/UserControl/CodOrderConfirmationMail.ascx");

                invoice = LoadControl("~/UserControls/CodOrderConfirm.ascx");


                StringBuilder sb = new StringBuilder();
                StringWriter sw = new StringWriter(sb);
                HtmlTextWriter htmlTw = new HtmlTextWriter(sw);
                invoice.RenderControl(htmlTw);
                string controlsHTML = sb.ToString();
                string EmailFrom = "orders@olivetheory.com";
                string EmailTo = oShoppingCart.GetEmail(sOrderId);

                EmailAddress objEmail = new EmailAddress();
                objEmail.email = EmailFrom;
                objEmail.name = "OliveTheory.com";
                string subject = sSubject;
                string sBody = controlsHTML;
                SendWebMail.SendEmailInfo(objEmail, EmailTo, subject, sBody);
                string semailsendorReject = SendMailByMandril.emailsendorReject.ToString();
                if (semailsendorReject == "Sent" || semailsendorReject == "Queued")
                    isEmailSend = true;
                if (isEmailSend)
                    oPacking.Update_SendEmailLog(sOrderId, isEmailSend, "2"); // 1 is for Order email
            }
        }


        private void SendOrderNotificationToSeller(string OrderNo)
        {
            try
            {
                SellerOrderEmail sellerOrderEmail = notification.Notification_SellerOrders(OrderNo);
                if (sellerOrderEmail != null)
                {
                    foreach (var data in sellerOrderEmail.sellerinfo)
                    {
                        int vendorid = data.vendorId;
                        if (!string.IsNullOrEmpty(data.CustomerEmail))
                        {
                            List<SellerNoticationData> orderItems = sellerOrderEmail.sellerOrderInfo.Where(x => x.VendorId == vendorid).ToList();
                            SendOrderNotification(orderItems, data.Email, OrderNo, data.SellerName);
                        }
                    }
                }
            }
            catch { }
        }
        private void SendOrderNotification(List<SellerNoticationData> sellerOrderInfo, string Email, string OrderNo, string sellerName)
        {

            bool isSend = false;
            string controlsHTML = string.Empty;
            EmailSellerOrder.Visible = true;
            EmailSellerOrder.SellerName = sellerName;
            EmailSellerOrder.LoadOrderData(sellerOrderInfo);
            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            HtmlTextWriter htmlTw = new HtmlTextWriter(sw);
            EmailSellerOrder.RenderControl(htmlTw);
            controlsHTML = sb.ToString();
            EmailSellerOrder.Visible = false;
            string subject = "New Order From OliveTheory.com " + OrderNo;
            string sFromEmail = "orders@olivetheory.com";
            EmailAddress objEmail = new EmailAddress();
            objEmail.email = sFromEmail;
            objEmail.name = "OliveTheory.com";
            SendWebMail.SendEmailCc(objEmail, Email, SellerBccEmail, subject, controlsHTML);
            string emailsendorReject = SendMailByMandril.emailsendorReject;

            if (emailsendorReject == "Sent" || emailsendorReject == "Queued")
                isSend = true;

            notification.Notification_EmailStatus(OrderNo, Email, controlsHTML, isSend);


        }

        public void UpdateStockEncompass(string OrderNo)
        {
            CartCmd objCmd = new CartCmd();

            DataSet dsItemsData = objCmd.GetOrderItemsForEncompass(OrderNo);

            if (dsItemsData.Tables.Count > 0)
            {
                if (dsItemsData.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow drItemRow in dsItemsData.Tables[0].Rows)
                    {
                        string ItemCode = Convert.ToString(drItemRow["ItemCode"]);

                        int Quantity = Convert.ToInt32(drItemRow["Quantity"]);

                        objCmd.UpdateItemStockEncompass(ItemCode, Quantity);
                    }
                }
            }
        }

    }
}