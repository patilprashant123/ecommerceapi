﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Ecommerce.api.com.codstatus.Exotel.Default" %>

<%@ Register Src="~/UserControls/ucSellerOrderNotification.ascx" TagName="ucSellerOrderNotification" TagPrefix="uc1" %>



<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <uc1:ucSellerOrderNotification ID="EmailSellerOrder" runat="server" Visible="false" />
        </div>
    </form>
</body>
</html>
