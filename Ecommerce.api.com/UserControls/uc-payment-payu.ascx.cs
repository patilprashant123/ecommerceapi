﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Text;


namespace Ecommerce.api.com.UserControls
{
    public partial class uc_payment_payu : System.Web.UI.UserControl
    {
        SqlConnection con;
        SqlConnection con1;
        string sOrderNo;

        /// Test
        //string Merchant_Id = "gtKFFx";

        /// Live
        //string Merchant_Id = "oScr7k";

        //string Merchant_Id = "kf1Zhx";

        //string redirectURL = "https://www.bedbathmore.com/PaymentStatus.aspx";

        string redirectURL = "http://www.bedbathmore.com/PaymentStatus.aspx";

        string Amount, Checksum;
        string billing_cust_name = "";
        string sDiscountOffer = "";
        string billing_cust_address, billing_cust_country, billing_cust_tel, billing_cust_email, billing_f_name, billing_l_name;
        string billing_cust_city, billing_cust_state, billing_cust_Zip;

        string delivery_cust_name, delivery_cust_address, delivery_cust_tel, delivery_cust_notes, Merchant_Param, delivery_f_name, delivery_l_name;
        string delivery_cust_city, delivery_cust_state, delivery_cust_ZIP, delivery_cust_country, delivery_cust_email;

        string _ProductInfo = "BBM PRODUCTS";

        string sUdf1, sUdf2, sUdf3, sUdf4, sUdf5 = string.Empty;


        string sCustomerEmail = string.Empty;

        public string OrderNo
        {
            set { sOrderNo = value; }
            get { return sOrderNo; }
        }
        public string MerchantId
        {
            get
            {
                if (IsTestPayment)
                {
                    return "gtKFFx";
                }
                else
                {
                    return "kf1Zhx";
                }
            }
        }
        #region SQL CONNECTION
        private void OpenSqlConnection()
        {
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["bbmSqlDb"].ToString());
            con.Open();
        }

        private void CloseSqlConnection()
        {
            con.Close();
        }
        private void OpenSqlConnection1()
        {
            con1 = new SqlConnection(ConfigurationManager.ConnectionStrings["bbmSqlDb"].ToString());
            con1.Open();
        }

        private void CloseSqlConnection1()
        {
            con1.Close();
        }
        #endregion

        #region CCAvenue Fields
        public string DiscountOffer
        {
            set { sDiscountOffer = value; }
            get { return sDiscountOffer; }

        }
        public string OrderAmount
        {
            set { Amount = value; }
            get { return Amount; }

        }
        public string CheckSum
        {
            set { Checksum = value; }
            get { return Checksum; }

        }
        public string CustomerEmail
        {
            set { sCustomerEmail = value; }
            get { return sCustomerEmail; }

        }
        public string BillingCustName
        {
            set { billing_cust_name = value; }
            get { return billing_cust_name; }

        }

        public string BillingFName
        {
            set { billing_f_name = value; }
            get { return billing_f_name; }

        }

        public string BillingLName
        {
            set { billing_l_name = value; }
            get { return billing_l_name; }

        }

        public string BillingPostCode
        {
            set { billing_cust_Zip = value; }
            get { return billing_cust_Zip; }
        }

        public string BillingCustAddress
        {
            set { billing_cust_address = value; }
            get { return billing_cust_address; }

        }
        public string BillingCustCity
        {
            set { billing_cust_city = value; }
            get { return billing_cust_city; }

        }
        public string BillingCustState
        {
            set { billing_cust_state = value; }
            get { return billing_cust_state; }

        }
        public string DeliveryCustCity
        {
            set { delivery_cust_city = value; }
            get { return delivery_cust_city; }

        }
        public string DeliveryCustZip
        {
            set { delivery_cust_ZIP = value; }
            get { return delivery_cust_ZIP; }

        }
        public string DeliveryCustEmail
        {
            set { delivery_cust_email = value; }
            get { return delivery_cust_email; }

        }
        public string DeliveryCustState
        {
            set { delivery_cust_state = value; }
            get { return delivery_cust_state; }

        }

        public string BillingCustCountry
        {
            set { billing_cust_country = value; }
            get { return billing_cust_country; }

        }
        public string BillingCustTel
        {
            set { billing_cust_tel = value; }
            get { return billing_cust_tel; }

        }
        public string BillingCustEmail
        {
            set { billing_cust_email = value; }
            get { return billing_cust_email; }

        }
        public string DeliveryCustName
        {
            set { delivery_cust_name = value; }
            get { return delivery_cust_name; }

        }

        public string DeliveryFName
        {
            set { delivery_f_name = value; }
            get { return delivery_f_name; }

        }

        public string DeliveryLName
        {
            set { delivery_l_name = value; }
            get { return delivery_l_name; }

        }

        public string DeliveryCustAddress
        {
            set { delivery_cust_address = value; }
            get { return delivery_cust_address; }

        }
        public string DeliveryCustTel
        {
            set { delivery_cust_tel = value; }
            get { return delivery_cust_tel; }

        }
        public string DeliveryCustNotes
        {
            set { delivery_cust_notes = value; }
            get { return delivery_cust_notes; }

        }
        public string DeliveryCustCountry
        {
            set { delivery_cust_country = value; }
            get { return delivery_cust_country; }

        }


        public string MerchantParam
        {
            set { Merchant_Param = value; }
            get { return Merchant_Param; }

        }

        public string HostServerUrl
        {
            get
            {
                return Convert.ToString(ConfigurationManager.AppSettings["HostServerUrl"]);
            }
        }


        public string RedirectURL
        {
            get
            {
                return Convert.ToString(ConfigurationManager.AppSettings["RedirectUrl"]);
            }

        }

        public string CancelURL
        {
            get
            {
                return Convert.ToString(ConfigurationManager.AppSettings["RedirectUrl"]);
            }
        }

        public string ProductInfo
        {
            set { _ProductInfo = value; }
            get { return _ProductInfo; }

        }


        public string Udf1
        {
            set { sUdf1 = value; }
            get { return sUdf1; }

        }

        public string Udf2
        {
            set { sUdf2 = value; }
            get { return sUdf2; }

        }

        public string Udf3
        {
            set { sUdf3 = value; }
            get { return sUdf3; }

        }

        public string Udf4
        {
            set { sUdf4 = value; }
            get { return sUdf4; }

        }
        public string Udf5
        {
            set { sUdf5 = value; }
            get { return sUdf5; }

        }

        public bool IsTestPayment
        {
            get
            {
                return Convert.ToBoolean(ConfigurationManager.AppSettings["IsTest"]);
            }
        }

        public string Salt
        {
            get
            {
                if (IsTestPayment)
                {
                    return "eCwWELxi";
                }
                else
                {
                    return "0WH8JnMZ";
                }
            }
        }


        public string TimeOutUrl
        {
            get { return HostServerUrl + "/TimeOutUrl.aspx"; }
        }

        public string CodUrl
        {
            get { return HostServerUrl + "/CartInfo.aspx"; }

        }

        public bool IsCod
        {
            get { return false; }
        }



        public bool IsUdf
        {
            get { return false; }

        }

        #endregion



        public override void RenderControl(HtmlTextWriter writer)
        {

            LoadControlData();
            string sOrder = MerchantId + "|" + OrderNo + "|" + OrderAmount + "|" + ProductInfo + "|" + BillingFName + "|" + BillingCustEmail + "|||||||||||" + Salt;
            CalculateCheckSum(sOrder);
            base.Render(writer);
        }

        protected void Page_Load(object sender, EventArgs e)
        {


        }

        public void CalculateCheckSum(string str)
        {
            string sHash = hashalg.CalculateHash(str, Encoding.ASCII, "SHA512");

            CheckSum = sHash.ToLower();
        }


        public void LoadControlData()
        {
            // Expire Cache..
            Response.ExpiresAbsolute = DateTime.Now.AddDays(-1d);
            Response.Expires = -1500;
            Response.CacheControl = "no-cache";

            if (Session["Oid"] != null)
            {
                OrderNo = Session["Oid"].ToString();
                GetOrderDetail();
            }

        }

        private bool GetOrderDetail()
        {

            int Source = UserSession.GetCurrentSourceId();
            OpenSqlConnection();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandText = "OrderDetail";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@OrderNo", SqlDbType.VarChar, 50).Value = OrderNo;

            SqlDataReader dr = cmd.ExecuteReader();


            int iOrderNo = dr.GetOrdinal("OrderNo");
            int iCustomerId = dr.GetOrdinal("CustomerId");
            int iOrderDate = dr.GetOrdinal("OrderDate");
            int iStatusId = dr.GetOrdinal("StatusId");
            int iBillingAddress = dr.GetOrdinal("BillingAddress");
            int iShippingAddress = dr.GetOrdinal("ShippingAddress");
            int iOrderSourceId = dr.GetOrdinal("OrderSourceId");
            int iBillingCity = dr.GetOrdinal("BillingCity");
            int iBillingState = dr.GetOrdinal("BillingState");
            int iBillingCountry = dr.GetOrdinal("BillingCountry");
            int iBillingPostCode = dr.GetOrdinal("BillingPostCode");
            int iShippingCity = dr.GetOrdinal("ShippingCity");
            int iShippingState = dr.GetOrdinal("ShippingState");
            int iShippingCountry = dr.GetOrdinal("ShippingCountry");
            //int iShippingPostCode = dr.GetOrdinal("ShippingPostCode");
            int iSubTotal = dr.GetOrdinal("SubTotal");
            //int iTaxCharge = dr.GetOrdinal("TaxCharge");
            //int iShippingCharge = dr.GetOrdinal("ShippingCharge");
            int iBillAmount = dr.GetOrdinal("BillAmount");
            //int iPaymentStatus = dr.GetOrdinal("PaymentStatus");
            //int iInvoiceDate = dr.GetOrdinal("InvoiceDate");
            //int iInvoiceNo = dr.GetOrdinal("InvoiceNo");
            //int iAgentId = dr.GetOrdinal("AgentId");

            int iBillingFirstName = dr.GetOrdinal("BillingFirstName");
            int iBillingLastName = dr.GetOrdinal("BillingLastName");
            int iShippingFirstName = dr.GetOrdinal("ShippingFirstName");
            int iShippingLastName = dr.GetOrdinal("ShippingLastName");

            if (dr.Read())
            {
                /* lbAmount.Text = "Order Amount : Rs. " + dr.GetValue(iSubTotal).ToString();
                 OrderAmount = dr.GetValue(iSubTotal).ToString();*/
                //lbAmount.Text = "Order Amount : Rs. " + dr.GetValue(iBillAmount).ToString();
                OrderAmount = dr.GetValue(iBillAmount).ToString();

                BillingFName = dr.GetValue(iBillingFirstName).ToString();
                BillingLName = dr.GetValue(iBillingLastName).ToString();

                BillingCustName = dr.GetValue(iBillingFirstName).ToString() + " " + dr.GetValue(iBillingLastName).ToString();
                BillingCustAddress = dr.GetValue(iBillingAddress).ToString(); //+ "," + dr.GetValue(iBillingCity).ToString() + "," + dr.GetValue(iBillingState).ToString();
                BillingCustCity = dr.GetValue(iBillingCity).ToString();
                BillingCustState = dr.GetValue(iBillingState).ToString();
                BillingCustCountry = dr.GetValue(iBillingCountry).ToString();
                BillingPostCode = Convert.ToString(dr["BillingPostCode"]);
                BillingCustTel = Convert.ToString(dr["MobileNo"]);
                BillingCustEmail = Convert.ToString(dr["Email"]);


                DeliveryFName = dr.GetValue(iShippingFirstName).ToString();
                DeliveryLName = dr.GetValue(iShippingLastName).ToString();

                DeliveryCustName = dr.GetValue(iShippingFirstName).ToString() + " " + dr.GetValue(iShippingLastName).ToString();
                DeliveryCustAddress = dr.GetValue(iShippingAddress).ToString(); // +"," + dr.GetValue(iShippingCity).ToString() + "," + dr.GetValue(iShippingState).ToString();
                DeliveryCustCity = dr.GetValue(iShippingCity).ToString();
                DeliveryCustZip = Convert.ToString(dr["ShippingPostCode"]);
                DeliveryCustState = dr.GetValue(iShippingState).ToString();
                DeliveryCustCountry = dr.GetValue(iShippingCountry).ToString();
                DeliveryCustTel = Convert.ToString(dr["MobileNo"]);
                DeliveryCustEmail = Convert.ToString(dr["Email"]);


                DiscountOffer = Convert.ToString(dr["DiscountOffer"]);
            }

            CloseSqlConnection();
            return true;
        }
    }
}