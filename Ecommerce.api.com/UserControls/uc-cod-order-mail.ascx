﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="uc-cod-order-mail.ascx.cs" Inherits="Ecommerce.api.com.UserControls.uc_cod_order_mail" %>

<table align="center" width="800px" cellspacing="0px" cellpadding="0px" style="border: solid 1px #ababab;">

    <tr>
        <td colspan="2" align="left" style="padding: 30px 30px 30px 30px;">

            <a href="#">
                <img src="../images/headerlogo.jpg"><a />
        </td>
    </tr>


    <tr>
        <td style="padding: 0px 0px 0px 50px;">
            <table width="700px">
                <tr>
                    <td colspan="2" align="left" style="padding: 0px 0px 0px 50px;">
                        <p style="color: #3e0009; line-height: 25px;">
                            Dear _____,
                            <br>
                            <br />
                            We're so happy that you've found something to add to your home.<br>
                            We're going to call you soon to verify and confirm your order.<br>
                            And as soon as we do, satisfaction isn't far away
                        </p>
                    </td>
                </tr>

                <tr>
                    <td colspan="2" align="left" style="padding: 40px 10px 10px 50px;">
                        <p style="color: #3e0009;"><strong style="color: #3e0009; text-decoration: underline;">About Your Order :</strong></p>
                        <p style="color: #3e0009;"><strong style="color: #3e0009;">Order number : </strong>-</p>
                        <p style="color: #3e0009;"><strong style="color: #3e0009;">Date of order : </strong>-</p>
                        <p style="color: #3e0009;"><strong style="color: #3e0009;">Product details : </strong>-</p>
                        <p style="color: #3e0009;"><strong style="color: #3e0009;">Billing information : </strong>-</p>
                        <p style="color: #3e0009;"><strong style="color: #3e0009;">Shipping information : </strong>-</p>
                    </td>
                </tr>




                <tr>

                    <td width="300" style="padding: 40px 50px 40px 50px;">
                        <p style="color: #3e0009; line-height: 25px;">
                            Need to get in touch?<br>
                            <br />
                            <strong style="color: #3e0009;">Email:</strong> reachus@olivetheory.com
  <strong style="color: #3e0009;">Phone:</strong> +91-22-3044 0066
  10am - 7pm, Monday to Saturday
                        </p>
                    </td>

                    <td width="300" style="padding: 50px 0px 40px 10px;" align="right">
                        <p style="color: #3e0009; line-height: 25px;">
                            Come back soon!<br>
                            The Olive Theorists
                        </p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>


    <tr>
        <td colspan="2">
            <img src="../images/footer.jpg" width="100%" height="185"></td>
    </tr>

</table>
