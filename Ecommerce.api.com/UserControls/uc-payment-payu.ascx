﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="uc-payment-payu.ascx.cs" Inherits="Ecommerce.api.com.UserControls.uc_payment_payu" %>

<input type="hidden" name="key" id="key" value="<%=MerchantId %>" />
<input type="hidden" name="txnid" id="txnid" value="<%=OrderNo %>" />
<input type="hidden" name="amount" id="amount" value="<%=OrderAmount %>" />
<input type="hidden" name="productinfo" id="productinfo" value="BBM PRODUCTS" />
<input type="hidden" name="firstname" id="firstname" value="<%=BillingFName%>" />
<input type="hidden" name="lastname" id="lastname" value="<%=BillingLName %>" />
<input type="hidden" name="address1" id="address1" value="<%=BillingCustAddress%>" />
<input type="hidden" name="address2" id="address2" value="" />
<input type="hidden" name="city" id="city" value="<%=BillingCustCity%>" />
<input type="hidden" name="state" id="state" value="<%=BillingCustState%>" />
<input type="hidden" name="country" id="country" value="<%=BillingCustCountry%>" />
<input type="hidden" name="zipcode" id="zipcode" value="<%=BillingPostCode%>" />
<input type="hidden" name="email" id="email" value="<%=BillingCustEmail %>" />
<input type="hidden" name="phone" id="phone" value="<%=BillingCustTel %>" />



<%if (IsCod)
  { %>

    <input type="hidden" name="shipping_firstname" id="shipping_firstname" value="<%=BillingFName%>" />
    <input type="hidden" name="shipping_lastname" id="shipping_lastname" value="<%=BillingLName %>" />
    <input type="hidden" name="shipping_address1" id="shipping_address1" value="<%=BillingCustAddress%>" />
    <input type="hidden" name="shipping_address2" id="shipping_address2" value="" />
    <input type="hidden" name="shipping_city" id="shipping_city" value="<%=BillingCustCity%>" />
    <input type="hidden" name="shipping_state" id="shipping_state" value="<%=BillingCustState%>" />
    <input type="hidden" name="shipping_country" id="shipping_country" value="<%=BillingCustCountry%>" />
    <input type="hidden" name="shipping_zipcode" id="shipping_zipcode" value="<%=DeliveryCustZip %>" />
    <input type="hidden" name="shipping_phone" id="shipping_phone" value="<%=BillingCustTel%>" />
    <input type="hidden" name="shipping_phoneverified" id="shipping_phoneverified" value="yes" />
    <input type="hidden" name="codurl" id="codurl" value="<%=CodUrl%>" />

<% }%>

<%if (IsUdf)
  { %>

    <input type="hidden" name="udf1" id="udf1" value="" />
    <input type="hidden" name="udf2" id="udf2" value="" />
    <input type="hidden" name="udf3" id="udf3" value="" />
    <input type="hidden" name="udf4" id="udf4" value="" />
    <input type="hidden" name="udf5" id="udf5" value="" />

<% }%>

<input type="hidden" name="surl" id="surl" value="<%=RedirectURL%>" />
<input type="hidden" name="furl" id="furl" value="<%=CancelURL%>" />
<input type="hidden" name="curl" id="curl" value="<%=CancelURL%>" />
<input type="hidden" name="hash" id="hash" value="<%=CheckSum%>" />
<input type="hidden" name="pg" id="pg" value="CC" />
<input type="hidden" name="touturl" id="touturl" value="<%=TimeOutUrl%>" />
<input type="hidden" name="drop_category" id="drop_category" value="" />
<input type="hidden" name="custom_note" id="custom_note" value="" />
<input type="hidden" name="note_category" id="note_category" value="" />
<input type="hidden" name="api_version" id="api_version" value="1" />
<input type="hidden" name="offer_key" id="offer_key" value="<%=DiscountOffer %>" />
<%--<input value="submit" type="submit" />--%>
