﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using IntegrationKit;
using System.Configuration;

namespace Ecommerce.api.com.UserControls
{
    public partial class uc_cc_avenue : System.Web.UI.UserControl
    {
        libfuncs myUtility;
        SqlConnection con;
        SqlConnection con1;
        string sOrderNo;

        string Merchant_Id = "M_man21227_21227";
        //string Merchant_Id = "308";

        string workingkey = "2q3s7xkt7keyer1tjk";
        // Added on 13 June
        //string workingkey = "E45E1CA5F0BA96F81072ECE6DE3AA6EA";
        string redirectURL = "http://localhost:54558/PaymentStatus.aspx";





        string Amount, Checksum;
        string billing_cust_name = "";

        string billing_cust_address, billing_cust_country, billing_cust_tel, billing_cust_email;
        string billing_cust_city, billing_cust_state, billing_cust_Zip;

        string delivery_cust_name, delivery_cust_address, delivery_cust_tel, delivery_cust_notes, Merchant_Param;
        string delivery_cust_city, delivery_cust_state, delivery_cust_ZIP, delivery_cust_country, delivery_cust_email;

        string sCustomerEmail = string.Empty;

        public string OrderNo
        {
            set { sOrderNo = value; }
            get { return sOrderNo; }
        }
        public string MerchantId
        {
            set { Merchant_Id = value; }
            get { return Merchant_Id; }
        }
        #region SQL CONNECTION
        private void OpenSqlConnection()
        {
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["bbmSqlDb"].ToString());
            con.Open();
        }

        private void CloseSqlConnection()
        {
            con.Close();
        }
        private void OpenSqlConnection1()
        {
            con1 = new SqlConnection(ConfigurationManager.ConnectionStrings["bbmSqlDb"].ToString());
            con1.Open();
        }

        private void CloseSqlConnection1()
        {
            con1.Close();
        }
        #endregion

        #region CCAvenue Fields
        public string OrderAmount
        {
            set { Amount = value; }
            get { return Amount; }

        }
        public string CheckSum
        {
            set { Checksum = value; }
            get { return Checksum; }

        }
        public string CustomerEmail
        {
            set { sCustomerEmail = value; }
            get { return sCustomerEmail; }

        }
        public string BillingCustName
        {
            set { billing_cust_name = value; }
            get { return billing_cust_name; }

        }
        public string BillingPostCode
        {
            set { billing_cust_Zip = value; }
            get { return billing_cust_Zip; }
        }

        public string BillingCustAddress
        {
            set { billing_cust_address = value; }
            get { return billing_cust_address; }

        }
        public string BillingCustCity
        {
            set { billing_cust_city = value; }
            get { return billing_cust_city; }

        }
        public string BillingCustState
        {
            set { billing_cust_state = value; }
            get { return billing_cust_state; }

        }
        public string DeliveryCustCity
        {
            set { delivery_cust_city = value; }
            get { return delivery_cust_city; }

        }
        public string DeliveryCustZip
        {
            set { delivery_cust_ZIP = value; }
            get { return delivery_cust_ZIP; }

        }
        public string DeliveryCustEmail
        {
            set { delivery_cust_email = value; }
            get { return delivery_cust_email; }

        }
        public string DeliveryCustState
        {
            set { delivery_cust_state = value; }
            get { return delivery_cust_state; }

        }

        public string BillingCustCountry
        {
            set { billing_cust_country = value; }
            get { return billing_cust_country; }

        }
        public string BillingCustTel
        {
            set { billing_cust_tel = value; }
            get { return billing_cust_tel; }

        }
        public string BillingCustEmail
        {
            set { billing_cust_email = value; }
            get { return billing_cust_email; }

        }
        public string DeliveryCustName
        {
            set { delivery_cust_name = value; }
            get { return delivery_cust_name; }

        }
        public string DeliveryCustAddress
        {
            set { delivery_cust_address = value; }
            get { return delivery_cust_address; }

        }
        public string DeliveryCustTel
        {
            set { delivery_cust_tel = value; }
            get { return delivery_cust_tel; }

        }
        public string DeliveryCustNotes
        {
            set { delivery_cust_notes = value; }
            get { return delivery_cust_notes; }

        }
        public string DeliveryCustCountry
        {
            set { delivery_cust_country = value; }
            get { return delivery_cust_country; }

        }


        public string MerchantParam
        {
            set { Merchant_Param = value; }
            get { return Merchant_Param; }

        }
        public string RedirectURL
        {
            get { return redirectURL; }

        }
        #endregion

        #region CCAvenue Fields
        /*private string OrderAmount
        {
            set { Amount.Value = value; }
            get { return Amount.Value; }

        }
        private string CheckSum
        {
            set { Checksum.Value = value; }
            get { return Checksum.Value; }

        }
        private string BillingCustName
        {
            set { billing_cust_name.Value = value; }
            get { return billing_cust_name.Value; }

        }
        private string BillingCustAddress
        {
            set { billing_cust_address.Value = value; }
            get { return billing_cust_address.Value; }

        }
        private string BillingCustCountry
        {
            set { billing_cust_country.Value = value; }
            get { return billing_cust_country.Value; }

        }
        private string BillingCustTel
        {
            set { billing_cust_tel.Value = value; }
            get { return billing_cust_tel.Value; }

        }
        private string BillingCustEmail
        {
            set { billing_cust_email.Value = value; }
            get { return billing_cust_email.Value; }

        }
        private string DeliveryCustName
        {
            set { delivery_cust_name.Value = value; }
            get { return delivery_cust_name.Value; }

        }
        private string DeliveryCustAddress
        {
            set { delivery_cust_address.Value = value; }
            get { return delivery_cust_address.Value; }

        }
        private string DeliveryCustTel
        {
            set { delivery_cust_tel.Value = value; }
            get { return delivery_cust_tel.Value; }

        }
        private string DeliveryCustNotes
        {
            set { delivery_cust_notes.Value = value; }
            get { return delivery_cust_notes.Value; }

        } 
        private string MerchantParam
        {
            set { Merchant_Param.Value = value; }
            get { return Merchant_Param.Value; }

        }*/
        #endregion

        public override void RenderControl(HtmlTextWriter writer)
        {

            LoadControlData();

            base.Render(writer);

        }

        protected void Page_Load(object sender, EventArgs e)
        {


        }


        public void LoadControlData()
        {
            // Expire Cache..
            Response.ExpiresAbsolute = DateTime.Now.AddDays(-1d);
            Response.Expires = -1500;
            Response.CacheControl = "no-cache";

            libfuncs myUtility = new libfuncs();

            if (Session["Oid"] != null)
            {
                OrderNo = Convert.ToString(Session["Oid"]);
                GetOrderDetail();
                Checksum = myUtility.getchecksum(MerchantId, OrderNo, OrderAmount, RedirectURL, workingkey);
            }

        }

        void Display(Object sender, EventArgs e)
        {
            // string Merchant_Id , Amount,Order_Id,Redirect_Url,WorkingKey, intChecksum;

            //string strMerchantId, strOrderNo, strAmount, strRedirect_Url, strWorkingKey, strChecksum;


            /*Merchant_Id.Value = Request.Form["Merchant_Id"];				    //This id(also User Id)  available at "Generate Working Key" of "Settings & Options" 
            Amount.Value = Request.Form["Order_Id"];						  //your script should substitute the amount here in the quotes provided here
            Order_Id.Value = Request.Form["Amount"];							//your script should substitute the order description here in the quotes provided here
            Redirect_Url.Value = Request.Form["Redirect_Url"];			 //your redirect URL where your customer will be redirected after authorisation from CCAvenue
            */
            //WorkingKey.Value  =  "";				  //put in the 32 bit alphanumeric key in the quotes provided here.Please note that get this key ,login to your CCAvenue merchant account and visit the "Generate Working Key" section at the "Settings & Options" page. 

            //Before Calling this method all parameters should have a value especially working key.


            //    strChecksum = myUtility.getchecksum(strMerchantId, strOrderNo, strAmount, strRedirect_Url, strWorkingKey);

            //Assign Following fields to send it ahead.
            /*
            Checksum.Value = strChecksum;
            billing_cust_name.Value = "";
            billing_cust_address.Value = "";
            billing_cust_country.Value = "";
            billing_cust_tel.Value = "";
            billing_cust_email.Value = "";
            delivery_cust_name.Value = "";
            delivery_cust_address.Value = "";
            delivery_cust_tel.Value = "";
            delivery_cust_notes.Value = "";
            Merchant_Param.Value = "";
            */

        }

        private bool GetOrderDetail()
        {

            int Source = UserSession.GetCurrentSourceId();
            OpenSqlConnection();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandText = "OrderDetail";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@OrderNo", SqlDbType.VarChar, 50).Value = OrderNo;

            SqlDataReader dr = cmd.ExecuteReader();


            int iOrderNo = dr.GetOrdinal("OrderNo");
            int iCustomerId = dr.GetOrdinal("CustomerId");
            int iOrderDate = dr.GetOrdinal("OrderDate");
            int iStatusId = dr.GetOrdinal("StatusId");
            int iBillingAddress = dr.GetOrdinal("BillingAddress");
            int iShippingAddress = dr.GetOrdinal("ShippingAddress");
            int iOrderSourceId = dr.GetOrdinal("OrderSourceId");
            int iBillingCity = dr.GetOrdinal("BillingCity");
            int iBillingState = dr.GetOrdinal("BillingState");
            int iBillingCountry = dr.GetOrdinal("BillingCountry");
            int iBillingPostCode = dr.GetOrdinal("BillingPostCode");
            int iShippingCity = dr.GetOrdinal("ShippingCity");
            int iShippingState = dr.GetOrdinal("ShippingState");
            int iShippingCountry = dr.GetOrdinal("ShippingCountry");
            //int iShippingPostCode = dr.GetOrdinal("ShippingPostCode");
            int iSubTotal = dr.GetOrdinal("SubTotal");
            //int iTaxCharge = dr.GetOrdinal("TaxCharge");
            //int iShippingCharge = dr.GetOrdinal("ShippingCharge");
            int iBillAmount = dr.GetOrdinal("BillAmount");
            //int iPaymentStatus = dr.GetOrdinal("PaymentStatus");
            //int iInvoiceDate = dr.GetOrdinal("InvoiceDate");
            //int iInvoiceNo = dr.GetOrdinal("InvoiceNo");
            //int iAgentId = dr.GetOrdinal("AgentId");

            int iBillingFirstName = dr.GetOrdinal("BillingFirstName");
            int iBillingLastName = dr.GetOrdinal("BillingLastName");
            int iShippingFirstName = dr.GetOrdinal("ShippingFirstName");
            int iShippingLastName = dr.GetOrdinal("ShippingLastName");

            if (dr.Read())
            {
                /* lbAmount.Text = "Order Amount : Rs. " + dr.GetValue(iSubTotal).ToString();
                 OrderAmount = dr.GetValue(iSubTotal).ToString();*/
                //lbAmount.Text = "Order Amount : Rs. " + dr.GetValue(iBillAmount).ToString();
                OrderAmount = dr.GetValue(iBillAmount).ToString();



                BillingCustName = dr.GetValue(iBillingFirstName).ToString() + " " + dr.GetValue(iBillingLastName).ToString();
                BillingCustAddress = dr.GetValue(iBillingAddress).ToString(); //+ "," + dr.GetValue(iBillingCity).ToString() + "," + dr.GetValue(iBillingState).ToString();
                BillingCustCity = dr.GetValue(iBillingCity).ToString();
                BillingCustState = dr.GetValue(iBillingState).ToString();
                BillingCustCountry = dr.GetValue(iBillingCountry).ToString();
                BillingPostCode = Convert.ToString(dr["BillingPostCode"]);
                BillingCustTel = Convert.ToString(dr["MobileNo"]);
                BillingCustEmail = Convert.ToString(dr["Email"]);



                DeliveryCustName = dr.GetValue(iShippingFirstName).ToString() + " " + dr.GetValue(iShippingLastName).ToString();
                DeliveryCustAddress = dr.GetValue(iShippingAddress).ToString(); // +"," + dr.GetValue(iShippingCity).ToString() + "," + dr.GetValue(iShippingState).ToString();
                DeliveryCustCity = dr.GetValue(iShippingCity).ToString();
                DeliveryCustZip = Convert.ToString(dr["ShippingPostCode"]);
                DeliveryCustState = dr.GetValue(iShippingState).ToString();
                DeliveryCustCountry = dr.GetValue(iShippingCountry).ToString();
                DeliveryCustTel = Convert.ToString(dr["MobileNo"]);
                DeliveryCustEmail = Convert.ToString(dr["Email"]);
            }

            CloseSqlConnection();
            return true;
        }
    }
}