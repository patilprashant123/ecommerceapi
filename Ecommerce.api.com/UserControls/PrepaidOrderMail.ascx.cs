﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;


namespace Ecommerce.api.com.UserControls
{
    public partial class PrepaidOrderMail : System.Web.UI.UserControl
    {
        decimal ItemSubTotal = 0;

        DataSet ds = null;

        public string CustomerName
        {
            get; set;
        }

        public string OrderNo
        {
            get; set;
        }

        public string OrderDate
        {
            get; set;
        }

        public string ShippingAddress
        {
            get; set;
        }

        public decimal BillAmount
        {
            get; set;
        }

        public string DiscountCoupon
        {
            get; set;
        }

        public decimal DiscountCouponVal
        {
            get; set;
        }

        public string ShippingState
        {
            get; set;
        }

        public string ShippingCity
        {
            get; set;
        }

        public override void RenderControl(HtmlTextWriter writer)
        {

            LoadControlData();

            base.Render(writer);

        }


        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void LoadControlData()
        {
            if (Context.Items["OrderId"] != null && Context.Items["OrderId"].ToString().Length > 0)
                OrderNo = Context.Items["OrderId"].ToString();

            ShoppingCart oShoppingCart = new ShoppingCart();

            ds = oShoppingCart.GetOrderItemsByOrderNo(OrderNo);

            grvCartItems.DataSource = ds.Tables[0];
            grvCartItems.DataBind();

            CustomerName = Convert.ToString(ds.Tables[0].Rows[0]["Name"]);
            OrderNo = Convert.ToString(ds.Tables[0].Rows[0]["OrderNo"]);
            ShippingAddress = Convert.ToString(ds.Tables[0].Rows[0]["ShippingAddress"]);
            OrderDate = Convert.ToString(ds.Tables[0].Rows[0]["OrderDate"]);
            BillAmount = Convert.ToDecimal(ds.Tables[0].Rows[0]["BillAmount"]);

            ShippingState = Convert.ToString(ds.Tables[0].Rows[0]["ShippingState"]);
            ShippingCity = Convert.ToString(ds.Tables[0].Rows[0]["ShippingCity"]);


            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[1].Rows.Count > 0)
                {
                    DiscountCoupon = Convert.ToString(ds.Tables[1].Rows[0]["DiscountCouponName"]);
                    DiscountCouponVal = Convert.ToDecimal(ds.Tables[1].Rows[0]["DiscountCouponValue"]);

                    if (DiscountCouponVal > 0)
                    {
                        pDiscount.Visible = true;
                    }
                    else
                    {
                        pDiscount.Visible = false;
                    }
                }
            }
        }

        protected void grvCartItems_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                ItemSubTotal += Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "Amount"));

                string PersonlizeText = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "PersonlizeText"));
                string PersonlizebackInitial = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "PersonlizebackInitial"));

                string DefaultCaption = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "DefaultCaption"));
                string PersonalizedCaption = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "PersonalizedCaption"));


                HtmlControl dvFront = (HtmlControl)e.Item.FindControl("dvFront");

                HtmlControl dvBack = (HtmlControl)e.Item.FindControl("dvBack");

                Label LblFront = (Label)e.Item.FindControl("LblFront");

                Label LblBack = (Label)e.Item.FindControl("LblBack");

                if (!string.IsNullOrEmpty(PersonlizeText))
                {
                    dvFront.Visible = true;
                }
                else
                {
                    dvFront.Visible = false;
                }

                if (!string.IsNullOrEmpty(PersonlizebackInitial))
                {
                    dvBack.Visible = true;
                }
                else
                {
                    dvBack.Visible = false;
                }


                if(!string.IsNullOrEmpty(DefaultCaption) && !string.IsNullOrEmpty(PersonalizedCaption))
                {
                    LblFront.Text = DefaultCaption;
                    LblBack.Text = PersonalizedCaption;
                }
                else
                {
                    LblFront.Text = "Front Initials";
                    LblBack.Text = "Back Initials";
                }

                bool IsBundleItem = Convert.ToBoolean(DataBinder.Eval(e.Item.DataItem, "IsBundleItem"));

                string OrderCartId = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "CartId"));

                if (IsBundleItem)
                {
                    Repeater rptBundleChoiceItem = (Repeater)e.Item.FindControl("rptBundleChoiceItem");
                    string filterBundleChoiceItem = " ParentCartItemId='" + OrderCartId + "'";
                    if (ds.Tables.Count > 2)
                    {
                        DataView dvFriends = new DataView(ds.Tables[2], filterBundleChoiceItem, "ParentCartItemId", DataViewRowState.Unchanged);
                        if (dvFriends != null)
                        {
                            rptBundleChoiceItem.DataSource = dvFriends;
                            rptBundleChoiceItem.DataBind();

                        }
                    }
                }
            }
            if (e.Item.ItemType == ListItemType.Footer)
            {
                Label lblOrderSubtotal = (Label)e.Item.FindControl("lblOrderSubtotal");

                lblOrderSubtotal.Text = ItemSubTotal.ToString("0.00");
            }
        }

        protected void rptBundleChoiceItem_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HtmlControl divHexVal = (HtmlControl)e.Item.FindControl("divHexVal");


                HtmlImage ImgBundleThumb = (HtmlImage)e.Item.FindControl("ImgBundleThumb");
                string sItemCode = DataBinder.Eval(e.Item.DataItem, "ItemCode").ToString();
                string sStandardCode = DataBinder.Eval(e.Item.DataItem, "StandardCode").ToString();

                string sItemImageName = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ItemThumbImage"));

                string spath = "" + DataBinder.Eval(e.Item.DataItem, "StandardCode") + "/" + sItemCode + "/" + sItemImageName;

                //ImgBundleThumb.Src = spath;

                string sItemhexCode = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ItemhexCode"));

                if (sItemhexCode != "")
                {
                    //  ImgBundleThumb.Visible = false;
                    divHexVal.Visible = true;
                    divHexVal.Attributes.Add("style", "width: 50px; height: 50px; background-color: #" + sItemhexCode);
                }
                else
                {
                    //ImgBundleThumb.Visible = true;
                    divHexVal.Attributes.Add("style", "visibility:hidden;");
                    divHexVal.Visible = false;
                }
            }
        }

    }
}