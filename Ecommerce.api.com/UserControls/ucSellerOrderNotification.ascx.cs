﻿using Ecommerce.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Ecommerce.api.com.UserControls
{
    public partial class ucSellerOrderNotification : System.Web.UI.UserControl
    {
        public string SellerName { set; get; }
     
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }
        public void LoadOrderData(List<SellerNoticationData> sellerOrderInfo)
        {
            rptOrderPaymentDetail.DataSource = sellerOrderInfo;
            rptOrderPaymentDetail.DataBind();
        }
    }
}