﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucSellerOrderNotification.ascx.cs" Inherits="Ecommerce.api.com.UserControls.ucSellerOrderNotification" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Seller Order Notification</title>

</head>

<body>
    <table align="center" width="800px" cellspacing="0px" cellpadding="0px" style="border: solid 1px #ababab;">

        <tr>
            <td colspan="2" align="left" style="padding: 30px 30px 30px 30px;">

                <a href="http://www.olivetheory.com">
                    <img src="https://ot-product-images.s3.amazonaws.com/ordermailer/headerlogo.jpg"></a>
            </td>
        </tr>


        <tr>
            <td style="padding: 0px 0px 0px 50px;">
                <table width="700px">
                    <tr>
                        <td colspan="2" align="left" style="padding: 0px 0px 0px 50px;">
                            <p style="color: #3e0009; line-height: 25px;">
                                Hi !
                                <br>
                                <br />
                                Now's the time to log in and check your panel, because you have a new order.
                                <br>
                                Customer can't wait to get their goodies!
                                <br>
                            </p>
                            <td>
                    </tr>



                    <tr>
                        <td colspan="2" style="padding: 10px 10px 10px 50px;">
                            <p><strong style="color: #3e0009;">You have received the following orders: </strong></p>
                            <table cellspacing="0px" cellpadding="0px" style="border: solid 1px #553e46; font-size: 12" width="100%">
                                <asp:Repeater ID="rptOrderPaymentDetail" runat="server">
                                    <HeaderTemplate>
                                        <tr>
                                            <th style="border-bottom: solid 1px #553e46; border-right: solid 1px #553e46; background-color: #e4e5e7; color: #3e0009;" height="40px">Order No</th>
                                            <th style="border-bottom: solid 1px #553e46; border-right: solid 1px #553e46; background-color: #e4e5e7; color: #3e0009;" height="40px">Order Date</th>
                                            <th style="border-bottom: solid 1px #553e46; border-right: solid 1px #553e46; background-color: #e4e5e7; color: #3e0009;" height="40px">Sku</th>
                                            <th style="border-bottom: solid 1px #553e46; border-right: solid 1px #553e46; background-color: #e4e5e7; color: #3e0009;" height="40px">Price</th>
                                            <th style="border-bottom: solid 1px #553e46; border-right: solid 1px #553e46; background-color: #e4e5e7; color: #3e0009;" height="40px">Qty</th>
                                            <th style="border-bottom: solid 1px #553e46; border-right: solid 1px #553e46; background-color: #e4e5e7; color: #3e0009;" height="40px">Desciption</th>
                                            <th style="border-bottom: solid 1px #553e46; border-right: solid 1px #553e46; background-color: #e4e5e7; color: #3e0009;" height="40px">Total Price</th>
                                        </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td style="border-right: solid 1px #553e46; color: #c9174b; font-weight: bold" height="40px" align="center"><%# Eval("OrderNo") %></td>
                                            <td style="border-right: solid 1px #553e46; color: #c9174b; font-weight: bold" height="40px" align="center"><%# DataBinder.Eval(Container.DataItem,"OrderDate","{0:MM/dd/yyyy}") %></td>
                                            <td style="border-right: solid 1px #553e46; color: #c9174b; font-weight: bold" height="40px" align="center"><%# Eval("ItemCode") %></td>
                                            <td style="border-right: solid 1px #553e46; color: #c9174b; font-weight: bold" height="40px" align="center"><%# Eval("RetailPrice") %></td>
                                            <td style="border-right: solid 1px #553e46; color: #c9174b; font-weight: bold" height="40px" align="center"><%# Eval("Quantity") %></td>
                                            <td style="border-right: solid 1px #553e46; color: #c9174b; font-weight: bold" height="40px" align="center"><%# Eval("categoryName") %></td>
                                            <td style="border-right: solid 1px #553e46; color: #c9174b; font-weight: bold" height="40px" align="center"><%# Eval("TotalRetailPrice") %></td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="2" style="padding: 10px 10px 10px 50px;">
                            <p style="color: #3e0009;">Please ship the order by logging into your panel.</p>
                        </td>
                    </tr>

                    <tr>

                        <td width="300" style="padding: 10px 50px 40px 50px;">
                            <p style="color: #3e0009; line-height: 25px;">
                                Need to get in touch?<br>
                                <br />
                                <strong style="color: #3e0009;">Email:</strong> reachus@olivetheory.com<br />
                                <strong style="color: #3e0009;">Phone:</strong> +91-22-3044 0066<br />
                                10am - 7pm, Monday to Saturday
                            </p>
                        </td>

                        <td width="300" style="padding: 50px 0px 40px 10px;" align="right">
                            <p style="color: #3e0009; line-height: 25px;">
                                Happy shopping<br>
                                Team Olive Theory
                            </p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>


        <tr>
            <td colspan="2">
                <img src="https://ot-product-images.s3.amazonaws.com/ordermailer/footer.jpg" width="100%" height="185"></td>
        </tr>

    </table>
</body>
</html>
