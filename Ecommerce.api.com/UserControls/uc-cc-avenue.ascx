﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="uc-cc-avenue.ascx.cs" Inherits="Ecommerce.api.com.UserControls.uc_cc_avenue" %>

<input type="hidden" name="Merchant_Id" id="Merchant_Id" value="<%=MerchantId%>" />
<input type="hidden" name="Amount" id="Amount" value="<%=OrderAmount%>" />
<input type="hidden" name="Order_Id" id="Order_Id" value="<%=OrderNo%>" />
<input type="hidden" name="Redirect_Url" id="Redirect_Url" value="<%=RedirectURL%>" />
<input type="hidden" name="Checksum" id="Checksum" value="<%=CheckSum%>" />
<input type="hidden" name="billing_cust_name" id="billing_cust_name" value="<%=BillingCustName%>" />
<input type="hidden" name="billing_cust_address" id="billing_cust_address" value="<%=BillingCustAddress%>" />
<input type="hidden" name="billing_cust_city" id="billing_cust_city" value="<%=BillingCustCity%>" />
<input type="hidden" name="billing_cust_state" id="billing_cust_state" value="<%=BillingCustState%>" />
<input type="hidden" name="billing_cust_country" id="billing_cust_country" value="<%=BillingCustCountry%>" />
<input type="hidden" name="billing_cust_tel" id="billing_cust_tel" value="<%=BillingCustTel%>" />
<input type="hidden" name="billing_zip_code" value="<%=BillingPostCode%>" />
<input type="hidden" name="billing_cust_email" id="billing_cust_email" value="<%=BillingCustEmail%>" />
<input type="hidden" name="delivery_cust_name" id="delivery_cust_name" value="<%=DeliveryCustName%>" />
<input type="hidden" name="delivery_cust_address" id="delivery_cust_address" value="<%=DeliveryCustAddress%>" />
<input type="hidden" name="delivery_cust_city" id="delivery_cust_city" value="<%=DeliveryCustCity%>" />
<input type="hidden" name="delivery_cust_state" id="delivery_cust_state" value="<%=DeliveryCustState%>" />
<input type="hidden" name="billing_cust_country" id="delivery_cust_country" value="<%=DeliveryCustCountry%>" />
<input type="hidden" name="delivery_cust_tel" id="delivery_cust_tel" value="<%=DeliveryCustTel%>" />
<input type="hidden" name="delivery_cust_notes" id="delivery_cust_notes" value="<%=DeliveryCustNotes%>" />
<input type="hidden" name="delivery_zip_code" value="<%=DeliveryCustZip%>" />
<input type="hidden" name="Merchant_Param" id="Merchant_Param" value="amka" />
<input type="hidden" value="Proceed To Payment" />
