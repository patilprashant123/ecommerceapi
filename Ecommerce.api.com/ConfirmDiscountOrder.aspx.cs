﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Uniware;
using System.Text;
using System.IO;
using Mandrill;
using ExotelSDK;
using IntegrationKit;
using System.Web.UI.HtmlControls;

namespace Ecommerce.api.com
{
    public partial class ConfirmDiscountOrder : System.Web.UI.Page
    {
        public string ProcessOrderUrl
        {
            get
            {
                return Convert.ToString(ConfigurationManager.AppSettings["ProcessOrder"]);
            }
        }

        public string OrderNo { get; set; }
        public string OrderId { get; set; }
        public string PaymentType { get; set; }

        #region SqlConnection

        SqlConnection con;

        private void OpenSqlConnection()
        {
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["bbmSqlDb"].ToString());
            con.Open();
        }

        private void CloseSqlConnection()
        {
            con.Close();
        }



        #endregion

        string strOrderNo = string.Empty;

        public string OrderNO
        {
            set { strOrderNo = value; }
            get { return strOrderNo; }
        }

        public StringBuilder StrJsScriptAc1 = new StringBuilder();

        public string JsScriptAC1
        {
            get { return StrJsScriptAc1.ToString(); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                CheckCodOrder();
            }
        }

        private void CheckCodOrder()
        {
            Session["DOrderNo"] = Request.QueryString["OrderNo"];

            if (Session["DOrderNo"] != null && Session["DOrderNo"].ToString() != "")
            {
                ShoppingCart oCart = new ShoppingCart();
                bool iSGuestUser = oCart.CheckGuestUser();
                string sOrderId = Convert.ToString(Session["DOrderNo"]);
                OrderNO = sOrderId;

                /* if (UserSession.GetCurrentCustId() > 0 && iSGuestUser.ToString() != "1")
                 {
                     OrderNO = Convert.ToString(Session["DOrderNo"]);
                     int iStatusId = 1;

                     //ClearShoppingCart();

                     UpdateOrderPaymentAndStatus(OrderNO, iStatusId, 2);

                     UpdateOrderCoupon(sOrderId); // Lock Coupon 'Single Used only'

                     //SendMailToCustomer(sOrderId);

                     Session["DiscountOId"] = null;
                     Session["DOrderNo"] = null;

                     //Push to Uniware
                     //UniComInfo oUniCom = new UniComInfo();
                     //bool IsSuccess = oUniCom.CreateUpdateOrder(OrderNO);

                     Response.Redirect(ProcessOrderUrl + OrderNO);

                 }
                 else
                 {
                     Response.Redirect("http://beta.olivetheory.com/");
                 }*/

                OrderNO = Convert.ToString(Session["DOrderNo"]);
                int iStatusId = 1;

                //ClearShoppingCart();

                UpdateOrderPaymentAndStatus(OrderNO, iStatusId, 2);

                UpdateOrderCoupon(sOrderId); // Lock Coupon 'Single Used only'

                SendMailToCustomer(sOrderId);

                Session["DiscountOId"] = null;
                Session["DOrderNo"] = null;

                //Push to Uniware
                UniComInfo oUniCom = new UniComInfo();
                bool IsSuccess = oUniCom.CreateUpdateOrder(OrderNO);

                Response.Redirect(ProcessOrderUrl + OrderNO);

            }
            else
            {
                Response.Redirect("http://www.olivetheory.com/");

                //divError.Visible = true;
                //divCodeOrder.Visible = false;
                //lblMsg.Text = "<br/>Illegal Access. &nbsp; <a href='/' style='color:#848282'>Click Here</a>  to continue Shopping";
            }

        }


        private void UpdateOrderPaymentAndStatus(string OrderNo, int OrderStatus, int PaymentStatus)
        { // not in use
            ///UpdateForPendingOrders
            OpenSqlConnection();
            //string sql = "Update Orders Set StatusId=@OrderStatusId , PaymentStatus=@PaymentStatusId where OrderNo=@OrderNo";
            string sql = "UpdateOrderPaymentAndStatus";
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = sql;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = con;
            cmd.Parameters.Add("@OrderNo", SqlDbType.VarChar).Value = OrderNo;
            cmd.Parameters.Add("@OrderStatusId", SqlDbType.BigInt).Value = OrderStatus;
            cmd.Parameters.Add("@PaymentStatusId", SqlDbType.Int).Value = PaymentStatus;
            cmd.ExecuteNonQuery();
            CloseSqlConnection();
        }


        public void ClearShoppingCart()
        {
            OpenSqlConnection();
            string sql = "[ClearShoppingCart]";
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = sql;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = con;
            cmd.Parameters.Add("@SessionId", SqlDbType.VarChar).Value = UserSession.GetWebSessionId();
            cmd.ExecuteNonQuery();
            CloseSqlConnection();
        }

        private void UpdateOrderCoupon(string OrderId)
        {
            OpenSqlConnection();
            SqlCommand cmd = new SqlCommand("UpdateCodCoupon", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = con;
            cmd.Parameters.Add("@OrderNo", SqlDbType.VarChar).Value = OrderId;
            cmd.ExecuteNonQuery();
            CloseSqlConnection();
        }



        #region Send Email

        protected void SendMailToCustomer(string sOrderId)
        {
            PackingAndDespatch oOders = new PackingAndDespatch();

            ShoppingCart oCart = new ShoppingCart();

            Context.Items["OrderId"] = sOrderId;

            Control invoice = LoadControl("~/UserControls/PrepaidOrderMail.ascx");
            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            HtmlTextWriter htmlTw = new HtmlTextWriter(sw);
            //LoadControl(invoice);
            invoice.RenderControl(htmlTw);
            string controlsHTML = sb.ToString();
            //Message.Text = controlsHTML;
            //string Email = GetEmail(sOrderId);


            // Literal1.Text = controlsHTML;
            //string EmailFrom = ConfigurationSettings.AppSettings["HelpDeskEmail"].ToString();
            //string EmailFrom = "orders@bedbathmore.com";
            string EmailTo = oCart.GetEmail(sOrderId);



            string subject = "Olive Theory Order Confirmed " + sOrderId;
            string sBody = controlsHTML;


            bool isEmailSend = oOders.CheckEmailSend_V1(sOrderId, Convert.ToInt32(CustomDataTypes.OrderEmailType.CodConfirm));

            if (!isEmailSend)
            {
                EmailAddress objEmail = new EmailAddress();
                objEmail.email = "orders@olivetheory.com";
                objEmail.name = "OliveTheory.com";


                SendWebMail.SendEmailInfo(objEmail, EmailTo, subject, sBody);
                string semailsendorReject = SendMailByMandril.emailsendorReject.ToString();

                if (semailsendorReject == "Sent" || semailsendorReject == "Queued")
                    isEmailSend = true;
                if (isEmailSend)
                    oOders.Update_SendEmailLog(sOrderId, isEmailSend, "1"); // 1 is for Order email*/
            }
        }

        #endregion
    }
}