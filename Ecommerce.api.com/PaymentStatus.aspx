﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PaymentStatus.aspx.cs" Inherits="Ecommerce.api.com.PaymentStatus" %>

<%@ Register Src="~/UserControls/ucSellerOrderNotification.ascx" TagPrefix="uc1" TagName="ucSellerOrderNotification" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <uc1:ucSellerOrderNotification runat="server" ID="EmailSellerOrder" Visible="false"  />
    </div>
    </form>
</body>
</html>
